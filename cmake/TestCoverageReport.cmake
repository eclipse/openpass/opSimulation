################################################################################
# Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

get_property(TestList GLOBAL PROPERTY test_target_list)

foreach(TEST_TARGET_NAME IN LISTS TestList)
  set(CoverageInfo ${CoverageInfo};${CMAKE_BINARY_DIR}/coverage/${TEST_TARGET_NAME}.info)
endforeach()

set(OVERALL_GENHTML_COMMAND ${GENHTML_EXECUTABLE}
    --demangle-cpp
    -o ${CMAKE_BINARY_DIR}/coverage/TestCoverageReport
    ${CoverageInfo}
)

add_test(NAME TestCoverageReport COMMAND ${OVERALL_GENHTML_COMMAND})

foreach(TEST_TARGET_NAME IN LISTS TestList)
  set(CoverageTests ${CoverageTests};${TEST_TARGET_NAME}_coverage)
endforeach()

set_tests_properties(TestCoverageReport PROPERTIES DEPENDS "${CoverageTests}")