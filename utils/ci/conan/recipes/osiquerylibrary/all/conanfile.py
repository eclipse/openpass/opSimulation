################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building OsiQueryLibrary with Conan
################################################################################

from conan import ConanFile
from conan.tools.scm import Git
from conan.tools.files import copy, export_conandata_patches, apply_conandata_patches
from conan.tools.cmake import CMake, CMakeToolchain
import os, subprocess

required_conan_version = ">=1.53.0"

class OsiQueryLibraryConan(ConanFile):
    name = "osiquerylibrary"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False],
               "fPIC": [True, False],
               "open_simulation_interface_version":["ANY"],
               "protobuf_version":["ANY"],
               "boost_version":["ANY"],
               "commit_id_value":[None, "ANY"],
               "with_test":["OFF", "ON"]
               }

    default_options = {"shared": False,
                       "fPIC": True,
                       "open_simulation_interface_version":"3.6.0",
                       "protobuf_version":"3.20.0",
                       "boost_version":"1.85.0",
                       "commit_id_value": None,
                       "with_test":"OFF"}

    no_copy_source = False
    short_paths = True
    commit = None

    def export_sources(self):
        export_conandata_patches(self)

    def requirements(self):
        self.requires(f"protobuf/{self.options.protobuf_version}", options={"debug_suffix": False})
        self.requires(f"open-simulation-interface/{self.options.open_simulation_interface_version}")
        self.requires(f"boost/{self.options.boost_version}")

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC


    def configure(self):
        if self.options.shared:
            self.options.rm_safe("fPIC")
        self._strict_options_requirements()
        if self.options.commit_id_value:
            self.commit = self.options.commit_id_value

    def _strict_options_requirements(self):
        self.options["boost"].shared = True
        for boost_options in self._required_boost_options:
            setattr(self.options["boost"], f"without_{boost_options}", True)

    @property
    def _required_boost_options(self):
        return ["chrono", "cobalt", "container", "context", "contract", "coroutine", "date_time", "fiber", "iostreams", "json", "locale", "log", "nowide", "stacktrace", "test", "thread", "timer", "type_erasure", "url", "wave"]

    def _get_url_sha(self):
        if self.version != "commitid" :
            url_https = self.conan_data["sources"][self.version]["url_https"]
            url_ssh = self.conan_data["sources"][self.version]["url_ssh"]
            self.commit = self.conan_data["sources"][self.version]["sha256"]
        else:
            if not self.commit:
                raise Exception("Invalid to build commitid version without specifying commit_id_value option")

            url_https = self.conan_data["sources"]["commitid"]["url_https"]
            url_ssh = self.conan_data["sources"]["commitid"]["url_ssh"]
        return url_https, url_ssh

    def generate(self):
        _cmake_prefix_paths = []
        for _, dependency in self.dependencies.items():
            _cmake_prefix_paths.append(dependency.package_folder)
        _cmake_prefix_paths = ';'.join(str(_cmake_prefix_path) for _cmake_prefix_path in _cmake_prefix_paths)
        tc = CMakeToolchain(self)
        tc.cache_variables["CMAKE_PREFIX_PATH"] = _cmake_prefix_paths
        tc.cache_variables["OSIQL_TEST"] = self.options.with_test
        tc.generate()

    def source(self):
        url_https, url_ssh = self._get_url_sha()
        if "GIT_USER" and "GIT_PASSWORD" in os.environ:
            git = Git(self)
            url = f"https://{os.getenv('GIT_USER')}:{os.getenv('GIT_PASSWORD')}@cc-github.bmwgroup.net/openpass/OsiQueryLibrary.git"
        else:
            git = Git(self)
            url = url_ssh
        git.clone(url=url, target=self.name)
        git.folder=self.name
        git.checkout(commit=self.commit)
        apply_conandata_patches(self)

    def build(self):
        cmake = CMake(self)
        cmake.configure(build_script_folder=self.name)
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()
