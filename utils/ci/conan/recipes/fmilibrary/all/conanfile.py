################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building FMILibrary with Conan
################################################################################

from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain
from conan.tools.files import export_conandata_patches, apply_conandata_patches
from conan.tools.scm import Git

required_conan_version = ">=1.60.0"

class FmiConan(ConanFile):
    name = "fmilibrary"
    license = "2-Clause BSD"
    author = "Michael Scharfenberg michael.scharfenberg@itk-engineering.de"
    url = "https://github.com/modelon-community"
    description = "The Functional Mock-up Interface (or FMI) defines a standardized interface to be used in computer simulations to develop complex cyber-physical systems"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False], "FMILIB_BUILD_STATIC_LIB": ["OFF", "ON"], "FMILIB_BUILD_SHARED_LIB": ["OFF", "ON"],
               "commit_id_value":[None, "ANY"], "FMILIB_BUILD_TESTS": ["OFF", "ON"]
               }
    default_options = {"shared": True, "fPIC": True,
                       "commit_id_value": None, "FMILIB_BUILD_TESTS": "OFF"}
    short_paths = True
    commit = None

    def export_sources(self):
        export_conandata_patches(self)

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.options.shared:
            self.options.rm_safe("fPIC")
        if self.options.commit_id_value:
            self.commit = self.options.commit_id_value
        if self.options.FMILIB_BUILD_STATIC_LIB or self.options.FMILIB_BUILD_SHARED_LIB:
            pass
        else:
            if self.options.shared:
                self.options.FMILIB_BUILD_STATIC_LIB = "OFF"
                self.options.FMILIB_BUILD_SHARED_LIB = "ON"
            else:
                self.options.FMILIB_BUILD_STATIC_LIB = "ON"
                self.options.FMILIB_BUILD_SHARED_LIB = "OFF"

    def _get_url_sha(self):
        if self.version != "commitid" :
            url = self.conan_data["sources"][self.version]["url"]
            self.commit = self.conan_data["sources"][self.version]["sha256"]
        else:
            url = self.conan_data["sources"]["commitid"]["url"]
        return url

    def source(self):
        url = self._get_url_sha()
        git = Git(self)
        git.clone(url=url, target=self.name)
        git.folder=self.name
        git.checkout(commit=self.commit)
        apply_conandata_patches(self)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.cache_variables["FMILIB_BUILD_STATIC_LIB"] = self.options.FMILIB_BUILD_STATIC_LIB
        tc.cache_variables["FMILIB_BUILD_SHARED_LIB"] = self.options.FMILIB_BUILD_SHARED_LIB
        tc.cache_variables["FMILIB_BUILD_TESTS"] = self.options.FMILIB_BUILD_TESTS
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure(build_script_folder=self.name)
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        if self.options.shared:
            self.cpp_info.libs = ["fmilib_shared"]
