################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building yase with Conan
################################################################################

from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain
from conan.tools.files import rm
from conan.tools.scm import Git

required_conan_version = ">=1.53"

class YaseConan(ConanFile):
    name = "yase"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False], "yase_TEST": [True, False],"commit_id_value":[None, "ANY"]}
    default_options = {"shared": True, "fPIC": False, "yase_TEST": False,
                       "commit_id_value": None}
    exports_sources = "*"
    short_paths = True
    no_copy_source = False
    commit = None

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.options.shared:
            self.options.rm_safe("fPIC")
        if self.options.commit_id_value:
            self.commit = self.options.commit_id_value

    def generate(self):
        tc = CMakeToolchain(self)
        tc.cache_variables["Yase_BUILD_TESTS"] = self.options.yase_TEST
        tc.generate()

    def _get_url_sha(self):
        if self.version != "commitid" :
            url = self.conan_data["sources"][self.version]["url"]
            self.commit = self.conan_data["sources"][self.version]["sha256"]
        else:
            url = self.conan_data["sources"]["commitid"]["url"]

        return url

    def source(self):
        url = self._get_url_sha()
        git = Git(self)
        git.clone(url=url, target=self.name)
        git.folder=self.name
        git.checkout(commit=self.commit)

    def build(self):
        cmake = CMake(self)
        cmake.configure(build_script_folder=self.name)
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()
        rm(self, "conanbuildinfo.txt", self.package_folder)
        rm(self, "conandata.yml", self.package_folder)

    def package_info(self):
        self.cpp_info.set_property("cmake_find_mode", "none")
        self.cpp_info.components["agnostic_behavior_tree"].set_property("cmake_target_name", "Yase::libagnostic_behavior_tree")
