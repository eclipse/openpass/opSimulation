################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#               2024      Volkswagen AG
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building open scenario api with Conan
################################################################################
import os

from conan import ConanFile
from conan.tools.files import copy, export_conandata_patches, apply_conandata_patches
from conan.tools.scm import Git
from conan.tools.cmake import CMake, CMakeToolchain

required_conan_version = ">=1.53.0"

class OpenScenarioApiConan(ConanFile):
    name = "openscenario_api"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False],"commit_id_value":[None, "ANY"]}
    default_options = {"shared": True, "fPIC": False,
                       "commit_id_value": None}
    short_paths = True
    no_copy_source = True
    library_type = None
    commit = None

    def export_sources(self):
        export_conandata_patches(self)

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.options.shared:
            self.library_type = "Shared"
        else:
            self.library_type = "Static"
        if self.options.commit_id_value:
            self.commit = self.options.commit_id_value

    def _get_url_sha(self):
        if self.version != "commitid" :
            url = self.conan_data["sources"][self.version]["url"]
            self.commit = self.conan_data["sources"][self.version]["sha256"]
        else:
            url = self.conan_data["sources"]["commitid"]["url"]
        return url

    def source(self):
        url = self._get_url_sha()
        git = Git(self)
        git.clone(url=url, target=self.name)
        git.folder=self.name
        git.checkout(commit=self.commit)
        apply_conandata_patches(self)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.user_presets_path="CMakePresets.json"
        tc.cache_variables["preset"] = "MSYS-shared-release"
        tc.generate()

    def build(self):
        _repo_source = os.path.join(self.source_folder, self.name)
        _artifact_path = os.path.join(_repo_source, "cpp", "buildArtifact")
        if self.settings.os == "Windows":
            # os.chdir(os.path.join(_repo_source, "cpp"))
            # os.system('cmake -Wno-dev --preset="MSYS-shared-release"')
            # os.system('cmake --build --preset="Build-MSYS-shared-release"')
            cmake = CMake(self)
            cmake.configure(build_script_folder=self.name+"/cpp", cli_args=['-Wno-dev'])
            cmake.build()
        else:
            os.chdir(_artifact_path)
            os.system('chmod +x generateLinux.sh')
            command = f'./generateLinux.sh "{self.library_type.lower()}" release make'
            os.system(command)

    def package(self):
        _repo_source = os.path.join(self.source_folder, self.name)
        _build_folder = self.build_folder
        copy(self, "*", src=os.path.join(_repo_source, "cpp/common"), dst=os.path.join(self.package_folder, "include/common"))

        copy(self, "*", src=os.path.join(_repo_source, "cpp/expressionsLib/inc"), dst=os.path.join(self.package_folder, "include/expressionsLib/inc"))

        copy(self, "*", src=os.path.join(_repo_source, "cpp/externalLibs/Filesystem"), dst=os.path.join(self.package_folder, "include/externalLibs/Filesystem"))
        copy(self, "*", src=os.path.join(_repo_source, "cpp/externalLibs/TinyXML2"), dst=os.path.join(self.package_folder, "include/externalLibs/TinyXML2"))

        copy(self, "*", src=os.path.join(_repo_source, "cpp/openScenarioLib/src"), dst=os.path.join(self.package_folder, "include/openScenarioLib/src"))

        copy(self, "*", src=os.path.join(_repo_source, "cpp/openScenarioLib/generated"), dst=os.path.join(self.package_folder, "include/openScenarioLib/generated"))

        if self.settings.os == "Windows":
            copy(self, "lib*", src=os.path.join(_build_folder, "openScenarioLib"), dst=os.path.join(self.package_folder, "lib"))
            copy(self, "lib*", src=os.path.join(_build_folder, "expressionsLib"), dst=os.path.join(self.package_folder, "lib"))
            copy(self, "*", src=os.path.join(_build_folder, "antlr4_runtime/src/antlr4_runtime/runtime/Cpp/dist"), dst=os.path.join(self.package_folder, "lib"))

        else:
            copy(self, "*", src=os.path.join(_repo_source, f"cpp/build/cgReleaseMake{self.library_type}/antlr4_runtime/src/antlr4_runtime/runtime/Cpp/dist"), dst=os.path.join(self.package_folder, "lib"))
            copy(self, "lib*", src=os.path.join(_repo_source, f"cpp/build/cgReleaseMake{self.library_type}/openScenarioLib"), dst=os.path.join(self.package_folder, "lib"))
            copy(self, "lib*", src=os.path.join(_repo_source, f"cpp/build/cgReleaseMake{self.library_type}/expressionsLib"), dst=os.path.join(self.package_folder, "lib"))

        os.chdir(self.package_folder)
        os.system('find . -name "*.cpp" -exec rm {} \;')

    def package_info(self):
        self.cpp_info.set_property("cmake_find_mode", "none")
