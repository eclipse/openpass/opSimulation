################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building mantle api with Conan
################################################################################

from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain
from conan.tools.files import rm, copy, rmdir
from conan.tools.scm import Git
import os

required_conan_version = ">=1.53"

class MantleAPIConan(ConanFile):
    name = "mantleapi"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False],
               "fPIC": [True, False],
               "MantleAPI_TEST":[True, False],
               "commit_id_value":[None, "ANY"]}

    default_options = {"shared": False,
                       "fPIC": True,
                       "MantleAPI_TEST":False,
                       "commit_id_value": None}

    no_copy_source = False
    short_paths = True
    commit = None

    def configure(self):
        if self.options.shared:
            self.options.rm_safe("fPIC")
        if self.options.commit_id_value:
            self.commit = self.options.commit_id_value

    def generate(self):
        tc = CMakeToolchain(self)
        tc.cache_variables["MantleAPI_TEST"] = self.options.MantleAPI_TEST
        tc.generate()

    def _get_url_sha(self):
        if self.version != "commitid" :
            url = self.conan_data["sources"][self.version]["url"]
            self.commit = self.conan_data["sources"][self.version]["sha256"]
        else:
            url = self.conan_data["sources"]["commitid"]["url"]

        return url

    def source(self):
        url = self._get_url_sha()
        git = Git(self)
        git.clone(url=url, target=self.name)
        git.folder=self.name
        git.checkout(commit=self.commit)

    def build(self):
        cmake = CMake(self)
        cmake.configure(build_script_folder=self.name)
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()
        copy(self, pattern="*", src=os.path.join(self.name, "test/MantleAPI"), dst=os.path.join(self.package_folder, "test/MantleAPI"))
        rm(self, "conanbuildinfo.txt", self.package_folder)
        rm(self, "units.h", os.path.join(self.package_folder, "include"))
        rmdir(self, os.path.join(self.package_folder, "share"))
        rm(self, "conandata.yml", self.package_folder)

    def package_info(self):
        self.cpp_info.set_property("cmake_find_mode", "none")
