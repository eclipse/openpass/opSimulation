################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building osi with Conan
################################################################################
import os

from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain
from conan.tools.files import get, copy, replace_in_file

required_conan_version = ">=1.53.0"

class OpenSimulationInterfaceConan(ConanFile):
    name = "open-simulation-interface"
    license = "2-Clause BSD"
    description = 'Generic interface environmental perception of automated driving functions in virtual scenarios'
    topics = ("asam", "adas", "open-simulation", "automated-driving", "openx")
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "protobuf_version":["ANY"]
    }
    default_options = {
        "shared": False,
        "fPIC": True,
        "protobuf_version":"3.20.0"
    }
    short_paths = True
    _repo_source = None

    def requirements(self):
        self.requires(f"protobuf/{self.options.protobuf_version}", transitive_headers=True, transitive_libs=True, options={"debug_suffix": False})

    def build_requirements(self):
        self.tool_requires(f"protobuf/{self.options.protobuf_version}", options={"debug_suffix": False})

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.options.shared:
            self.options.rm_safe("fPIC")

    def source(self):
        get(self, **self.conan_data["sources"][self.version], strip_root=True)
        if self.version in ["3.5.0", "3.6.0"]:
            replace_in_file(self, os.path.join(self.source_folder, "CMakeLists.txt"), "osi_trafficcommand.proto", "osi_trafficcommand.proto\nosi_trafficcommandupdate.proto")

    def generate(self):
        tc = CMakeToolchain(self)
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        cmake.install()

    def package(self):
        copy(self, "LICENSE", dst=os.path.join(self.package_folder, "licenses"), src=self.source_folder)
        cmake = CMake(self)
        cmake.install()
