################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building units with Conan
################################################################################

from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain
from conan.tools.files import rm, export_conandata_patches, apply_conandata_patches
from conan.tools.scm import Git

required_conan_version = ">=1.47.0"

class UnitsConan(ConanFile):
    name = "units"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False],
               "fPIC": [True, False],
               "units_Tests": [True, False],
               "commit_id_value":[None, "ANY"]
               }

    default_options = {"shared": False,
                       "fPIC": True,
                       "units_Tests": False,
                        "commit_id_value": None}
    exports_sources = "*"
    no_copy_source = False
    short_paths = True
    commit = None

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.options.shared:
            self.options.rm_safe("fPIC")
        if self.options.commit_id_value:
            self.commit = self.options.commit_id_value

    def export_sources(self):
        export_conandata_patches(self)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.cache_variables["BUILD_TESTS"] = self.options.units_Tests
        tc.generate()

    def _get_url_sha(self):
        if self.version != "commitid" :
            url = self.conan_data["sources"][self.version]["url"]
            self.commit = self.conan_data["sources"][self.version]["sha256"]
        else:
            url = self.conan_data["sources"]["commitid"]["url"]
        return url

    def source(self):
        url = self._get_url_sha()
        git = Git(self)
        git.clone(url=url, target=self.name)
        git.folder=self.name
        git.checkout(commit=self.commit)

    def build(self):
        apply_conandata_patches(self)
        cmake = CMake(self)
        cmake.configure(build_script_folder=self.name)
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()
        rm(self, "conanbuildinfo.txt", self.package_folder)
        rm(self, "conandata.yml", self.package_folder)
