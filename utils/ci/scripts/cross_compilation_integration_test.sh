#!/bin/bash

################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script executes the build and installs files to destination directory
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../../../build" || exit 1
exit_code=0

cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/integrationTests/Spawner_IntegrationTests 
cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/integrationTests/Algorithm_FmuWrapper_IntegrationTest 
cp $MYDIR/../../../../dist/opSimulation/modules/*.dll $MYDIR/../../../../build/sim/tests/integrationTests/Algorithm_FmuWrapper_IntegrationTest 
cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/integrationTests/Algorithm_SSPParser_IntegrationTest 
cp $MYDIR/../../../../dist/opSimulation/modules/*.dll $MYDIR/../../../../build/sim/tests/integrationTests/Algorithm_SSPParser_IntegrationTest 
cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/integrationTests/Algorithm_SSPWrapper_IntegrationTest 
cp $MYDIR/../../../../dist/opSimulation/modules/*.dll $MYDIR/../../../../build/sim/tests/integrationTests/Algorithm_SSPWrapper_IntegrationTest 
cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/integrationTests/Observation_IntegrationTests 
cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/integrationTests/opSimulation_IntegrationTests 
cp $MYDIR/../../../../dist/opSimulation/modules/*.dll $MYDIR/../../../../build/sim/tests/integrationTests/opSimulation_IntegrationTests 

cd $MYDIR/../../../../build/sim/tests/integrationTests/Spawner_IntegrationTests 
./Spawner_IntegrationTests.exe 
ret=$?
if [ $ret -ne 0 ]; then
    exit_code=1
fi

cd $MYDIR/../../../../build/sim/tests/integrationTests/Algorithm_FmuWrapper_IntegrationTest 
./Algorithm_FmuWrapper_IntegrationTests.exe --gtest_filter="-*LinearTrajectorySignal_TrafficUpdateIsCorrectlyConvertedToDynamicsSignal"
ret=$?
if [ $ret -ne 0 ]; then
    exit_code=1
fi

cd $MYDIR/../../../../build/sim/tests/integrationTests/Algorithm_SSPParser_IntegrationTest 
./Algorithm_SspParser_IntegrationTests.exe
ret=$?
if [ $ret -ne 0 ]; then
    exit_code=1
fi

cd $MYDIR/../../../../build/sim/tests/integrationTests/Observation_IntegrationTests 
./Observation_IntegrationTests.exe
ret=$?
if [ $ret -ne 0 ]; then
    exit_code=1
fi

cd $MYDIR/../../../../build/sim/tests/integrationTests/opSimulation_IntegrationTests 
./opSimulation_IntegrationTests.exe
ret=$?
if [ $ret -ne 0 ]; then
    exit_code=1
fi

exit 0