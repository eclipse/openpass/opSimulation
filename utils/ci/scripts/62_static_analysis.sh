#!/bin/bash

################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#               2024 Volkswagen AG
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

if [[ "$(uname)" != "Linux" ]]; then
  echo "Skipping static code analysis on non-Linux platform"
  exit 0
fi

MYDIR="$(dirname "$(readlink -f $0)")"
REPO_DIR="${MYDIR}/../../.."

export CTCACHE_DIR="${CTCACHE_BASE_DIR}"
export CTCACHE_NO_LOCAL_STATS=1

CLANG_TIDY_CHECK_CLANG_TIDY="clang-tidy-14"
CLANG_TIDY_CHECK_BUILD_DIR="${REPO_DIR}/../build"
CLANG_TIDY_CHECK_ARTIFACT_DIR="${REPO_DIR}/../artifacts"

CLANG_TIDY_CHECK_BASE_DIR="${REPO_DIR}/sim"

CLANG_TIDY_CHECK_INCLUDE_EXTS=(
  "cpp"
)

CLANG_TIDY_CHECK_EXCLUDE_DIRS=(
  "contrib"
  "deps"
  "doc"
  ".env"
)

CLANG_TIDY_CHECK_EXCLUDE_FILES=()

# as array variables cannot be exported, source the actual clang-tidy check script
. ${MYDIR}/check_clang_tidy.sh

RESULT_FILE="${CLANG_TIDY_CHECK_ARTIFACT_DIR}/clang-tidy_results.stdout.log"

[ -f "$RESULT_FILE" ] && [ ! -s "$RESULT_FILE" ] && exit 0 || exit 1
