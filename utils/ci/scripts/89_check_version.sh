#!/bin/bash

#####################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#####################################################################################

#####################################################################################
# This script checks if the opSimulation version is correctly set when GIT_TAG is set
#####################################################################################

if [[ -z "${TAG_NAME}" ]]; then
  echo "TAG_NAME is not set. The version check is only for the TAG build"
  exit
fi

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../../../dist/opSimulation" || exit 1

if [[ "${OSTYPE}" = "msys" ]]; then
  OP_EXE_NAME=opSimulation.exe
else
  OP_EXE_NAME=opSimulation
fi

# Execute the command and capture its output
version_output=$(./$OP_EXE_NAME --version 2>&1)
exit_status=$?

# Check if the command was successful
if [[ $exit_status -ne 0 ]]; then
  echo "Failed to execute ./${OP_EXE_NAME} --version"
  echo "Error: $version_output"
  exit 1
fi

# Check if the output is exactly the tag name (ignoring 'v' prefix)
if [[ "$version_output" =~ ${TAG_NAME#v}$ ]]; then
    echo "Version is correctly set: $version_output"
else
    echo "Version is falsely set: $version_output"
    exit 1
fi
