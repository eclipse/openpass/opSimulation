#!/bin/bash

################################################################################
# Copyright (c) 2021 in-tech GmbH
#               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script executes the build and installs files to destination directory
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"
OP_REPO_DIR="${OP_REPO_DIR:=$MYDIR/../../..}"
cd "$MYDIR/../../../../build" || exit 1

if hash nproc 2>/dev/null; then
  # calculation is kept for reference
  MAKE_JOB_COUNT=$(($(nproc)/4))
else
  # fallback, if nproc doesn't exist
  MAKE_JOB_COUNT=1
fi

case "${OSTYPE}" in
    msys*) source "${OP_REPO_DIR}/.env/Scripts/activate" ;;
    *)     source "${OP_REPO_DIR}/.env/bin/activate" ;;
esac

make -j$MAKE_JOB_COUNT install || exit 1

if [[ "${CROSS_COMPILE}" = true ]]; then
  cd ../dist/opSimulation
  cp $MYDIR/../../../../deps/direct_deploy/yase/bin/libagnostic_behavior_tree.dll $MYDIR/../../../../dist/opSimulation
  cp $MYDIR/../../../../deps/direct_deploy/openscenario_engine/bin/libOpenScenarioEngine.dll $MYDIR/../../../../dist/opSimulation
  cp $MYDIR/../../../../deps/direct_deploy/openscenario_api/lib/*.dll $MYDIR/../../../../dist/opSimulation
fi
