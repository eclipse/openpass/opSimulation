#!/bin/bash

################################################################################
# Copyright (c) 2021 in-tech GmbH
#               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script packs the artifacts
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../../../dist" || exit 1

SIM_NAME=openPASS_SIM
E2E_NAME=openPASS_EndToEndTests

if [[ "${OSTYPE}" = "msys" ]]; then
  PLATFORM="windows"
  ARCHIVE_TOOL="$MYDIR/util_zip.sh"
  ARCHIVE_EXT="zip"
else
  PLATFORM="linux"
  ARCHIVE_TOOL="$MYDIR/util_tar.sh"
  ARCHIVE_EXT="tar.gz"
fi

mkdir -p ../artifacts/testreport || exit 1
mv opSimulation/testreport ../artifacts/testreport/$PLATFORM

if [ -d "opSimulation/artifacts" ]; then
  if [ -n "$(ls -A "opSimulation/artifacts")" ]; then
    $ARCHIVE_TOOL ../artifacts/${E2E_NAME}.${ARCHIVE_EXT} opSimulation/artifacts
  fi
  rm -rf opSimulation/artifacts
fi

rm -rf opSimulation/configs opSimulation/results
mkdir opSimulation/configs

$ARCHIVE_TOOL ../artifacts/${SIM_NAME}.${ARCHIVE_EXT} opSimulation
