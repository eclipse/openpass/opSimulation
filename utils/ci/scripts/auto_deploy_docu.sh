#!/bin/bash

################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script creates a merge request at openpass-docs-site
# The script should be triggered when a git tag is being built
# Upon merging, the documentation at the webspace will be updated
################################################################################

# Exit the script if any command fails
set -e
set -o pipefail

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../.." || exit 1

if [ -z "${TAG_NAME}" ]; then
    echo "Automatic deployment of documentation happens only when tag is set. Skipping now"
    exit 0
fi
# Define paths and variables
REPO_DIR="$PWD"
BASE_DIR="$REPO_DIR/.."
BUILD_DIR="$BASE_DIR/build"
GENERATED_HTML_PATH="$BUILD_DIR/doc/html"  # Path where 'make doc' generates HTML files
DOCS_REPO_NAME="openpass-docs-site"
DOCS_BRANCH_NAME="doc-${TAG_NAME}"

# Git credentials
GIT_USER_NAME="openpass-bot"
GIT_USER_EMAIL="openpass-bot@eclipse.org"

# Clone the openpass-docs-site repository and create a new branch
cd "$BASE_DIR" || exit 1
git clone https://${GIT_USER_NAME}:${GITLAB_BOT_TOKEN}@gitlab.eclipse.org/eclipse/openpass/${DOCS_REPO_NAME}.git "$DOCS_REPO_NAME"
cd "$BASE_DIR/$DOCS_REPO_NAME" || exit 1
git checkout -b "$DOCS_BRANCH_NAME"

# Configure Git user credentials locally
git config --local user.name "$GIT_USER_NAME"
git config --local user.email "$GIT_USER_EMAIL"

# Remove old documentation and copy new one
cd "$BASE_DIR/$DOCS_REPO_NAME/content/html" || exit 1
rm -rf *
cp -r "$GENERATED_HTML_PATH/"* .
# Commit and push the changes
git add --all
git commit -m "Update docs for opSimulation $TAG_NAME"
git push -u origin "$DOCS_BRANCH_NAME"

# Create merge request using GitLab API
MERGE_REQUEST_URL="https://gitlab.eclipse.org/api/v4/projects/7034/merge_requests"

# JSON data for the merge request
merge_request_data=$(cat <<EOF
{
  "source_branch": "${DOCS_BRANCH_NAME}",
  "target_branch": "master",
  "title": "Update docs for opSimulation $TAG_NAME",
  "description": ""
}
EOF
)

# Create a merge request
curl --header "PRIVATE-TOKEN: ${GITLAB_BOT_TOKEN}" \
    --header "Content-Type: application/json" \
    --request POST \
    --data "${merge_request_data}" \
    "${MERGE_REQUEST_URL}"
