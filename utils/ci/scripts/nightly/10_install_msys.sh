#!/bin/bash

################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script installs latest msys through conan
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../../../.." || exit 1

# MSYS install location
MSYS_INSTALL_FOLDER="/c/msys-nightly"

# Removes msys2 package, if present
conan remove --force msys2/*

# Installs msys2 and other relevant packages required to build openPASS
conan install msys2/cci.latest@ -g deploy --install-folder="${MSYS_INSTALL_FOLDER}" --build=missing -o msys2:no_kill=True -o msys2:additional_packages=mingw-w64-x86_64-boost,mingw-w64-x86_64-ccache,mingw-w64-x86_64-cmake,mingw-w64-x86_64-doxygen,mingw-w64-x86_64-gcc,mingw-w64-x86_64-gdb,mingw-w64-x86_64-graphviz,mingw-w64-x86_64-gtest,make,mingw-w64-x86_64-python,mingw-w64-x86_64-python-pip,mingw-w64-x86_64-python-lxml,mingw-w64-x86_64-zziplib,mingw-w64-x86_64-texlive-bin,mingw-w64-x86_64-texlive-core,mingw-w64-x86_64-texlive-font-utils,libxslt-devel,mingw-w64-x86_64-clang,mingw-w64-x86_64-texlive-latex-extra,zip
