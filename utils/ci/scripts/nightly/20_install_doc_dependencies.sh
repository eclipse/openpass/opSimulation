#!/bin/bash

################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script installs packages relavant for sphinx documentation
################################################################################

# additional python packages for documentation
PYTHON_PACKAGES=("Sphinx" "sphinx-rtd-theme" "sphinx-tabs" "breathe" "exhale" "sphinxcontrib-spelling")
# Install Python packages
yes | pip install --no-input "${PYTHON_PACKAGES[@]}"

