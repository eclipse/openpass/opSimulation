#!/usr/bin/env bash

################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script checks if the included headers are wrapper correctly or not
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../.." || exit 1
exit_code=0

# Iterate through all .h and .cpp files in the specified directory and its subdirectories
while IFS= read -r file; do

    # Use grep to extract lines starting with #include from the file
    includes=$(grep '^[[:space:]]*#include' "$file" | awk '{print $2}')

    # Loop through each include and check if it exists in any of the subdirectories
    for include in $includes; do

        # Extract just the filename from the include and remove enclosing quotes/brackets if present
        filename=$(basename "$include" | sed 's/["><]//g')

        # Use find to search for the filename in all subdirectories
        file_count=$(find sim ! -path "sim/deps/*" ! -path ".env/*" -name "$filename" | wc -l)

        if [[ $file_count -gt 0 ]]; then
            if [[ "$include" != "\""*"\""  ]]; then
                echo "Header of project file included with ANGULAR BRACKETS instead of QUOTES: $file: $include"
                exit_code=1
            fi
        else
            if [[ "$include" != "<"*">" ]]; then
                echo "Header of project file included with QUOTES instead of ANGULAR BRACKETS: $file: $include"
                exit_code=1
            fi
        fi
    done
done < <(find ./sim -type f \( -name "*.h" -o -name "*.cpp" \) ! -path "./sim/deps/*" ! -path "./.env/*" ! -path "./sim/contrib/*" ! -path "./sim/doc/*" ! -path "./build/*" ! -path "./gui/*" ! -path "./.git/*")

if [ $exit_code -eq 0 ]; then
    echo "SUCCESS [header-include-style]: Header files are enclosed correctly"
fi

exit $exit_code
