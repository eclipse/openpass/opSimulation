#!/bin/bash

################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script cleans up after the build
################################################################################

if [[ "${OSTYPE}" = "msys" ]]; then
  echo "ccache stats before cleanup:"
  ccache -s

  CCACHE_MAXSIZE=8GiB ccache -c

  echo "ccache stats after cleanup:"
  ccache -s
else
  echo "ccache size before cleanup:"
  du -sh "$CCACHE_DIR"

  ccache --trim-max-size 8GiB

  echo "ccache size after cleanup:"
  du -sh "$CCACHE_DIR"

  echo "Removing outdated ctcache directories"
  cd "$CTCACHE_BASE_DIR"
  find . -type d -mtime +90 -exec rm -rf {} +
fi
exit 0

