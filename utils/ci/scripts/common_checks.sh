#!/bin/bash

################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

# Function to check if a required environment variable is set
# Arguments:
#   $1: The name of the environment variable to check
function check_env_variable {
    if [ -z "${!1}" ]; then
        echo "Error: $1 is not set. Please set the $1 environment variable before running this script."
        exit 1
    fi
}

# Function to check if a directory exists
# Arguments:
#   $1: The path of the directory to check
function check_directory {
    if [ ! -d "$1" ]; then
        echo "Could not find $1 (forgot to use an absolute path?)"
        exit 1
    fi
}

function check_msys64_environment {
    # Check for MINGW64 in PATH before /usr/local/bin if running on Windows
    if [[ "${OSTYPE}" == "msys" ]] && ! (echo $PATH | grep -q /mingw64/bin && echo $PATH | awk -F ':' '{print index($0, "/mingw64/bin") < index($0, "/usr/local/bin")}'); then
        echo "Error: Not using MINGW64 environment" >&2
        exit 1
    fi
}

# Checks if OP_BASE_DIR and OP_REPO_DIR are properly set
#
# Both directories must be set as absolute paths and
# expected to exist
#
# $OP_BASE_DIR is the root for the building
# $OP_REPO_DIR defines where the repository is checked out
#
function check_env_vars_and_dirs {
    check_msys64_environment

    check_env_variable "OP_BASE_DIR"
    check_env_variable "OP_REPO_DIR"
    check_env_variable "OP_DIST_DIR"
    check_env_variable "OP_BUILD_DIR"
    check_env_variable "OP_ARTIFACTS_DIR"
    check_env_variable "OP_DEPS_DIR"

    check_directory "$OP_BASE_DIR"
    check_directory "$OP_REPO_DIR"
}
