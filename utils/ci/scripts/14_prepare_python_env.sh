#!/bin/bash

################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script creates a python virtual environments
# and install python packages mentioned in the requirements.txt
################################################################################

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

OP_REPO_DIR="${OP_REPO_DIR:=$SCRIPT_DIR/../../..}"

echo "Python virtual environment (.env) is being created at ${OP_REPO_DIR}"

if [ ! -f "${OP_REPO_DIR}/requirements.txt" ]; then
    echo "Error: requirements.txt not found at ${OP_REPO_DIR}!"
    exit 1
fi

"${PYTHON_EXECUTABLE}" -m venv "${OP_REPO_DIR}/.env"

if [[ "${OSTYPE}" = "msys" ]]; then
    venv_python_executable="${OP_REPO_DIR}/.env/Scripts/python.exe"
else
    venv_python_executable="${OP_REPO_DIR}/.env/bin/python3"
fi

"${venv_python_executable}" -m pip install --upgrade pip

"${venv_python_executable}" -m pip install -r "${OP_REPO_DIR}/requirements.txt"  || { echo "Failed to install dependencies"; exit 1; }

echo "Following packages are installed successfully in the virtual environment at ${OP_REPO_DIR}/.env."

"${venv_python_executable}" -m pip list -v
