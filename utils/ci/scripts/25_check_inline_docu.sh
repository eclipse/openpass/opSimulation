#!/bin/bash

################################################################################
# Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script checks the inline documentation using doxygen
################################################################################

# compare two versions using semantic version syntax
#
# usage: greater_or_equal "1.2.3" "2.3.4"
# param lhs: left hand side of the operation "lhs >= rhs"
# param rhs: right hand side of the operation "lhs >= rhs"
greater_or_equal()
{
    local v_lhs="$1"
    local v_rhs="$2"
    local version_regex='([0-9]+\.[0-9]+\.[0-9]+)'

    [[ $v_lhs =~ $version_regex ]] && v_lhs=${BASH_REMATCH[1]}
    [[ $v_rhs =~ $version_regex ]] && v_rhs=${BASH_REMATCH[1]}

    IFS='.' read -r -a i_lhs <<< "$v_lhs"
    IFS='.' read -r -a i_rhs <<< "$v_rhs"

    for ((i = 0; i < ${#i_rhs[@]}; i++)); do
        if [ "${i_rhs[i]}" -gt "${i_lhs[i]}" ]; then
            return 1
        fi
    done
    return 0
}

sed_if_greater_or_equal()
{
    local lhs="$1"
    local rhs="$2"
    local message="$3"
    shift 3
    local sed_commands=("$@")

    if greater_or_equal "$lhs" "$rhs"; then
        echo "Filtering warning/errors for doxygen "$lhs": $message"
        for sed_command in "${sed_commands[@]}"; do
          echo "sed -i \"$sed_command\" \"DoxygenWarningLog.txt\""
          sed -i "$sed_command" "DoxygenWarningLog.txt"
        done
    fi
}

filter()
{
    local bug_detected_version="$1"
    local message="$2"
    shift 2
    local remaining_args=("$@")

    sed_if_greater_or_equal $doxy_version "${bug_detected_version}" "${message}" "${remaining_args[@]}"
}

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../.." || exit 1

doxy_version="$(doxygen --version | grep -oP '[0-9]+\.[0-9]+\.[0-9]+')"
echo "Doxygen Version $doxy_version"
doxygen Doxyfile

# filter diverse erroneous messages due to bugs in the individual doxygen versions

# dealing with bugs in Doxygen 1.8.17 (or later)
# bug description - https://github.com/doxygen/doxygen/issues/7411
# fixed in 1.8.18 - https://github.com/doxygen/doxygen/pull/7483
filter "1.8.17" \
     "Filtering Doxygen warnings \"return type of member ... is not documented\" (see https://github.com/doxygen/doxygen/issues/7411)" \
     "/warning: return type of member/d"

# dealing with bugs in Doxygen 1.9.1 (or later)
# bug description - https://github.com/doxygen/doxygen/issues/8091
filter "1.9.1" \
     "Filtering Doxygen warnings \"Member OpenPASS_ ... is not documented\" (see https://github.com/doxygen/doxygen/issues/8091)" \
     "/warning: Member OpenPASS_.*is not documented/d" \
     "/.*OpenPASS_.* has multiple @param documentation sections/d" \
     "/^\s*$/d"

# dealing with DOT issues bugs in Doxygen 1.9.6 (or later)
# bug description - https://github.com/doxygen/doxygen/issues/8091
filter "1.9.6" \
     "Filtering Doxygen error: Problems running dot: exit code=2, command='dot'" \
     "/error: Problems running dot: exit code=2, command='dot'/d" \
     "/error: Problems running dot: exit code=2, command='dot.exe'/d" \
     "/^\s*$/d"

# hotfixing Doxygen 1.10.0 (or later)
# we have to update our codebase: https://github.com/doxygen/doxygen/pull/10289
filter "1.10.0" \
     "Filtering Doxygen warning: Found non-existing group" \
     "/warning: Found non-existing group/d"

# hotfixing Doxygen 1.10.0 (or later)
# we might have to update our codebase: https://stackoverflow.com/questions/11678567/doxygen-warning-documented-function-not-declared-or-defined
filter "1.10.0" \
    "Filtering Doxygen warning: documented symbol was not declared or defined" \
    "/warning: documented symbol '.*std::function\(.*\)' was not declared or defined\./d"

# filtering issues related to dependency MantleAPI
sed -i "/include\/MantleAPI/d" DoxygenWarningLog.txt

# filtering warnings not related to in-line documentation
sed -i "/Detected potential recursive class relation/d" DoxygenWarningLog.txt
sed -i "/warning: argument 'param' from the argument list of EgoAgentInterface::executeQuery has multiple @param documentation sections/d" DoxygenWarningLog.txt

# remove blank lines
sed -i '/^\s*$/d' DoxygenWarningLog.txt

if [ -s DoxygenWarningLog.txt ]
then
     echo "ERROR: Doxygen warnings"
     cat DoxygenWarningLog.txt
     exit 1
else
     echo "No Doxygen warnings found"
     exit 0
fi
