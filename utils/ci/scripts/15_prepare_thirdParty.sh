#!/bin/bash

################################################################################
# Copyright (c) 2021 ITK Engineering GmbH
#               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script prepares the thirdParty dependencies
################################################################################

SCRIPT_DIR="$(dirname "$(readlink -f $0)")"

if [[ -z "${PYTHON_EXECUTABLE}" ]]; then
  echo "Error: Environment variable PYTHON_EXECUTABLE not set."
  exit 1
fi

# Check if the environment variable is defined
# NOTE: This check is necessary for this shell script to work locally
if [[ -z "${OP_REPO_DIR}" || -z ${OP_BASE_DIR} || -z ${OP_DEPS_DIR} ]]; then
  # If not defined, set a default value
  OP_REPO_DIR="$SCRIPT_DIR/../../.."
  OP_BASE_DIR="$SCRIPT_DIR/../../../.."
  OP_DEPS_DIR="$SCRIPT_DIR/../../../../deps"
  echo "Warning: At least one of the OP_REPO_DIR, OP_BASE_DIR and OP_DEPS_DIR variables is not set. Setting the following values:
        OP_REPO_DIR is ${OP_REPO_DIR}
        OP_BASE_DIR is ${OP_BASE_DIR}
        OP_DEPS_DIR is ${OP_DEPS_DIR}"
fi

# Set the default value
conanfile="$OP_REPO_DIR/utils/ci/conan/conanfile.txt" # Accessing the path of conanfile.txt
build_type="build_type=Release"
build_strategy="--build=missing"

# Set python command depending on OS
if [[ "${OSTYPE}" = "msys" ]]; then
  export CONAN_CMAKE_GENERATOR="MSYS Makefiles"
  conanprofile="$OP_REPO_DIR/utils/ci/conan/conanprofile_windows"
  conanprofilehost="$OP_REPO_DIR/utils/ci/conan/conanprofile_windows"
  # rm -rf /c/Windows/system32/config/systemprofile/.conan2
else
  conanprofilehost="$OP_REPO_DIR/utils/ci/conan/conanprofile_linux"
  conanprofile="$OP_REPO_DIR/utils/ci/conan/conanprofile_linux"
fi
export CONAN_REVISIONS_ENABLED=1
# required for cross compilation
if [[ "${CROSS_COMPILE}" = true ]]; then
  conanprofilehost="$OP_REPO_DIR/utils/ci/conan/conanprofile_crosscompilation"
  CONAN_ARGS="$update -pr:b \"$conanprofile\" -pr:h=\"$conanprofilehost\""
else
  CONAN_ARGS="-pr:a $conanprofile"
fi

# Check if build_type argument is provided
# Process arguments
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -d|--debug) build_type="build_type=Debug" ;;
        -pr|--profile)
          shift
          if [[ -n "$1" && ! "$1" =~ ^- ]]; then
              conanprofile="$1"
          else
              echo "Error: --profile requires a path argument."
              exit 1
          fi;;
        --build=*) build_strategy="$1" ;;
        -u|--update) update="--update" ;;
        *) echo "Error: Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

"$PYTHON_EXECUTABLE" -m conans.conan remote add eclipse-conan-local-repo ${OP_REPO_DIR}/utils/ci/conan --force
# remove conancenter remote if it exists, update it and add it back.
# sideeffect: local remote is set as default (added first)
"$PYTHON_EXECUTABLE" -m conans.conan remote remove conancenter
"$PYTHON_EXECUTABLE" -m conans.conan remote add conancenter  https://center2.conan.io
"$PYTHON_EXECUTABLE" -m conans.conan remote list

# Note: If there is a change in recipe of the existing package and would want to
# reinstall the existing package. Then execute conan remove "package/version" command
# before executing the below "for" loop

# Clean deps folder before installing
rm -rf "$OP_DEPS_DIR"

# # Clean conan cache to prevent issues from cached recipies due to aborted builds
# "$PYTHON_EXECUTABLE" -m conans.conan list "*"
# "$PYTHON_EXECUTABLE" -m conans.conan remove -c "*"
# "$PYTHON_EXECUTABLE" -m conans.conan cache clean
# "$PYTHON_EXECUTABLE" -m conans.conan list "*"

# Command to install all the packages into the required folder.
# --build=missing argument is necessary as if the package is not available it builds from my-local-repo (utils/ci/conan/recipes)
"$PYTHON_EXECUTABLE" -m conans.conan install $conanfile $CONAN_ARGS $build_strategy --deployer=direct_deploy -of="$OP_DEPS_DIR" -s:a $build_type -r eclipse-conan-local-repo || { "$PYTHON_EXECUTABLE" -m conans.conan remote remove -c eclipse-conan-local-repo; "$PYTHON_EXECUTABLE" -m conans.conan cache clean --source --build "*/*"; exit 1;}

"$PYTHON_EXECUTABLE" -m conans.conan remote remove eclipse-conan-local-repo
"$PYTHON_EXECUTABLE" -m conans.conan cache clean --source --build "*/*"
