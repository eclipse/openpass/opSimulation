#!/bin/bash

################################################################################
# Copyright (c) 2021 in-tech GmbH
#               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script builds and executes unit tests
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../../../build" || exit 1

if hash nproc 2>/dev/null; then
  MAKE_JOB_COUNT=$(($(nproc)/4))
else
  # fallback, if nproc doesn't exist
  MAKE_JOB_COUNT=1
fi

if [[ $MAKE_JOB_COUNT -eq 0 ]]; then
  # fallback, if nproc == 1
  MAKE_JOB_COUNT=1
fi

export MAKEFLAGS=-j${MAKE_JOB_COUNT}

if [[ "${CROSS_COMPILE}" = true ]]; then
  test_regex="IntegrationTests_build"
else
  test_regex=".*"
fi

# HOTFIX TILL RUNTIME DEPS ARE RESOLVED AUTOMATICALLY
if [[ "${OSTYPE}" = "msys" ]]; then
  mkdir -p "$MYDIR/../../../../build/sim/tests/unitTests/components/Algorithm_SspWrapper"
  cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/unitTests/components/Algorithm_SspWrapper

  mkdir -p "$MYDIR/../../../../build/sim/tests/unitTests/components/AlgorithmAEB"
  cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/unitTests/components/AlgorithmAEB
  cp $MYDIR/../../../../deps/direct_deploy/boost/bin/*.dll $MYDIR/../../../../build/sim/tests/unitTests/components/AlgorithmAEB

  mkdir -p "$MYDIR/../../../../build/sim/tests/unitTests/components/SensorAggregation_OSI"
  cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/unitTests/components/SensorAggregation_OSI 
  cp $MYDIR/../../../../deps/direct_deploy/boost/bin/*.dll $MYDIR/../../../../build/sim/tests/unitTests/components/SensorAggregation_OSI 

  mkdir -p "$MYDIR/../../../../build/sim/tests/unitTests/components/SensorFusionErrorless_OSI"
  cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/unitTests/components/SensorFusionErrorless_OSI 
  cp $MYDIR/../../../../deps/direct_deploy/boost/bin/*.dll $MYDIR/../../../../build/sim/tests/unitTests/components/SensorFusionErrorless_OSI  

  mkdir -p "$MYDIR/../../../../build/sim/tests/unitTests/components/Sensor_OSI"
  cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/unitTests/components/Sensor_OSI
  cp $MYDIR/../../../../deps/direct_deploy/boost/bin/*.dll $MYDIR/../../../../build/sim/tests/unitTests/components/Sensor_OSI  
  
  mkdir -p "$MYDIR/../../../../build/sim/tests/unitTests/core/opSimulation"
  cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/unitTests/core/opSimulation
  cp $MYDIR/../../../../deps/direct_deploy/boost/bin/*.dll $MYDIR/../../../../build/sim/tests/unitTests/core/opSimulation

  mkdir -p "$MYDIR/../../../../build/sim/tests/unitTests/core/opSimulation/modules/SpawnerPreRunCommon"
  cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/unitTests/core/opSimulation/modules/SpawnerPreRunCommon
  cp $MYDIR/../../../../deps/direct_deploy/boost/bin/*.dll $MYDIR/../../../../build/sim/tests/unitTests/core/opSimulation/modules/SpawnerPreRunCommon
  
  mkdir -p "$MYDIR/../../../../build/sim/tests/unitTests/core/opSimulation/modules/SpawnerRuntimeCommon"
  cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/unitTests/core/opSimulation/modules/SpawnerRuntimeCommon
  cp $MYDIR/../../../../deps/direct_deploy/boost/bin/*.dll $MYDIR/../../../../build/sim/tests/unitTests/core/opSimulation/modules/SpawnerRuntimeCommon
  
  mkdir -p "$MYDIR/../../../../build/sim/tests/unitTests/core/opSimulation/Scheduler"
  cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/unitTests/core/opSimulation/Scheduler
  cp $MYDIR/../../../../deps/direct_deploy/boost/bin/*.dll $MYDIR/../../../../build/sim/tests/unitTests/core/opSimulation/Scheduler

  mkdir -p "$MYDIR/../../../../build/sim/tests/unitTests/core/opSimulation/modules/World_OSI"
  cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/unitTests/core/opSimulation/modules/World_OSI
  cp $MYDIR/../../../../deps/direct_deploy/boost/bin/*.dll $MYDIR/../../../../build/sim/tests/unitTests/core/opSimulation/modules/World_OSI
  
  mkdir -p "$MYDIR/../../../../build/sim/tests/integrationTests/Spawner_IntegrationTests"
  cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/integrationTests/Spawner_IntegrationTests
  cp $MYDIR/../../../../deps/direct_deploy/boost/bin/*.dll $MYDIR/../../../../build/sim/tests/integrationTests/Spawner_IntegrationTests
  
  mkdir -p "$MYDIR/../../../../build/sim/tests/integrationTests/opSimulation_IntegrationTests"
  cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/integrationTests/opSimulation_IntegrationTests
  cp $MYDIR/../../../../deps/direct_deploy/boost/bin/*.dll $MYDIR/../../../../build/sim/tests/integrationTests/opSimulation_IntegrationTests

  mkdir -p "$MYDIR/../../../../build/sim/tests/integrationTests/Observation_IntegrationTests"
  cp $MYDIR/../../../../dist/opSimulation/*.dll $MYDIR/../../../../build/sim/tests/integrationTests/Observation_IntegrationTests 
  cp $MYDIR/../../../../deps/direct_deploy/boost/bin/*.dll $MYDIR/../../../../build/sim/tests/integrationTests/Observation_IntegrationTests 

else
  mkdir -p "$MYDIR/../../../../build/sim/tests/unitTests/components/Algorithm_SspWrapper"
  cp "$MYDIR/../../../../deps/direct_deploy/openscenario_engine/lib/libOpenScenarioEngine.so" "$MYDIR/../../../../build/sim/tests/unitTests/components/Algorithm_SspWrapper"
  cp "$MYDIR/../../../../deps/direct_deploy/openscenario_api/lib/libantlr4-runtime.so" "$MYDIR/../../../../build/sim/tests/unitTests/components/Algorithm_SspWrapper"
  cp "$MYDIR/../../../../deps/direct_deploy/openscenario_api/lib/libExpressionsLib.so" "$MYDIR/../../../../build/sim/tests/unitTests/components/Algorithm_SspWrapper"
  cp "$MYDIR/../../../../deps/direct_deploy/openscenario_api/lib/libOpenScenarioLib.so" "$MYDIR/../../../../build/sim/tests/unitTests/components/Algorithm_SspWrapper"
  cp "$MYDIR/../../../../deps/direct_deploy/yase/lib/libagnostic_behavior_tree.so" "$MYDIR/../../../../build/sim/tests/unitTests/components/Algorithm_SspWrapper"
fi 
# END OF HOTFIX

ctest -R "${test_regex}" -j1 --output-on-failure
