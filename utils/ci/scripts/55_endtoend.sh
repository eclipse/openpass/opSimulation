#!/bin/bash

################################################################################
# Copyright (c) 2021 in-tech GmbH
#               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script executes end-to-end tests
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"
OP_REPO_DIR="${OP_REPO_DIR:=$MYDIR/../../..}"

if [[ "${CROSS_COMPILE}" = true ]]; then
  echo "When cross compiling, end-to-end test is not executed on build system"
  exit 0
fi

# HOTFIX TILL RUNTIME DEPS ARE RESOLVED AUTOMATICALLY
if [[ "${OSTYPE}" = "msys" ]]; then
    cp "$MYDIR/../../../../deps/openscenario_engine/bin/libOpenScenarioEngine.dll" "$MYDIR/../../../../dist/opSimulation"
fi
# END OF HOTFIX

case "${OSTYPE}" in
    msys*) source "${OP_REPO_DIR}/.env/Scripts/activate" ;;
    *)     source "${OP_REPO_DIR}/.env/bin/activate" ;;
esac

cd "$MYDIR/../../../../build" || exit 1
make pyOpenPASS || exit 1

cd "$MYDIR/../../../../dist" || exit 1
rm -rf opSimulation/artifacts || exit 1
