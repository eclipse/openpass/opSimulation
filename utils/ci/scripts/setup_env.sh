#!/bin/bash

################################################################################
# Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

for f in $(find /etc/apt -type f -name '*.list'); do
  sed -i -e 's/focal/jammy/g' "$f"
done

apt-get -qq update && apt-get -qq dist-upgrade && apt-get install -qq -y --no-install-recommends \
    build-essential \
    ca-certificates \
    ccache \
    cmake \
    doxygen \
    git \
    google-mock \
    googletest \
    graphviz \
    lcov \
    libgmock-dev \
    libgtest-dev \
    libprotobuf-dev \
    protobuf-compiler \
    python3 \
    python3-distutils \
    python3-pip \
    && apt-get -qq clean

apt-get install -qq -y --no-install-recommends \
    texlive-base \
    texlive-latex-extra


pip install \
    approvaltests==3.1.0 \
    empty-files \
    breathe \
    conan \
    exhale \
    junitparser \
    lxml \
    pandas \
    pytest \
    sphinx \
    sphinx-rtd-theme \
    sphinx-tabs \
    sphinxcontrib-spelling \
    watchdog
