..
  *******************************************************************************
  Copyright (c) 2021 ITK Engineering GmbH
                2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _building_with_conan:

Building with Conan
===================

General
-------

Conan is an open source, decentralized and multi-platform package manager to create and share native binaries.

To learn more about Conan itself you can visit `Conan <https://conan.io/>`_.

Conan is used in the |op| project to create and manage the binaries of the used ThirdParty libraries.

Additional Prerequisites
--------------------------

To be able to work with Conan it is necessary to add additional packages.

.. tabs::

   .. tab:: Windows (MSYS2)

      .. code-block:: bash

         pip install "conan>=2.2.0"

   .. tab:: Linux (Debian Bookworm)

      .. code-block:: bash

         apt install "conan>=2.2.0"

   pip install "conan>2.0"

Conan Recipes
-------------

A *recipe* is python file, which is used by conan to build and create a package.
In this file there are several hooks available, which can be used for several tasks.
E.g. Retrieving the source files, building from source, configuring dependencies, packaging, etc.
Visit `Conan <https://conan.io/>`_. for detailed information about *recipes*.

Conan Create
------------

The `conan create` command is used to create Conan packages from source code. It involves building the source code, packaging the resulting binaries and artifacts, and generating the necessary metadata to publish the package.
The basic syntax of the command is as follows:

.. code-block:: bash

   $ conan create <path_to_recipe> --name $pkg --version $version --user $user --channel $channel --build=missing $packageOptions -pr:a "$conanprofile"

- `<path_to_source>`: The path to the conan recipe of that corresponding package.
- `--name $pkg`: Specifies the name of the package being created. $pkg is a placeholder for the actual package name.
- `--version $version`: Specifies the version of the package being created. $version is a placeholder for the actual version number.
- `--user $user`: Specifies the username of the package creator or owner. $user is a placeholder for the actual username.
- `--channel $channel`: Specifies the channel or repository where the package will be published. $channel is a placeholder for the actual channel name.
- `--build=missing`: Instructs Conan to build the package if its dependencies are missing or outdated. This ensures that all required dependencies are available and up to date before building the package.
- `$packageOptions`: Optional package-specific options that can be passed to the Conan recipe. These options can customize the build process or package configuration according to user requirements.
- `-pr:a "$conanprofile"`: Specifies a profile ($conanprofile) to be used during package creation. Profiles define the build environment and configuration settings for building packages. The -pr:a flag tells Conan to apply this profile specifically for building the package.

.. code-block:: bash

   $ conan create . --name openscenario_engine --version 0.1 --user openpass --channel testing --build=missing -pr:a default

This command will build the package `openscenario_engine` with version `1.0` under the user `openpass` and channel `testing`.

**Optional Settings**

The `-o` option allows you to pass optional settings to the `conan create` command, which can be used to customize the build process of the package. These settings are defined in the `conanfile.py` recipe.
For example, you can specify build options, compiler flags, or any other configuration settings that influence the package creation process.

.. code-block:: bash

   $ conan create . --name openscenario_engine --version 0.1 --user openpass --channel testing --build=missing -o MantleAPI_version=0.1 -o Yase_version=0.1 -pr:a default

In this example, the `-o` option is used to set the `MantleAPI_version` and `Yase_version` build options for the `openscenario_engine` package. These options will be considered during the package creation process.

.. note::

   For the packages MantleAPI, Yase, openscenario_api and openscenario_engine corresponding commit ids can be used instead of version when creating the package

Conan Install
-------------

The `conan install` command is used to install Conan packages from a Conan recipe (a `conanfile.py` file) and its associated dependencies. It resolves and fetches the required packages from a remote Conan repository or local conan cache and installs them in the specified target location.
The basic syntax of the command is as follows:

.. code-block:: bash

   $ conan install --requires=<pkg/version@user/channel> --build <missing|outdated> --deployer=<generator> -of=<install-folder-folder> -g CMakeDeps -pr:a "$conanprofile"

- `<pkg/version@user/channel>`: The user and channel specify the namespace under which the package with the corresponding version will be built.
- `-g <generator>`: Specifies the build system generator (e.g., `CMakeDeps`) for generating build files.
- `-s <setting=value>`: Overrides a Conan setting defined in the recipe with the given value.
- `--build <missing|outdated>`: Specifies whether to build missing or outdated packages.
- `-pr:a "$conanprofile"`: Specifies a profile ($conanprofile) to be used during package creation. Profiles define the build environment and configuration settings for building packages. The -pr:a flag tells Conan to apply this profile specifically for building the package.

**Generate Deployment Files**

The `-g direct_deploy` option with the `conan install` command is used to generate deployment-related files and scripts for installing and running the package on a target system.
These files can include packaging scripts, installation scripts, and other artifacts required for deploying the package to a specific environment.

.. code-block:: bash

   $ conan install openscenario_engine/0.1@openpass/testing -g direct_deploy --install-folder="my/installation/path"

This command will deploy the package `openscenario_engine/0.1@openpass/testing` at the installation path.

.. note::

   There are several additional commands, arguments and fields for these commands available.
   Please see `Conan Docs <https://docs.conan.io/en/latest/>`_ for additional information.

Build only ThirdParties
-----------------------

|op| requires number of third parties libraries and they are listed in ``conanfile.txt``. To install all the dependencies, the script ``15_prepare_thirdParty.sh`` can be used.

The exact list of dependency can be found below.

.. literalinclude:: @OP_REL_ROOT@/utils/ci/conan/conanfile.txt
   :language: text
