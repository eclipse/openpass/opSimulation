..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
                2021 in-tech GmbH

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _prerequisites:

Installing Prerequisites
========================

Before being able to compile and run |op|, make sure to have all dependencies installed.
The third party software dependency of |op| can be divided into:

- **Build environment**, which manages third party software and is used to compile and install |op|
- **Binary packages**, which can be easily installed via a package manager of the respective build environment
- **Source packages**, which need to be built from source code

This section gives detailed information about the prerequisites and tested version.

.. _building_under_windows:


Installing the Build Environment
--------------------------------

.. tabs::

   .. tab:: Windows

      #. MSYS2

         On Windows, the build environment of choice are :term:`MSYS2` programming tools.
         :term:`MSYS2` is used to install some third-party software on which |op| depends.
         Also, the unix-like shell simplifies c++ compilation on Windows.
         For details, see `MSYS2 website <https://www.msys2.org/>`_.


         .. _msys2:

      #. Download MSYS2

         The latest 64-bit package is located in the Installation section of `MSYS2 website <https://www.msys2.org/>`_.


         .. _msys2_installation:

      #. Install MSYS2

         Run the downloaded executable and adjust suggested settings to your needs (defaults are fine).

         .. admonition:: Installation Path

            It is assumed that MSYS2 is installed in the directory ``C:\op\msys64``, as our CI scripts depend on this specific path.
            If you choose to install MSYS2 in a different location, please ensure that you update any related configurations to reflect the new installation path, such as the conan profiles mentioned below."

      #. Understand the Build Environment

         MSYS2 provides three different environments, located in the MSYS2 installation directory:

         .. _fig_msys2_environments:

         .. figure:: _static/images/msys2.png
            :align: center
            :alt: MSYS2 Apps

         MSYS2 Environments

         - MSYS2 MSYS: Common environment, i.e. for package management
         - MSYS2 MinGW 32-bit: A MinGW 32-bit environment
         - MSYS2 MinGW 64-bit: A MinGW 64-bit environment

         .. warning::

            | MSYS2 MinGW 64-bit is **the**  |op| development environment and will be referred to as |mingw_shell|.

   .. _building_under_linux:

   .. tab:: Linux

      On Linux, no special build environment is needed. |Op| is developed under Debian 64-Bit,
      which means that developing under a recent Ubuntu distribution will also work.
      Debian Bookworm or Ubuntu 22.04 LTS is recommended. Debian uses ``apt`` (or ``apt-get``) as package managing system.
      Details will be given in :ref:`binary_packages` and :ref:`building_prerequisites`.


.. _binary_packages:

Installing the Binary Packages
------------------------------

The first set of dependencies we need to install in order to successfully compile |op| are the binary packages. These can be installed via appropiate package manager.

.. tabs::

   .. tab:: Windows (MSYS2)

      #. Open ``MSYS2 MinGW 64-bit`` and execute the following package manager ``pacman`` commands to update the package repository and upgrade system packages:

         .. code-block::

            pacman -Syuu

         If the upgrade requires a restart of MSYS2, resume the upgrade by re-opening the shell and call:

         .. code-block::

            pacman -Suu

      #. Required packages (can be specified in single command line if desired):

         .. literalinclude:: _static/msys2_packages.txt
            :language: bash

         .. admonition:: Versions

            | MSYS2 provides rolling release versions, so some packages might be too "up-to-date".

      #. Required python packages

         .. code-block::

            pip3 install "approvaltests==3.1.0" breathe "conan>2.0" empty-files exhale watchdog

      .. admonition:: GIT/SSH

         The |mingw_shell| does not access an already existing git installation or available SSH keys.
         Make sure, to update/copy your configuration and credentials within the |mingw_shell| before working with git.

   .. tab:: Linux (Debian Bookworm)

      #. Update  the package database on the system

         .. code-block::

            apt update

      #. Upgrade existing software to latest version

         .. code-block::

            apt upgrade

      #. Install required binary packages

         .. code-block::

            # for simulator
            apt -y install antlr4
            apt -y install build-essential
            apt -y install ca-certificates
            apt -y install ccache
            apt -y install cmake
            apt -y install clang-format-15
            apt -y install doxygen
            apt -y install g++
            apt -y install gcc
            apt -y install git
            apt -y install googletest
            apt -y install graphviz
            apt -y install libboost-filesystem-dev
            apt -y install libprotobuf-dev
            apt -y install libantlr4-runtime4.9
            apt -y install libantlr4-runtime-java
            apt -y install libantlr4-runtime-dev
            apt -y install openjdk-17-jre
            apt -y install pkg-config
            apt -y install protobuf-compiler
            apt -y install python3
            apt -y install python3-distutils
            apt -y install python3-pip
            apt -y install uuid-dev

            # python packages
            pip3 install "approvaltests==3.1.0" empty-files "conan>2.0" watchdog
            pip3 install breathe exhale myst-parser "sphinx==7.2.6" sphinx-rtd-theme sphinx-tabs sphinxcontrib-spelling

**EndToEnd Test Framework**

If end to end tests shall be executed, additional requirements have to be considered.
Please refer to :ref:`testing_endtoend` for more details on installation of the prerequisites and usage of the framework.


.. _building_prerequisites:

Installing the Dependencies
---------------------------

This section describes how to compile prerequisites of |op| using conan (``conan>2.0``).

The goal of this section is to build and install necessary packages into a suitable directory using conan.

.. note::

   If you are unfamiliar to ``conan`` or working with it, Section :ref:`building_with_conan` might give you a short introduction on conan in the scope of building dependencies for |op|.

To keep the installation process of |op| and its dependencies as simple as possible, we recommend a specific folder structure.
If you strictly follow the recommended paths of this guide, no command modifications are necessary.

.. tabs::

   .. tab:: Notes for Windows

      - Recommended checkout path of |op| repository: ``C:\openpass``
      - Dependency directory within the |op| repository: ``C:\deps``
      - Recommended installation directory of |op|: ``C:\openPASS\bin\core``

   .. tab:: Notes for Linux

      - Recommended checkout path of |op| repository: ``~/openpass``
      - Dependency directory within the |op| repository: ``~/deps``
      - Recommended installation directory of |op|: ``/usr/local/openPASS/bin/core``

The above directory structure will be created by following the instructions of this guide.

#. Clone |Op| repository

   As described above, the checkout path of the repository is assumed to be the default ``openpass``.

   .. tabs::

      .. tab:: Windows

         Start |mingw_shell|

         .. code-block::

            cd /C/
            git clone https://gitlab.eclipse.org/eclipse/openpass/opSimulation.git openpass

         .. note::

            As stated in :ref:`Building_under_windows`, the windows programming tools suffer from a `path length restriction`.
            This error manifests as strange **file not found** compile errors.
            It is therefore recommended to use a short path for source code checkout, e.g. a drive letter.
            This can also be done by the windows command `subst <https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/subst>`_.

      .. tab:: Linux

         Start ``Bash`` shell

         .. code-block::

            cd ~
            git clone https://gitlab.eclipse.org/eclipse/openpass/opSimulation.git openpass

#. Navigate into repository and checkout main branch

   .. code-block::

      cd openpass
      git checkout main

#. The essential packages necessary for |Op| are listed in the file ``utils/ci/conan/conanfile.txt``.

   If a required package is available on ConanCenter, they will be used directly.
   For packages not available on ConanCenter, the respective recipes are stored in the ``utils/ci/conan/recipe`` folder.

   .. note::

      It is important to have conancenter remote enabled. To check the status of conancenter and to enable, if it is disabled, execute the following commands

      .. code-block::

         conan remote list
         conan remote enable conancenter

#. There are two Conan profiles ``conanprofile_linux`` and ``conanprofile_windows`` available in the ``utils/ci/conan/recipe`` folder, which will be automatically used in the next step.

   .. admonition:: Adjustments

      Check and adjust the compiler version and the path to the MSYS installation in these profiles before running the prepare scripts.

   .. note::

      The recommended compiler is GCC. Usage of other compilers may lead to unexpected outcomes.

#. To build and install the dependencies, run the script ``15_prepare_thirdParty.sh`` located in ``utils/ci/scripts``.

   .. tabs::

      .. tab:: Windows

         #. One of the dependencies of |Op| is OpenScenarioEngine which requires OpenSCENARIO API and it in turn requires JAVA executable to build.
            If Java executable is not available on your machine, download the java from `here <https://cdn.azul.com/zulu/bin/zulu11.50.19-ca-jdk11.0.12-win_x64.zip>`_ and extract the zip file

         #. If ``C:\Downloads\zulu`` is the extracted path, then add the variable JAVA_HOME to the environmental variable, and append the java executable path to the PATH environmental variable as below

            .. code-block::

               export JAVA_HOME=/c/Downloads/zulu/
               export PATH=$PATH:/c/Downloads/zulu/bin

         #. The script ``15_prepare_thirdParty.sh`` also requires the variable PYTHON_EXECUTABLE set. This variable refers to the path of the python installation (For example: It could be either on native Windows or on MSYS).
            If the python is installed on MSYS then set the variable with the following command.

            .. code-block::

               export PYTHON_EXECUTABLE=python3

         #. Execute the script ``15_prepare_thirdParty.sh`` on MSYS

            .. code-block::

               cd utils/ci/scripts
               ./15_prepare_thirdParty.sh

      .. tab:: Linux

         #. The script ``15_prepare_thirdParty.sh`` also requires the variable PYTHON_EXECUTABLE set. This variable refers to the path of the python installation.

            .. code-block::

               export PYTHON_EXECUTABLE=python3


         #. Execute the script

            .. code-block::

               cd utils/ci/scripts
               ./15_prepare_thirdParty.sh

#. Upon successful execution of the script, the dependencies will be installed under ``C:\deps`` on Windows and  ``~/deps`` on Linux.
