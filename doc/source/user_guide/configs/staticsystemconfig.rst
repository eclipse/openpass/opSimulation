..
  *******************************************************************************
  Copyright (c) 2021 in-tech GmbH

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _staticsystemconfig:

Static SystemConfig
===================

SystemConfig files are optional.
They describe static configurations of agents and are therefore an alternative to the dynamic sampling of an agent during runtime.
The schema is the same as for the :ref:`systemconfigblueprint`.



SSP
---------------
One usecase for the static SystemConfig is :ref:`ssp`.
We use SSP to chain multiple FMUs together, which transfer data between them during one time step.
SSP is added to the SystemConfig like any other Component. It requires the following parameters:

.. table::
   :class: tight-table

   ===================== ================================================== =========
   Tag                   Description                                        Mandatory
   ===================== ================================================== =========
   id                    Id should be set to "SspPath"                      yes
   type                  type should be string                              yes
   unit                  not used                                           no
   value                 relative path to the ssp file
                         relative to the configuration path                 yes
   ===================== ================================================== =========

Example
~~~~~~~

.. literalinclude:: @OP_REL_SIM@/contrib/examples/Configurations/StaticSSPStepper/SystemConfig.xml
   :language: xml
   :start-at: <component doc="SSP">
   :end-at: </component>



Connecting SSP with OpenPass
----------------------------------
During OpenPass Simulation, information is communicated via osi messages between the Simulation Core and SSP.
Like any other component communication, these connections need to be explicitly declared as connections in the SystemConfig.
One connection is defined with the following attributes:

.. table::
   :class: tight-table

   ===================== ================================================== =========
   Tag                   Description                                        Mandatory
   ===================== ================================================== =========
   id                    unique identifier for connection                   yes
   source                pair of sending agent component and local link id  yes
   target                pair of receiving agent component
                         and local link id                                  yes
   ===================== ================================================== =========

Example
~~~~~~~

.. literalinclude:: @OP_REL_SIM@/contrib/examples/Configurations/StaticSSPStepper/SystemConfig.xml
   :language: xml
   :start-at: <connection doc="SSP">
   :end-at: </connection>
