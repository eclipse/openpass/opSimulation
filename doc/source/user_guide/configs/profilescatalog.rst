..
  *******************************************************************************
  Copyright (c) 2021 in-tech GmbH

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _profilescatalog:

ProfilesCatalog
===============

The ProfilesCatalog contains all AgentProfiles, SystemProfiles and generic ProfileGroups and Profiles.
Depending on the configuration the simulator could require a "Driver"-ProfileGroup, a "Spawner"- and "TrafficGroup"-ProfileGroup, a "TrafficRules"-ProfileGroup or sensor and vehiclecomponent specific ProfileGroups.

* :ref:`profilescatalog_agentprofiles`
* :ref:`profilescatalog_systemprofiles`
* :ref:`profilescatalog_profilegroups`
* :ref:`profilescatalog_driverprofiles`
* :ref:`profilescatalog_vehiclecomponentprofiles`
* :ref:`profilescatalog_spawnerprofiles`
* :ref:`profilescatalog_trafficrulesprofiles`

.. _profilescatalog_agentprofiles:

AgentProfiles
-------------

In this section all AgentProfiles are defined.
An AgentProfile is either static or dynamic.
A static AgentProfile consists of a SystemConfig and a VehicleModel.
A dynamic AgentProfile specifies the composition of the agent according to certain probabilities.
Here the initial driver, system profile and the vehicle models of an agent get specified.
At least one AgentProfile is required.
But every AgentProfile referenced in the used :ref:`scenario` file or :ref:`components_trafficgroups` must exist.
All probabilities must add up to 1.0.

Composition of a static AgentProfile:

+-----------------+-----------------------------------------------------------------------+-----------+
| Tag             | Description                                                           | Required  |
+=================+=======================================================================+===========+
| System          | Reference to a system defined in a SystemConfig                       | Yes       |
+-----------------+-----------------------------------------------------------------------+-----------+
| VehicleModel    | Name of the VehicleModel in the :ref:`scenario_vehiclemodels`         | Yes       |
+-----------------+-----------------------------------------------------------------------+-----------+

Composition of a dynamic AgentProfile:

+-----------------+---------------------------------------------------------------------------------+--------------------+
| Tag             | Description                                                                     | Required           |
+=================+=================================================================================+====================+
| DriverProfiles  | List of :ref:`profilescatalog_driverprofiles` for random selection              | At least one entry |
+-----------------+---------------------------------------------------------------------------------+--------------------+
| SystemProfiles  | List of :ref:`profilescatalog_systemprofiles` for random selection              | At least one entry |
+-----------------+---------------------------------------------------------------------------------+--------------------+
| VehicleModels   | List of vehicle models for random selection                                     | At least one entry |
+-----------------+---------------------------------------------------------------------------------+--------------------+

Example
~~~~~~~

In this experiment the ego agent is defined by the system with Id 0 in systemConfig.xml and the VehicleModel car_bmw_7.
Every LuxuryClassCarAgent has the driver profile "AgentFollowingDriver".
Regarding the system profile 50% have a NoADAS and the other 50% have WithAEB.
Regarding the vehicle model 50% use a car_mini_cooper and the other 50% use car_bmw_7_1 vehicle model.

.. code-block:: xml

   <AgentProfiles>
       <AgentProfile Name="EgoAgent" Type="Static">
           <System>
               <File>systemConfig.xml</File>
               <Id>0</Id>
           </System>
           <VehicleModel>car_bmw_7</VehicleModel>
       </AgentProfile>
       <AgentProfile Name="LuxuryClassCarAgent" Type="Dynamic">
           <DriverProfiles>
               <DriverProfile Name="AgentFollowingDriver" Probability="1.0"/>
           </DriverProfiles>
           <SystemProfiles>
               <SystemProfile Name="NoADAS" Probability="0.5"/>
               <SystemProfile Name="WithAEB" Probability="0.5"/>
           </SystemProfiles>
           <VehicleModels>
             <VehicleModel Name="car_mini_cooper" Probability="0.5"/>
             <VehicleModel Name="car_bmw_7_1" Probability="0.5"/>
           </VehicleModels>
       </AgentProfile>
   </AgentProfiles>

.. _profilescatalog_systemprofiles:

SystemProfiles
--------------

This sections contains all system profiles.
Every SystemProfile used by :ref:`profilescatalog_agentprofiles` must be listed here.

.. code-block:: xml

   <SystemProfiles>
      <SystemProfile Name="WithAEB">
          <Components>
                 <Component Type="AEB">
                  <Profiles>
                      <Profile Name="AebProfile" Probability="0.5"/>
                  </Profiles>
                  <SensorLinks>
                      <SensorLink SensorId="0" InputId="Camera"/>
                  </SensorLinks>
                 </Component>
          </Components>
          <Sensors>
              <Sensor Id="0" Position="Default">
                  <Profile Type="Geometric2D" Name="Standard"/>
              </Sensor>
          </Sensors>
      </SystemProfile>
      ...
   </SystemProfiles>

+-------------+--------------------------------------------------------+
| Attribute   | Description                                            |
+=============+========================================================+
| Name        | Name of the vehicle profile                            |
+-------------+--------------------------------------------------------+
| Components  | Lists all ADAS and other components in the vehicle     |
+-------------+--------------------------------------------------------+
| Sensors     | Lists all sensors in the vehicle                       |
+-------------+--------------------------------------------------------+

.. _profilescatalog_components:

Components
~~~~~~~~~~

+-------------+-------------------------------------------------------------------------------------------------+
| Attribute   | Description                                                                                     |
+=============+=================================================================================================+
| Type        | Type of the component.                                                                          |
|             |                                                                                                 |
|             | Must match component name in SystemConfigBlueprint                                              |
+-------------+-------------------------------------------------------------------------------------------------+
| Profile     | Possible profiles of the component with probabilities.                                          |
|             |                                                                                                 |
|             | The profiles are defined in the :ref:`profilescatalog_vehiclecomponentprofiles` section         |
|             |                                                                                                 |
|             | Probabilities do not need to add up to 1.                                                       |
+-------------+-------------------------------------------------------------------------------------------------+
| SensorLinks | Defines which sensor this component uses as input                                               |
+-------------+-------------------------------------------------------------------------------------------------+

.. note:: The possibility of "not having that component" can be achieved if the probabilities of the profiles do not add up to 1.

.. _profilescatalog_sensors:

Sensors
~~~~~~~

+-------------+-------------------------------------------------------------------------------------------------+
| Attribute   | Description                                                                                     |
+=============+=================================================================================================+
| Id          | Identifier for the sensor used by the SensorLink definition of the components                   |
+-------------+-------------------------------------------------------------------------------------------------+
| Position    | Position of the sensor in the vehicle in relative coordinates                                   |
+-------------+-------------------------------------------------------------------------------------------------+
| Profile     | All possible profiles of the sensor with probabilities.                                         |
|             |                                                                                                 |
|             | The profiles are defined in the :ref:`profilescatalog_vehiclecomponentprofiles` section         |
+-------------+-------------------------------------------------------------------------------------------------+

.. code-block:: xml

      <Sensors>
        <Sensor Id="0" Position="Default">
          <Profile Type="ReceiverCar2X" Name="Standard"/>
        </Sensor>
        <Sensor Id="1" Position="FrontWindow">
          <Profile Type="Geometric2D" Name="Standard"/>
        </Sensor>
      </Sensors>

.. note:: The Position attribute in the XML specifies placement of a sensor within a vehicle. The position is defined in VehicleModelsCatalog.
  It follows the format "SensorPosition/Position/Parameters". The parameters height, lateral, longitudinal, pitch, roll and yaw are required.
  For example, "FrontWindow" describes a sensor mounted on the front window, with corresponding positional parameters.
  See example catalog in :ref:`scenario_vehiclemodels`.

.. _profilescatalog_profilegroups:

ProfileGroups
-------------

A ProfileGroup defines all the possible profiles of a component.
A single profile is a set of parameters that are passed to the component in the same way as the parameters in the SystemConfig.
Note: For components that have their parameters defined in the ProfilesCatalog the parameters in the SystemConfigBlueprint are ignored.
Parameters can either be simple or stochastic.
Simple parameters only have one value, while stochastic parameters have a minimum and maximum value as well as distribution specific parameters.
Which parameters are needed/supported depends on the component.

.. code-block:: xml

   <ProfileGroup Type="ComponentName">
       <Profile Name="ExampleProfile">
           <String Key="StringParameter" Value="Lorem ipsum"/>
           <DoubleVector Key="DoubleParameter" Value="12.3,4.56,78.9"/>
           <NormalDistribution Key="RandomParameter" Mean="4.5" SD="0.5" Min="3.5" Max="10.0"/>
       </Profile>
       <Profile Name="AnotherProfile">
           ...
       </Profile>
   </ProfileGroup>

There are the following types of simple parameters:

* Bool
* Int
* Double
* String
* IntVector
* DoubleVector
* StringVector

If a parameter is stochastic it can be defined as any to be drawn from any of the following distributions:

+-------------------------+--------------------------------------------------+
| Distribution            | Additional Attributes                            |
+=========================+==================================================+
| NormalDistribution      | (Mean and SD) or (Mu and Sigma) - equivalent     |
+-------------------------+--------------------------------------------------+
| LogNormalDistribution   | (Mean and SD) or (Mu and Sigma) - not equivalent |
+-------------------------+--------------------------------------------------+
| UniformDistribution     | None                                             |
+-------------------------+--------------------------------------------------+
| ExponentialDistribution | Lambda or Mean (Mean = 1 / Lambda)               |
+-------------------------+--------------------------------------------------+
| GammaDistribution       | (Mean and SD) or (Shape and Scale)               |
+-------------------------+--------------------------------------------------+

Additionally there is the list type.
The list contains any number of list items which itself contain a list of parameters.
Lists can be nested at most two times.

.. code-block:: xml

   <List Name="ExampleList">
       <ListItem>
           <String Key="FirstParameter" Value="Lorem"/>
           <Double Key="SecondParameter" Value="0.4"/>
       </ListItem>
       <ListItem>
           <String Key="FirstParameter" Value="ipsum"/>
           <Double Key="SecondParameter" Value="0.6"/>
       </ListItem>
   </List>


A Profile can also reference another Profile in another ProfileGroup.
In these case the importer handles the reference as if it was substituted by all subelements of the referenced Profile.
References may not be nested.

.. code-block:: xml

   <Reference Type="GroupType" Name="ProfileName"/>


.. _profilescatalog_driverprofiles:

Driver ProfileGroup
-------------------

This section contains all driver profiles used by the simulation.
At least one driver profile is required.
The special parameter "Type" defines the name of the component (i.e. library name).
For details on the individual parameters see the :ref:`components reference <components_driver>`.

.. code-block:: xml

  <ProfileGroup Type="Driver">
    <Profile Name="Name">
      <String Key="Type" Value="DriverLibrary"/>
      ...
    </Profile>
    ...
  </ProfileGroup>


.. _profilescatalog_vehiclecomponentprofiles:

VehicleComponent ProfileGroups
------------------------------

This sections contains all driver assistance systems and other vehicle components and their parameter sets.
For every used VehicleComponent type there must be a ProfileGroup with this type and every profile of this type used by :ref:`profilescatalog_systemprofiles` must be listed here.
For details on the individual parameters see the :ref:`components reference <components_vehiclecomponents>`.

.. code-block:: xml

  <ProfileGroup Type="LibraryName">
    <Profile Name="Name">
      ...
    </Profile>
    ...
  </ProfileGroup>


.. _profilescatalog_spawnerprofiles:

SpawnerProfile ProfileGroup
---------------------------

This sections contains all parameters of the spawners referenced in the :ref:`simulationconfig`.
For details on the individual parameters see the :ref:`components reference <components_spawner>`.

.. code-block:: xml

  <ProfileGroup Type="Spawner">
    <Profile Name="Name">
      ...
    </Profile>
    ...
  </ProfileGroup>


.. _profilescatalog_trafficrulesprofiles:

TrafficRules ProfileGroup
-------------------------

This sections contains the global traffic rules, that may vary depending on the country.
The following country are supported:

+-------------+---------------+
| Name        | Description   |
+=============+===============+
| DE          | Germany       |
+-------------+---------------+
| US          | United States |
+-------------+---------------+
| CN          | China         |
+-------------+---------------+

The :ref:`simulationconfig_environment` section in the SimulationConfig defines which set is used.
Currently there are only rules regulating highway traffic. These are the following:

+---------------------------+---------+---------------------------------------------------------------------------------------------------------------------+
| Name                      | Type    | Description                                                                                                         |
+===========================+=========+=====================================================================================================================+
| OpenSpeedLimit            | Double  | maximum allowed speed if not restricted by signs                                                                    |
+---------------------------+---------+---------------------------------------------------------------------------------------------------------------------+
| KeepToOuterLanes          | Bool    | if true, vehicles must use the outermost free lane for driving                                                      |
+---------------------------+---------+---------------------------------------------------------------------------------------------------------------------+
| DontOvertakeOnOuterLanes  | Bool    | if true, it is prohibited to overtake another vehicle, that is driving further left (or right for lefthand traffic) |
+---------------------------+---------+---------------------------------------------------------------------------------------------------------------------+
| FormRescueLane            | Bool    | if true, vehicles driving in a traffic jam must form a corridor for emergency vehicles                              |
+---------------------------+---------+---------------------------------------------------------------------------------------------------------------------+
| ZipperMerge               | Bool    | if true all merging shall be performed using zipper merge                                                           |
+---------------------------+---------+---------------------------------------------------------------------------------------------------------------------+

.. literalinclude:: @OP_REL_SIM@/contrib/examples/Common/ProfilesCatalog.xml
   :language: xml
   :start-at: <ProfileGroup Type="TrafficRules">
   :end-at: </ProfileGroup>
