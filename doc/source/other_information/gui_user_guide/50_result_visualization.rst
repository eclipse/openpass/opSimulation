..
  *******************************************************************************
  Copyright (c) 2021 ITK Engineering GmbH

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _result_visualization:

Result Visualization
====================

      * The deprecated “Qt GUI” for simulation configuration based on Qt mainly aimed to edit PCM-based openPASS experiments was part of minor releases v0.5 – v0.11.
      * The latest version of this deprecated GUI can be found here: https://ci.eclipse.org/openpass/job/openPASS_simulator_build/view/tags/job/openPASS_0.11/

.. image:: result_visualization/_static/images/timeplot/select.png

.. toctree::
   :glob:
   :maxdepth: 1

   result_visualization/*

