# Notices for openPASS

This content is produced and maintained by the Eclipse openPASS project.

 * Project home: https://projects.eclipse.org/projects/automotive.openpass

[TOC]

## Copyright

All content is the property of the respective authors or their employers.
For more information regarding authorship of content, please consult the listed source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
https://www.eclipse.org/legal/epl-2.0/.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

 * https://gitlab.eclipse.org/eclipse/openpass/opSimulation.git
 * git@gitlab.eclipse.org:eclipse/openpass/opSimulation.git

## Third-party Content

### Required Dependencies

The listed versions are confirmed working versions, not minimum requirements:

| Name                      | Purpose                                                                          | License                                                   | Version | Source                                                                                                                                                                                                                                                          |
| ------------------------- | -------------------------------------------------------------------------------- | --------------------------------------------------------- | ------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| antlr4                    | Parser generator                                                                 | [New BSD](https://opensource.org/license/bsd-3-clause)    | 4.8.0   | [Homepage](https://www.antlr.org/), [GitHub](https://github.com/antlr/antlr4)                                                                                                                                                                                   |
| boost                     | C++ development libraries                                                        | [BSL-1.0](https://opensource.org/license/bsl-1-0)         | 1.85.0  | [Homepage](https://www.boost.org/), [GitHub](https://github.com/boostorg/boost)                                                                                                                                                                                 |
| bzip2                     | Data compressor                                                                  | [bzip2-1.0.6](https://spdx.org/licenses/bzip2-1.0.6.html) | 1.0.8   | [Homepage](https://sourceware.org/bzip2/), [GitLab](https://gitlab.com/federicomenaquintero/bzip2)                                                                                                                                                              |
| CMake                     | Build system generator                                                           | [New BSD](https://opensource.org/license/bsd-3-clause)    | 3.28.1  | [Homepage](https://cmake.org/), [GitLab](https://gitlab.kitware.com/cmake/cmake)                                                                                                                                                                                |
| fmilibrary                | FMU importer                                                                     | [New BSD](https://opensource.org/license/bsd-3-clause)    | 2.0.3   | [GitHub](https://github.com/modelon-community/fmi-library)                                                                                                                                                                                                      |
| libiconv                  | Character set conversion                                                         | [LGPL-2.0](https://www.gnu.org/licenses/old-licenses/lgpl-2.0.de.html)         | 1.17.0  | [Homepage](https://www.gnu.org/software/libiconv/), [GitHub](https://github.com/bnoordhuis/libiconv)                                                                                                                                                            |
| libxml2                   | XML toolkit                                                                      | [MIT](https://opensource.org/license/mit)                 | 2.12.5  | [GitLab](https://gitlab.gnome.org/GNOME/libxml2), [Documentation](https://gitlab.gnome.org/GNOME/libxml2/-/wikis/home)                                                                                                                                          |
| mantleapi                 | Interface between scenario engine & environment simulator                        | [EPL-2.0](https://www.eclipse.org/legal/epl-2.0/)         | 5.1.0   | [GitLab](https://gitlab.eclipse.org/eclipse/openpass/mantle-api)                                                                                                                                                                                                |
| minizip                   | .zip file unpacker                                                               | [zlib](https://www.zlib.net/zlib_license.html)            | 1.2.13  | See zlib                                                                                                                                                                                                                                                        |
| open-simulation-interface | Simulation data interface                                                        | [MPL-2.0](https://www.mozilla.org/en-US/MPL/2.0/)         | 3.5.0   | [Homepage](https://www.asam.net/standards/detail/osi/), [GitHub](https://github.com/OpenSimulationInterface/open-simulation-interface), [Documentation](https://opensimulationinterface.github.io/osi-antora-generator/asamosi/latest/specification/index.html) |
| openscenario_api          | [ASAM OpenSCENARIO](https://www.asam.net/standards/detail/openscenario/) library | [Apache-2.0](https://www.apache.org/licenses/LICENSE-2.0) | 1.3.1   | [GitHub](https://github.com/RA-Consulting-GmbH/openscenario.api.test)                                                                                                                                                                                           |
| protobuf(-shared)         | Data serialization interface                                                     | [New BSD](https://opensource.org/license/bsd-3-clause)    | 3.20.0  | [GitHub](https://github.com/protocolbuffers/protobuf), [Documentation](https://protobuf.dev/)                                                                                                                                                                   |
| units                     | Dimensional analysis library                                                     | [MIT](https://opensource.org/license/mit)                 | 2.3.3   | [GitHub](https://github.com/nholthaus/units), [Documentation](https://nholthaus.github.io/units/)                                                                                                                                                               |
| yase                      | Vehicle simulation engine                                                        | [EPL-2.0](https://www.eclipse.org/legal/epl-2.0/)         | 0.1     | [GitLab](https://gitlab.eclipse.org/eclipse/openpass/yase)                                                                                                                                                                                                      |
| zlib                      | File decompression                                                               | [zlib](https://www.zlib.net/zlib_license.html)            | 1.3.1   | [Homepage](https://www.zlib.net/), [GitHub](https://github.com/madler/zlib)                                                                                                                                                                                     |

For more information, please refer to the [openPASS Documentation](https://eclipse.dev/openpass), section `Installing Prerequisites`.

### Optional Dependencies

These dependencies are for optional features for continuous development such as package management, documentation generation and test execution.

The listed versions are confirmed working versions, not minimum requirements:

| Name                   | Purpose                              | License                                                                   | Version | Source                                                                                                                          |
| ---------------------- | ------------------------------------ | ------------------------------------------------------------------------- | ------- | ------------------------------------------------------------------------------------------------------------------------------- |
| breathe                | Sphinx plugin for Doxygen            | [New BSD](https://opensource.org/license/bsd-3-clause)                    | 4.35.0  | [Homepage](https://www.breathe-doc.org/), [GitHub](https://github.com/breathe-doc/breathe)                                      |
| conan                  | Package management                   | [MIT](https://opensource.org/license/mit)                                 | 2.9.3   | [Homepage](https://conan.io/), [GitHub](https://github.com/conan-io/conan)                                                      |
| ctcache                | Cache for clang-tidy results         | [BSL-1.0](https://opensource.org/license/bsl-1-0)                         | 1.1.0   | [GitHub](https://github.com/matus-chochlik/ctcache)                                                                             |
| doxygen                | C++ doc generator                    | [GPL-2.0](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html#SEC1) | 1.10.0  | [Homepage](https://www.doxygen.nl/), [GitHub](https://github.com/doxygen/doxygen)                                               |
| exhale                 | C++ API doc generator                | [New BSD](https://opensource.org/license/bsd-3-clause)                    | 0.3.7   | [Github](https://github.com/svenevs/exhale), [Documentation](https://exhale.readthedocs.io/en/latest/)                          |
| filelock               | File lock for Python                 | [Unlicense](https://unlicense.org/)                                       | 3.12.3  | [GitHub](https://github.com/tox-dev/filelock), [Documentation](https://py-filelock.readthedocs.io/en/latest/index.html)         |
| gtest                  | C++ test framework                   | [New BSD](https://opensource.org/license/bsd-3-clause)                    | 1.14.0  | [GitHub](https://github.com/google/googletest), [Documentation](https://google.github.io/googletest/)                           |
| junitparser            | XML unit test result parser          | [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0)                 | 3.1.0   | [GitHub](https://github.com/weiwei/junitparser), [Documentation](https://junitparser.readthedocs.io/en/latest/)                 |
| lxml                   | Python binding of libxml2            | [New BSD](https://opensource.org/license/bsd-3-clause)                    | 4.9.3   | [Homepage](https://lxml.de/), [GitHub](https://github.com/lxml/lxml)                                                            |
| myst-parser            | Markdown extensions for Sphinx       | [MIT](https://opensource.org/license/mit)                                 | 4.0.0   | [GitHub](https://github.com/executablebooks/MyST-Parser), [Documentation](https://myst-parser.readthedocs.io/en/latest/)        |
| pandas                 | Python data analysis library         | [New BSD](https://opensource.org/license/bsd-3-clause)                    | 2.2.2   | [Homepage](https://pandas.pydata.org/), [GitHub](https://github.com/pandas-dev/pandas)                                          |
| pip                    | Python package installer             | [MIT](https://opensource.org/license/mit)                                 | 24.3.1  | [GitHub](https://github.com/pypa/pip), [Documentation](https://pip.pypa.io/en/stable/)                                          |
| pytest                 | Python test framework                | [MIT](https://opensource.org/license/mit)                                 | 7.4.2   | [GitHub](https://github.com/pytest-dev/pytest), [Documentation](https://docs.pytest.org/en/stable/)                             |
| pyYAML                 | YAML framework for merging CSV files | [MIT](https://opensource.org/license/mit)                                 | 6.0.2   | [Homepage](https://pyyaml.org/), [GitHub](https://github.com/yaml/pyyaml)                                                       |
| psutil                 | Profiling library                    | [New BSD](https://opensource.org/license/bsd-3-clause)                    | 5.9.5   | [GitHub](https://github.com/giampaolo/psutil), [Documentation](https://psutil.readthedocs.io/en/latest/)                        |
| sphinx                 | HTML doc generator                   | [FreeBSD](https://www.freebsd.org/copyright/freebsd-license/)             | 7.2.6   | [Homepage](https://www.sphinx-doc.org/en/master/), [GitHub](https://github.com/sphinx-doc/sphinx)                               |
| sphinx-rtd-theme       | ReadTheDocs theme                    | [MIT](https://opensource.org/license/mit)                                 | 2.0.0   | [GitHub](https://github.com/readthedocs/sphinx_rtd_theme), [Documentation](https://sphinx-rtd-theme.readthedocs.io/en/stable/)  |
| sphinx-tabs            | Adds tabs to HTML doc                | [MIT](https://opensource.org/license/mit)                                 | 3.4.7   | [GitHub](https://github.com/executablebooks/sphinx-tabs), [Documentation](https://sphinx-tabs.readthedocs.io/en/latest/)        |
| sphinxcontrib-spelling | Spell-checker for Sphinx             | [FreeBSD](https://www.freebsd.org/copyright/freebsd-license/)             | 8.0.0   | [GitHub](https://github.com/sphinx-contrib/spelling), [Documentation](https://sphinxcontrib-spelling.readthedocs.io/en/latest/) |

## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.
