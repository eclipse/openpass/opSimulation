/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "runtimeInformation.h"

TEST(RuntimeInformation_Tests, GenerateEntityOutputDir_WhenCurrentRunEquals1TotalRunsEquals10)
{
  openpass::common::RuntimeInformation fakeRuntimeInfo{
      1, 10, {"", std::filesystem::current_path().string(), "fakeOutDir", ""}};

  auto outDirectoryActual = openpass::common::GenerateEntityOutputDir(fakeRuntimeInfo, "0001");
  std::filesystem::path outDirectoryExpected = std::filesystem::path("fakeOutDir") / "run1" / "entity0001";

  ASSERT_EQ(outDirectoryActual, outDirectoryExpected);
}

TEST(RuntimeInformation_Tests, GenerateEntityOutputDir_WhenCurrentRunEquals1TotalRunsEquals100)
{
  openpass::common::RuntimeInformation fakeRuntimeInfo{
      1, 100, {"", std::filesystem::current_path().string(), "fakeOutDir", ""}};

  auto outDirectoryActual = openpass::common::GenerateEntityOutputDir(fakeRuntimeInfo, "0010");
  std::filesystem::path outDirectoryExpected = std::filesystem::path("fakeOutDir") / "run01" / "entity0010";

  ASSERT_EQ(outDirectoryActual, outDirectoryExpected);
}

TEST(RuntimeInformation_Tests, GenerateEntityOutputDir_WhenCurrentRunEquals1TotalRunsEquals101)
{
  openpass::common::RuntimeInformation fakeRuntimeInfo{
      1, 101, {"", std::filesystem::current_path().string(), "fakeOutDir", ""}};

  auto outDirectoryActual = openpass::common::GenerateEntityOutputDir(fakeRuntimeInfo, "0100");
  std::filesystem::path outDirectoryExpected = std::filesystem::path("fakeOutDir") / "run001" / "entity0100";

  ASSERT_EQ(outDirectoryActual, outDirectoryExpected);
}
