/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <FMI2/fmi2_types.h>
#include <algorithm>
#include <map>
#include <memory>
#include <osi3/osi_sensordata.pb.h>
#include <string>
#include <units.h>
#include <utility>
#include <variant>
#include <vector>

#include "SSPElements/Connector/GroupConnector.h"
#include "SSPElements/NetworkElement.h"
#include "SspLogger.h"
#include "Visitors/Network/SspInitVisitor.h"
#include "common/vector2d.h"
#include "components/Algorithm_FmuWrapper/src/fmuFileHelper.h"
#include "fakeAgent.h"
#include "fakeCallback.h"
#include "fakeFmuWrapper.h"
#include "fakeParameter.h"
#include "fakeSignalInterface.h"
#include "fakeWorld.h"
#include "include/fmuHandlerInterface.h"
#include "include/signalInterface.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Component.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Connector/OSMPConnector.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/System.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/UpdateInputSignalVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/UpdateOutputSignalVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Network/SspTriggerVisitor.h"

namespace ssp
{
class ConnectorInterface;
class OSMPConnectorBase;
}  // namespace ssp

using ::testing::Eq;
using ::testing::NiceMock;
using ::testing::ReturnRef;

class SSPNetworkTest : public ::testing::Test
{
public:
  SSPNetworkTest() { SspLogger::SetLogger(&fakeCallback, 0, ""); }

protected:
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeParameter> fakeParameter;
  NiceMock<FakeCallback> fakeCallback;
};

TEST_F(SSPNetworkTest, UpdateInput_EmptyComponents_DoesNothing)
{
  FakeCallback fakeCallback{};
  ssp::System rootSystem("root");
  std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
  ssp::UpdateInputSignalVisitor updateOutputSignalVisitor{0, signalInterface, 0, &fakeWorld, &fakeAgent, &fakeCallback};
  for (const auto& system : rootSystem.elements)
  {
    ssp::GroupConnector updateOutputSystemConnector{system->GetOutputConnectors()};
    updateOutputSystemConnector.Accept(updateOutputSignalVisitor);
  }
}

TEST_F(SSPNetworkTest, Trigger_EmptyComponents_DoesNothing)
{
  FakeCallback fakeCallback{};
  ssp::System rootSystem("root");
  std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
  ssp::SspTriggerVisitor triggerVisitor{0};
  for (const auto& system : rootSystem.elements)
  {
    system->Accept(triggerVisitor);
  }
}

TEST_F(SSPNetworkTest, UpdateOutput_EmptyComponents_DoesNothing)
{
  FakeCallback fakeCallback{};
  ssp::System rootSystem("root");
  std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
  ssp::UpdateInputSignalVisitor updateOutputSignalVisitor{0, signalInterface, 0, &fakeWorld, &fakeAgent, &fakeCallback};
  for (const auto& system : rootSystem.elements)
  {
    ssp::GroupConnector updateOutputSystemConnector{system->GetOutputConnectors()};
    updateOutputSystemConnector.Accept(updateOutputSignalVisitor);
  }
}

TEST_F(SSPNetworkTest, UpdateInput_OneComponent_ExpectCallsToFmuInterface)
{
  FakeCallback fakeCallback{};
  auto fmuWrapperInterface = std::make_shared<FakeFmuWrapper>();

  fmi2_integer_t lo{}, hi{}, size{0};
  FmuVariables fmuVariables;
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.lo",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.hi",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.size",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  EXPECT_CALL(*fmuWrapperInterface, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));

  auto baseLoValue = FmuValue{.intValue = lo};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
  auto baseHiValue = FmuValue{.intValue = hi};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
  auto sizeValue = FmuValue{.intValue = size};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

  std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
  auto osmpTestConnector = std::make_shared<ssp::OsmpConnector<osi3::SensorData, FMI2>>(
      "Connector", "SensorData", fmuWrapperInterface, 10);

  std::shared_ptr<ssp::OSMPConnectorBase> sensorViewConnectorFMU1_in = osmpTestConnector;
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> emptyConnectors{};
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> inputConnectors{osmpTestConnector};

  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> emptyElements;
  auto fmu1Test = std::make_shared<ssp::FmuComponent>(
      "fmu1", std::move(emptyElements), std::move(inputConnectors), std::move(emptyConnectors), fmuWrapperInterface);

  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> elements{};
  elements.push_back(std::move(fmu1Test));
  auto system = std::make_shared<ssp::System>(
      "system1", std::move(elements), std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>());

  system->systemInputConnector->connectors.emplace_back(osmpTestConnector);

  EXPECT_CALL(*fmuWrapperInterface, Init()).Times(1);
  EXPECT_CALL(*fmuWrapperInterface, UpdateInput(Eq(42), signalInterface, Eq(0))).Times(1);

  ssp::SspInitVisitor initVisitor{};
  system->Accept(initVisitor);

  ssp::UpdateInputSignalVisitor updateInputSignalVisitor{42, signalInterface, 0, &fakeWorld, &fakeAgent, &fakeCallback};
  ssp::GroupConnector updateInputSystemConnector{system->GetInputConnectors()};
  updateInputSystemConnector.Accept(updateInputSignalVisitor);
}

TEST_F(SSPNetworkTest, UpdateInput_OneComponentUpdateInputCallesTwice_InitOnlyCalledOnce)
{
  FakeCallback fakeCallback{};
  auto fmuWrapperInterface = std::make_shared<FakeFmuWrapper>();

  fmi2_integer_t lo{}, hi{}, size{0};
  FmuVariables fmuVariables;
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.lo",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.hi",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.size",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  EXPECT_CALL(*fmuWrapperInterface, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));

  auto baseLoValue = FmuValue{.intValue = lo};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
  auto baseHiValue = FmuValue{.intValue = hi};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
  auto sizeValue = FmuValue{.intValue = size};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

  ssp::System rootSystem("root");
  std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
  auto osmpTestConnector = std::make_shared<ssp::OsmpConnector<osi3::SensorData, FMI2>>(
      "Connector", "SensorData", fmuWrapperInterface, 10);

  std::shared_ptr<ssp::OSMPConnectorBase> sensorViewConnectorFMU1_in = osmpTestConnector;
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> emptyConnectors{};
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> inputConnectors{osmpTestConnector};
  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> emptyElements;
  auto fmu1Test = std::make_shared<ssp::FmuComponent>(
      "fmu1", std::move(emptyElements), std::move(inputConnectors), std::move(emptyConnectors), fmuWrapperInterface);

  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> elements{};
  elements.push_back(std::move(fmu1Test));
  auto system = std::make_shared<ssp::System>(
      "system1", std::move(elements), std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>());

  system->systemInputConnector->connectors.emplace_back(osmpTestConnector);

  EXPECT_CALL(*fmuWrapperInterface, Init()).Times(1);
  EXPECT_CALL(*fmuWrapperInterface, UpdateInput(Eq(42), signalInterface, Eq(0))).Times(2);

  ssp::SspInitVisitor initVisitor{};
  system->Accept(initVisitor);

  ssp::UpdateInputSignalVisitor updateInputSignalVisitor{42, signalInterface, 0, &fakeWorld, &fakeAgent, &fakeCallback};
  ssp::GroupConnector updateInputSystemConnector{system->GetInputConnectors()};
  updateInputSystemConnector.Accept(updateInputSignalVisitor);
  updateInputSystemConnector.Accept(updateInputSignalVisitor);
}

TEST_F(SSPNetworkTest, UpdateInput_NoConnector_ExpectNoUpdateInput)
{
  FakeCallback fakeCallback{};
  auto fmuWrapperInterface = std::make_shared<FakeFmuWrapper>();

  fmi2_integer_t lo{}, hi{}, size{0};
  FmuVariables fmuVariables;
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.lo",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.hi",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.size",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  EXPECT_CALL(*fmuWrapperInterface, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));

  auto baseLoValue = FmuValue{.intValue = lo};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
  auto baseHiValue = FmuValue{.intValue = hi};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
  auto sizeValue = FmuValue{.intValue = size};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

  ssp::System rootSystem("root");
  std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
  auto osmpTestConnector = std::make_shared<ssp::OsmpConnector<osi3::SensorData, FMI2>>(
      "Connector", "SensorData", fmuWrapperInterface, 10);

  std::shared_ptr<ssp::OSMPConnectorBase> sensorViewConnectorFMU1_in = osmpTestConnector;
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> emptyConnectors{};
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> inputConnectors{osmpTestConnector};
  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> emptyElements;
  auto fmu1Test = std::make_unique<ssp::FmuComponent>(
      "fmu1", std::move(emptyElements), std::move(emptyConnectors), std::move(emptyConnectors), fmuWrapperInterface);

  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> elements{};
  elements.push_back(std::move(fmu1Test));
  auto system = std::make_shared<ssp::System>(
      "system1", std::move(elements), std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>());

  EXPECT_CALL(*fmuWrapperInterface, Init()).Times(0);
  EXPECT_CALL(*fmuWrapperInterface, UpdateInput(Eq(0), Eq(nullptr), Eq(0))).Times(0);

  ssp::UpdateInputSignalVisitor updateInputSignalVisitor{0, signalInterface, 0, &fakeWorld, &fakeAgent, &fakeCallback};
  for (const auto& system : rootSystem.elements)
  {
    ssp::GroupConnector updateInputSystemConnector{system->GetInputConnectors()};
    updateInputSystemConnector.Accept(updateInputSignalVisitor);
  }
}

TEST_F(SSPNetworkTest, Trigger_OneComponent_ExpectCallsToFmuInterface_TriggerWithNoConnectors)
{
  FakeCallback fakeCallback{};
  auto fmuWrapperInterface = std::make_shared<FakeFmuWrapper>();

  fmi2_integer_t lo{}, hi{}, size{0};
  FmuVariables fmuVariables;
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.lo",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.hi",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.size",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  EXPECT_CALL(*fmuWrapperInterface, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));

  auto baseLoValue = FmuValue{.intValue = lo};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
  auto baseHiValue = FmuValue{.intValue = hi};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
  auto sizeValue = FmuValue{.intValue = size};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

  std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
  auto osmpTestConnector = std::make_shared<ssp::OsmpConnector<osi3::SensorData, FMI2>>(
      "Connector", "SensorData", fmuWrapperInterface, 10);

  std::shared_ptr<ssp::OSMPConnectorBase> sensorViewConnectorFMU1_in = osmpTestConnector;
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> emptyConnectors{};
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> outConnectors{osmpTestConnector};
  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> emptyElements;
  auto fmu1Test = std::make_shared<ssp::FmuComponent>(
      "fmu1", std::move(emptyElements), std::move(emptyConnectors), std::move(emptyConnectors), fmuWrapperInterface);

  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> elements{};
  elements.push_back(std::move(fmu1Test));
  auto system = std::make_shared<ssp::System>(
      "system1", std::move(elements), std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>());

  EXPECT_CALL(*fmuWrapperInterface, Init()).Times(1);
  EXPECT_CALL(*fmuWrapperInterface, Trigger(Eq(0))).Times(1);

  ssp::SspInitVisitor initVisitor{};
  system->Accept(initVisitor);

  ssp::SspTriggerVisitor triggerVisitor{0};
  system->Accept(triggerVisitor);
}

TEST_F(SSPNetworkTest, Trigger_OneComponentTriggerCallsTwice_InitOnlyCalledOnce)
{
  FakeCallback fakeCallback{};
  auto fmuWrapperInterface = std::make_shared<FakeFmuWrapper>();

  fmi2_integer_t lo{}, hi{}, size{0};
  FmuVariables fmuVariables;
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.lo",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.hi",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.size",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  EXPECT_CALL(*fmuWrapperInterface, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));

  auto baseLoValue = FmuValue{.intValue = lo};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
  auto baseHiValue = FmuValue{.intValue = hi};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
  auto sizeValue = FmuValue{.intValue = size};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

  std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
  auto osmpTestConnector = std::make_shared<ssp::OsmpConnector<osi3::SensorData, FMI2>>(
      "Connector", "SensorData", fmuWrapperInterface, 10);

  std::shared_ptr<ssp::OSMPConnectorBase> sensorViewConnectorFMU1_in = osmpTestConnector;
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> emptyConnectors{};
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> outConnectors{osmpTestConnector};
  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> emptyElements;
  auto fmu1Test = std::make_shared<ssp::FmuComponent>(
      "fmu1", std::move(emptyElements), std::move(emptyConnectors), std::move(emptyConnectors), fmuWrapperInterface);

  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> elements{};
  elements.push_back(std::move(fmu1Test));
  auto system = std::make_shared<ssp::System>(
      "system1", std::move(elements), std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>());

  EXPECT_CALL(*fmuWrapperInterface, Init()).Times(1);
  EXPECT_CALL(*fmuWrapperInterface, Trigger(Eq(0))).Times(1);
  EXPECT_CALL(*fmuWrapperInterface, Trigger(Eq(100))).Times(1);

  ssp::SspInitVisitor initVisitor{};
  system->Accept(initVisitor);

  ssp::SspTriggerVisitor triggerVisitor{0};
  system->Accept(triggerVisitor);

  ssp::SspTriggerVisitor triggerVisitor100{100};
  system->Accept(triggerVisitor100);
}

TEST_F(SSPNetworkTest, UpdateOutput_OneComponent_ExpectCallsToFmuInterface)
{
  FakeCallback fakeCallback{};
  auto fmuWrapperInterface = std::make_shared<FakeFmuWrapper>();

  fmi2_integer_t lo{}, hi{}, size{0};
  FmuVariables fmuVariables;
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("SensorDataOut.base.lo",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, "", fmi2_causality_enu_output, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorDataOut.base.hi",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, "", fmi2_causality_enu_output, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorDataOut.size",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, "", fmi2_causality_enu_output, fmi2_variability_enu_discrete));

  EXPECT_CALL(*fmuWrapperInterface, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));

  auto baseLoValue = FmuValue{.intValue = lo};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
  auto baseHiValue = FmuValue{.intValue = hi};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
  auto sizeValue = FmuValue{.intValue = size};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

  std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
  auto osmpTestConnector = std::make_shared<ssp::OsmpConnector<osi3::SensorData, FMI2>>(
      "Connector", "SensorDataOut", fmuWrapperInterface, 10);

  std::shared_ptr<ssp::OSMPConnectorBase> sensorViewConnectorFMU1_in = osmpTestConnector;
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> emptyConnectors{};
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> outputConnectors{osmpTestConnector};
  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> emptyElements;
  auto fmu1Test = std::make_shared<ssp::FmuComponent>(
      "fmu1", std::move(emptyElements), std::move(emptyConnectors), std::move(outputConnectors), fmuWrapperInterface);

  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> elements{};
  elements.push_back(std::move(fmu1Test));
  auto system = std::make_unique<ssp::System>(
      "system1", std::move(elements), std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>());

  system->systemOutputConnector->connectors.emplace_back(osmpTestConnector);

  EXPECT_CALL(*fmuWrapperInterface, Init()).Times(1);
  EXPECT_CALL(*fmuWrapperInterface, UpdateOutput(Eq(6), signalInterface, Eq(0))).Times(1);

  ssp::SspInitVisitor initVisitor{};
  system->Accept(initVisitor);

  ssp::UpdateOutputSignalVisitor updateOutputSignalVisitor{
      6, signalInterface, 0, &fakeWorld, &fakeAgent, &fakeCallback};
  ssp::GroupConnector updateOutputSystemConnector{system->GetOutputConnectors()};
  updateOutputSystemConnector.Accept(updateOutputSignalVisitor);
}

TEST_F(SSPNetworkTest, UpdateOutput_OneComponentUpdateOutputFmuWrapperSetsSignal_GetsSignal)
{
  FakeCallback fakeCallback{};
  auto fmuWrapperInterface = std::make_shared<FakeFmuWrapper>();

  fmi2_integer_t lo{}, hi{}, size{0};
  FmuVariables fmuVariables;
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("SensorDataOut.base.lo",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, "", fmi2_causality_enu_output, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorDataOut.base.hi",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, "", fmi2_causality_enu_output, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorDataOut.size",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, "", fmi2_causality_enu_output, fmi2_variability_enu_discrete));

  EXPECT_CALL(*fmuWrapperInterface, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));

  auto baseLoValue = FmuValue{.intValue = lo};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
  auto baseHiValue = FmuValue{.intValue = hi};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
  auto sizeValue = FmuValue{.intValue = size};
  EXPECT_CALL(*fmuWrapperInterface, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

  std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
  auto osmpTestConnector = std::make_shared<ssp::OsmpConnector<osi3::SensorData, FMI2>>(
      "Connector", "SensorDataOut", fmuWrapperInterface, 10);

  std::shared_ptr<ssp::OSMPConnectorBase> sensorViewConnectorFMU1_in = osmpTestConnector;
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> emptyConnectors{};
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> outputConnectors{osmpTestConnector};
  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> emptyElements;
  auto fmu1Test = std::make_shared<ssp::FmuComponent>(
      "fmu1", std::move(emptyElements), std::move(emptyConnectors), std::move(outputConnectors), fmuWrapperInterface);

  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> elements{};
  elements.push_back(std::move(fmu1Test));
  auto system = std::make_shared<ssp::System>(
      "system1", std::move(elements), std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>());

  system->systemOutputConnector->connectors.emplace_back(osmpTestConnector);

  EXPECT_CALL(*fmuWrapperInterface, UpdateOutput(Eq(6), signalInterface, Eq(100))).Times(1);

  ON_CALL(*fmuWrapperInterface, UpdateOutput)
      .WillByDefault([](int localLinkId, std::shared_ptr<const SignalInterface>& data, int time) mutable
                     { data = std::make_shared<const FakeSignalInterface>(localLinkId, time); });
  ssp::UpdateOutputSignalVisitor updateOutputSignalVisitor{
      6, signalInterface, 100, &fakeWorld, &fakeAgent, &fakeCallback};

  ssp::GroupConnector updateOutputSystemConnector{system->GetOutputConnectors()};
  updateOutputSystemConnector.Accept(updateOutputSignalVisitor);

  ASSERT_TRUE(signalInterface);
  auto retrievedSignal = std::dynamic_pointer_cast<const FakeSignalInterface>(signalInterface);
  ASSERT_TRUE(retrievedSignal);
  ASSERT_EQ(6, retrievedSignal->localLinkId);
  ASSERT_EQ(100, retrievedSignal->time);
}
