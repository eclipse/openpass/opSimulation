/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <FMI2/fmi2_types.h>
#include <fmi_version.h>
#include <map>
#include <memory>
#include <stddef.h>
#include <string>
#include <vector>

#include "include/fmuHandlerInterface.h"
#include "sim/include/fmuWrapperInterface.h"

class SignalInterface;

namespace ssp
{

/// Class representing a SimpleAffineFMU
class SimpleAffineFMU : public FmuWrapperInterface
{
  bool isInitialized{};

public:
  /**
   * @brief initialze
   *
   */
  void Init() override;

  /**
   * \brief Update Inputs
   *
   * Function is called by framework when another component delivers a signal over
   * a channel to this component (scheduler calls update taks of other component).
   *
   * Refer to module description for input channels and input ids.
   *
   * @param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
   * @param[in]     data           Referenced signal (copied by sending component)
   */
  void UpdateInput(int localLinkId, const std::shared_ptr<const SignalInterface> &data, int) override;

  /**
   * @brief Trigger the component
   *
   */
  void Trigger(int) override;

  /**
   * @brief Update Output
   *
   * @param data
   */
  void UpdateOutput(int, std::shared_ptr<const SignalInterface> &data, int) override;

  /**
   * @brief Get the Fmu Handler object
   *
   * @return const FmuHandlerInterface*
   */
  [[nodiscard]] const FmuHandlerInterface *GetFmuHandler() const override { return nullptr; }

  /**
   * @brief Get the Fmu Variables object
   *
   * @return const FmuVariables&
   */
  [[nodiscard]] const FmuVariables &GetFmuVariables() const override { return fmuVariables; }
  [[nodiscard]] const FmuValue &GetValue(int valueReference, VariableType) const override
  {
    return fmiValues.at(valueReference);
  }
  void SetValue(const FmuValue &value, int valueReference, VariableType) override
  {
    fmiValues.at(valueReference) = value;
  }
  fmi_version_enu_t getFmiVersion() override { return fmi_version_2_0_enu; }

  void SetFmuValues(std::vector<int> valueReferences,
                    std::vector<FmuValue> fmuValuesIn,
                    VariableType dataType) override;
  void GetFmuValues(std::vector<int> valueReferences,
                    std::vector<FmuValue> &fmuValuesOut,
                    VariableType dataType) override;

  SimpleAffineFMU(double factor, int priority);

  /**
   * @brief update input
   *
   * @param index
   * @param data
   */
  void UpdateInput(size_t index, double data);

  /**
   * @brief Get the Priority object
   *
   * @return Priority of the component
   */
  [[nodiscard]] int getPriority() const { return priority; }

  std::map<fmi2_value_reference_t, FmuValue> fmiValues;
  const std::string COMPONENTNAME = "SimpleAffineFMU";
  /// fmu variables
  FmuVariables fmuVariables;
  /// factor
  double factor = 0;
  /// priority
  int priority = -1;
  /// trigger counter
  int triggerCounter = 0;
};

}  // namespace ssp
