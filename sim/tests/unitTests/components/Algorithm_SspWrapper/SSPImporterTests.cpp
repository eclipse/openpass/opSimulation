/*******************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <filesystem>
#include <map>
#include <memory>
#include <string>
#include <units.h>
#include <utility>
#include <variant>
#include <vector>

#include "Importer/FileElements/SsdComponent.h"
#include "Importer/FileElements/SsdSystem.h"
#include "Importer/SsdFile.h"
#include "Importer/SsdFileImporter.h"
#include "OWL/fakes/fakeWorldData.h"
#include "ParserTypes.h"
#include "SspLogger.h"
#include "common/runtimeInformation.h"
#include "common/vector2d.h"
#include "fakeAgent.h"
#include "fakeCallback.h"
#include "fakeEgoAgent.h"
#include "fakeParameter.h"
#include "fakeWorld.h"

using ::testing::NiceMock;

const std::filesystem::path OSMP_CONNECTION_TEST_SSD{"Resources/OSMPConnectionTest.ssd"};

class SSPImporterTests : public ::testing::Test
{
public:
  SSPImporterTests() { SspLogger::SetLogger(&fakeCallback, 0, ""); }
  std::filesystem::path GetAbsolutePath(const std::filesystem::path& configPath,
                                        const std::basic_string<char>& configBasePath_)
  {
    std::filesystem::path sspPath = configPath;
    if (!sspPath.is_absolute())
    {
      std::filesystem::path configBasePath(configBasePath_);
      sspPath = configBasePath / sspPath;
    }
    return sspPath;
  }
  void Import(const std::filesystem::path& path)
  {
    std::filesystem::path sspPath = GetAbsolutePath(path.string(), runtimeInformation.directories.configuration);

    if (!SsdFileImporter::Import(sspPath, ssdFiles))
    {
      FAIL();
    }
    if (ssdFiles.empty())
    {
      FAIL();
    }
  }

protected:
  openpass::common::RuntimeInformation runtimeInformation{
      0, 100, {"", std::filesystem::current_path().string(), "", ""}};
  NiceMock<FakeCallback> fakeCallback;
  std::vector<std::shared_ptr<SsdFile>> ssdFiles;

  NiceMock<FakeParameter> fakeParameter;
  NiceMock<FakeWorld> fakeWorld;
  NiceMock<OWL::Fakes::WorldData> fakeWorldData;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
};

TEST_F(SSPImporterTests, ImportSystem)
{
  Import(OSMP_CONNECTION_TEST_SSD);

  auto system = ssdFiles[0]->ssdSystem;
  ASSERT_EQ(system->GetName(), "OSMPConnectionTest");
}

TEST_F(SSPImporterTests, ImportSystemComponents)
{
  Import(OSMP_CONNECTION_TEST_SSD);

  auto system = ssdFiles[0]->ssdSystem;

  auto components = system->GetComponents();
  ASSERT_EQ(components.size(), 2);

  auto OSMPDummySensor = components[0];
  ASSERT_EQ(OSMPDummySensor->GetName(), "DummySensor");
  ASSERT_EQ(OSMPDummySensor->GetPriority(), 42);
  auto OSMPSensorDataToTrafficUpdateStepper = components[1];
  ASSERT_EQ(OSMPSensorDataToTrafficUpdateStepper->GetName(), "TrafficStepper");
  ASSERT_EQ(OSMPSensorDataToTrafficUpdateStepper->GetPriority(), 50);
}

TEST_F(SSPImporterTests, ImportSystemComponentParameterBindings)
{
  Import(OSMP_CONNECTION_TEST_SSD);

  auto system = ssdFiles[0]->ssdSystem;

  auto components = system->GetComponents();
  auto component = components[0];

  auto parameters = component->GetParameters();
  ASSERT_EQ(parameters.size(), 5);

  auto parameterLogging = parameters[0];
  ASSERT_EQ(parameterLogging.first, "Logging");
  ASSERT_EQ(std::get<bool>(parameterLogging.second), true);

  auto parameterInputOSMPSensorViewIn = parameters[4];
  ASSERT_EQ(parameterInputOSMPSensorViewIn.first, "Input_OSMPSensorViewIn");
  ASSERT_EQ(std::get<std::string>(parameterInputOSMPSensorViewIn.second), "SensorView");

  auto writeMessageParameterSet = component->GetWriteMessageParameters();
  ASSERT_EQ(writeMessageParameterSet.size(), 2);

  auto writeJsonSensorData = writeMessageParameterSet[0];
  ASSERT_EQ(writeJsonSensorData.first, "WriteJson_SensorData");
  ASSERT_EQ(writeJsonSensorData.second, "OSMPSensorDataOut");
}

TEST_F(SSPImporterTests, ImportSystemConnectionsOutwardConnection)
{
  Import(OSMP_CONNECTION_TEST_SSD);

  auto system = ssdFiles[0]->ssdSystem;

  auto connections = system->GetConnections();
  ASSERT_EQ(connections.size(), 15);

  auto connection0 = connections[0];
  ASSERT_EQ(connection0->size(), 4);
  ASSERT_EQ(connection0->at("startElement"), "DummySensor");
  ASSERT_EQ(connection0->at("startConnector"), "SensorViewIn.size");
  ASSERT_EQ(connection0->at("endElement"), "OSMPConnectionTest");
  ASSERT_EQ(connection0->at("endConnector"), "SensorViewIn.size");
  auto connection1 = connections[1];
  ASSERT_EQ(connection1->size(), 4);
  ASSERT_EQ(connection1->at("startElement"), "DummySensor");
  ASSERT_EQ(connection1->at("startConnector"), "SensorViewIn.base.lo");
  ASSERT_EQ(connection0->at("endElement"), "OSMPConnectionTest");
  ASSERT_EQ(connection1->at("endConnector"), "SensorViewIn.base.lo");
  auto connection2 = connections[2];
  ASSERT_EQ(connection2->size(), 4);
  ASSERT_EQ(connection2->at("startElement"), "DummySensor");
  ASSERT_EQ(connection2->at("startConnector"), "SensorViewIn.base.hi");
  ASSERT_EQ(connection0->at("endElement"), "OSMPConnectionTest");
  ASSERT_EQ(connection2->at("endConnector"), "SensorViewIn.base.hi");
}

TEST_F(SSPImporterTests, ImportSystemConnectionsWithinSystem)
{
  Import(OSMP_CONNECTION_TEST_SSD);

  auto system = ssdFiles[0]->ssdSystem;

  auto connections = system->GetConnections();
  ASSERT_EQ(connections.size(), 15);

  auto connection12 = connections[12];
  ASSERT_EQ(connection12->size(), 4);
  ASSERT_EQ(connection12->at("startElement"), "DummySensor");
  ASSERT_EQ(connection12->at("startConnector"), "SensorDataOut.size");
  ASSERT_EQ(connection12->at("endElement"), "TrafficStepper");
  ASSERT_EQ(connection12->at("endConnector"), "SensorDataIn.size");
  auto connection13 = connections[13];
  ASSERT_EQ(connection13->size(), 4);
  ASSERT_EQ(connection13->at("startElement"), "DummySensor");
  ASSERT_EQ(connection13->at("startConnector"), "SensorDataOut.base.lo");
  ASSERT_EQ(connection13->at("endElement"), "TrafficStepper");
  ASSERT_EQ(connection13->at("endConnector"), "SensorDataIn.base.lo");
  auto connection14 = connections[14];
  ASSERT_EQ(connection14->size(), 4);
  ASSERT_EQ(connection14->at("startElement"), "DummySensor");
  ASSERT_EQ(connection14->at("startConnector"), "SensorDataOut.base.hi");
  ASSERT_EQ(connection14->at("endElement"), "TrafficStepper");
  ASSERT_EQ(connection14->at("endConnector"), "SensorDataIn.base.hi");
}

TEST_F(SSPImporterTests, ImportSystemConnectors)
{
  Import(OSMP_CONNECTION_TEST_SSD);

  auto system = ssdFiles[0]->ssdSystem;

  auto connectors = system->GetConnectors();
  ASSERT_EQ(connectors.size(), 12);

  auto connector0 = connectors[0];
  ASSERT_EQ(connector0.first, "output");
  ASSERT_EQ(std::get<1>(connector0.second).first, "TrafficUpdateOut.size");

  auto connector1 = connectors[1];
  ASSERT_EQ(connector1.first, "output");
  ASSERT_EQ(std::get<1>(connector1.second).first, "TrafficUpdateOut.base.lo");

  auto connector2 = connectors[2];
  ASSERT_EQ(connector2.first, "output");
  ASSERT_EQ(std::get<1>(connector2.second).first, "TrafficUpdateOut.base.hi");

  auto connector3 = connectors[3];
  ASSERT_EQ(connector3.first, "input");
  ASSERT_EQ(std::get<1>(connector3.second).first, "SensorViewIn.size");

  auto connector4 = connectors[4];
  ASSERT_EQ(connector4.first, "input");
  ASSERT_EQ(std::get<1>(connector4.second).first, "SensorViewIn.base.lo");

  auto connector5 = connectors[5];
  ASSERT_EQ(connector5.first, "input");
  ASSERT_EQ(std::get<1>(connector5.second).first, "SensorViewIn.base.hi");
}

TEST_F(SSPImporterTests, ImportComponentConnctors)
{
  Import(OSMP_CONNECTION_TEST_SSD);

  auto system = ssdFiles[0]->ssdSystem;
  auto components = system->GetComponents();
  auto component = components[0];

  auto connectors = component->GetConnectors();
  ASSERT_EQ(connectors.size(), 12);

  auto connector0 = connectors[0];
  ASSERT_EQ(connector0.first, "output");
  ASSERT_EQ(std::get<0>(connector0.second).first, "DummySensor");
  ASSERT_EQ(std::get<0>(connector0.second).second.name, "SensorDataOut.size");
  ASSERT_EQ(std::get<0>(connector0.second).second.osmpLinkName, "OSMPSensorDataOut");

  auto connector1 = connectors[1];
  ASSERT_EQ(connector1.first, "output");
  ASSERT_EQ(std::get<0>(connector1.second).first, "DummySensor");
  ASSERT_EQ(std::get<0>(connector1.second).second.name, "SensorDataOut.base.lo");
  ASSERT_EQ(std::get<0>(connector1.second).second.osmpLinkName, "OSMPSensorDataOut");

  auto connector2 = connectors[2];
  ASSERT_EQ(connector2.first, "output");
  ASSERT_EQ(std::get<0>(connector2.second).first, "DummySensor");
  ASSERT_EQ(std::get<0>(connector2.second).second.name, "SensorDataOut.base.hi");
  ASSERT_EQ(std::get<0>(connector2.second).second.osmpLinkName, "OSMPSensorDataOut");

  auto connector3 = connectors[3];
  ASSERT_EQ(connector3.first, "input");
  ASSERT_EQ(std::get<0>(connector3.second).first, "DummySensor");
  ASSERT_EQ(std::get<0>(connector3.second).second.name, "SensorViewIn.size");
  ASSERT_EQ(std::get<0>(connector3.second).second.osmpLinkName, "OSMPSensorViewIn");

  auto connector4 = connectors[4];
  ASSERT_EQ(connector4.first, "input");
  ASSERT_EQ(std::get<0>(connector4.second).first, "DummySensor");
  ASSERT_EQ(std::get<0>(connector4.second).second.name, "SensorViewIn.base.lo");
  ASSERT_EQ(std::get<0>(connector4.second).second.osmpLinkName, "OSMPSensorViewIn");

  auto connector5 = connectors[5];
  ASSERT_EQ(connector5.first, "input");
  ASSERT_EQ(std::get<0>(connector5.second).first, "DummySensor");
  ASSERT_EQ(std::get<0>(connector5.second).second.name, "SensorViewIn.base.hi");
  ASSERT_EQ(std::get<0>(connector5.second).second.osmpLinkName, "OSMPSensorViewIn");
}
