/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <MantleAPI/Traffic/entity_properties.h>
#include <filesystem>
#include <fmi2_enums.h>
#include <stdexcept>

#include "FmuHandler.h"
#include "OWL/fakes/fakeWorldData.h"
#include "common/runtimeInformation.h"
#include "common/stochasticDefinitions.h"
#include "fakeAgent.h"
#include "fakeCallback.h"
#include "fakeEgoAgent.h"
#include "fakeParameter.h"
#include "fakeRadio.h"
#include "fakeStochastics.h"
#include "fakeWorld.h"
#include "include/fmuHandlerInterface.h"

using ::testing::_;
using ::testing::AnyNumber;
using ::testing::DoubleEq;
using ::testing::Eq;
using ::testing::HasSubstr;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;
using ::testing::StrEq;
using ::testing::ThrowsMessage;

template <size_t FMI>
class FmuHandlerPartialMock : FmuHandler<FMI2>
{
public:
  FmuHandlerPartialMock(std::string componentName,
                        fmu_check_data_t &cdata,
                        FmuVariables &fmuVariables,
                        std::map<ValueReferenceAndType, FmuValue> &fmuVariableValues,
                        const ParameterInterface *parameters,
                        WorldInterface *world,
                        AgentInterface *agent,
                        std::shared_ptr<ScenarioControlInterface> const scenarioControl,
                        StochasticsInterface *stochastics,
                        const CallbackInterface *callbacks)
      : FmuHandler<FMI>(componentName,
                        cdata,
                        fmuVariables,
                        fmuVariableValues,
                        parameters,
                        world,
                        agent,
                        stochastics,
                        callbacks,
                        scenarioControl)
  {
  }

  void PrepareInit() override { FmuHandler<FMI>::PrepareInit(); }

  void Init() override { FmuHandler<FMI>::Init(); }

  void ReadValues() override { FmuHandler<FMI>::ReadValues(); }

  void SyncFmuVariablesAndParameters() override { FmuHandler<FMI>::SyncFmuVariablesAndParameters(); }

  void WriteValues() override { FmuHandler<FMI>::WriteValues(); }

  std::unordered_map<std::string, std::unordered_map<int, std::string>> GetFmuTypeDefinitions() override { return {}; }

  MOCK_METHOD(FmuVariables, GetFmuVariables, (), (override));

  MOCK_METHOD(void, SetFmuValue, (int, FmuValue, VariableType), (override));
  MOCK_METHOD(void, SetFmuValues, (std::vector<int>, std::vector<FmuValue>, VariableType), (override));
  MOCK_METHOD(void, GetFmuValue, (int, FmuValue &, VariableType), (override));
  MOCK_METHOD(void, GetFmuValues, (std::vector<int>, std::vector<FmuValue> &, VariableType), (override));

  MOCK_METHOD(jm_status_enu_t, PrepareFmuInit, (), (override));
  MOCK_METHOD(jm_status_enu_t, FmiEndHandling, (), (override));
  MOCK_METHOD(jm_status_enu_t, FmiSimulateStep, (double), (override));
  MOCK_METHOD(jm_status_enu_t, FmiPrepSimulate, (), (override));

  MOCK_METHOD(void, HandleFmiStatus, (jm_status_enu_t status, std::string logPrefix), (override));
};

class FmuWriteAndReadValuesTests : public ::testing::Test
{
public:
  FmuWriteAndReadValuesTests()
  {
    ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
    ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
    ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
    ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
    ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(parameterLists));
    ON_CALL(fakeParameter, GetParametersStochastic()).WillByDefault(ReturnRef(stochasticParameter));
    ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(ReturnRef(fakeRuntimeInfo));

    ON_CALL(fakeWorld, GetWorldData()).WillByDefault(Return(&fakeWorldData));
    ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));
    ON_CALL(fakeAgent, GetId()).WillByDefault(Return(4321));

    InitFmu();
  }

  void fillParameters()
  {
    intParameter.insert(std::make_pair("Parameter_ParameterConstant", 0));
    intParameter.insert(std::make_pair("Parameter_ParameterFixed", 1));
    intParameter.insert(std::make_pair("Parameter_ParameterTunable", 2));
  }

  void fillVariables()
  {
    FmuVariable2 fmuVarParaConst(0, VariableType::Int, "", fmi2_causality_enu_parameter, fmi2_variability_enu_constant);
    FmuVariable2 fmuVarParaFixed(1, VariableType::Int, "", fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);
    FmuVariable2 fmuVarParaTunable(
        2, VariableType::Int, "", fmi2_causality_enu_parameter, fmi2_variability_enu_tunable);

    FmuVariable2 fmuVarOutConst(3, VariableType::Int, "", fmi2_causality_enu_output, fmi2_variability_enu_constant);
    FmuVariable2 fmuVarOutFixed(4, VariableType::Int, "", fmi2_causality_enu_output, fmi2_variability_enu_fixed);
    FmuVariable2 fmuVarOutContinuous(
        5, VariableType::Int, "", fmi2_causality_enu_output, fmi2_variability_enu_continuous);
    FmuVariable2 fmuVarOutDiscrete(6, VariableType::Int, "", fmi2_causality_enu_output, fmi2_variability_enu_discrete);

    FmuVariable2 fmuVarCalcParaConst(
        7, VariableType::Int, "", fmi2_causality_enu_calculated_parameter, fmi2_variability_enu_constant);
    FmuVariable2 fmuVarCalcParaFixed(
        8, VariableType::Int, "", fmi2_causality_enu_calculated_parameter, fmi2_variability_enu_fixed);
    FmuVariable2 fmuVarCalcParaTunable(
        9, VariableType::Int, "", fmi2_causality_enu_calculated_parameter, fmi2_variability_enu_tunable);

    FmuVariable2 fmuVarInConst(10, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_constant);
    FmuVariable2 fmuVarInFixed(11, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_fixed);
    FmuVariable2 fmuVarInContinuous(
        12, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_continuous);
    FmuVariable2 fmuVarInDiscrete(13, VariableType::Int, "", fmi2_causality_enu_input, fmi2_variability_enu_discrete);

    std::vector<std::pair<std::string, FmuVariable2>> fmuVariables2;

    fmuVariables2.emplace_back("ParameterConstant", fmuVarParaConst);
    fmuVariables2.emplace_back("ParameterFixed", fmuVarParaFixed);
    fmuVariables2.emplace_back("ParameterTunable", fmuVarParaTunable);

    fmuVariables2.emplace_back("InputConstant", fmuVarOutConst);
    fmuVariables2.emplace_back("InputFixed", fmuVarOutFixed);
    fmuVariables2.emplace_back("InputTunable", fmuVarOutContinuous);
    fmuVariables2.emplace_back("Inputunable", fmuVarOutDiscrete);

    fmuVariables2.emplace_back("CalculatedParameterConstant", fmuVarCalcParaConst);
    fmuVariables2.emplace_back("CalculatedParameterFixed", fmuVarCalcParaFixed);
    fmuVariables2.emplace_back("CalculatedParameterTunable", fmuVarCalcParaTunable);

    fmuVariables2.emplace_back("OutputConstant", fmuVarInConst);
    fmuVariables2.emplace_back("OutputFixed", fmuVarInFixed);
    fmuVariables2.emplace_back("OutputTunable", fmuVarInContinuous);
    fmuVariables2.emplace_back("OutputDiscrete", fmuVarInDiscrete);

    FmuVariables fmuVariables
        = std::unordered_map<std::string, FmuVariable2>(fmuVariables2.begin(), fmuVariables2.end());
    this->fmuVariables = std::make_shared<FmuVariables>(fmuVariables);
  }

  void fillVariableValues()
  {
    ValueReferenceAndType valueReferenceAndType0 = std::make_pair(0, VariableType::Int);
    ValueReferenceAndType valueReferenceAndType1 = std::make_pair(1, VariableType::Int);
    ValueReferenceAndType valueReferenceAndType2 = std::make_pair(2, VariableType::Int);

    ValueReferenceAndType valueReferenceAndType3 = std::make_pair(3, VariableType::Int);
    ValueReferenceAndType valueReferenceAndType4 = std::make_pair(4, VariableType::Int);
    ValueReferenceAndType valueReferenceAndType5 = std::make_pair(5, VariableType::Int);
    ValueReferenceAndType valueReferenceAndType6 = std::make_pair(6, VariableType::Int);

    ValueReferenceAndType valueReferenceAndType7 = std::make_pair(7, VariableType::Int);
    ValueReferenceAndType valueReferenceAndType8 = std::make_pair(8, VariableType::Int);
    ValueReferenceAndType valueReferenceAndType9 = std::make_pair(9, VariableType::Int);

    ValueReferenceAndType valueReferenceAndType10 = std::make_pair(10, VariableType::Int);
    ValueReferenceAndType valueReferenceAndType11 = std::make_pair(11, VariableType::Int);
    ValueReferenceAndType valueReferenceAndType12 = std::make_pair(12, VariableType::Int);
    ValueReferenceAndType valueReferenceAndType13 = std::make_pair(13, VariableType::Int);

    // NOLINTBEGIN(cppcoreguidelines-pro-type-union-access)
    FmuValue fmuValue0{};
    fmuValue0.intValue = 0;
    FmuValue fmuValue1{};
    fmuValue0.intValue = 1;
    FmuValue fmuValue2{};
    fmuValue0.intValue = 2;

    FmuValue fmuValue3{};
    fmuValue3.intValue = 3;
    FmuValue fmuValue4{};
    fmuValue4.intValue = 4;
    FmuValue fmuValue5{};
    fmuValue5.intValue = 5;
    FmuValue fmuValue6{};
    fmuValue6.intValue = 6;

    FmuValue fmuValue7{};
    fmuValue7.intValue = 7;
    FmuValue fmuValue8{};
    fmuValue8.intValue = 8;
    FmuValue fmuValue9{};
    fmuValue9.intValue = 9;

    FmuValue fmuValue10{};
    fmuValue10.intValue = 10;
    FmuValue fmuValue11{};
    fmuValue11.intValue = 11;
    FmuValue fmuValue12{};
    fmuValue12.intValue = 12;
    FmuValue fmuValue13{};
    fmuValue13.intValue = 13;
    // NOLINTEND(cppcoreguidelines-pro-type-union-access)

    std::map<ValueReferenceAndType, FmuValue> fmuVariableValues;

    fmuVariableValues.insert(std::make_pair(valueReferenceAndType0, fmuValue0));
    fmuVariableValues.insert(std::make_pair(valueReferenceAndType1, fmuValue1));
    fmuVariableValues.insert(std::make_pair(valueReferenceAndType2, fmuValue2));

    fmuVariableValues.insert(std::make_pair(valueReferenceAndType3, fmuValue3));
    fmuVariableValues.insert(std::make_pair(valueReferenceAndType4, fmuValue4));
    fmuVariableValues.insert(std::make_pair(valueReferenceAndType5, fmuValue5));
    fmuVariableValues.insert(std::make_pair(valueReferenceAndType6, fmuValue6));

    fmuVariableValues.insert(std::make_pair(valueReferenceAndType7, fmuValue7));
    fmuVariableValues.insert(std::make_pair(valueReferenceAndType8, fmuValue8));
    fmuVariableValues.insert(std::make_pair(valueReferenceAndType9, fmuValue9));

    fmuVariableValues.insert(std::make_pair(valueReferenceAndType10, fmuValue10));
    fmuVariableValues.insert(std::make_pair(valueReferenceAndType11, fmuValue11));
    fmuVariableValues.insert(std::make_pair(valueReferenceAndType12, fmuValue12));
    fmuVariableValues.insert(std::make_pair(valueReferenceAndType13, fmuValue13));

    this->fmuVariableValues = std::make_shared<std::map<ValueReferenceAndType, FmuValue>>(fmuVariableValues);
  }

  void InitFmu()
  {
    fmuCheckData = std::make_shared<fmu_check_data_t>();
    fillVariables();
    fillVariableValues();
    fillParameters();

    entityProperties = std::make_shared<mantle_api::VehicleProperties>();

    ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(entityProperties));

    fmuHandlerPartialMock = std::make_unique<FmuHandlerPartialMock<FMI2>>("FmuHandler",
                                                                          *fmuCheckData,
                                                                          *fmuVariables,
                                                                          *fmuVariableValues,
                                                                          &fakeParameter,
                                                                          &fakeWorld,
                                                                          &fakeAgent,
                                                                          scenarioControl,
                                                                          &fakeStochastics,
                                                                          &fakeCallback);

    ON_CALL(*fmuHandlerPartialMock, GetFmuVariables).WillByDefault([this]() { return *fmuVariables; });
    ON_CALL(*fmuHandlerPartialMock, PrepareFmuInit()).WillByDefault(Return(jm_status_success));
    ON_CALL(*fmuHandlerPartialMock, FmiEndHandling()).WillByDefault(Return(jm_status_success));
    ON_CALL(*fmuHandlerPartialMock, FmiSimulateStep(_)).WillByDefault(Return(jm_status_success));
    ON_CALL(*fmuHandlerPartialMock, FmiPrepSimulate()).WillByDefault(Return(jm_status_success));

    fmuHandlerPartialMock->PrepareInit();
  }

protected:
  std::unique_ptr<FmuHandler<FMI2>> fmuHandler;
  std::shared_ptr<FmuVariables> fmuVariables;
  std::shared_ptr<std::map<ValueReferenceAndType, FmuValue>> fmuVariableValues;
  std::shared_ptr<fmu_check_data_t> fmuCheckData;
  std::shared_ptr<ScenarioControlInterface> const scenarioControl;

  NiceMock<FakeWorld> fakeWorld;
  NiceMock<OWL::Fakes::WorldData> fakeWorldData;
  FakeAgent fakeAgent;
  NiceMock<FakeEgoAgent> fakeEgoAgent;
  NiceMock<FakeCallback> fakeCallback;
  NiceMock<FakeParameter> fakeParameter;
  NiceMock<FakeStochastics> fakeStochastics;

  std::unique_ptr<FmuHandlerPartialMock<FMI2>> fmuHandlerPartialMock;

  std::map<std::string, const std::string> stringParameter{};
  std::map<std::string, bool> boolParameter{};
  std::map<std::string, int> intParameter{};
  std::map<std::string, double> doubleParameter{};
  std::map<std::string, FakeParameter::ParameterLists> parameterLists{};
  std::map<std::string, const openpass::parameter::StochasticDistribution> stochasticParameter{};

  openpass::common::RuntimeInformation fakeRuntimeInfo{
      0, 100, {"", std::filesystem::current_path().string(), "fakeOutDir", ""}};

  std::shared_ptr<mantle_api::VehicleProperties> entityProperties;
};

TEST_F(FmuWriteAndReadValuesTests, TestWriteParameters)
{
  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(_, _, _)).Times(AnyNumber());
  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(0, _, _)).Times(0);
  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(1, _, _)).Times(1);
  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(2, _, _)).Times(1);
  fmuHandlerPartialMock->ReadValues();
  fmuHandlerPartialMock->WriteValues();

  fmuHandlerPartialMock->Init();

  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(_, _, _)).Times(AnyNumber());
  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(0, _, _)).Times(0);
  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(1, _, _)).Times(0);
  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(2, _, _)).Times(0);

  fmuHandlerPartialMock->ReadValues();
  fmuHandlerPartialMock->WriteValues();
}

TEST_F(FmuWriteAndReadValuesTests, TestReadCalculatedParameters)
{
  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(_, _, _)).Times(AnyNumber());
  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(7, _, _)).Times(1);
  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(8, _, _)).Times(1);
  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(9, _, _)).Times(1);
  fmuHandlerPartialMock->ReadValues();
  fmuHandlerPartialMock->WriteValues();

  fmuHandlerPartialMock->Init();

  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(_, _, _)).Times(AnyNumber());
  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(7, _, _)).Times(0);
  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(8, _, _)).Times(0);
  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(9, _, _)).Times(1);

  fmuHandlerPartialMock->ReadValues();
  fmuHandlerPartialMock->WriteValues();
}

TEST_F(FmuWriteAndReadValuesTests, TestWriteInputs)
{
  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(_, _, _)).Times(AnyNumber());
  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(10, _, _)).Times(0);
  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(11, _, _)).Times(1);
  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(12, _, _)).Times(1);
  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(13, _, _)).Times(1);
  fmuHandlerPartialMock->ReadValues();
  fmuHandlerPartialMock->WriteValues();

  fmuHandlerPartialMock->Init();

  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(_, _, _)).Times(AnyNumber());
  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(10, _, _)).Times(0);
  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(11, _, _)).Times(0);
  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(12, _, _)).Times(1);
  EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(13, _, _)).Times(1);

  fmuHandlerPartialMock->ReadValues();
  fmuHandlerPartialMock->WriteValues();
}

TEST_F(FmuWriteAndReadValuesTests, TestReadOuputs)
{
  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(_, _, _)).Times(AnyNumber());
  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(3, _, _)).Times(1);
  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(4, _, _)).Times(1);
  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(5, _, _)).Times(1);
  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(6, _, _)).Times(1);
  fmuHandlerPartialMock->ReadValues();
  fmuHandlerPartialMock->WriteValues();

  fmuHandlerPartialMock->Init();

  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(_, _, _)).Times(AnyNumber());
  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(3, _, _)).Times(0);
  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(4, _, _)).Times(0);
  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(5, _, _)).Times(1);
  EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(6, _, _)).Times(1);

  fmuHandlerPartialMock->ReadValues();
  fmuHandlerPartialMock->WriteValues();
}

TEST_F(FmuWriteAndReadValuesTests, ParseParameters_ParseParameters)
{
  intParameter = {{"Parameter_ParameterInt", 3}};
  boolParameter = {{"Parameter_ParameterBool", true}};
  doubleParameter = {{"Parameter_ParameterDouble", 4.4}};
  stochasticParameter = {{"Parameter_ParameterStoch", openpass::parameter::NormalDistribution{5.5, 1, 5.5, 5.5}}};
  stringParameter = {{"Parameter_ParameterString", "a string"}};

  FmuVariable2 fmuVarParaBool(0, VariableType::Bool, "", fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);
  FmuVariable2 fmuVarParaInt(14, VariableType::Int, "", fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);
  FmuVariable2 fmuVarParaDouble(0, VariableType::Double, "", fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);
  FmuVariable2 fmuVarParaStoch(1, VariableType::Double, "", fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);
  FmuVariable2 fmuVarParaString(0, VariableType::String, "", fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);

  std::get<FMI2>(*fmuVariables).clear();
  std::get<FMI2>(*fmuVariables).insert({"ParameterBool", fmuVarParaBool});
  std::get<FMI2>(*fmuVariables).insert({"ParameterInt", fmuVarParaInt});
  std::get<FMI2>(*fmuVariables).insert({"ParameterDouble", fmuVarParaDouble});
  std::get<FMI2>(*fmuVariables).insert({"ParameterStoch", fmuVarParaStoch});
  std::get<FMI2>(*fmuVariables).insert({"ParameterString", fmuVarParaString});

  ValueReferenceAndType valueReferenceAndType14{14, VariableType::Int};
  ValueReferenceAndType valueReferenceAndType15{0, VariableType::Bool};
  ValueReferenceAndType valueReferenceAndType16{0, VariableType::Double};
  ValueReferenceAndType valueReferenceAndType17{1, VariableType::Double};
  ValueReferenceAndType valueReferenceAndType18{0, VariableType::String};

  FmuValue fmuValue14{};
  FmuValue fmuValue15{};
  FmuValue fmuValue16{};
  FmuValue fmuValue17{};
  FmuValue fmuValue18{};

  fmuVariableValues->insert({valueReferenceAndType14, fmuValue14});
  fmuVariableValues->insert({valueReferenceAndType15, fmuValue15});
  fmuVariableValues->insert({valueReferenceAndType16, fmuValue16});
  fmuVariableValues->insert({valueReferenceAndType17, fmuValue17});
  fmuVariableValues->insert({valueReferenceAndType18, fmuValue18});

  fmuHandlerPartialMock->PrepareInit();
  fmuHandlerPartialMock->SyncFmuVariablesAndParameters();
  fmuHandlerPartialMock->WriteValues();

  ASSERT_THAT(fmuVariableValues->at({0, VariableType::Bool}).boolValue, Eq(true));
  ASSERT_THAT(fmuVariableValues->at({0, VariableType::Double}).realValue, DoubleEq(4.4));
  ASSERT_THAT(fmuVariableValues->at({1, VariableType::Double}).realValue, DoubleEq(5.5));
  ASSERT_THAT(fmuVariableValues->at({14, VariableType::Int}).intValue, Eq(3));
  ASSERT_THAT(fmuVariableValues->at({0, VariableType::String}).stringValue, StrEq("a string"));
}

TEST_F(FmuWriteAndReadValuesTests, ParseParameterAssignment_RandomSeed)
{
  stringParameter.insert({"Parameter_AssignSpecial_ParameterInt", "RandomSeed"});

  const unsigned int fakeSeed = 4321;
  ON_CALL(fakeStochastics, GetRandomSeed()).WillByDefault(Return(fakeSeed));

  FmuVariable2 fmuVarParaInt(14, VariableType::Int, "", fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);
  std::get<FMI2>(*fmuVariables).insert({"ParameterInt", fmuVarParaInt});
  fmuVariableValues->insert({{14, VariableType::Int}, FmuValue{}});

  fmuHandlerPartialMock->PrepareInit();
  fmuHandlerPartialMock->SyncFmuVariablesAndParameters();

  ASSERT_THAT(fmuVariableValues->at({14, VariableType::Int}).intValue, Eq(fakeSeed));
}

TEST_F(FmuWriteAndReadValuesTests, ParseParameterAssignment_ConfigPath)
{
  stringParameter.insert({"Parameter_AssignSpecial_ParameterString", "ConfigPath"});
  fmuCheckData->FMUPath = "fakeFmu.fmu";

  FmuVariable2 fmuVarParaString(0, VariableType::String, "", fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);
  std::get<FMI2>(*fmuVariables).insert({"ParameterString", fmuVarParaString});
  fmuVariableValues->insert({{0, VariableType::String}, FmuValue{}});

  fmuHandlerPartialMock->PrepareInit();
  fmuHandlerPartialMock->SyncFmuVariablesAndParameters();

  ASSERT_THAT(fmuVariableValues->at({0, VariableType::String}).stringValue,
              StrEq(std::filesystem::current_path().string()));
}

TEST_F(FmuWriteAndReadValuesTests, ParseParameterAssignment_OutputPath)
{
  stringParameter.insert({"Parameter_AssignSpecial_ParameterString", "OutputPath"});
  fmuCheckData->FMUPath = "fakeFmu.fmu";

  FmuVariable2 fmuVarParaString(0, VariableType::String, "", fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);
  std::get<FMI2>(*fmuVariables).insert({"ParameterString", fmuVarParaString});
  fmuVariableValues->insert({{0, VariableType::String}, FmuValue{}});

  fmuHandlerPartialMock->PrepareInit();
  fmuHandlerPartialMock->SyncFmuVariablesAndParameters();

  ASSERT_THAT(fmuVariableValues->at({0, VariableType::String}).stringValue,
#ifdef WIN32
              StrEq("fakeOutDir\\run00\\entity4321\\fakeFmu"));
#else
              StrEq("fakeOutDir/run00/entity4321/fakeFmu"));
#endif
}

TEST_F(FmuWriteAndReadValuesTests, ParseParameterAssignment_MaxSteering)
{
  entityProperties->front_axle.max_steering = 10.0_rad;
  stringParameter.insert({"Parameter_AssignSpecial_ParameterDouble", "MaxSteering"});

  FmuVariable2 fmuVarParaDouble(0, VariableType::Double, "", fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);
  std::get<FMI2>(*fmuVariables).insert({"ParameterDouble", fmuVarParaDouble});
  fmuVariableValues->insert({{0, VariableType::Double}, FmuValue{}});

  fmuHandlerPartialMock->PrepareInit();
  fmuHandlerPartialMock->SyncFmuVariablesAndParameters();

  ASSERT_THAT(fmuVariableValues->at({0, VariableType::Double}).realValue, DoubleEq(10.0));
}

TEST_F(FmuWriteAndReadValuesTests, ParseParameterAssignment_AxleRatio)
{
  entityProperties->properties["AxleRatio"] = "1.0";
  stringParameter.insert({"Parameter_AssignSpecial_ParameterDouble", "AxleRatio"});

  FmuVariable2 fmuVarParaDouble(0, VariableType::Double, "", fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);
  std::get<FMI2>(*fmuVariables).insert({"ParameterDouble", fmuVarParaDouble});
  fmuVariableValues->insert({{0, VariableType::Double}, FmuValue{}});

  fmuHandlerPartialMock->PrepareInit();
  fmuHandlerPartialMock->SyncFmuVariablesAndParameters();

  ASSERT_THAT(fmuVariableValues->at({0, VariableType::Double}).realValue, DoubleEq(1.0));
}

TEST_F(FmuWriteAndReadValuesTests, ParseParameterAssignment_SteeringRatio)
{
  entityProperties->properties["SteeringRatio"] = "11.0";
  stringParameter.insert({"Parameter_AssignSpecial_ParameterDouble", "SteeringRatio"});

  FmuVariable2 fmuVarParaDouble(0, VariableType::Double, "", fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);
  std::get<FMI2>(*fmuVariables).insert({"ParameterDouble", fmuVarParaDouble});
  fmuVariableValues->insert({{0, VariableType::Double}, FmuValue{}});

  fmuHandlerPartialMock->PrepareInit();
  fmuHandlerPartialMock->SyncFmuVariablesAndParameters();

  ASSERT_THAT(fmuVariableValues->at({0, VariableType::Double}).realValue, DoubleEq(11.0));
}

TEST_F(FmuWriteAndReadValuesTests, ParseParameterAssignment_Gears)
{
  entityProperties->properties["NumberOfGears"] = "2";
  entityProperties->properties["GearRatio1"] = "1.5";
  entityProperties->properties["GearRatio2"] = "2.5";

  stringParameter.insert({"Parameter_AssignSpecial_ParameterInt", "NumberOfGears"});
  stringParameter.insert({"Parameter_AssignSpecial_ParameterDouble1", "GearRatio1"});
  stringParameter.insert({"Parameter_AssignSpecial_ParameterDouble2", "GearRatio2"});

  FmuVariable2 fmuVarParaInt(14, VariableType::Int, "", fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);
  FmuVariable2 fmuVarParaDouble1(0, VariableType::Double, "", fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);
  FmuVariable2 fmuVarParaDouble2(1, VariableType::Double, "", fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);

  std::get<FMI2>(*fmuVariables).insert({"ParameterInt", fmuVarParaInt});
  std::get<FMI2>(*fmuVariables).insert({"ParameterDouble1", fmuVarParaDouble1});
  std::get<FMI2>(*fmuVariables).insert({"ParameterDouble2", fmuVarParaDouble2});

  fmuVariableValues->insert({{14, VariableType::Int}, FmuValue{}});
  fmuVariableValues->insert({{0, VariableType::Double}, FmuValue{}});
  fmuVariableValues->insert({{1, VariableType::Double}, FmuValue{}});

  fmuHandlerPartialMock->PrepareInit();
  fmuHandlerPartialMock->SyncFmuVariablesAndParameters();

  ASSERT_THAT(fmuVariableValues->at({14, VariableType::Int}).intValue, Eq(2));
  ASSERT_THAT(fmuVariableValues->at({0, VariableType::Double}).realValue, DoubleEq(1.5));
  ASSERT_THAT(fmuVariableValues->at({1, VariableType::Double}).realValue, DoubleEq(2.5));
}

TEST_F(FmuWriteAndReadValuesTests, ParseParameterAssignment_UnknownAssignment_Throws)
{
  stringParameter.insert({"Parameter_AssignSpecial_ParameterDouble", "NotAKnownAssignment"});

  FmuVariable2 fmuVarParaDouble(0, VariableType::Double, "", fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);
  std::get<FMI2>(*fmuVariables).insert({"ParameterDouble", fmuVarParaDouble});
  fmuVariableValues->insert({{0, VariableType::Double}, FmuValue{}});

  EXPECT_THAT([&]() { fmuHandlerPartialMock->PrepareInit(); },
              ThrowsMessage<std::runtime_error>(AllOf(HasSubstr("Unknown"), HasSubstr("NotAKnownAssignment"))));
}

TEST_F(FmuWriteAndReadValuesTests, ParseParameterAssignment_InvalidProperty_Throws)
{
  entityProperties->properties["NumberOfGears"] = "not a valid int";
  stringParameter.insert({"Parameter_AssignSpecial_ParameterInt", "NumberOfGears"});

  FmuVariable2 fmuVarParaInt(14, VariableType::Int, "", fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);
  std::get<FMI2>(*fmuVariables).insert({"ParameterInt", fmuVarParaInt});
  fmuVariableValues->insert({{14, VariableType::Int}, FmuValue{}});

  EXPECT_THAT([&]() { fmuHandlerPartialMock->PrepareInit(); },
              ThrowsMessage<std::runtime_error>(AllOf(HasSubstr("process"), HasSubstr("NumberOfGears"))));
}
