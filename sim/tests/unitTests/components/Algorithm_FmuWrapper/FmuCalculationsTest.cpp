/*******************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "FmuCalculations.h"
#include "OWL/fakes/fakeMovingObject.h"
#include "OWL/fakes/fakeWorldData.h"
#include "fakeAgent.h"
#include "fakeEgoAgent.h"
#include "fakeWorld.h"
#include "fakeWorldObject.h"

using ::testing::_;
using ::testing::Eq;
using ::testing::Return;
using ::testing::ReturnRef;
using ::testing::SizeIs;

TEST(FmuCalculationss, CalculateSensorFusionInfo_CorrectSorting)
{
  osi3::SensorData sensorDataIn;
  FakeAgent agent;
  FakeEgoAgent egoAgent;
  FakeWorld world;

  polygon_t boundingBox;
  GlobalRoadPositions roadPosition;

  ON_CALL(agent, GetEgoAgent()).WillByDefault(ReturnRef(egoAgent));
  ON_CALL(agent, GetPositionX()).WillByDefault(Return(0_m));
  ON_CALL(agent, GetPositionY()).WillByDefault(Return(0_m));
  ON_CALL(agent, GetBoundingBox2D()).WillByDefault(ReturnRef(boundingBox));

  sensorDataIn.add_moving_object()->mutable_header()->add_ground_truth_id()->set_value(10);
  sensorDataIn.add_moving_object()->mutable_header()->add_ground_truth_id()->set_value(11);
  sensorDataIn.add_moving_object()->mutable_header()->add_ground_truth_id()->set_value(12);
  sensorDataIn.add_stationary_object()->mutable_header()->add_ground_truth_id()->set_value(20);
  sensorDataIn.add_stationary_object()->mutable_header()->add_ground_truth_id()->set_value(21);

  FakeAgent agent10;
  ON_CALL(agent10, GetId()).WillByDefault(Return(10));
  ON_CALL(agent10, GetPositionX()).WillByDefault(Return(100_m));
  ON_CALL(agent10, GetPositionY()).WillByDefault(Return(10_m));
  ON_CALL(agent10, GetBoundingBox2D()).WillByDefault(ReturnRef(boundingBox));
  ON_CALL(agent10, GetRoadPosition(_)).WillByDefault(ReturnRef(roadPosition));
  ON_CALL(world, GetAgent(10)).WillByDefault(Return(&agent10));

  FakeAgent agent11;
  ON_CALL(agent11, GetId()).WillByDefault(Return(11));
  ON_CALL(agent11, GetPositionX()).WillByDefault(Return(-150_m));
  ON_CALL(agent11, GetPositionY()).WillByDefault(Return(0_m));
  ON_CALL(agent11, GetBoundingBox2D()).WillByDefault(ReturnRef(boundingBox));
  ON_CALL(agent11, GetRoadPosition(_)).WillByDefault(ReturnRef(roadPosition));
  ON_CALL(world, GetAgent(11)).WillByDefault(Return(&agent11));

  FakeAgent agent12;
  ON_CALL(agent12, GetId()).WillByDefault(Return(12));
  ON_CALL(agent12, GetPositionX()).WillByDefault(Return(125_m));
  ON_CALL(agent12, GetPositionY()).WillByDefault(Return(-10_m));
  ON_CALL(agent12, GetBoundingBox2D()).WillByDefault(ReturnRef(boundingBox));
  ON_CALL(agent12, GetRoadPosition(_)).WillByDefault(ReturnRef(roadPosition));
  ON_CALL(world, GetAgent(12)).WillByDefault(Return(&agent12));

  OWL::Fakes::WorldData worldData;

  FakeWorldObject object20;
  ON_CALL(object20, GetId()).WillByDefault(Return(20));
  ON_CALL(object20, GetPositionX()).WillByDefault(Return(125_m));
  ON_CALL(object20, GetPositionY()).WillByDefault(Return(5_m));
  ON_CALL(object20, GetBoundingBox2D()).WillByDefault(ReturnRef(boundingBox));
  ON_CALL(object20, GetRoadPosition(_)).WillByDefault(ReturnRef(roadPosition));
  OWL::Fakes::StationaryObject stationaryObject20;
  stationaryObject20.SetLinkedObjectForTesting(&object20);
  ON_CALL(worldData, GetStationaryObject(20)).WillByDefault(ReturnRef(stationaryObject20));

  FakeWorldObject object21;
  ON_CALL(object21, GetId()).WillByDefault(Return(21));
  ON_CALL(object21, GetPositionX()).WillByDefault(Return(-5_m));
  ON_CALL(object21, GetPositionY()).WillByDefault(Return(110_m));
  ON_CALL(object21, GetBoundingBox2D()).WillByDefault(ReturnRef(boundingBox));
  ON_CALL(object21, GetRoadPosition(_)).WillByDefault(ReturnRef(roadPosition));
  OWL::Fakes::StationaryObject stationaryObject21;
  stationaryObject21.SetLinkedObjectForTesting(&object21);
  ON_CALL(worldData, GetStationaryObject(21)).WillByDefault(ReturnRef(stationaryObject21));

  ON_CALL(world, GetWorldData()).WillByDefault(Return(&worldData));

  auto result = FmuCalculations::CalculateSensorFusionInfo(sensorDataIn, &world, &agent);

  ASSERT_THAT(result, SizeIs(5));
  EXPECT_THAT(result[0].id, Eq(10));
  EXPECT_THAT(result[1].id, Eq(21));
  EXPECT_THAT(result[2].id, Eq(20));
  EXPECT_THAT(result[3].id, Eq(12));
  EXPECT_THAT(result[4].id, Eq(11));
}