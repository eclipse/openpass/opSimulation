/********************************************************************************
 * Copyright (c) 2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/poly_line.h>
#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Common/spline.h>
#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Common/trajectory.h>
#include <MantleAPI/Traffic/control_strategy.h>
#include <numeric>
#include <units.h>
#include <vector>

#include "algorithm_RouteControl_implementation.h"
#include "common/vector2d.h"
#include "dontCare.h"
#include "fakeAgent.h"
#include "fakeParameter.h"
#include "fakeScenarioControl.h"

using ::testing::_;
using ::testing::DontCare;
using ::testing::DoubleEq;
using ::testing::DoubleNear;
using ::testing::Eq;
using ::testing::Ne;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

//class SignalInterface;

TEST(RouteControl, TestCases)
{
  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, double> fakeParametersDouble;

  double fakeDrivingAggressiveness = 1.0;
  double fakePedalsKp = -2.0;
  double fakePedalsKi = -1.5;
  double fakePedalsKd = 0.0;
  double fakeSteeringKp = -18.0;
  double fakeSteeringKi = -0.6;
  double fakeSteeringKd = 0.0;

  fakeParametersDouble.insert(std::pair<std::string, double>("DrivingAggressiveness", fakeDrivingAggressiveness));
  fakeParametersDouble.insert(std::pair<std::string, double>("PedalsKp", fakePedalsKp));
  fakeParametersDouble.insert(std::pair<std::string, double>("PedalsKi", fakePedalsKi));
  fakeParametersDouble.insert(std::pair<std::string, double>("PedalsKd", fakePedalsKd));
  fakeParametersDouble.insert(std::pair<std::string, double>("SteeringKp", fakeSteeringKp));
  fakeParametersDouble.insert(std::pair<std::string, double>("SteeringKi", fakeSteeringKi));
  fakeParametersDouble.insert(std::pair<std::string, double>("SteeringKd", fakeSteeringKd));

  ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeParametersDouble));

  double fakeMinBrakeTorque = -10000.0;
  double fakeMass = 1000.0;
  double fakeMaxEnginePower = 100000.0;
  double fakeSteeringRatio = 1.0;

  NiceMock<FakeAgent> fakeAgent;

  auto fakeVehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();

  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MaximumEnginePower", std::to_string(fakeMaxEnginePower)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("MinimumEngineTorque", std::to_string(fakeMinBrakeTorque)));
  fakeVehicleModelParameters->properties.insert(
      std::pair<std::string, std::string>("SteeringRatio", std::to_string(fakeSteeringRatio)));
  fakeVehicleModelParameters->mass = units::mass::kilogram_t(fakeMass);

  ON_CALL(fakeAgent, GetVehicleModelParameters()).WillByDefault(Return(fakeVehicleModelParameters));

  // fake trajectory
  int pointCount = 120;
  units::length::meter_t prevPosX = 20.0_m;
  units::velocity::meters_per_second_t longVelocityBasis = 10_mps;  // in m/s // in s
  units::time::second_t cycleTime = 0.01_s;
  mantle_api::PolyLine fakePolyLine;

  for (int j = 0; j < pointCount; j++)
  {
    auto tPos = j * cycleTime;  // in s
    auto longVelocity = longVelocityBasis;
    if ((j > 10) && (j < 15))  // velocity decrease
    {
      longVelocity *= 0.7;
    }
    if ((j > 60) && (j < 65))  // velocity increase
    {
      longVelocity *= 1.3;
    }
    auto posX = prevPosX + longVelocity * cycleTime;
    mantle_api::Pose fakePose{{posX, 0_m, 0_m}, {0_rad, 0_rad, 0_rad}};
    mantle_api::PolyLinePoint fakePolyLinePoint{fakePose, tPos};
    fakePolyLine.push_back(fakePolyLinePoint);
    prevPosX = posX;
  }
  mantle_api::Trajectory fakeCoordinates = {"", fakePolyLine};

  auto followTrajectoryControlStrategy = std::make_shared<mantle_api::FollowTrajectoryControlStrategy>();
  followTrajectoryControlStrategy->trajectory = fakeCoordinates;
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> strategies{followTrajectoryControlStrategy};

  const std::shared_ptr<NiceMock<FakeScenarioControl>> fakeScenarioControl{
      std::make_shared<NiceMock<FakeScenarioControl>>()};
  ON_CALL(*fakeScenarioControl, GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(strategies));

  ON_CALL(fakeAgent, GetPositionY()).WillByDefault(Return(0.0_m));
  ON_CALL(fakeAgent, GetYaw()).WillByDefault(Return(0.0_rad));

  int nbtests = 6;
  for (int i = 1; i <= nbtests; i++)
  {
    int cycleMin = 0;
    int cycleMax = 0;
    units::length::meter_t basePosX;
    units::velocity::meters_per_second_t velX;

    // TC1: Neither acceleration nor braking at start
    if (i == 1)
    {
      cycleMin = 0;
      cycleMax = 0;
      basePosX = 20_m;
      velX = 10_mps;
    }
    // TC2: Acceleration at start
    if (i == 2)
    {
      cycleMin = 0;
      cycleMax = 0;
      basePosX = 20_m;
      velX = 9_mps;
    }
    // TC3: Braking at start
    if (i == 3)
    {
      cycleMin = 0;
      cycleMax = 0;
      basePosX = 20_m;
      velX = 11_mps;
    }
    // TC4
    if (i == 4)
    {
      cycleMin = 0;
      cycleMax = 10;
      basePosX = 20_m;
      velX = 10_mps;
    }
    // TC5
    if (i == 5)
    {
      cycleMin = 9;
      cycleMax = 19;
      basePosX = 21.10_m;
      velX = 10_mps;
    }
    // TC6
    if (i == 6)
    {
      cycleMin = 58;
      cycleMax = 68;
      basePosX = 25.78_m;
      velX = 10_mps;
    }

    ON_CALL(fakeAgent, GetVelocity(_)).WillByDefault(Return(Common::Vector2d{velX, 0.0_mps}));
    int fakeCycleTime = 10;  // in ms
    Algorithm_Routecontrol_Implementation AlgoRouteControl_implementation("Algorithm_RouteControl",
                                                                          false,
                                                                          DontCare<int>(),
                                                                          DontCare<int>(),
                                                                          DontCare<int>(),
                                                                          fakeCycleTime,
                                                                          nullptr,
                                                                          nullptr,
                                                                          &fakeParameters,
                                                                          nullptr,
                                                                          nullptr,
                                                                          &fakeAgent,
                                                                          fakeScenarioControl);

    std::shared_ptr<SignalInterface const> outputSignalLong;
    std::shared_ptr<SignalInterface const> outputSignalSteer;

    int fakeTimeMs = 0;
    auto curPosX = basePosX;

    AlgoRouteControl_implementation.UpdateInput(0, nullptr, 0);
    for (int k = cycleMin; k < cycleMax + 1; k++)
    {
      ON_CALL(fakeAgent, GetPositionX()).WillByDefault(Return(curPosX));

      fakeTimeMs = k * 10;  // in ms
      AlgoRouteControl_implementation.Trigger(fakeTimeMs);
      curPosX += velX * cycleTime;
    }
    AlgoRouteControl_implementation.UpdateOutput(0, outputSignalLong, 0);
    AlgoRouteControl_implementation.UpdateOutput(1, outputSignalSteer, 0);
    auto resultLongitudinalSignal = std::dynamic_pointer_cast<LongitudinalSignal const>(outputSignalLong);
    auto resultSteeringSignal = std::dynamic_pointer_cast<SteeringSignal const>(outputSignalSteer);

    // Test results
    // TC1: Neither acceleration nor braking at start
    if (i == 1)
    {
      EXPECT_THAT(resultLongitudinalSignal, Ne(nullptr));
      EXPECT_THAT(resultLongitudinalSignal->accPedalPos, DoubleNear(0.0, 1e-3));
      EXPECT_THAT(resultLongitudinalSignal->brakePedalPos, DoubleNear(0.0, 1e-3));
      EXPECT_THAT(resultLongitudinalSignal->gear, Eq(1));
      EXPECT_THAT(resultSteeringSignal, Ne(nullptr));
      EXPECT_THAT(resultSteeringSignal->steeringWheelAngle.value(), DoubleEq(0.0));
    }
    // TC2: Acceleration at start
    if (i == 2)
    {
      EXPECT_THAT(resultLongitudinalSignal->accPedalPos, DoubleEq(1.0));
      EXPECT_THAT(resultLongitudinalSignal->brakePedalPos, DoubleNear(0.0, 1e-3));
    }
    // TC3: Braking at start
    if (i == 3)
    {
      EXPECT_THAT(resultLongitudinalSignal->accPedalPos, DoubleNear(0.0, 1e-3));
      EXPECT_THAT(resultLongitudinalSignal->brakePedalPos, DoubleNear(0.824318181818, 1e-3));
    }
    // TC4
    if (i == 4)
    {
      EXPECT_THAT(resultLongitudinalSignal->accPedalPos, DoubleNear(0.0, 1e-3));
      EXPECT_THAT(resultLongitudinalSignal->brakePedalPos, DoubleNear(1.0, 1e-3));
    }
    // TC5
    if (i == 5)
    {
      EXPECT_THAT(resultLongitudinalSignal->accPedalPos, DoubleNear(0.0, 1e-3));
      EXPECT_THAT(resultLongitudinalSignal->brakePedalPos, DoubleNear(0.0405, 1e-3));
    }
    // TC6
    if (i == 6)
    {
      EXPECT_THAT(resultLongitudinalSignal->accPedalPos, DoubleNear(1.0, 1e-3));
      EXPECT_THAT(resultLongitudinalSignal->brakePedalPos, DoubleNear(0.0, 1e-3));
    }
  }
}
