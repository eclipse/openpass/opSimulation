################################################################################

# Copyright (c) 2023-2024 Volkswagen AG
#               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_TEST_NAME ActionSteeringSystem_Tests)
set(COMPONENT_SOURCE_DIR ${OPENPASS_SIMCORE_DIR}/components/Action_SteeringSystem/src)

add_openpass_target(
  NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT module
  DEFAULT_MAIN

  SOURCES
    Steeringsystem_Tests.cpp
    ${COMPONENT_SOURCE_DIR}/steeringsystem.cpp

  HEADERS
    ${COMPONENT_SOURCE_DIR}/steeringsystem.h

  INCDIRS
    ${COMPONENT_SOURCE_DIR}

  LIBRARIES
    MantleAPI::MantleAPI
)
