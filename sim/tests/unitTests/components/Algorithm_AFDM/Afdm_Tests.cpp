/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <map>
#include <memory>
#include <string>
#include <units.h>
#include <vector>

#include "common/accelerationSignal.h"
#include "fakeParameter.h"
#include "followingDriverModel.h"
#include "sensorDriverSignal.h"
#include "sensor_driverDefinitions.h"

class SignalInterface;

using ::testing::DoubleEq;
using ::testing::Ge;
using ::testing::Le;
using ::testing::Lt;
using ::testing::NiceMock;
using ::testing::ReturnRef;

TEST(AgentFollowingDriverModel, TriggerWithNoFrontAgentAndVelocityHigherWish_AccWithinLimits)
{
  const double maxAcceleration = 1.4;
  const double maxDeceleration = 2.0;

  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, double> fakeParametersDouble;

  ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeParametersDouble));

  AlgorithmAgentFollowingDriverModelImplementation implementation(
      "", false, 0, 0, 0, 0, nullptr, nullptr, &fakeParameters, nullptr, nullptr, nullptr);

  OwnVehicleInformation vehicleInfo;
  TrafficRuleInformation trafficRuleInfo;
  GeometryInformation geometricInfo;
  SurroundingObjects surroundingObjects;

  surroundingObjects.objectFront.exist = false;
  vehicleInfo.absoluteVelocity = units::velocity::kilometers_per_hour_t(200.0);
  const auto sensorDriverSignal0
      = std::make_shared<SensorDriverSignal const>(vehicleInfo, trafficRuleInfo, geometricInfo, surroundingObjects);
  std::shared_ptr<SignalInterface const> outputSignal;

  implementation.UpdateInput(0, sensorDriverSignal0, 0);
  implementation.Trigger(0);
  implementation.UpdateOutput(2, outputSignal, 0);

  auto brakingSignal = std::static_pointer_cast<AccelerationSignal const>(outputSignal);
  units::acceleration::meters_per_second_squared_t acceleration = brakingSignal->acceleration;

  ASSERT_THAT(acceleration.value(), Ge(-maxDeceleration));
  ASSERT_THAT(acceleration.value(), Le(maxAcceleration));

  vehicleInfo.absoluteVelocity = units::velocity::kilometers_per_hour_t(20.0);
  const auto sensorDriverSignal1
      = std::make_shared<SensorDriverSignal const>(vehicleInfo, trafficRuleInfo, geometricInfo, surroundingObjects);

  implementation.UpdateInput(0, sensorDriverSignal1, 100);
  implementation.Trigger(100);
  implementation.UpdateOutput(2, outputSignal, 100);

  brakingSignal = std::static_pointer_cast<AccelerationSignal const>(outputSignal);
  acceleration = brakingSignal->acceleration;

  ASSERT_THAT(acceleration.value(), Ge(-maxDeceleration));
  ASSERT_THAT(acceleration.value(), Le(maxAcceleration));
}

TEST(AgentFollowingDriverModel, TriggerWithNoFrontAgentAndVelocityIsWish_HoldVWish)
{
  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, double> fakeParametersDouble;

  ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeParametersDouble));

  AlgorithmAgentFollowingDriverModelImplementation implementation(
      "", false, 0, 0, 0, 0, nullptr, nullptr, &fakeParameters, nullptr, nullptr, nullptr);

  OwnVehicleInformation vehicleInfo;
  TrafficRuleInformation trafficRuleInfo;
  GeometryInformation geometricInfo;
  SurroundingObjects surroundingObjects;

  surroundingObjects.objectFront.exist = false;
  vehicleInfo.absoluteVelocity = units::velocity::kilometers_per_hour_t(120.0);
  const auto sensorDriverSignal
      = std::make_shared<SensorDriverSignal const>(vehicleInfo, trafficRuleInfo, geometricInfo, surroundingObjects);
  std::shared_ptr<SignalInterface const> outputSignal;

  implementation.UpdateInput(0, sensorDriverSignal, 0);
  implementation.Trigger(0);
  implementation.UpdateOutput(2, outputSignal, 0);

  auto brakingSignal = std::static_pointer_cast<AccelerationSignal const>(outputSignal);
  units::acceleration::meters_per_second_squared_t acceleration = brakingSignal->acceleration;
  ASSERT_THAT(acceleration.value(), DoubleEq(0.0));
}

TEST(AgentFollowingDriverModel, TriggerWithNoFrontAgentAndVelocityHigherWish_AccUntilWish)
{
  NiceMock<FakeParameter> fakeParameters;
  std::map<std::string, double> fakeParametersDouble;

  ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeParametersDouble));

  AlgorithmAgentFollowingDriverModelImplementation implementation(
      "", false, 0, 0, 0, 0, nullptr, nullptr, &fakeParameters, nullptr, nullptr, nullptr);

  OwnVehicleInformation vehicleInfo;
  TrafficRuleInformation trafficRuleInfo;
  GeometryInformation geometricInfo;
  SurroundingObjects surroundingObjects;

  surroundingObjects.objectFront.exist = false;
  vehicleInfo.absoluteVelocity = units::velocity::kilometers_per_hour_t(150.0);
  const auto sensorDriverSignal0
      = std::make_shared<SensorDriverSignal const>(vehicleInfo, trafficRuleInfo, geometricInfo, surroundingObjects);
  std::shared_ptr<SignalInterface const> outputSignal;

  implementation.UpdateInput(0, sensorDriverSignal0, 0);
  implementation.Trigger(0);
  implementation.UpdateOutput(2, outputSignal, 0);

  auto brakingSignal = std::static_pointer_cast<AccelerationSignal const>(outputSignal);
  units::acceleration::meters_per_second_squared_t acceleration = brakingSignal->acceleration;
  ASSERT_THAT(acceleration.value(), Lt(0.0));

  vehicleInfo.absoluteVelocity = units::velocity::kilometers_per_hour_t(120.0);
  const auto sensorDriverSignal1
      = std::make_shared<SensorDriverSignal const>(vehicleInfo, trafficRuleInfo, geometricInfo, surroundingObjects);

  implementation.UpdateInput(0, sensorDriverSignal1, 100);
  implementation.Trigger(100);
  implementation.UpdateOutput(2, outputSignal, 0);

  brakingSignal = std::static_pointer_cast<AccelerationSignal const>(outputSignal);
  acceleration = brakingSignal->acceleration;
  ASSERT_THAT(acceleration.value(), DoubleEq(0.0));
}
