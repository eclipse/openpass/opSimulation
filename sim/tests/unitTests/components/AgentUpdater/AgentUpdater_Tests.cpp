/********************************************************************************
 * Copyright (c) 2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmath>
#include <numeric>

#include "agentUpdaterImpl.h"
#include "common/dynamicsSignal.h"
#include "fakeAgent.h"
#include "fakePublisher.h"

using ::testing::_;
using ::testing::NiceMock;

TEST(AgentUpdater, SetAgentAccelerationVector)
{
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakePublisher> fakePublisher;

  int const fakeCycleTimeMs = 1;
  AgentUpdaterImplementation implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, nullptr, nullptr, &fakePublisher, nullptr, &fakeAgent);

  Common::Vector2d<double, double> agentUpdaterAcceleration;

  ON_CALL(fakeAgent, SetAccelerationVector(_))
      .WillByDefault(
          [&](mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> acceleration)
          {
            agentUpdaterAcceleration.x = acceleration.x.value();
            agentUpdaterAcceleration.y = acceleration.y.value();
          });

  DynamicsInformation fakeDynamicsInformation;

  fakeDynamicsInformation.accelerationX = 2_mps_sq;
  fakeDynamicsInformation.accelerationY = 3_mps_sq;

  const auto fakeDynamicsSignal = std::make_shared<DynamicsSignal const>(
      ComponentState::Acting, fakeDynamicsInformation, "AgentUpdaterTest", "AgentUpdaterTest");

  implementation.UpdateInput(0, fakeDynamicsSignal, 0);
  int const fakeTimeMs = 0;

  implementation.Trigger(fakeTimeMs);

  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.accelerationX.value(), agentUpdaterAcceleration.x);
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.accelerationY.value(), agentUpdaterAcceleration.y);
}

TEST(AgentUpdater, SetAgentVelocityVector)
{
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakePublisher> fakePublisher;

  int const fakeCycleTimeMs = 1;
  AgentUpdaterImplementation implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, nullptr, nullptr, &fakePublisher, nullptr, &fakeAgent);

  Common::Vector2d<double, double> agentUpdaterVelocity;

  ON_CALL(fakeAgent, SetVelocityVector(_))
      .WillByDefault(
          [&](mantle_api::Vec3<units::velocity::meters_per_second_t> velocity)
          {
            agentUpdaterVelocity.x = velocity.x.value();
            agentUpdaterVelocity.y = velocity.y.value();
          });

  DynamicsInformation fakeDynamicsInformation;

  fakeDynamicsInformation.accelerationX = 0.0_mps_sq;
  fakeDynamicsInformation.accelerationY = 0.0_mps_sq;
  fakeDynamicsInformation.velocityX = 2_mps;
  fakeDynamicsInformation.velocityY = 3_mps;

  const auto fakeDynamicsSignal = std::make_shared<DynamicsSignal const>(
      ComponentState::Acting, fakeDynamicsInformation, "AgentUpdaterTest", "AgentUpdaterTest");

  implementation.UpdateInput(0, fakeDynamicsSignal, 0);
  int const fakeTimeMs = 0;

  implementation.Trigger(fakeTimeMs);

  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.velocityX.value(), agentUpdaterVelocity.x);
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.velocityY.value(), agentUpdaterVelocity.y);
}

TEST(AgentUpdater, SetAgentPosition)
{
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakePublisher> fakePublisher;

  int const fakeCycleTimeMs = 1;
  AgentUpdaterImplementation implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, nullptr, nullptr, &fakePublisher, nullptr, &fakeAgent);

  Common::Vector2d<double, double> agentUpdaterPosition;

  ON_CALL(fakeAgent, SetPositionX(_))
      .WillByDefault([&](units::length::meter_t position) { agentUpdaterPosition.x = position.value(); });

  ON_CALL(fakeAgent, SetPositionY(_))
      .WillByDefault([&](units::length::meter_t position) { agentUpdaterPosition.y = position.value(); });

  DynamicsInformation fakeDynamicsInformation;

  fakeDynamicsInformation.accelerationX = 0.0_mps_sq;
  fakeDynamicsInformation.accelerationY = 0.0_mps_sq;
  fakeDynamicsInformation.positionX = 2_m;
  fakeDynamicsInformation.positionY = 3_m;

  const auto fakeDynamicsSignal = std::make_shared<DynamicsSignal const>(
      ComponentState::Acting, fakeDynamicsInformation, "AgentUpdaterTest", "AgentUpdaterTest");

  implementation.UpdateInput(0, fakeDynamicsSignal, 0);
  int const fakeTimeMs = 0;

  implementation.Trigger(fakeTimeMs);

  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.positionX.value(), agentUpdaterPosition.x);
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.positionY.value(), agentUpdaterPosition.y);
}

TEST(AgentUpdater, SetAgentYaw)
{
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakePublisher> fakePublisher;

  int const fakeCycleTimeMs = 1;
  AgentUpdaterImplementation implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, nullptr, nullptr, &fakePublisher, nullptr, &fakeAgent);

  double agentUpdaterYaw = NAN, agentUpdaterYawRate = NAN, agentUpdaterYawAcceleration = NAN;

  ON_CALL(fakeAgent, SetYaw(_)).WillByDefault([&](units::angle::radian_t yaw) { agentUpdaterYaw = yaw.value(); });

  ON_CALL(fakeAgent, SetYawRate(_))
      .WillByDefault([&](units::angular_velocity::radians_per_second_t yawRate)
                     { agentUpdaterYawRate = yawRate.value(); });

  ON_CALL(fakeAgent, SetYawAcceleration(_))
      .WillByDefault([&](units::angular_acceleration::radians_per_second_squared_t yawAcceleration)
                     { agentUpdaterYawAcceleration = yawAcceleration.value(); });

  DynamicsInformation fakeDynamicsInformation;

  fakeDynamicsInformation.accelerationX = 0.0_mps_sq;
  fakeDynamicsInformation.accelerationY = 0.0_mps_sq;
  fakeDynamicsInformation.yaw = 2_rad;
  fakeDynamicsInformation.yawRate = 3_rad_per_s;
  fakeDynamicsInformation.yawAcceleration = 4_rad_per_s_sq;

  const auto fakeDynamicsSignal = std::make_shared<DynamicsSignal const>(
      ComponentState::Acting, fakeDynamicsInformation, "AgentUpdaterTest", "AgentUpdaterTest");

  implementation.UpdateInput(0, fakeDynamicsSignal, 0);
  int const fakeTimeMs = 0;

  implementation.Trigger(fakeTimeMs);

  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.yaw.value(), agentUpdaterYaw);
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.yawRate.value(), agentUpdaterYawRate);
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.yawAcceleration.value(), agentUpdaterYawAcceleration);
}

TEST(AgentUpdater, SetAgentRoll)
{
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakePublisher> fakePublisher;

  int const fakeCycleTimeMs = 1;
  AgentUpdaterImplementation implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, nullptr, nullptr, &fakePublisher, nullptr, &fakeAgent);

  double agentUpdaterRoll = NAN;

  ON_CALL(fakeAgent, SetRoll(_)).WillByDefault([&](units::angle::radian_t roll) { agentUpdaterRoll = roll.value(); });

  DynamicsInformation fakeDynamicsInformation;

  fakeDynamicsInformation.accelerationX = 0.0_mps_sq;
  fakeDynamicsInformation.accelerationY = 0.0_mps_sq;
  fakeDynamicsInformation.roll = 2_rad;

  const auto fakeDynamicsSignal = std::make_shared<DynamicsSignal const>(
      ComponentState::Acting, fakeDynamicsInformation, "AgentUpdaterTest", "AgentUpdaterTest");

  implementation.UpdateInput(0, fakeDynamicsSignal, 0);
  int const fakeTimeMs = 0;

  implementation.Trigger(fakeTimeMs);

  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.roll.value(), agentUpdaterRoll);
}

TEST(AgentUpdater, SetAgentSteeringWheelAngle)
{
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakePublisher> fakePublisher;

  int const fakeCycleTimeMs = 1;
  AgentUpdaterImplementation implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, nullptr, nullptr, &fakePublisher, nullptr, &fakeAgent);

  double agentUpdaterSteeringWheelAngle = NAN;

  ON_CALL(fakeAgent, SetSteeringWheelAngle(_))
      .WillByDefault([&](units::angle::radian_t steeringWheelAngle)
                     { agentUpdaterSteeringWheelAngle = steeringWheelAngle.value(); });

  DynamicsInformation fakeDynamicsInformation;

  fakeDynamicsInformation.accelerationX = 0.0_mps_sq;
  fakeDynamicsInformation.accelerationY = 0.0_mps_sq;
  fakeDynamicsInformation.steeringWheelAngle = 2_rad;

  const auto fakeDynamicsSignal = std::make_shared<DynamicsSignal const>(
      ComponentState::Acting, fakeDynamicsInformation, "AgentUpdaterTest", "AgentUpdaterTest");

  implementation.UpdateInput(0, fakeDynamicsSignal, 0);
  int const fakeTimeMs = 0;

  implementation.Trigger(fakeTimeMs);

  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.steeringWheelAngle.value(), agentUpdaterSteeringWheelAngle);
}

TEST(AgentUpdater, SetAgentTravelDistance)
{
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakePublisher> fakePublisher;
  ;

  int const fakeCycleTimeMs = 1;
  AgentUpdaterImplementation implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, nullptr, nullptr, &fakePublisher, nullptr, &fakeAgent);

  double agentUpdaterTravelDistance = NAN;

  ON_CALL(fakeAgent, SetDistanceTraveled(_))
      .WillByDefault([&](units::length::meter_t travelDistance)
                     { agentUpdaterTravelDistance = travelDistance.value(); });

  DynamicsInformation fakeDynamicsInformation;

  fakeDynamicsInformation.accelerationX = 0.0_mps_sq;
  fakeDynamicsInformation.accelerationY = 0.0_mps_sq;
  fakeDynamicsInformation.travelDistance = 2_m;

  const auto fakeDynamicsSignal = std::make_shared<DynamicsSignal const>(
      ComponentState::Acting, fakeDynamicsInformation, "AgentUpdaterTest", "AgentUpdaterTest");

  implementation.UpdateInput(0, fakeDynamicsSignal, 0);
  int const fakeTimeMs = 0;

  implementation.Trigger(fakeTimeMs);

  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.travelDistance.value(), agentUpdaterTravelDistance);
}

TEST(AgentUpdater, SetAgentWheelData)
{
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakePublisher> fakePublisher;
  ;

  int const fakeCycleTimeMs = 1;
  AgentUpdaterImplementation implementation(
      "", false, 0, 0, 0, fakeCycleTimeMs, nullptr, nullptr, nullptr, &fakePublisher, nullptr, &fakeAgent);

  std::array<double, 4> agentUpdaterWheelRotationRate{};
  std::array<mantle_api::Vec3<units::angle::radian_t>, 4> agentUpdaterWheelOrientation;

  ON_CALL(fakeAgent, SetWheelRotationRate(_, _, _))
      .WillByDefault([&](int axleIndex, int wheelIndex, units::angular_velocity::radians_per_second_t rotationRate)
                     { agentUpdaterWheelRotationRate.at(axleIndex * 2 + wheelIndex) = rotationRate.value(); });

  ON_CALL(fakeAgent, SetWheelOrientation(_, _, _))
      .WillByDefault([&](int axleIndex, int wheelIndex, mantle_api::Vec3<units::angle::radian_t> orientation)
                     { agentUpdaterWheelOrientation.at(axleIndex * 2 + wheelIndex) = orientation; });

  DynamicsInformation fakeDynamicsInformation;

  fakeDynamicsInformation.accelerationX = 0.0_mps_sq;
  fakeDynamicsInformation.accelerationY = 0.0_mps_sq;
  fakeDynamicsInformation.wheelRotationRate = {1_rad_per_s, 2_rad_per_s, 3_rad_per_s, 4_rad_per_s};

  fakeDynamicsInformation.wheelRoll = {1_rad, 2_rad, 3_rad, 4_rad};
  fakeDynamicsInformation.wheelPitch = {1_rad, 2_rad, 3_rad, 4_rad};
  fakeDynamicsInformation.wheelYaw = {1_rad, 2_rad, 3_rad, 4_rad};

  const auto fakeDynamicsSignal = std::make_shared<DynamicsSignal const>(
      ComponentState::Acting, fakeDynamicsInformation, "AgentUpdaterTest", "AgentUpdaterTest");

  implementation.UpdateInput(0, fakeDynamicsSignal, 0);
  int const fakeTimeMs = 0;

  implementation.Trigger(fakeTimeMs);

  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.wheelRotationRate[0].value(),
                   agentUpdaterWheelRotationRate[0]);
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.wheelRotationRate[1].value(),
                   agentUpdaterWheelRotationRate[1]);
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.wheelRotationRate[2].value(),
                   agentUpdaterWheelRotationRate[2]);
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.wheelRotationRate[3].value(),
                   agentUpdaterWheelRotationRate[3]);
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.wheelRoll[0].value(),
                   agentUpdaterWheelOrientation[0].x.value());
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.wheelPitch[0].value(),
                   agentUpdaterWheelOrientation[0].y.value());
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.wheelYaw[0].value(),
                   agentUpdaterWheelOrientation[0].z.value());
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.wheelRoll[1].value(),
                   agentUpdaterWheelOrientation[1].x.value());
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.wheelPitch[1].value(),
                   agentUpdaterWheelOrientation[1].y.value());
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.wheelYaw[1].value(),
                   agentUpdaterWheelOrientation[1].z.value());
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.wheelRoll[2].value(),
                   agentUpdaterWheelOrientation[2].x.value());
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.wheelPitch[2].value(),
                   agentUpdaterWheelOrientation[2].y.value());
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.wheelYaw[2].value(),
                   agentUpdaterWheelOrientation[2].z.value());
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.wheelRoll[3].value(),
                   agentUpdaterWheelOrientation[3].x.value());
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.wheelPitch[3].value(),
                   agentUpdaterWheelOrientation[3].y.value());
  ASSERT_DOUBLE_EQ(fakeDynamicsSignal->dynamicsInformation.wheelYaw[3].value(),
                   agentUpdaterWheelOrientation[3].z.value());
}