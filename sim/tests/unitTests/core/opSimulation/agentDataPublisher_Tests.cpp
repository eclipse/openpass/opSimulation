/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>
#include <variant>
#include <vector>

#include "agentDataPublisher.h"
#include "common/logEntryBase.h"
#include "common/namedType.h"
#include "common/openPassTypes.h"
#include "dontCare.h"
#include "fakeDataBuffer.h"
#include "include/dataBufferInterface.h"

using ::testing::NotNull;

using namespace openpass::databuffer;
using namespace openpass::publisher;
using namespace openpass::type;

TEST(AgentDataPublisher, CallingPublish_ForwardsParametersToDataBuffer)
{
  FakeDataBuffer fakeDataBuffer;

  const EntityId agentId = 1;
  const Key key = "theKey";
  const Value value = 2;

  auto adp = std::make_unique<AgentDataPublisher>(&fakeDataBuffer, agentId);

  EXPECT_CALL(fakeDataBuffer, PutCyclic(agentId, key, value));

  ASSERT_THAT(adp, NotNull());

  adp->Publish(key, value);
}
