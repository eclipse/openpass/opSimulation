/********************************************************************************
 * Copyright (c) 2018-2022 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <algorithm>
#include <functional>
#include <map>
#include <memory>
#include <string>
#include <units.h>
#include <utility>
#include <variant>
#include <vector>

#include "common/openPassTypes.h"
#include "common/vector2d.h"
#include "fakeAgent.h"
#include "fakeDataBuffer.h"
#include "fakeRunResult.h"
#include "fakeWorld.h"
#include "include/dataBufferInterface.h"
#include "observationCyclics.h"
#include "observation_logImplementation.h"

using ::testing::ElementsAre;
using ::testing::Eq;

TEST(ObservationCyclics_Test, GetHeader_ReturnsCorrectHeader)
{
  ObservationCyclics cyclics;
  cyclics.Insert(0, "ParameterA", "123");
  cyclics.Insert(0, "ParameterC", "234");
  cyclics.Insert(100, "ParameterA", "345");
  cyclics.Insert(100, "ParameterB", "456");

  std::string header = cyclics.GetHeader();
  ASSERT_THAT(header, Eq("ParameterA, ParameterB, ParameterC"));
}

TEST(ObservationCyclics_Test, GetTimeSteps_ReturnsCorrectTimesteps)
{
  ObservationCyclics cyclics;
  cyclics.Insert(0, "ParameterA", "123");
  cyclics.Insert(0, "ParameterC", "234");
  cyclics.Insert(100, "ParameterA", "345");
  cyclics.Insert(150, "ParameterB", "456");

  const auto& timesteps = cyclics.GetTimeSteps();
  ASSERT_THAT(timesteps, ElementsAre(0, 100, 150));
}

TEST(ObservationCyclics_Test, GetSamplesLineAllSamplesExisting_ReturnsCorrectLine)
{
  ObservationCyclics cyclics;
  cyclics.Insert(0, "ParameterA", "123");
  cyclics.Insert(0, "ParameterC", "234");
  cyclics.Insert(0, "ParameterB", "345");
  cyclics.Insert(100, "ParameterA", "456");
  cyclics.Insert(100, "ParameterC", "567");
  cyclics.Insert(100, "ParameterB", "678");

  std::string samplesLine = cyclics.GetSamplesLine(0);
  ASSERT_THAT(samplesLine, Eq("123, 345, 234"));
  samplesLine = cyclics.GetSamplesLine(1);
  ASSERT_THAT(samplesLine, Eq("456, 678, 567"));
}

TEST(ObservationCyclics_Test, GetSamplesLineSamplesAddedAfter_ReturnsLineWithEmptyString)
{
  ObservationCyclics cyclics;
  cyclics.Insert(0, "ParameterA", "123");
  cyclics.Insert(100, "ParameterA", "234");
  cyclics.Insert(100, "ParameterC", "345");
  cyclics.Insert(200, "ParameterA", "456");
  cyclics.Insert(200, "ParameterC", "567");
  cyclics.Insert(200, "ParameterB", "678");

  std::string samplesLine = cyclics.GetSamplesLine(0);
  ASSERT_THAT(samplesLine, Eq("123, , "));
  samplesLine = cyclics.GetSamplesLine(1);
  ASSERT_THAT(samplesLine, Eq("234, , 345"));
  samplesLine = cyclics.GetSamplesLine(2);
  ASSERT_THAT(samplesLine, Eq("456, 678, 567"));
}

TEST(ObservationCyclics_Test, GetSamplesLineSamplesMissingInBetween_ReturnsLineWithEmptyString)
{
  ObservationCyclics cyclics;
  cyclics.Insert(0, "ParameterA", "123");
  cyclics.Insert(0, "ParameterC", "234");
  cyclics.Insert(0, "ParameterB", "345");
  cyclics.Insert(100, "ParameterA", "456");
  cyclics.Insert(200, "ParameterA", "567");
  cyclics.Insert(200, "ParameterC", "678");
  cyclics.Insert(200, "ParameterB", "789");

  std::string samplesLine = cyclics.GetSamplesLine(1);
  ASSERT_THAT(samplesLine, Eq("456, , "));
}

TEST(ObservationCyclics_Test, GetSamplesLineSamplesNotUntilEnd_ReturnsLineWithEmptyString)
{
  ObservationCyclics cyclics;
  cyclics.Insert(0, "ParameterA", "123");
  cyclics.Insert(0, "ParameterC", "234");
  cyclics.Insert(0, "ParameterB", "345");
  cyclics.Insert(100, "ParameterA", "456");
  cyclics.Insert(100, "ParameterB", "678");
  cyclics.Insert(200, "ParameterB", "789");

  std::string samplesLine = cyclics.GetSamplesLine(0);
  ASSERT_THAT(samplesLine, Eq("123, 345, 234"));
  samplesLine = cyclics.GetSamplesLine(1);
  ASSERT_THAT(samplesLine, Eq("456, 678, "));
  samplesLine = cyclics.GetSamplesLine(2);
  ASSERT_THAT(samplesLine, Eq(", 789, "));
}
