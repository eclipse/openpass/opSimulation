/********************************************************************************
 * Copyright (c) 2019-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>

#include "commandLineParser.h"

TEST(CommandLineParser, GivenAllValues_ParsesValuesAppropriately)
{
  int argc = 11;
  char* argv[] = {"placeholder_executableName",  // NOLINT(cppcoreguidelines-avoid-c-arrays, modernize-avoid-c-arrays)
                  "--logFile",
                  "testLogFile",
                  "--logLevel",
                  "3",
                  "--lib",
                  "testLibraryPath",
                  "--configs",
                  "testConfigPath",
                  "--results",
                  "testResultPath"};

  auto parsedArguments = CommandLineParser::Parse(
      argc, argv, "some_fake_version");  // NOLINT(cppcoreguidelines-pro-bounds-array-to-pointer-decay)

  EXPECT_THAT(parsedArguments.logFile, "testLogFile");
  EXPECT_THAT(parsedArguments.logLevel, 3);
  EXPECT_THAT(parsedArguments.libPath, "testLibraryPath");
  EXPECT_THAT(parsedArguments.configsPath, "testConfigPath");
  EXPECT_THAT(parsedArguments.resultsPath, "testResultPath");
}
