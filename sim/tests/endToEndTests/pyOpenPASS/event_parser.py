################################################################################
# Copyright (c) 2020-2021 in-tech GmbH
#               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

import pandas as pd
import re

def sanitize(column):
    """Replacing non-alphanumeric characters with underscores and removing extra underscores."""
    return re.sub(r'_+', '_', re.sub(r'[^\w]', '_', column)).strip('_')


def parse_regular(events, event_name):
    if len(events.index) == 0:
        return pd.DataFrame(
            columns=['RunId', 'Timestep', 'Source', 'Name', 'AgentId', 'IsTriggering', 'IsAffected',
                     sanitize(f'Event_{event_name}')]
        )

    # subset all events of specific type
    df = events[events.Name == event_name].copy()

    # store current columns except the one dropped in the next step
    columns = df.columns.drop(['Key', 'Value'])

    # Merge Key/Value columns into a single Column
    df_tmp = pd.pivot_table(
        df, values='Value', index=df.index, columns='Key', aggfunc='first')

    # Concat with original events
    df = pd.concat([df[columns], df_tmp], axis='columns')

    # New columns (= Key) get prefix 'Event_Name_' and are properly sanitized
    new_columns = df.columns.drop(columns)
    mapping = dict()
    for new_column in new_columns:
        mapping.update({new_column: sanitize(f'Event_{new_column}_{event_name}')})
    df.rename(columns=mapping, inplace=True)

    # Add Event_Name to dataframe
    df[sanitize(f'Event_{event_name}')] = True

    return df
