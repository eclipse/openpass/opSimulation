/********************************************************************************
 * Copyright (c) 2017-2021 in-tech GmbH
 *               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#define TESTING
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <MantleAPI/Common/bounding_box.h>
#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <algorithm>
#include <cmath>
#include <cstdint>
#include <filesystem>
#include <map>
#include <memory>
#include <optional>
#include <osi3/osi_common.pb.h>
#include <osi3/osi_groundtruth.pb.h>
#include <osi3/osi_lane.pb.h>
#include <osi3/osi_logicallane.pb.h>
#include <osi3/osi_referenceline.pb.h>
#include <osi3/osi_roadmarking.pb.h>
#include <osi3/osi_trafficlight.pb.h>
#include <osi3/osi_trafficsign.pb.h>
#include <ostream>
#include <string>
#include <type_traits>
#include <units.h>
#include <unordered_map>
#include <utility>
#include <variant>
#include <vector>

#include "DataTypes.h"
#include "WorldData.h"
#include "WorldDataQuery.h"
#include "bindings/world.h"
#include "bindings/worldBinding.h"
#include "common/callbacks.h"
#include "common/globalDefinitions.h"
#include "common/worldDefinitions.h"
#include "core/opSimulation/modules/Stochastics/stochastics_implementation.h"
#include "fakeDataBuffer.h"
#include "framework/idManager.h"
#include "importer/scenery.h"
#include "importer/sceneryImporter.h"
#include "include/agentBlueprintInterface.h"
#include "include/agentInterface.h"
#include "include/egoAgentInterface.h"
#include "include/roadInterface/roadElementTypes.h"
#include "include/roadInterface/roadInterface.h"
#include "include/roadInterface/roadObjectInterface.h"

using ::testing::AllOf;
using ::testing::Contains;
using ::testing::DoubleEq;
using ::testing::DoubleNear;
using ::testing::ElementsAre;
using ::testing::Eq;
using ::testing::Ge;
using ::testing::IsEmpty;
using ::testing::IsTrue;
using ::testing::Le;
using ::testing::Ne;
using ::testing::NiceMock;
using ::testing::SizeIs;
using ::testing::UnorderedElementsAre;

using namespace Configuration;
using namespace Importer;

struct TESTSCENERY_FACTORY
{
  const std::string libraryName = "World_OSI";

  NiceMock<FakeDataBuffer> fakeDataBuffer;
  SimulationCommon::Callbacks callbacks;
  StochasticsImplementation stochastics{&callbacks};
  core::WorldBinding worldBinding;
  core::World world;
  Scenery scenery;
  core::IdManager idManager;

  TESTSCENERY_FACTORY()
      : worldBinding(libraryName, &callbacks, &idManager, &stochastics, &fakeDataBuffer), world(&worldBinding)
  {
  }

  bool instantiate(
      const std::string &sceneryFile /*, const std::vector<TrafficSignalController> &&trafficSignalControllers = {}*/)
  {
    std::filesystem::path sceneryPath = std::filesystem::current_path() / "Resources" / "ImporterTest" / sceneryFile;

    if (!world.Instantiate())
    {
      return false;
    }

    if (!SceneryImporter::Import(sceneryPath.string(), &scenery))
    {
      return false;
    }

    if (!(world.CreateScenery(&scenery, {})))
    {
      return false;
    }

    return true;
  }
};

namespace RelativeWorldView
{
std::ostream &operator<<(std::ostream &os, const Lane &lane)
{
  os << "id: " << lane.relativeId << ", "
     << "direction: " << lane.inDrivingDirection << ","
     << "type: " << static_cast<int>(lane.type) << ","
     << "predecessor: " << lane.predecessor.value_or(-999) << ","
     << "successor: " << lane.successor.value_or(-999);

  return os;
}
}  // namespace RelativeWorldView

//! This enum is used to help checking lane connections as specified in the OpenDRIVE file.
//! Note: It's possible for two connected lanes to be each others predecessor/successor.
enum LaneConnectionType
{
  REGULAR = 0,  //lane a has next lane b, lane b has previous lane a
  NEXT = 1,     //lane a has next lane b, lane b has next lane a
  PREVIOUS = 2  //lane a has previous lane b, lane b has previous lane a
};

//! Helper function to sort all sections for a given road by their length
//! This is used to make checking the correct import of the sceneries easier
//! Note: in all sceneries section lengths in each road are increasing.
std::vector<const OWL::Interfaces::Section *> GetDistanceSortedSectionsForRoad(OWL::Interfaces::WorldData *worldData,
                                                                               const std::string &roadId)
{
  //Extract sections for given roadID
  auto sections = worldData->GetRoads().at(roadId)->GetSections();
  std::vector<const OWL::Interfaces::Section *> queriedSections{sections.cbegin(), sections.cend()};

  //Sort by distance
  std::sort(queriedSections.begin(),
            queriedSections.end(),
            [](auto s1, auto s2) {
              return s1->GetDistance(OWL::MeasurementPoint::RoadStart)
                   < s2->GetDistance(OWL::MeasurementPoint::RoadStart);
            });

  return queriedSections;
}

//! Query lane by id for a given section
const OWL::Interfaces::Lane *GetLaneById(const std::vector<const OWL::Interfaces::Lane *> &sectionLanes, int laneId)
{
  auto queriedLane = std::find_if(sectionLanes.begin(),
                                  sectionLanes.end(),
                                  [laneId](const OWL::Interfaces::Lane *lane) { return lane->GetOdId() == laneId; });

  return *queriedLane;
}

//! Check if lanes are connected according to OpenDRIVE definition.
//! The connection (e.g. predecessor or succesor) can be specified for each lane.
void CheckLaneConnections(const std::vector<const OWL::Interfaces::Lane *> &firstSectionLanes,
                          const std::vector<const OWL::Interfaces::Lane *> &secondSectionLanes,
                          int firstLaneId,
                          int secondLaneId,
                          LaneConnectionType howIsConnection = LaneConnectionType::REGULAR,
                          bool strict = true)
{
  const auto *firstLane = GetLaneById(firstSectionLanes, firstLaneId);
  const auto *secondLane = GetLaneById(secondSectionLanes, secondLaneId);

  if (strict)
  {
    switch (howIsConnection)
    {
      case PREVIOUS:
        ASSERT_THAT(firstLane->GetPrevious(), ElementsAre(secondLane->GetId()));
        ASSERT_THAT(secondLane->GetPrevious(), ElementsAre(firstLane->GetId()));
        break;
      case NEXT:
        ASSERT_THAT(firstLane->GetNext(), ElementsAre(secondLane->GetId()));
        ASSERT_THAT(secondLane->GetNext(), ElementsAre(firstLane->GetId()));
        break;
      default:
        ASSERT_THAT(firstLane->GetNext(), ElementsAre(secondLane->GetId()));
        ASSERT_THAT(secondLane->GetPrevious(), ElementsAre(firstLane->GetId()));
        break;
    }
  }
  else
  {
    switch (howIsConnection)
    {
      case PREVIOUS:
        ASSERT_THAT(firstLane->GetPrevious(), Contains(secondLane->GetId()));
        ASSERT_THAT(secondLane->GetPrevious(), Contains(firstLane->GetId()));
        break;
      case NEXT:
        ASSERT_THAT(firstLane->GetNext(), Contains(secondLane->GetId()));
        ASSERT_THAT(secondLane->GetNext(), Contains(firstLane->GetId()));
        break;
      default:
        ASSERT_THAT(firstLane->GetNext(), Contains(secondLane->GetId()));
        ASSERT_THAT(secondLane->GetPrevious(), Contains(firstLane->GetId()));
        break;
    }
  }
}

//! Test correctly imported scenery
//! Scope is on World-level
TEST(SceneryImporter_IntegrationTests, SingleRoad_ImportWithCorrectLanes)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("IntegrationTestScenery.xodr"), IsTrue());

  auto &world = tsf.world;

  RoadGraph roadGraph;
  RoadGraphVertex root = add_vertex(RouteElement{"1", true}, roadGraph);
  const auto relativeLanes = world.GetRelativeLanes(roadGraph, root, -1, 0.0_m, 150.0_m).at(root);
  ASSERT_EQ(relativeLanes.size(), 5);

  const auto &firstSection = relativeLanes.at(0);
  ASSERT_EQ(firstSection.startS.value(), 0.0);
  ASSERT_EQ(firstSection.endS.value(), 10.0);
  ASSERT_THAT(firstSection.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Driving, std::nullopt, 0}));

  const auto &secondSection = relativeLanes.at(1);
  ASSERT_EQ(secondSection.startS.value(), 10.0);
  ASSERT_EQ(secondSection.endS.value(), 30.0);
  ASSERT_THAT(secondSection.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Driving, 0, 0},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, std::nullopt, -1}));

  const auto &thirdSection = relativeLanes.at(2);
  ASSERT_EQ(thirdSection.startS.value(), 30.0);
  ASSERT_EQ(thirdSection.endS.value(), 60.0);
  ASSERT_THAT(thirdSection.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Driving, 0, 0},
                                   RelativeWorldView::Lane{-1, true, LaneType::Biking, -1, -1},
                                   RelativeWorldView::Lane{-2, true, LaneType::Sidewalk, std::nullopt, -2}));

  const auto &forthSection = relativeLanes.at(3);
  ASSERT_EQ(forthSection.startS.value(), 60.0);
  ASSERT_EQ(forthSection.endS.value(), 100.0);
  ASSERT_THAT(forthSection.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Stop, 0, std::nullopt},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, -1, -1},
                                   RelativeWorldView::Lane{-2, true, LaneType::Driving, -2, -2}));

  const auto &fifthSection = relativeLanes.at(4);
  ASSERT_EQ(fifthSection.startS.value(), 100.0);
  ASSERT_EQ(fifthSection.endS.value(), 150.0);
  ASSERT_THAT(fifthSection.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{-1, true, LaneType::Driving, -1, std::nullopt},
                                   RelativeWorldView::Lane{-2, true, LaneType::Stop, -2, std::nullopt}));

  units::length::meter_t maxSearchDistance = 1000.0_m;
  ASSERT_DOUBLE_EQ(
      world.GetDistanceToEndOfLane(roadGraph, root, -1, 0.0_m, maxSearchDistance, {LaneType::Driving, LaneType::Stop})
          .at(root)
          .value(),
      100.0);
  ASSERT_DOUBLE_EQ(
      world
          .GetDistanceToEndOfLane(roadGraph, root, -2, 15.0_m, maxSearchDistance, {LaneType::Driving, LaneType::Biking})
          .at(root)
          .value(),
      135.0);
  ASSERT_DOUBLE_EQ(
      world
          .GetDistanceToEndOfLane(
              roadGraph, root, -3, 35.0_m, maxSearchDistance, {LaneType::Driving, LaneType::Stop, LaneType::Sidewalk})
          .at(root)
          .value(),
      115.0);

  EXPECT_THAT(world.GetLaneWidth("1", -1, 60.0_m).value(), DoubleNear(3.0, EQUALITY_BOUND));
  EXPECT_THAT(world.GetLaneWidth("1", -2, 35.0_m).value(), DoubleNear(4.0, EQUALITY_BOUND));
  EXPECT_THAT(world.GetLaneWidth("1", -2, 45.0_m).value(), DoubleNear(4.5, EQUALITY_BOUND));
  EXPECT_THAT(world.GetLaneWidth("1", -2, 55.0_m).value(), DoubleNear(5.0, EQUALITY_BOUND));
  EXPECT_THAT(world.GetLaneWidth("1", -3, 60.0_m).value(), DoubleNear(5.0, EQUALITY_BOUND));
}

TEST(SceneryImporter_IntegrationTests, MultipleRoads_ImportWithCorrectLanes)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("MultipleRoadsIntegrationScenery.xodr"), IsTrue());

  auto &world = tsf.world;

  RoadGraph roadGraph;
  RoadGraphVertex node1 = add_vertex(RouteElement{"1", true}, roadGraph);
  RoadGraphVertex node2 = add_vertex(RouteElement{"2", false}, roadGraph);
  RoadGraphVertex node3 = add_vertex(RouteElement{"3", true}, roadGraph);
  add_edge(node1, node2, roadGraph);
  add_edge(node2, node3, roadGraph);

  const auto relativeLanes = world.GetRelativeLanes(roadGraph, node1, -1, 0.0_m, 6000.0_m).at(node3);
  ASSERT_EQ(relativeLanes.size(), 6);

  const auto &firstSection = relativeLanes.at(0);
  ASSERT_EQ(firstSection.startS.value(), 0.0);
  ASSERT_EQ(firstSection.endS.value(), 400.0);
  ASSERT_THAT(firstSection.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Driving, std::nullopt, 0},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, std::nullopt, -1},
                                   RelativeWorldView::Lane{-2, true, LaneType::Driving, std::nullopt, -2}));

  const auto &secondSection = relativeLanes.at(1);
  ASSERT_EQ(secondSection.startS.value(), 400.0);
  ASSERT_EQ(secondSection.endS.value(), 1000.0);
  ASSERT_THAT(secondSection.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Driving, 0, 0},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, -1, -1},
                                   RelativeWorldView::Lane{-2, true, LaneType::Driving, -2, -2}));

  const auto &thirdSection = relativeLanes.at(2);
  ASSERT_EQ(thirdSection.startS.value(), 1000.0);
  ASSERT_EQ(thirdSection.endS.value(), 2100.0);
  ASSERT_THAT(thirdSection.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Driving, 0, 0},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, -1, -1},
                                   RelativeWorldView::Lane{-2, true, LaneType::Driving, -2, -2}));

  const auto &forthSection = relativeLanes.at(3);
  ASSERT_EQ(forthSection.startS.value(), 2100.0);
  ASSERT_EQ(forthSection.endS.value(), 3000.0);
  ASSERT_THAT(forthSection.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Driving, 0, 0},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, -1, -1},
                                   RelativeWorldView::Lane{-2, true, LaneType::Driving, -2, -2}));

  const auto &fifthSection = relativeLanes.at(4);
  ASSERT_EQ(fifthSection.startS.value(), 3000.0);
  ASSERT_EQ(fifthSection.endS.value(), 4400.0);
  ASSERT_THAT(fifthSection.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Driving, 0, 0},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, -1, -1},
                                   RelativeWorldView::Lane{-2, true, LaneType::Driving, -2, -2}));

  const auto &sixthSection = relativeLanes.at(5);
  ASSERT_EQ(sixthSection.startS.value(), 4400.0);
  ASSERT_EQ(sixthSection.endS.value(), 6000.0);
  ASSERT_THAT(sixthSection.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Driving, 0, std::nullopt},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, -1, std::nullopt},
                                   RelativeWorldView::Lane{-2, true, LaneType::Driving, -2, std::nullopt}));

  units::length::meter_t maxSearchLength = 10000.0_m;
  //--------------------------------------------------------RoId, laneId, s, maxsearch
  ASSERT_DOUBLE_EQ(
      world.GetDistanceToEndOfLane(roadGraph, node1, -1, 0.0_m, maxSearchLength, {LaneType::Driving, LaneType::Stop})
          .at(node3)
          .value(),
      6000.0);
  ASSERT_DOUBLE_EQ(
      world.GetDistanceToEndOfLane(roadGraph, node1, -2, 650.0_m, maxSearchLength, {LaneType::Driving, LaneType::Stop})
          .at(node3)
          .value(),
      5350.0);
  ASSERT_DOUBLE_EQ(
      world.GetDistanceToEndOfLane(roadGraph, node2, 2, 0.0_m, maxSearchLength, {LaneType::Driving, LaneType::Stop})
          .at(node3)
          .value(),
      3000.0);
  ASSERT_DOUBLE_EQ(
      world.GetDistanceToEndOfLane(roadGraph, node2, 2, 1500.0_m, maxSearchLength, {LaneType::Driving, LaneType::Stop})
          .at(node3)
          .value(),
      4500.0);
  ASSERT_DOUBLE_EQ(
      world.GetDistanceToEndOfLane(roadGraph, node3, -3, 0.0_m, maxSearchLength, {LaneType::Driving, LaneType::Stop})
          .at(node3)
          .value(),
      3000.0);
  ASSERT_DOUBLE_EQ(
      world.GetDistanceToEndOfLane(roadGraph, node3, -3, 1500.0_m, maxSearchLength, {LaneType::Driving, LaneType::Stop})
          .at(node3)
          .value(),
      1500.0);

  //-----------------------------RoId, laneId, s
  ASSERT_DOUBLE_EQ(world.GetLaneWidth("1", -1, 60.0_m).value(), 3.0);
  ASSERT_DOUBLE_EQ(world.GetLaneWidth("1", -3, 999.9_m).value(), 5.0);
  ASSERT_DOUBLE_EQ(world.GetLaneWidth("2", 1, 0.0_m).value(), 3.0);
  ASSERT_DOUBLE_EQ(world.GetLaneWidth("3", -2, 1500.0_m).value(), 4.0);
}

TEST(SceneryImporter_IntegrationTests, MultipleRoadsWithJunctions_ImportWithCorrectLanes)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("MultipleRoadsWithJunctionIntegrationScenery.xodr"), IsTrue());

  auto &world = tsf.world;

  RoadGraph roadGraph;
  RoadGraphVertex node1 = add_vertex(RouteElement{"1", true}, roadGraph);
  RoadGraphVertex node2 = add_vertex(RouteElement{"2", true}, roadGraph);
  RoadGraphVertex node3 = add_vertex(RouteElement{"3", true}, roadGraph);
  RoadGraphVertex node4 = add_vertex(RouteElement{"4", true}, roadGraph);
  RoadGraphVertex node5 = add_vertex(RouteElement{"5", true}, roadGraph);
  add_edge(node1, node4, roadGraph);
  add_edge(node4, node2, roadGraph);
  add_edge(node1, node5, roadGraph);
  add_edge(node5, node3, roadGraph);

  const auto relativeLanes = world.GetRelativeLanes(roadGraph, node1, -1, 0.0_m, 320.0_m);
  const auto &relativeLanesUp = relativeLanes.at(node2);
  ASSERT_EQ(relativeLanesUp.size(), 3);

  const auto &firstSectionUp = relativeLanesUp.at(0);
  ASSERT_EQ(firstSectionUp.startS.value(), 0.0);
  ASSERT_EQ(firstSectionUp.endS.value(), 100.0);
  ASSERT_THAT(firstSectionUp.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Driving, std::nullopt, 0},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, std::nullopt, -1},
                                   RelativeWorldView::Lane{-2, true, LaneType::Driving, std::nullopt, std::nullopt},
                                   RelativeWorldView::Lane{-3, true, LaneType::Driving, std::nullopt, std::nullopt}));

  const auto &secondSectionUp = relativeLanesUp.at(1);
  ASSERT_EQ(secondSectionUp.startS.value(), 100.0);
  ASSERT_EQ(secondSectionUp.endS.value(), 120.0);
  ASSERT_THAT(secondSectionUp.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Driving, 0, 0},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, -1, -1}));

  const auto &thirdSectionUp = relativeLanesUp.at(2);
  ASSERT_EQ(thirdSectionUp.startS.value(), 120.0);
  ASSERT_EQ(thirdSectionUp.endS.value(), 320.0);
  ASSERT_THAT(thirdSectionUp.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Driving, 0, std::nullopt},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, -1, std::nullopt}));

  const auto &relativeLanesDown = relativeLanes.at(node3);
  ASSERT_EQ(relativeLanesDown.size(), 3);

  const auto &firstSectionDown = relativeLanesDown.at(0);
  ASSERT_EQ(firstSectionDown.startS.value(), 0.0);
  ASSERT_EQ(firstSectionDown.endS.value(), 100.0);
  ASSERT_THAT(firstSectionDown.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Driving, std::nullopt, std::nullopt},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, std::nullopt, std::nullopt},
                                   RelativeWorldView::Lane{-2, true, LaneType::Driving, std::nullopt, -1},
                                   RelativeWorldView::Lane{-3, true, LaneType::Driving, std::nullopt, -2}));

  const auto &secondSectionDown = relativeLanesDown.at(1);
  ASSERT_EQ(secondSectionDown.startS.value(), 100.0);
  ASSERT_EQ(secondSectionDown.endS.value(), 120.0);
  ASSERT_THAT(secondSectionDown.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{-1, true, LaneType::Driving, -2, -1},
                                   RelativeWorldView::Lane{-2, true, LaneType::Driving, -3, -2}));

  const auto &thirdSectionDown = relativeLanesDown.at(2);
  ASSERT_EQ(thirdSectionDown.startS.value(), 120.0);
  ASSERT_EQ(thirdSectionDown.endS.value(), 420.0);
  ASSERT_THAT(thirdSectionDown.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{-1, true, LaneType::Driving, -1, std::nullopt},
                                   RelativeWorldView::Lane{-2, true, LaneType::Driving, -2, std::nullopt}));

  units::length::meter_t maxSearchLength = 1000.0_m;
  //--------------------------------------------------------RoId, laneId, s, maxsearch
  ASSERT_DOUBLE_EQ(
      world.GetDistanceToEndOfLane(roadGraph, node1, -1, 0.0_m, maxSearchLength, {LaneType::Driving, LaneType::Stop})
          .at(node2)
          .value(),
      320.0);
  ASSERT_DOUBLE_EQ(
      world.GetDistanceToEndOfLane(roadGraph, node1, -2, 90.0_m, maxSearchLength, {LaneType::Driving, LaneType::Stop})
          .at(node2)
          .value(),
      230.0);
  ASSERT_DOUBLE_EQ(
      world.GetDistanceToEndOfLane(roadGraph, node1, -3, 10.0_m, maxSearchLength, {LaneType::Driving, LaneType::Stop})
          .at(node3)
          .value(),
      410.0);
  ASSERT_DOUBLE_EQ(
      world.GetDistanceToEndOfLane(roadGraph, node1, -4, 0.0_m, maxSearchLength, {LaneType::Driving, LaneType::Stop})
          .at(node3)
          .value(),
      420.0);
  ASSERT_DOUBLE_EQ(
      world.GetDistanceToEndOfLane(roadGraph, node2, -1, 0.0_m, maxSearchLength, {LaneType::Driving, LaneType::Stop})
          .at(node2)
          .value(),
      200.0);
  ASSERT_DOUBLE_EQ(
      world.GetDistanceToEndOfLane(roadGraph, node2, -2, 150.0_m, maxSearchLength, {LaneType::Driving, LaneType::Stop})
          .at(node2)
          .value(),
      50.0);
  ASSERT_DOUBLE_EQ(
      world.GetDistanceToEndOfLane(roadGraph, node3, -1, 0.0_m, maxSearchLength, {LaneType::Driving, LaneType::Stop})
          .at(node3)
          .value(),
      300.0);
  ASSERT_DOUBLE_EQ(
      world.GetDistanceToEndOfLane(roadGraph, node4, -1, 0.0_m, maxSearchLength, {LaneType::Driving, LaneType::Stop})
          .at(node2)
          .value(),
      220.0);
  ASSERT_DOUBLE_EQ(
      world.GetDistanceToEndOfLane(roadGraph, node5, -2, 18.0_m, maxSearchLength, {LaneType::Driving, LaneType::Stop})
          .at(node3)
          .value(),
      302.0);

  //-----------------------------RoId, laneId, s
  ASSERT_DOUBLE_EQ(world.GetLaneWidth("1", -1, 60.0_m).value(), 3.0);
  ASSERT_DOUBLE_EQ(world.GetLaneWidth("1", -2, 95.0_m).value(), 4.0);
  ASSERT_DOUBLE_EQ(world.GetLaneWidth("1", -3, 99.0_m).value(), 5.0);
  ASSERT_DOUBLE_EQ(world.GetLaneWidth("1", -4, 0.0_m).value(), 6.0);
  ASSERT_DOUBLE_EQ(world.GetLaneWidth("2", -1, 1.0_m).value(), 3.0);
  ASSERT_DOUBLE_EQ(world.GetLaneWidth("2", -2, 20.0_m).value(), 4.0);
  ASSERT_DOUBLE_EQ(world.GetLaneWidth("3", -1, 123.0_m).value(), 5.0);
  ASSERT_DOUBLE_EQ(world.GetLaneWidth("3", -2, 200.0_m).value(), 6.0);
  ASSERT_DOUBLE_EQ(world.GetLaneWidth("4", -1, 15.0_m).value(), 3.0);
  ASSERT_DOUBLE_EQ(world.GetLaneWidth("4", -2, 15.0_m).value(), 4.0);
  ASSERT_DOUBLE_EQ(world.GetLaneWidth("5", -1, 0.0_m).value(), 5.0);
  ASSERT_DOUBLE_EQ(world.GetLaneWidth("5", -2, 15.0_m).value(), 6.0);
}

TEST(SceneryImporter_IntegrationTests, TJunction_ImportWithCorrectLanes)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("TJunctionBig.xodr"), IsTrue());

  auto &world = tsf.world;

  RoadGraph roadGraph;
  RoadGraphVertex node1 = add_vertex(RouteElement{"R1", false}, roadGraph);
  RoadGraphVertex node2 = add_vertex(RouteElement{"R2", true}, roadGraph);
  RoadGraphVertex node3 = add_vertex(RouteElement{"R3", false}, roadGraph);
  RoadGraphVertex node2_1 = add_vertex(RouteElement{"R2-1", false}, roadGraph);
  RoadGraphVertex node2_3 = add_vertex(RouteElement{"R2-3", true}, roadGraph);
  add_edge(node2, node2_1, roadGraph);
  add_edge(node2_1, node1, roadGraph);
  add_edge(node2, node2_3, roadGraph);
  add_edge(node2_3, node3, roadGraph);

  const auto relativeLanes = world.GetRelativeLanes(roadGraph, node2, -1, 0.0_m, 320.0_m);
  const auto &relativeLanesLeft = relativeLanes.at(node1);
  ASSERT_THAT(relativeLanesLeft, SizeIs(4));

  const auto &firstSectionLeft = relativeLanesLeft.at(0);
  ASSERT_THAT(firstSectionLeft.startS.value(), DoubleEq(0.0));
  ASSERT_THAT(firstSectionLeft.endS.value(), DoubleEq(200.0));
  ASSERT_THAT(firstSectionLeft.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{2, false, LaneType::Driving, std::nullopt, std::nullopt},
                                   RelativeWorldView::Lane{1, false, LaneType::Driving, std::nullopt, std::nullopt},
                                   RelativeWorldView::Lane{0, true, LaneType::Driving, std::nullopt, 0},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, std::nullopt, -1}));

  const auto &secondSectionLeft = relativeLanesLeft.at(1);
  ASSERT_THAT(secondSectionLeft.startS.value(), DoubleEq(200.0));
  ASSERT_THAT(secondSectionLeft.endS.value(), DoubleEq(205.708));
  ASSERT_THAT(secondSectionLeft.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Driving, 0, 0},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, -1, -1}));

  const auto &thirdSectionLeft = relativeLanesLeft.at(2);
  ASSERT_THAT(thirdSectionLeft.startS.value(), DoubleEq(205.708));
  ASSERT_THAT(thirdSectionLeft.endS.value(), DoubleEq(215.708));
  ASSERT_THAT(thirdSectionLeft.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Driving, 0, 0},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, -1, -1}));

  const auto &forthSectionLeft = relativeLanesLeft.at(3);
  ASSERT_THAT(forthSectionLeft.startS.value(), DoubleEq(215.708));
  ASSERT_THAT(forthSectionLeft.endS.value(), DoubleEq(415.708));
  ASSERT_THAT(forthSectionLeft.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{2, false, LaneType::Driving, std::nullopt, std::nullopt},
                                   RelativeWorldView::Lane{1, false, LaneType::Driving, std::nullopt, std::nullopt},
                                   RelativeWorldView::Lane{0, true, LaneType::Driving, 0, std::nullopt},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, -1, std::nullopt}));

  const auto &relativeLanesRight = relativeLanes.at(node3);
  ASSERT_THAT(relativeLanesRight, SizeIs(4));

  const auto &firstSectionRight = relativeLanesRight.at(0);
  ASSERT_THAT(firstSectionRight.startS.value(), DoubleEq(0.0));
  ASSERT_THAT(firstSectionRight.endS.value(), DoubleEq(200.0));
  ASSERT_THAT(firstSectionRight.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{2, false, LaneType::Driving, std::nullopt, std::nullopt},
                                   RelativeWorldView::Lane{1, false, LaneType::Driving, std::nullopt, std::nullopt},
                                   RelativeWorldView::Lane{0, true, LaneType::Driving, std::nullopt, 0},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, std::nullopt, -1}));

  const auto &secondSectionRight = relativeLanesRight.at(1);
  ASSERT_THAT(secondSectionRight.startS.value(), DoubleEq(200.0));
  ASSERT_THAT(secondSectionRight.endS.value(), DoubleEq(210.0));
  ASSERT_THAT(secondSectionRight.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Driving, 0, 0},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, -1, -1}));

  const auto &thirdSectionRight = relativeLanesRight.at(2);
  ASSERT_THAT(thirdSectionRight.startS.value(), DoubleEq(210.0));
  ASSERT_THAT(thirdSectionRight.endS.value(), DoubleEq(215.708));
  ASSERT_THAT(thirdSectionRight.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{0, true, LaneType::Driving, 0, 0},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, -1, -1}));

  const auto &forthSectionRight = relativeLanesRight.at(3);
  ASSERT_THAT(forthSectionRight.startS.value(), DoubleEq(215.708));
  ASSERT_THAT(forthSectionRight.endS.value(), DoubleEq(415.708));
  ASSERT_THAT(forthSectionRight.lanes,
              UnorderedElementsAre(RelativeWorldView::Lane{2, false, LaneType::Driving, std::nullopt, std::nullopt},
                                   RelativeWorldView::Lane{1, false, LaneType::Driving, std::nullopt, std::nullopt},
                                   RelativeWorldView::Lane{0, true, LaneType::Driving, 0, std::nullopt},
                                   RelativeWorldView::Lane{-1, true, LaneType::Driving, -1, std::nullopt}));
}

//! Test correct lane predeccessor and successors
//! Scope is on WorldData and OWL-Level
TEST(SceneryImporter_IntegrationTests, SingleRoad_CheckForCorrectLaneConnections)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("IntegrationTestScenery.xodr"), IsTrue());

  auto &world = tsf.world;

  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());

  ASSERT_EQ(worldData->GetRoads().at("1")->GetSections().size(), 5);

  auto sections = GetDistanceSortedSectionsForRoad(worldData, "1");

  const std::vector<int> numberOfLanesPerSection = {1, 2, 3, 3, 2};
  const std::vector<std::vector<int>> laneConnections = {{-1}, {-1, -2}, {-1, -2, -3}, {0, -1, -2}};

  for (unsigned count = 0; count < 4; count++)
  {
    const auto *firstSection = sections.at(count);
    auto firstSectionLanes = firstSection->GetLanes();
    const auto *secondSection = sections.at(count + 1);
    auto secondSectionLanes = secondSection->GetLanes();

    ASSERT_EQ(firstSectionLanes.size(), numberOfLanesPerSection[count]);

    for (int laneNumber = 0; laneNumber < numberOfLanesPerSection[count]; laneNumber++)
    {
      int secondLaneId = laneConnections.at(count).at(static_cast<unsigned>(laneNumber));
      if (secondLaneId != 0)
      {
        CheckLaneConnections(firstSectionLanes, secondSectionLanes, -laneNumber - 1, secondLaneId);
      }
    }
  }
}

TEST(SceneryImporter_IntegrationTests, MultipleRoads_CheckForCorrectLaneConnections)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("MultipleRoadsIntegrationScenery.xodr"), IsTrue());

  auto &world = tsf.world;

  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());

  ASSERT_EQ(worldData->GetRoads().at("1")->GetSections().size(), 2);
  ASSERT_EQ(worldData->GetRoads().at("2")->GetSections().size(), 2);
  ASSERT_EQ(worldData->GetRoads().at("3")->GetSections().size(), 2);

  auto sectionsRoad1 = GetDistanceSortedSectionsForRoad(worldData, "1");
  const auto &lanesRoad1Section1 = sectionsRoad1.front()->GetLanes();
  const auto &lanesRoad1Section2 = sectionsRoad1.back()->GetLanes();
  auto sectionsRoad2 = GetDistanceSortedSectionsForRoad(worldData, "2");
  const auto &lanesRoad2Section1 = sectionsRoad2.front()->GetLanes();
  const auto &lanesRoad2Section2 = sectionsRoad2.back()->GetLanes();
  auto sectionsRoad3 = GetDistanceSortedSectionsForRoad(worldData, "3");
  const auto &lanesRoad3Section1 = sectionsRoad3.front()->GetLanes();
  const auto &lanesRoad3Section2 = sectionsRoad3.back()->GetLanes();

  //check connections inside road
  CheckLaneConnections(lanesRoad1Section1, lanesRoad1Section2, -1, -1);
  CheckLaneConnections(lanesRoad1Section1, lanesRoad1Section2, -2, -2);
  CheckLaneConnections(lanesRoad1Section1, lanesRoad1Section2, -3, -3);

  CheckLaneConnections(lanesRoad2Section1, lanesRoad2Section2, 1, 1);
  CheckLaneConnections(lanesRoad2Section1, lanesRoad2Section2, 2, 2);
  CheckLaneConnections(lanesRoad2Section1, lanesRoad2Section2, 3, 3);

  CheckLaneConnections(lanesRoad3Section1, lanesRoad3Section2, -1, -1);
  CheckLaneConnections(lanesRoad3Section1, lanesRoad3Section2, -2, -2);
  CheckLaneConnections(lanesRoad3Section1, lanesRoad3Section2, -3, -3);

  //check connections between roads
  CheckLaneConnections(lanesRoad1Section2, lanesRoad2Section2, -1, 1, LaneConnectionType::NEXT);
  CheckLaneConnections(lanesRoad1Section2, lanesRoad2Section2, -2, 2, LaneConnectionType::NEXT);
  CheckLaneConnections(lanesRoad1Section2, lanesRoad2Section2, -3, 3, LaneConnectionType::NEXT);

  CheckLaneConnections(lanesRoad2Section1, lanesRoad3Section1, 1, -1, LaneConnectionType::PREVIOUS);
  CheckLaneConnections(lanesRoad2Section1, lanesRoad3Section1, 2, -2, LaneConnectionType::PREVIOUS);
  CheckLaneConnections(lanesRoad2Section1, lanesRoad3Section1, 3, -3, LaneConnectionType::PREVIOUS);
}

TEST(SceneryImporter_IntegrationTests, MultipleRoadsWithJunctions_CheckForCorrectLaneConnections)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("MultipleRoadsWithJunctionIntegrationScenery.xodr"), IsTrue());

  auto &world = tsf.world;

  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());

  ASSERT_EQ(worldData->GetRoads().at("1")->GetSections().size(), 1);
  ASSERT_EQ(worldData->GetRoads().at("2")->GetSections().size(), 1);
  ASSERT_EQ(worldData->GetRoads().at("3")->GetSections().size(), 1);
  ASSERT_EQ(worldData->GetRoads().at("4")->GetSections().size(), 1);
  ASSERT_EQ(worldData->GetRoads().at("5")->GetSections().size(), 1);

  const auto &lanesIncomingRoad = GetDistanceSortedSectionsForRoad(worldData, "1").back()->GetLanes();
  const auto &lanesUpperOutgoingRoad = GetDistanceSortedSectionsForRoad(worldData, "2").back()->GetLanes();
  const auto &lanesLowerOutgoingRoad = GetDistanceSortedSectionsForRoad(worldData, "3").back()->GetLanes();
  const auto &lanesUpperConnectingRoad = GetDistanceSortedSectionsForRoad(worldData, "4").back()->GetLanes();
  const auto &lanesLowerConnectingRoad = GetDistanceSortedSectionsForRoad(worldData, "5").back()->GetLanes();

  //check connections between incoming road and connecting roads
  CheckLaneConnections(lanesIncomingRoad, lanesUpperConnectingRoad, -1, -1);
  CheckLaneConnections(lanesIncomingRoad, lanesUpperConnectingRoad, -2, -2);

  CheckLaneConnections(lanesIncomingRoad, lanesLowerConnectingRoad, -3, -1);
  CheckLaneConnections(lanesIncomingRoad, lanesLowerConnectingRoad, -4, -2);

  //check connections between connecting roads and outgoing roads
  CheckLaneConnections(lanesUpperConnectingRoad, lanesUpperOutgoingRoad, -1, -1);
  CheckLaneConnections(lanesUpperConnectingRoad, lanesUpperOutgoingRoad, -2, -2);

  CheckLaneConnections(lanesLowerConnectingRoad, lanesLowerOutgoingRoad, -1, -1);
  CheckLaneConnections(lanesLowerConnectingRoad, lanesLowerOutgoingRoad, -2, -2);
}

TEST(SceneryImporter_IntegrationTests, TJunction_CheckForCorrectLaneConnections)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("TJunctionBig.xodr"), IsTrue());

  auto &world = tsf.world;

  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());

  ASSERT_EQ(worldData->GetRoads().at("R1")->GetSections().size(), 1);
  ASSERT_EQ(worldData->GetRoads().at("R2")->GetSections().size(), 1);
  ASSERT_EQ(worldData->GetRoads().at("R3")->GetSections().size(), 1);
  ASSERT_EQ(worldData->GetRoads().at("R2-1")->GetSections().size(), 2);
  ASSERT_EQ(worldData->GetRoads().at("R2-3")->GetSections().size(), 2);

  const auto &lanesRoad1 = GetDistanceSortedSectionsForRoad(worldData, "R1").back()->GetLanes();
  const auto &lanesRoad2 = GetDistanceSortedSectionsForRoad(worldData, "R2").back()->GetLanes();
  const auto &lanesRoad3 = GetDistanceSortedSectionsForRoad(worldData, "R3").back()->GetLanes();
  const auto &lanesRoad2_1first = GetDistanceSortedSectionsForRoad(worldData, "R2-1").front()->GetLanes();
  const auto &lanesRoad2_1second = GetDistanceSortedSectionsForRoad(worldData, "R2-1").back()->GetLanes();
  const auto &lanesRoad2_3first = GetDistanceSortedSectionsForRoad(worldData, "R2-3").front()->GetLanes();
  const auto &lanesRoad2_3second = GetDistanceSortedSectionsForRoad(worldData, "R2-3").back()->GetLanes();

  //check connections between incoming road and connecting roads
  CheckLaneConnections(lanesRoad2, lanesRoad2_1second, -1, 1, LaneConnectionType::NEXT, false);
  CheckLaneConnections(lanesRoad2, lanesRoad2_1second, -2, 2, LaneConnectionType::NEXT, false);

  CheckLaneConnections(lanesRoad2, lanesRoad2_3first, -1, -1, LaneConnectionType::REGULAR, false);
  CheckLaneConnections(lanesRoad2, lanesRoad2_3first, -2, -2, LaneConnectionType::REGULAR, false);

  //check connections between connecting roads and outgoing roads
  CheckLaneConnections(lanesRoad1, lanesRoad2_1first, 1, 1, LaneConnectionType::REGULAR, false);
  CheckLaneConnections(lanesRoad1, lanesRoad2_1first, 2, 2, LaneConnectionType::REGULAR, false);

  CheckLaneConnections(lanesRoad2_3second, lanesRoad3, -1, 1, LaneConnectionType::NEXT, false);
  CheckLaneConnections(lanesRoad2_3second, lanesRoad3, -2, 2, LaneConnectionType::NEXT, false);
}

void CheckLaneNeighbours(const std::vector<const OWL::Interfaces::Lane *> &lanes, int leftLaneId, int rightLaneId)
{
  const auto *leftLane = GetLaneById(lanes, leftLaneId);
  const auto *rightLane = GetLaneById(lanes, rightLaneId);

  EXPECT_THAT(&leftLane->GetRightLane(), Eq(rightLane));
  EXPECT_THAT(&rightLane->GetLeftLane(), Eq(leftLane));
}

TEST(SceneryImporter_IntegrationTests, TJunction_CheckForCorrectLaneNeighbours)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("TJunctionBig.xodr"), IsTrue());

  auto &world = tsf.world;

  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());

  const auto &lanesRoad2 = GetDistanceSortedSectionsForRoad(worldData, "R2").back()->GetLanes();
  const auto &lanesRoad2_1 = GetDistanceSortedSectionsForRoad(worldData, "R2-1").back()->GetLanes();
  const auto &lanesRoad2_3 = GetDistanceSortedSectionsForRoad(worldData, "R2-3").back()->GetLanes();

  CheckLaneNeighbours(lanesRoad2, 2, 1);
  CheckLaneNeighbours(lanesRoad2, 1, -1);
  CheckLaneNeighbours(lanesRoad2, -1, -2);

  CheckLaneNeighbours(lanesRoad2_1, 2, 1);

  CheckLaneNeighbours(lanesRoad2_3, -1, -2);
}
TEST(SceneryImporter_IntegrationTests, TJunction_ImportWithCorrectLaneMarkings)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("TJunctionBig.xodr"), IsTrue());

  auto &world = tsf.world;

  RoadGraph roadGraph;
  RoadGraphVertex node1 = add_vertex(RouteElement{"R1", true}, roadGraph);
  RoadGraphVertex node2 = add_vertex(RouteElement{"R2", false}, roadGraph);
  RoadGraphVertex node1_2 = add_vertex(RouteElement{"R1-2", true}, roadGraph);
  add_edge(node1, node1_2, roadGraph);
  add_edge(node1_2, node2, roadGraph);

  auto laneMarkings = world.GetLaneMarkings(roadGraph, node1, 2, 0.0_m, 400.0_m, Side::Left).at(node2);

  ASSERT_THAT(laneMarkings, SizeIs(1));
  ASSERT_THAT(laneMarkings.at(0).relativeStartDistance.value(), DoubleEq(0.0));
  ASSERT_THAT(laneMarkings.at(0).type, Eq(LaneMarking::Type::Solid));
  ASSERT_THAT(laneMarkings.at(0).width.value(), DoubleEq(0.15));
  ASSERT_THAT(laneMarkings.at(0).color, Eq(LaneMarking::Color::White));

  laneMarkings = world.GetLaneMarkings(roadGraph, node1, 2, 0.0_m, 400.0_m, Side::Right).at(node2);

  ASSERT_THAT(laneMarkings, SizeIs(1));
  ASSERT_THAT(laneMarkings.at(0).relativeStartDistance.value(), DoubleEq(0.0));
  ASSERT_THAT(laneMarkings.at(0).type, Eq(LaneMarking::Type::Broken));
  ASSERT_THAT(laneMarkings.at(0).width.value(), DoubleEq(0.15));
  ASSERT_THAT(laneMarkings.at(0).color, Eq(LaneMarking::Color::White));

  laneMarkings = world.GetLaneMarkings(roadGraph, node1, 1, 0.0_m, 400.0_m, Side::Left).at(node2);

  ASSERT_THAT(laneMarkings, SizeIs(1));
  ASSERT_THAT(laneMarkings.at(0).relativeStartDistance.value(), DoubleEq(0.0));
  ASSERT_THAT(laneMarkings.at(0).type, Eq(LaneMarking::Type::Broken));
  ASSERT_THAT(laneMarkings.at(0).width.value(), DoubleEq(0.15));
  ASSERT_THAT(laneMarkings.at(0).color, Eq(LaneMarking::Color::White));

  laneMarkings = world.GetLaneMarkings(roadGraph, node1, 1, 0.0_m, 400.0_m, Side::Right).at(node2);

  ASSERT_THAT(laneMarkings, SizeIs(1));
  ASSERT_THAT(laneMarkings.at(0).relativeStartDistance.value(), DoubleEq(0.0));
  ASSERT_THAT(laneMarkings.at(0).type, Eq(LaneMarking::Type::Solid));
  ASSERT_THAT(laneMarkings.at(0).width.value(), DoubleEq(0.15));
  ASSERT_THAT(laneMarkings.at(0).color, Eq(LaneMarking::Color::White));

  laneMarkings = world.GetLaneMarkings(roadGraph, node1, -1, 0.0_m, 400.0_m, Side::Left).at(node2);
  ASSERT_THAT(laneMarkings, SizeIs(3));
  ASSERT_THAT(laneMarkings.at(0).relativeStartDistance.value(), DoubleEq(0.0));
  ASSERT_THAT(laneMarkings.at(0).type, Eq(LaneMarking::Type::Solid));
  ASSERT_THAT(laneMarkings.at(0).width.value(), DoubleEq(0.15));
  ASSERT_THAT(laneMarkings.at(0).color, Eq(LaneMarking::Color::White));
  ASSERT_THAT(laneMarkings.at(1).relativeStartDistance.value(), DoubleEq(200.0));
  ASSERT_THAT(laneMarkings.at(1).type, Eq(LaneMarking::Type::None));
  ASSERT_THAT(laneMarkings.at(2).relativeStartDistance.value(), DoubleEq(215.708));
  ASSERT_THAT(laneMarkings.at(2).type, Eq(LaneMarking::Type::Solid));
  ASSERT_THAT(laneMarkings.at(2).width.value(), DoubleEq(0.15));
  ASSERT_THAT(laneMarkings.at(2).color, Eq(LaneMarking::Color::White));
}

TEST(SceneryImporter_IntegrationTests, SingleRoad_ImportWithCorrectObjects)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("IntegrationTestScenery.xodr"), IsTrue());

  auto &scenery = tsf.scenery;
  const auto &roadObjects = scenery.GetRoad("1")->GetRoadObjects();
  ASSERT_THAT(roadObjects, SizeIs(2));
  ASSERT_THAT(roadObjects.at(0)->GetType(), Eq(RoadObjectType::crosswalk));
  ASSERT_THAT(roadObjects.at(0)->GetWidth().value(), DoubleEq(8.0));
  ASSERT_THAT(roadObjects.at(0)->GetLength().value(), DoubleEq(4.0));
  ASSERT_THAT(roadObjects.at(1)->GetType(), Eq(RoadObjectType::tree));
  ASSERT_THAT(roadObjects.at(1)->GetWidth().value(), DoubleEq(1.0));
  ASSERT_THAT(roadObjects.at(1)->GetLength().value(), DoubleEq(1.0));
  ASSERT_THAT(roadObjects.at(1)->GetHeight().value(), DoubleEq(2.0));
}

//!Workaround, because the OSI lane is a private member
osi3::Lane GetOsiLane(const OWL::Interfaces::Lane *lane)
{
  osi3::GroundTruth groundTruth;
  lane->CopyToGroundTruth(groundTruth);
  return groundTruth.lane(0);
}

//!Workaround, because the OSI Logical lane is a private member
osi3::LogicalLane GetOsiLogicalLane(const OWL::Interfaces::Lane *lane)
{
  osi3::GroundTruth groundTruth;
  lane->CopyToGroundTruth(groundTruth);
  return groundTruth.logical_lane(0);
}

void CheckLanePairings(const OWL::Interfaces::Lane *lane,
                       const std::vector<std::pair<OWL::Id, OWL::Id>> &expectedPairings)
{
  auto osiLane = GetOsiLane(lane);
  EXPECT_THAT(osiLane.classification().lane_pairing_size(), Eq(expectedPairings.size()));
  for (const auto &lanePairing : osiLane.classification().lane_pairing())
  {
    EXPECT_THAT(expectedPairings,
                Contains(std::make_pair(OWL::Id{lanePairing.antecessor_lane_id().value()},
                                        OWL::Id{lanePairing.successor_lane_id().value()})));
  }
}

void CheckLaneNeighbours(const std::vector<const OWL::Interfaces::Lane *> &lanes)
{
  auto nrOfLanes = static_cast<int>(lanes.size());
  for (int laneId = -1; -laneId < nrOfLanes; --laneId)
  {
    const auto *firstLane = GetLaneById(lanes, laneId);
    const auto *secondLane = GetLaneById(lanes, laneId - 1);
    auto firstLaneId = firstLane->GetId();
    auto secondLaneId = secondLane->GetId();
    EXPECT_THAT(GetOsiLane(firstLane).classification().right_adjacent_lane_id(0).value(), secondLaneId);
    EXPECT_THAT(GetOsiLane(secondLane).classification().left_adjacent_lane_id(0).value(), firstLaneId);
  }
}

void CheckLogicalLaneNeighbours(const std::vector<const OWL::Interfaces::Lane *> &lanes)
{
  auto nrOfLanes = static_cast<int>(lanes.size());
  for (int laneId = -1; -laneId < nrOfLanes; --laneId)
  {
    const auto *firstLane = GetLaneById(lanes, laneId);
    const auto *secondLane = GetLaneById(lanes, laneId - 1);
    auto firstLaneId = firstLane->GetLogicalLaneId();
    auto secondLaneId = secondLane->GetLogicalLaneId();
    auto sStart = firstLane->GetDistance(OWL::MeasurementPoint::RoadStart);
    auto sEnd = firstLane->GetDistance(OWL::MeasurementPoint::RoadEnd);
    EXPECT_THAT(GetOsiLogicalLane(firstLane).right_adjacent_lane(0).other_lane_id().value(), secondLaneId);
    EXPECT_THAT(GetOsiLogicalLane(firstLane).right_adjacent_lane(0).start_s(), sStart.value());
    EXPECT_THAT(GetOsiLogicalLane(firstLane).right_adjacent_lane(0).start_s_other(), sStart.value());
    EXPECT_THAT(GetOsiLogicalLane(firstLane).right_adjacent_lane(0).end_s(), sEnd.value());
    EXPECT_THAT(GetOsiLogicalLane(firstLane).right_adjacent_lane(0).end_s_other(), sEnd.value());
    EXPECT_THAT(GetOsiLogicalLane(secondLane).left_adjacent_lane(0).other_lane_id().value(), firstLaneId);
    EXPECT_THAT(GetOsiLogicalLane(secondLane).left_adjacent_lane(0).start_s(), sStart.value());
    EXPECT_THAT(GetOsiLogicalLane(secondLane).left_adjacent_lane(0).start_s_other(), sStart.value());
    EXPECT_THAT(GetOsiLogicalLane(secondLane).left_adjacent_lane(0).end_s(), sEnd.value());
    EXPECT_THAT(GetOsiLogicalLane(secondLane).left_adjacent_lane(0).end_s_other(), sEnd.value());
  }
}

void CheckLaneType(const std::vector<const OWL::Interfaces::Lane *> &lanes,
                   const std::vector<osi3::Lane_Classification_Type> &expectedTypes)
{
  // Only negative lanes are checked and lane "0" is only a placeholder without internal representation.
  // Calling GetLaneById with 0 would fail so start at 1.
  // Yet, we expect expectedTypes to carry information about the 0th lane
  for (auto i = 1; i < lanes.size(); ++i)
  {
    int laneId = -i;
    const auto *lane = GetLaneById(lanes, laneId);
    EXPECT_THAT(GetOsiLane(lane).classification().type(), expectedTypes.at(i));
  }
}

void CheckLaneSubtype(const std::vector<const OWL::Interfaces::Lane *> &lanes,
                      const std::vector<osi3::Lane_Classification_Subtype> &expectedTypes)
{
  // Only negative lanes are checked and lane "0" is only a placeholder without internal representation.
  // Calling GetLaneById with 0 would fail so start at 1.
  // Yet, we expect expectedTypes to carry information about the 0th lane
  for (auto i = 1; i < lanes.size(); ++i)
  {
    int laneId = -i;
    const auto *lane = GetLaneById(lanes, laneId);
    EXPECT_THAT(GetOsiLane(lane).classification().subtype(), expectedTypes.at(i));
  }
}

TEST(SceneryImporter_IntegrationTests, TJunction_CheckForCorrectLanePairing)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("TJunctionBig.xodr"), IsTrue());

  auto &world = tsf.world;

  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());

  const auto &lanesR1 = worldData->GetRoads().at("R1")->GetSections().front()->GetLanes();
  const auto &lanesR12 = worldData->GetRoads().at("R1-2")->GetSections().front()->GetLanes();
  const auto &lanesR13 = worldData->GetRoads().at("R1-3")->GetSections().front()->GetLanes();
  const auto &lanesR21 = worldData->GetRoads().at("R2-1")->GetSections().front()->GetLanes();
  const auto &lanesR31 = worldData->GetRoads().at("R3-1")->GetSections().front()->GetLanes();
  const auto &lanesR2 = worldData->GetRoads().at("R2")->GetSections().front()->GetLanes();

  const auto *laneR1_Minus2 = GetLaneById(lanesR1, -2);
  const auto *laneR1_Minus1 = GetLaneById(lanesR1, -1);
  const auto *laneR1_Plus1 = GetLaneById(lanesR1, 1);
  const auto *laneR1_Plus2 = GetLaneById(lanesR1, 2);
  const auto *laneR12_Minus2 = GetLaneById(lanesR12, -2);
  const auto *laneR12_Minus1 = GetLaneById(lanesR12, -1);
  const auto *laneR13_Minus2 = GetLaneById(lanesR13, -2);
  const auto *laneR13_Minus1 = GetLaneById(lanesR13, -1);
  const auto *laneR21_Plus1 = GetLaneById(lanesR21, 1);
  const auto *laneR21_Plus2 = GetLaneById(lanesR21, 2);
  const auto *laneR31_Plus1 = GetLaneById(lanesR31, 1);
  const auto *laneR31_Plus2 = GetLaneById(lanesR31, 2);
  const auto *laneR2_Plus1 = GetLaneById(lanesR2, 1);
  const auto *laneR2_Plus2 = GetLaneById(lanesR2, 2);

  CheckLanePairings(laneR1_Minus2,
                    {{std::make_pair(-1, laneR12_Minus2->GetId()), std::make_pair(-1, laneR13_Minus2->GetId())}});
  CheckLanePairings(laneR1_Minus1,
                    {{std::make_pair(-1, laneR12_Minus1->GetId()), std::make_pair(-1, laneR13_Minus1->GetId())}});
  CheckLanePairings(laneR1_Plus1,
                    {{std::make_pair(-1, laneR21_Plus1->GetId()), std::make_pair(-1, laneR31_Plus1->GetId())}});
  CheckLanePairings(laneR1_Plus2,
                    {{std::make_pair(-1, laneR21_Plus2->GetId()), std::make_pair(-1, laneR31_Plus2->GetId())}});

  CheckLanePairings(laneR12_Minus2, {{std::make_pair(laneR1_Minus2->GetId(), laneR2_Plus2->GetId())}});
  CheckLanePairings(laneR12_Minus1, {{std::make_pair(laneR1_Minus1->GetId(), laneR2_Plus1->GetId())}});
}

TEST(SceneryImporter_IntegrationTests, SingleRoad_CheckForCorrectOsiLaneClassification)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("IntegrationTestScenery.xodr"), IsTrue());

  auto &world = tsf.world;

  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());

  ASSERT_EQ(worldData->GetRoads().at("1")->GetSections().size(), 5);

  auto sections = GetDistanceSortedSectionsForRoad(worldData, "1");

  for (const auto *section : sections)
  {
    CheckLaneNeighbours(section->GetLanes());
  }

  CheckLaneType(sections[0]->GetLanes(),
                {osi3::Lane_Classification_Type_TYPE_NONDRIVING, osi3::Lane_Classification_Type_TYPE_DRIVING});

  CheckLaneSubtype(sections[0]->GetLanes(),
                   {osi3::Lane_Classification_Subtype_SUBTYPE_OTHER, osi3::Lane_Classification_Subtype_SUBTYPE_NORMAL});

  CheckLaneType(sections[1]->GetLanes(),
                {osi3::Lane_Classification_Type_TYPE_NONDRIVING,
                 osi3::Lane_Classification_Type_TYPE_DRIVING,
                 osi3::Lane_Classification_Type_TYPE_DRIVING});

  CheckLaneSubtype(sections[1]->GetLanes(),
                   {osi3::Lane_Classification_Subtype_SUBTYPE_OTHER,
                    osi3::Lane_Classification_Subtype_SUBTYPE_NORMAL,
                    osi3::Lane_Classification_Subtype_SUBTYPE_NORMAL});

  CheckLaneType(sections[2]->GetLanes(),
                {osi3::Lane_Classification_Type_TYPE_NONDRIVING,
                 osi3::Lane_Classification_Type_TYPE_DRIVING,
                 osi3::Lane_Classification_Type_TYPE_NONDRIVING,
                 osi3::Lane_Classification_Type_TYPE_NONDRIVING});

  CheckLaneSubtype(sections[2]->GetLanes(),
                   {osi3::Lane_Classification_Subtype_SUBTYPE_OTHER,
                    osi3::Lane_Classification_Subtype_SUBTYPE_NORMAL,
                    osi3::Lane_Classification_Subtype_SUBTYPE_BIKING,
                    osi3::Lane_Classification_Subtype_SUBTYPE_SIDEWALK});

  CheckLaneType(sections[3]->GetLanes(),
                {osi3::Lane_Classification_Type_TYPE_NONDRIVING,
                 osi3::Lane_Classification_Type_TYPE_NONDRIVING,
                 osi3::Lane_Classification_Type_TYPE_DRIVING,
                 osi3::Lane_Classification_Type_TYPE_DRIVING});

  CheckLaneSubtype(sections[3]->GetLanes(),
                   {osi3::Lane_Classification_Subtype_SUBTYPE_OTHER,
                    osi3::Lane_Classification_Subtype_SUBTYPE_OTHER,
                    osi3::Lane_Classification_Subtype_SUBTYPE_NORMAL,
                    osi3::Lane_Classification_Subtype_SUBTYPE_NORMAL});
}

TEST(SceneryImporter_IntegrationTests, MultipleRoadsWithJunctions_CheckForCorrectOsiLaneClassification)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("MultipleRoadsWithJunctionIntegrationScenery.xodr"), IsTrue());

  auto &world = tsf.world;

  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());

  ASSERT_EQ(worldData->GetRoads().at("1")->GetSections().size(), 1);

  auto sections1 = GetDistanceSortedSectionsForRoad(worldData, "1");

  CheckLaneNeighbours(sections1[0]->GetLanes());

  CheckLaneType(sections1[0]->GetLanes(),
                {osi3::Lane_Classification_Type_TYPE_NONDRIVING,
                 osi3::Lane_Classification_Type_TYPE_DRIVING,
                 osi3::Lane_Classification_Type_TYPE_DRIVING,
                 osi3::Lane_Classification_Type_TYPE_DRIVING,
                 osi3::Lane_Classification_Type_TYPE_DRIVING});

  ASSERT_EQ(worldData->GetRoads().at("4")->GetSections().size(), 1);

  auto sections4 = GetDistanceSortedSectionsForRoad(worldData, "4");

  CheckLaneNeighbours(sections4[0]->GetLanes());

  CheckLaneType(sections4[0]->GetLanes(),
                {osi3::Lane_Classification_Type_TYPE_NONDRIVING,
                 osi3::Lane_Classification_Type_TYPE_INTERSECTION,
                 osi3::Lane_Classification_Type_TYPE_INTERSECTION});
}

TEST(SceneryImporter_IntegrationTests, MultipleRoads_CheckForCorrectLaneSourceReference)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("MultipleRoadsIntegrationScenery.xodr"), IsTrue());

  auto &world = tsf.world;

  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());

  const auto &lanesR1 = worldData->GetRoads().at("1")->GetSections().front()->GetLanes();
  const auto *lane1 = GetLaneById(lanesR1, -2);
  const auto osiLane1 = GetOsiLane(lane1);

  EXPECT_THAT(osiLane1.source_reference(0).type(), Eq("net.asam.opendrive"));
  EXPECT_THAT(osiLane1.source_reference(0).identifier(0), Eq("1"));
  EXPECT_THAT(std::stod(osiLane1.source_reference(0).identifier(1)), DoubleEq(0.0));
  EXPECT_THAT(osiLane1.source_reference(0).identifier(2), Eq("-2"));

  const auto &lanesR2 = worldData->GetRoads().at("2")->GetSections().back()->GetLanes();
  const auto *lane2 = GetLaneById(lanesR2, 3);
  const auto osiLane2 = GetOsiLane(lane2);

  EXPECT_THAT(osiLane2.source_reference(0).type(), Eq("net.asam.opendrive"));
  EXPECT_THAT(osiLane2.source_reference(0).identifier(0), Eq("2"));
  EXPECT_THAT(std::stod(osiLane2.source_reference(0).identifier(1)), DoubleEq(900.0));
  EXPECT_THAT(osiLane2.source_reference(0).identifier(2), Eq("3"));
}

TEST(SceneryImporter_IntegrationTests, SingleRoad_CheckForCorrectOsiLogicalLaneSInterval)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("IntegrationTestScenery.xodr"), IsTrue());

  auto &world = tsf.world;

  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());

  auto sections = GetDistanceSortedSectionsForRoad(worldData, "1");
  for (const auto &lane : sections[0]->GetLanes())
  {
    const auto logicalLane = GetOsiLogicalLane(lane);
    EXPECT_THAT(logicalLane.start_s(), DoubleEq(0.0));
    EXPECT_THAT(logicalLane.end_s(), DoubleEq(10.0));
  }
  for (const auto &lane : sections[1]->GetLanes())
  {
    const auto logicalLane = GetOsiLogicalLane(lane);
    EXPECT_THAT(logicalLane.start_s(), DoubleEq(10.0));
    EXPECT_THAT(logicalLane.end_s(), DoubleEq(30.0));
  }
  for (const auto &lane : sections[2]->GetLanes())
  {
    const auto logicalLane = GetOsiLogicalLane(lane);
    EXPECT_THAT(logicalLane.start_s(), DoubleEq(30.0));
    EXPECT_THAT(logicalLane.end_s(), DoubleEq(60.0));
  }
  for (const auto &lane : sections[3]->GetLanes())
  {
    const auto logicalLane = GetOsiLogicalLane(lane);
    EXPECT_THAT(logicalLane.start_s(), DoubleEq(60.0));
    EXPECT_THAT(logicalLane.end_s(), DoubleEq(100.0));
  }
  for (const auto &lane : sections[4]->GetLanes())
  {
    const auto logicalLane = GetOsiLogicalLane(lane);
    EXPECT_THAT(logicalLane.start_s(), DoubleEq(100.0));
    EXPECT_THAT(logicalLane.end_s(), DoubleEq(150.0));
  }
}

TEST(SceneryImporter_IntegrationTests, SingleRoad_CheckForCorrectOsiLogicalLaneNeighbours)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("IntegrationTestScenery.xodr"), IsTrue());

  auto &world = tsf.world;

  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());

  ASSERT_EQ(worldData->GetRoads().at("1")->GetSections().size(), 5);

  auto sections = GetDistanceSortedSectionsForRoad(worldData, "1");

  for (const auto *section : sections)
  {
    CheckLogicalLaneNeighbours(section->GetLanes());
  }
}

void CheckLogicalLaneConnections(const osi3::LogicalLane &lane,
                                 const std::vector<std::pair<const osi3::LogicalLane *, bool>> &predecessors,
                                 const std::vector<std::pair<const osi3::LogicalLane *, bool>> &successors)
{
  EXPECT_THAT(lane.predecessor_lane_size(), Eq(predecessors.size()));
  for (const auto &laneConnection : lane.predecessor_lane())
  {
    const auto predecessorIt
        = std::find_if(predecessors.cbegin(),
                       predecessors.cend(),
                       [&laneConnection](std::pair<const osi3::LogicalLane *, bool> predecessor)
                       { return predecessor.first->id().value() == laneConnection.other_lane_id().value(); });
    EXPECT_THAT(predecessorIt, Ne(predecessors.cend()))
        << "LaneConnection " << laneConnection.other_lane_id().value() << " not found";
    EXPECT_THAT(laneConnection.at_begin_of_other_lane(), Eq(predecessorIt->second));
  }
  EXPECT_THAT(lane.successor_lane_size(), Eq(successors.size()));
  for (const auto &laneConnection : lane.successor_lane())
  {
    const auto successorIt
        = std::find_if(successors.cbegin(),
                       successors.cend(),
                       [&laneConnection](std::pair<const osi3::LogicalLane *, bool> successor)
                       { return successor.first->id().value() == laneConnection.other_lane_id().value(); });
    EXPECT_THAT(successorIt, Ne(successors.cend()))
        << "LaneConnection " << laneConnection.other_lane_id().value() << " not found";
    EXPECT_THAT(laneConnection.at_begin_of_other_lane(), Eq(successorIt->second));
  }
}

TEST(SceneryImporter_IntegrationTests, TJunction_CheckForCorrectOsiLogicalLaneConnections)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("TJunctionBig.xodr"), IsTrue());

  auto &world = tsf.world;

  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());

  const auto &lanesR1 = worldData->GetRoads().at("R1")->GetSections().front()->GetLanes();
  const auto &lanesR12 = worldData->GetRoads().at("R1-2")->GetSections().front()->GetLanes();
  const auto &lanesR13 = worldData->GetRoads().at("R1-3")->GetSections().front()->GetLanes();
  const auto &lanesR21 = worldData->GetRoads().at("R2-1")->GetSections().front()->GetLanes();
  const auto &lanesR31 = worldData->GetRoads().at("R3-1")->GetSections().front()->GetLanes();
  const auto &lanesR2 = worldData->GetRoads().at("R2")->GetSections().front()->GetLanes();

  const auto laneR1_Minus2 = GetOsiLogicalLane(GetLaneById(lanesR1, -2));
  const auto laneR1_Minus1 = GetOsiLogicalLane(GetLaneById(lanesR1, -1));
  const auto laneR1_Plus1 = GetOsiLogicalLane(GetLaneById(lanesR1, 1));
  const auto laneR1_Plus2 = GetOsiLogicalLane(GetLaneById(lanesR1, 2));
  const auto laneR12_Minus2 = GetOsiLogicalLane(GetLaneById(lanesR12, -2));
  const auto laneR12_Minus1 = GetOsiLogicalLane(GetLaneById(lanesR12, -1));
  const auto laneR13_Minus2 = GetOsiLogicalLane(GetLaneById(lanesR13, -2));
  const auto laneR13_Minus1 = GetOsiLogicalLane(GetLaneById(lanesR13, -1));
  const auto laneR21_Plus1 = GetOsiLogicalLane(GetLaneById(lanesR21, 1));
  const auto laneR21_Plus2 = GetOsiLogicalLane(GetLaneById(lanesR21, 2));
  const auto laneR31_Plus1 = GetOsiLogicalLane(GetLaneById(lanesR31, 1));
  const auto laneR31_Plus2 = GetOsiLogicalLane(GetLaneById(lanesR31, 2));
  const auto laneR2_Plus1 = GetOsiLogicalLane(GetLaneById(lanesR2, 1));
  const auto laneR2_Plus2 = GetOsiLogicalLane(GetLaneById(lanesR2, 2));

  CheckLogicalLaneConnections(laneR1_Minus2, {}, {{&laneR12_Minus2, true}, {&laneR13_Minus2, true}});

  CheckLogicalLaneConnections(laneR1_Minus1, {}, {{&laneR12_Minus1, true}, {&laneR13_Minus1, true}});

  CheckLogicalLaneConnections(laneR12_Minus2, {{&laneR1_Minus2, false}}, {{&laneR2_Plus2, false}});

  CheckLogicalLaneConnections(laneR12_Minus1, {{&laneR1_Minus1, false}}, {{&laneR2_Plus1, false}});

  CheckLogicalLaneConnections(laneR1_Plus2, {}, {{&laneR21_Plus2, true}, {&laneR31_Plus2, true}});

  CheckLogicalLaneConnections(laneR1_Plus1, {}, {{&laneR21_Plus1, true}, {&laneR31_Plus1, true}});
}

TEST(SceneryImporter_IntegrationTests, MultipleRoadsWithNonIntersectingJunctions_JunctionsHaveNoIntersectionInformation)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("MultipleRoadsWithJunctionIntegrationScenery.xodr"), IsTrue());

  auto &world = tsf.world;

  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());
  const auto &junctionMap = worldData->GetJunctions();

  ASSERT_THAT(junctionMap.size(), 1);

  const auto &junction = junctionMap.begin()->second;

  ASSERT_THAT(junction->GetIntersections().size(), 0);
}

MATCHER_P(GeometryDoublePairEq, comparisonPair, "")
{
  constexpr static const double EPS = 1e-3;  // epsilon value for geometric comparisons
  return std::abs(arg.first.value() - comparisonPair.first) < EPS
      && std::abs(arg.second.value() - comparisonPair.second) < EPS;
}
TEST(SceneryImporter_IntegrationTests, MultipleRoadsWithIntersectingJunctions_JunctionsHaveIntersectionInformation)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("IntersectedJunctionScenery.xodr"), IsTrue());

  auto &world = tsf.world;
  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());
  WorldDataQuery worldDataQuery(*worldData);

  const std::string verticalRoadStringId = "vertical_connecting";
  const std::string horizontalRoadStringId = "horizontal_connecting";

  const auto *verticalRoad = worldDataQuery.GetRoadByOdId(verticalRoadStringId);
  const auto *horizontalRoad = worldDataQuery.GetRoadByOdId(horizontalRoadStringId);

  const auto &verticalLane1 = worldDataQuery.GetLaneByOdId(verticalRoadStringId, -1, 0.0_m);
  const auto &verticalLane2 = worldDataQuery.GetLaneByOdId(verticalRoadStringId, -2, 0.0_m);
  const auto &horizontalLane1 = worldDataQuery.GetLaneByOdId(horizontalRoadStringId, -1, 0.0_m);
  const auto &horizontalLane2 = worldDataQuery.GetLaneByOdId(horizontalRoadStringId, -2, 0.0_m);

  const std::pair<OWL::Id, OWL::Id> v1h1{verticalLane1.GetId(), horizontalLane1.GetId()};
  const std::pair<OWL::Id, OWL::Id> v1h2{verticalLane1.GetId(), horizontalLane2.GetId()};
  const std::pair<OWL::Id, OWL::Id> v2h1{verticalLane2.GetId(), horizontalLane1.GetId()};
  const std::pair<OWL::Id, OWL::Id> v2h2{verticalLane2.GetId(), horizontalLane2.GetId()};

  const std::pair<OWL::Id, OWL::Id> h1v1{horizontalLane1.GetId(), verticalLane1.GetId()};
  const std::pair<OWL::Id, OWL::Id> h1v2{horizontalLane1.GetId(), verticalLane2.GetId()};
  const std::pair<OWL::Id, OWL::Id> h2v1{horizontalLane2.GetId(), verticalLane1.GetId()};
  const std::pair<OWL::Id, OWL::Id> h2v2{horizontalLane2.GetId(), verticalLane2.GetId()};

  const std::pair<double, double> v1h1SOffset{10.0, 13.0};
  const std::pair<double, double> v1h2SOffset{13.0, 16.0};
  const std::pair<double, double> v2h1SOffset{10.0, 13.0};
  const std::pair<double, double> v2h2SOffset{13.0, 16.0};

  const std::pair<double, double> h1v1SOffset{7, 10};
  const std::pair<double, double> h1v2SOffset{4, 7};
  const std::pair<double, double> h2v1SOffset{7, 10};
  const std::pair<double, double> h2v2SOffset{4, 7};

  const auto &junctionMap = worldData->GetJunctions();
  ASSERT_THAT(junctionMap.size(), 1);

  const auto &junction = junctionMap.begin()->second;
  ASSERT_THAT(junction->GetIntersections().size(), 2);

  std::vector<OWL::IntersectionInfo> verticalIntersectionInfos;
  std::vector<OWL::IntersectionInfo> horizontalIntersectionInfos;

  ASSERT_THAT(junction->GetIntersections().count(verticalRoadStringId), Eq(1));
  ASSERT_THAT(junction->GetIntersections().count(horizontalRoadStringId), Eq(1));

  verticalIntersectionInfos = junction->GetIntersections().at(verticalRoadStringId);
  horizontalIntersectionInfos = junction->GetIntersections().at(horizontalRoadStringId);

  ASSERT_THAT(verticalIntersectionInfos.size(), 1);
  ASSERT_THAT(horizontalIntersectionInfos.size(), 1);

  const auto &verticalConnectionInfo = verticalIntersectionInfos.front();
  const auto &horizontalConnectionInfo = horizontalIntersectionInfos.front();

  ASSERT_THAT(verticalConnectionInfo.intersectingRoad, Eq(horizontalRoad->GetId()));  // horizontalRoadIdPair->first);
  ASSERT_THAT(horizontalConnectionInfo.intersectingRoad, Eq(verticalRoad->GetId()));

  ASSERT_THAT(verticalConnectionInfo.relativeRank, IntersectingConnectionRank::Higher);
  ASSERT_THAT(horizontalConnectionInfo.relativeRank, IntersectingConnectionRank::Lower);

  ASSERT_THAT(verticalConnectionInfo.sOffsets.at(v1h1), GeometryDoublePairEq(v1h1SOffset));
  ASSERT_THAT(verticalConnectionInfo.sOffsets.at(v1h2), GeometryDoublePairEq(v1h2SOffset));
  ASSERT_THAT(verticalConnectionInfo.sOffsets.at(v2h1), GeometryDoublePairEq(v2h1SOffset));
  ASSERT_THAT(verticalConnectionInfo.sOffsets.at(v2h2), GeometryDoublePairEq(v2h2SOffset));

  ASSERT_THAT(horizontalConnectionInfo.sOffsets.at(h1v1), GeometryDoublePairEq(h1v1SOffset));
  ASSERT_THAT(horizontalConnectionInfo.sOffsets.at(h1v2), GeometryDoublePairEq(h1v2SOffset));
  ASSERT_THAT(horizontalConnectionInfo.sOffsets.at(h2v1), GeometryDoublePairEq(h2v1SOffset));
  ASSERT_THAT(horizontalConnectionInfo.sOffsets.at(h2v2), GeometryDoublePairEq(h2v2SOffset));
}

[[nodiscard]] AgentInterface &ADD_AGENT(core::World &world,
                                        mantle_api::UniqueId id,
                                        units::length::meter_t x,
                                        units::length::meter_t y,
                                        units::length::meter_t width = 1.0_m,
                                        units::length::meter_t length = 1.0_m)
{
  AgentBuildInstructions agentBuildInstructions;
  agentBuildInstructions.agentCategory = AgentCategory::Scenario;

  auto vehicleParameter = std::make_shared<mantle_api::VehicleProperties>();
  vehicleParameter->type = mantle_api::EntityType::kVehicle;
  vehicleParameter->classification = mantle_api::VehicleClass::kMedium_car;
  vehicleParameter->bounding_box.dimension.width = width;
  vehicleParameter->bounding_box.dimension.length = length;
  vehicleParameter->bounding_box.geometric_center.x = 0.0_m;

  agentBuildInstructions.entityProperties = vehicleParameter;

  SpawnParameter spawnParameter;
  agentBuildInstructions.spawnParameter.position.x = x;
  agentBuildInstructions.spawnParameter.position.y = y;
  agentBuildInstructions.spawnParameter.velocity = 1.0_mps;
  agentBuildInstructions.spawnParameter.orientation.yaw = 0.0_rad;
  auto position = world.WorldCoord2LaneCoord(x, y, 0_rad);
  RoadGraph roadGraph;
  auto vertex = add_vertex(RouteElement{position.cbegin()->second.roadId, true}, roadGraph);
  agentBuildInstructions.spawnParameter.route = {roadGraph, vertex, vertex};

  return world.CreateAgentAdapter(id, agentBuildInstructions);
}

TEST(GetObjectsInRange_IntegrationTests, OneObjectOnQueriedLane)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("SceneryLeftLaneEnds.xodr"), IsTrue());

  auto &world = tsf.world;
  auto &agent0 = ADD_AGENT(world, 0, 10.0_m, 2.0_m);
  auto &agent1 = ADD_AGENT(world, 1, 10.0_m, 5.0_m);
  auto &agent2 = ADD_AGENT(world, 2, 10.0_m, 9.0_m);
  world.SyncGlobalData();

  RoadGraph roadGraph;
  RoadGraphVertex root = add_vertex(RouteElement{"1", true}, roadGraph);
  const auto objectsInRange = world.GetObjectsInRange(roadGraph, root, -3, 0_m, 0_m, 500_m).at(root);
  ASSERT_THAT(objectsInRange, SizeIs(1));
  ASSERT_THAT(objectsInRange.at(0), Eq(world.GetAgent(1)));
}

TEST(GetObjectsInRange_IntegrationTests, NoObjectOnQueriedLane)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("SceneryLeftLaneEnds.xodr"), IsTrue());

  auto &world = tsf.world;
  auto &agent0 = ADD_AGENT(world, 0, 10.0_m, 2.0_m);
  auto &agent1 = ADD_AGENT(world, 1, 10.0_m, 9.0_m);
  world.SyncGlobalData();

  RoadGraph roadGraph;
  RoadGraphVertex root = add_vertex(RouteElement{"1", true}, roadGraph);
  const auto objectsInRange = world.GetObjectsInRange(roadGraph, root, -3, 0_m, 0_m, 500_m).at(root);
  ASSERT_THAT(objectsInRange, IsEmpty());
}

TEST(GetObjectsInRange_IntegrationTests, TwoObjectsInDifferentSections)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("SceneryLeftLaneEnds.xodr"), IsTrue());

  auto &world = tsf.world;
  auto &agent0 = ADD_AGENT(world, 0, 10.0_m, 2.0_m);
  auto &agent1 = ADD_AGENT(world, 1, 310.0_m, 5.0_m);
  auto &agent2 = ADD_AGENT(world, 2, 10.0_m, 5.0_m);
  auto &agent3 = ADD_AGENT(world, 3, 10.0_m, 9.0_m);
  world.SyncGlobalData();

  RoadGraph roadGraph;
  RoadGraphVertex root = add_vertex(RouteElement{"1", true}, roadGraph);
  const auto objectsInRange = world.GetObjectsInRange(roadGraph, root, -3, 0_m, 0_m, 500_m).at(root);
  ASSERT_THAT(objectsInRange, SizeIs(2));
  ASSERT_THAT(objectsInRange.at(0), Eq(world.GetAgent(2)));
  ASSERT_THAT(objectsInRange.at(1), Eq(world.GetAgent(1)));
}

TEST(GetObjectsInRange_IntegrationTests, OneObjectOnSectionBorder)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("SceneryLeftLaneEnds.xodr"), IsTrue());

  auto &world = tsf.world;
  auto &agent0 = ADD_AGENT(world, 0, 300.0_m, 2.0_m);
  auto &agent1 = ADD_AGENT(world, 1, 300.0_m, 5.0_m);
  auto &agent2 = ADD_AGENT(world, 2, 300.0_m, 9.0_m);
  world.SyncGlobalData();

  RoadGraph roadGraph;
  RoadGraphVertex root = add_vertex(RouteElement{"1", true}, roadGraph);
  const auto objectsInRange = world.GetObjectsInRange(roadGraph, root, -3, 0_m, 0_m, 500_m).at(root);
  ASSERT_THAT(objectsInRange, SizeIs(1));
  ASSERT_THAT(objectsInRange.at(0), Eq(world.GetAgent(1)));
}

TEST(GetObjectsInRange_IntegrationTests, MultipleRoads)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("MultipleRoadsIntegrationScenery.xodr"), IsTrue());

  auto &world = tsf.world;
  auto &agent0 = ADD_AGENT(world, 0, 510.0_m, 6.0_m);
  auto &agent1 = ADD_AGENT(world, 1, 1300.0_m, 2.0_m);
  auto &agent2 = ADD_AGENT(world, 2, 510.0_m, 2.0_m);
  auto &agent3 = ADD_AGENT(world, 3, 510.0_m, -1.0_m);
  world.SyncGlobalData();

  RoadGraph roadGraph;
  RoadGraphVertex node1 = add_vertex(RouteElement{"1", true}, roadGraph);
  RoadGraphVertex node2 = add_vertex(RouteElement{"2", false}, roadGraph);
  RoadGraphVertex node3 = add_vertex(RouteElement{"3", true}, roadGraph);
  add_edge(node1, node2, roadGraph);
  add_edge(node2, node3, roadGraph);
  const auto objectsInRange = world.GetObjectsInRange(roadGraph, node1, -2, 500_m, 0_m, 1500_m).at(node3);
  ASSERT_THAT(objectsInRange, SizeIs(2));
  ASSERT_THAT(objectsInRange.at(0), Eq(world.GetAgent(2)));
  ASSERT_THAT(objectsInRange.at(1), Eq(world.GetAgent(1)));
}

TEST(Locator_IntegrationTests, AgentOnStraightRoad_CalculatesCorrectLocateResult)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("MultipleRoadsIntegrationScenery.xodr"), IsTrue());

  auto &world = tsf.world;
  const auto &agent1 = ADD_AGENT(world, 0, 399.0_m, 1.0_m, 2.0_m, 5.0_m);
  const auto &agent2 = ADD_AGENT(world, 1, 2500.0_m, 1.0_m, 2.0_m, 5.0_m);
  world.SyncGlobalData();

  EXPECT_THAT(agent1.GetRoads(ObjectPointPredefined::FrontCenter), ElementsAre("1"));
  EXPECT_THAT(agent1.GetRoadPosition(ObjectPointPredefined::FrontCenter).at("1").laneId, Eq(-2));
  EXPECT_THAT(agent1.GetTouchedRoads().at("1").sMin.roadPosition.s.value(), DoubleNear(396.5, 0.01));
  EXPECT_THAT(agent1.GetTouchedRoads().at("1").sMax.roadPosition.s.value(), DoubleNear(401.5, 0.01));
  EXPECT_THAT(agent1.GetTouchedRoads().at("1").tMin.roadPosition.t.value(), DoubleNear(2.0, 0.01));
  EXPECT_THAT(agent1.GetTouchedRoads().at("1").tMin.laneId, Eq(-3));
  EXPECT_THAT(agent1.GetTouchedRoads().at("1").tMax.roadPosition.t.value(), DoubleNear(-0.5, 0.01));
  EXPECT_THAT(agent1.GetTouchedRoads().at("1").tMax.laneId, Eq(-2));
  EXPECT_THAT(agent2.GetRoads(ObjectPointPredefined::FrontCenter), ElementsAre("2"));
  EXPECT_THAT(agent2.GetRoadPosition(ObjectPointPredefined::FrontCenter).at("2").laneId, Eq(2));
  EXPECT_THAT(agent2.GetTouchedRoads().at("2").sMin.roadPosition.s.value(), DoubleNear(497.5, 0.01));
  EXPECT_THAT(agent2.GetTouchedRoads().at("2").sMax.roadPosition.s.value(), DoubleNear(502.5, 0.01));
  EXPECT_THAT(agent2.GetTouchedRoads().at("2").tMin.roadPosition.t.value(), DoubleNear(0.5, 0.01));
  EXPECT_THAT(agent2.GetTouchedRoads().at("2").tMin.laneId, Eq(2));
  EXPECT_THAT(agent2.GetTouchedRoads().at("2").tMax.roadPosition.t.value(), DoubleNear(-2.0, 0.01));
  EXPECT_THAT(agent2.GetTouchedRoads().at("2").tMax.laneId, Eq(3));
}

TEST(Locator_IntegrationTests, AgentOnJunction_CalculatesCorrectLocateResult)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("TJunction.xodr"), IsTrue());

  auto &world = tsf.world;
  auto &agent = ADD_AGENT(world, 0, 208.0_m, -2.0_m, 2.0_m, 4.0_m);
  world.SyncGlobalData();

  EXPECT_THAT(agent.GetTouchedRoads().at("R1-3").sMin.roadPosition.s.value(), DoubleNear(6.0, 0.01));
  EXPECT_THAT(agent.GetTouchedRoads().at("R1-3").sMax.roadPosition.s.value(), DoubleNear(10.0, 0.01));
  EXPECT_THAT(agent.GetTouchedRoads().at("R2-3").sMin.roadPosition.s.value(),
              DoubleNear(M_PI, 0.15));  //front left corner
  EXPECT_THAT(agent.GetTouchedRoads().at("R2-3").sMax.roadPosition.s.value(),
              DoubleNear(std::atan(2.5) * 6, 0.15));  //intersection point on right boundary
  EXPECT_THAT(agent.GetTouchedRoads().at("R3-2").sMin.roadPosition.s.value(),
              DoubleNear(std::acos(5.0 / 6.0) * 6, 0.15));  //intersection point on left boundary
  EXPECT_THAT(agent.GetTouchedRoads().at("R3-2").sMax.roadPosition.s.value(),
              DoubleNear(std::atan(2.0) * 6, 0.15));  //rear right corner
  EXPECT_THAT(agent.GetTouchedRoads().at("R2-1").sMin.roadPosition.s.value(),
              DoubleNear(std::asin(0.3) * 6, 0.15));  //rear left corner
  EXPECT_THAT(agent.GetTouchedRoads().at("R2-1").sMax.roadPosition.s.value(),
              DoubleNear(std::atan(5.0 / 6.0) * 6, 0.15));  //intersection point on right boundary
}

TEST(SceneryImporter_IntegrationTests, SingleRoad_ImportWithCorrectLaneMarkings)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("IntegrationTestScenery.xodr"), IsTrue());

  auto &world = tsf.world;

  RoadGraph roadGraph;
  RoadGraphVertex root = add_vertex(RouteElement{"1", true}, roadGraph);
  auto laneMarkings = world.GetLaneMarkings(roadGraph, root, -1, 0.0_m, 99.0_m, Side::Left).at(root);
  ASSERT_THAT(laneMarkings, SizeIs(5));
  ASSERT_THAT(laneMarkings.at(0).relativeStartDistance.value(), DoubleEq(0.0));
  ASSERT_THAT(laneMarkings.at(0).type, Eq(LaneMarking::Type::Solid));
  ASSERT_THAT(laneMarkings.at(0).width.value(), DoubleEq(0.15));
  ASSERT_THAT(laneMarkings.at(0).color, Eq(LaneMarking::Color::White));
  ASSERT_THAT(laneMarkings.at(1).relativeStartDistance.value(), DoubleEq(10.0));
  ASSERT_THAT(laneMarkings.at(1).type, Eq(LaneMarking::Type::None));
  ASSERT_THAT(laneMarkings.at(1).width.value(), DoubleEq(0.15));
  ASSERT_THAT(laneMarkings.at(1).color, Eq(LaneMarking::Color::White));
  ASSERT_THAT(laneMarkings.at(2).relativeStartDistance.value(), DoubleEq(18.0));
  ASSERT_THAT(laneMarkings.at(2).type, Eq(LaneMarking::Type::Solid));
  ASSERT_THAT(laneMarkings.at(2).width.value(), DoubleEq(0.3));
  ASSERT_THAT(laneMarkings.at(2).color, Eq(LaneMarking::Color::White));
  ASSERT_THAT(laneMarkings.at(3).relativeStartDistance.value(), DoubleEq(30.0));

  laneMarkings = world.GetLaneMarkings(roadGraph, root, -1, 0.0_m, 99.0_m, Side::Right).at(root);
  ASSERT_THAT(laneMarkings, SizeIs(5));
  ASSERT_THAT(laneMarkings.at(0).relativeStartDistance.value(), DoubleEq(0.0));
  ASSERT_THAT(laneMarkings.at(0).type, Eq(LaneMarking::Type::Broken));
  ASSERT_THAT(laneMarkings.at(0).width.value(), DoubleEq(0.15));
  ASSERT_THAT(laneMarkings.at(0).color, Eq(LaneMarking::Color::Yellow));
  ASSERT_THAT(laneMarkings.at(1).relativeStartDistance.value(), DoubleEq(10.0));
  ASSERT_THAT(laneMarkings.at(1).type, Eq(LaneMarking::Type::Broken_Solid));
  ASSERT_THAT(laneMarkings.at(1).width.value(), DoubleEq(0.3));
  ASSERT_THAT(laneMarkings.at(1).color, Eq(LaneMarking::Color::Red));
  ASSERT_THAT(laneMarkings.at(2).relativeStartDistance.value(), DoubleEq(21.0));
  ASSERT_THAT(laneMarkings.at(2).type, Eq(LaneMarking::Type::Solid_Broken));
  ASSERT_THAT(laneMarkings.at(2).width.value(), DoubleEq(0.3));
  ASSERT_THAT(laneMarkings.at(2).color, Eq(LaneMarking::Color::Blue));
  ASSERT_THAT(laneMarkings.at(3).relativeStartDistance.value(), DoubleEq(30.0));
  ASSERT_THAT(laneMarkings.at(3).type, Eq(LaneMarking::Type::None));

  laneMarkings = world.GetLaneMarkings(roadGraph, root, -2, 11.0_m, 88.0_m, Side::Left).at(root);
  ASSERT_THAT(laneMarkings, SizeIs(4));
  ASSERT_THAT(laneMarkings.at(0).relativeStartDistance.value(), DoubleEq(-1.0));
  ASSERT_THAT(laneMarkings.at(0).type, Eq(LaneMarking::Type::Broken_Solid));
  ASSERT_THAT(laneMarkings.at(0).width.value(), DoubleEq(0.3));
  ASSERT_THAT(laneMarkings.at(0).color, Eq(LaneMarking::Color::Red));
  ASSERT_THAT(laneMarkings.at(1).relativeStartDistance.value(), DoubleEq(10.0));
  ASSERT_THAT(laneMarkings.at(1).type, Eq(LaneMarking::Type::Solid_Broken));
  ASSERT_THAT(laneMarkings.at(1).width.value(), DoubleEq(0.3));
  ASSERT_THAT(laneMarkings.at(1).color, Eq(LaneMarking::Color::Blue));
  ASSERT_THAT(laneMarkings.at(2).relativeStartDistance.value(), DoubleEq(19.0));

  laneMarkings = world.GetLaneMarkings(roadGraph, root, -2, 11.0_m, 88.0_m, Side::Right).at(root);
  ASSERT_THAT(laneMarkings, SizeIs(4));
  ASSERT_THAT(laneMarkings.at(0).relativeStartDistance.value(), DoubleEq(-1.0));
  ASSERT_THAT(laneMarkings.at(0).type, Eq(LaneMarking::Type::Broken_Broken));
  ASSERT_THAT(laneMarkings.at(0).width.value(), DoubleEq(0.15));
  ASSERT_THAT(laneMarkings.at(0).color, Eq(LaneMarking::Color::White));
  ASSERT_THAT(laneMarkings.at(1).relativeStartDistance.value(), DoubleEq(4.0));
  ASSERT_THAT(laneMarkings.at(1).type, Eq(LaneMarking::Type::Solid_Solid));
  ASSERT_THAT(laneMarkings.at(1).width.value(), DoubleEq(0.3));
  ASSERT_THAT(laneMarkings.at(1).color, Eq(LaneMarking::Color::White));
  ASSERT_THAT(laneMarkings.at(2).relativeStartDistance.value(), DoubleEq(19.0));
}

TEST(SceneryImporter_IntegrationTests, SingleRoad_ImportWithCorrectTrafficSignGeometries)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("IntegrationTestScenery.xodr"), IsTrue());

  auto &world = tsf.world;

  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());
  const auto &groundTruth = worldData->GetOsiGroundTruth();

  ASSERT_THAT(groundTruth.traffic_sign_size(), Eq(5));
  const auto &trafficSign0 = groundTruth.traffic_sign(0);
  ASSERT_THAT(trafficSign0.main_sign().base().position().x(), DoubleEq(15));
  ASSERT_THAT(trafficSign0.main_sign().base().position().y(), DoubleEq(-0.5));
  ASSERT_THAT(trafficSign0.main_sign().base().position().z(), DoubleEq(1.7));
  ASSERT_THAT(trafficSign0.main_sign().base().dimension().width(), DoubleEq(0.4));
  ASSERT_THAT(trafficSign0.main_sign().base().dimension().height(), DoubleEq(0.4));
  ASSERT_THAT(trafficSign0.main_sign().base().orientation().yaw(), DoubleNear(0.0, 1e-4));
  ASSERT_THAT(trafficSign0.main_sign().base().orientation().pitch(), DoubleNear(0.1, 1e-4));
  ASSERT_THAT(trafficSign0.main_sign().base().orientation().roll(), DoubleNear(0.2, 1e-4));

  const auto &trafficSign1 = groundTruth.traffic_sign(1);
  ASSERT_THAT(trafficSign1.main_sign().base().position().x(), DoubleEq(25));
  ASSERT_THAT(trafficSign1.main_sign().base().position().y(), DoubleEq(-0.5));
  ASSERT_THAT(trafficSign1.main_sign().base().position().z(), DoubleEq(1.7));
  ASSERT_THAT(trafficSign1.main_sign().base().dimension().width(), DoubleEq(0.4));
  ASSERT_THAT(trafficSign1.main_sign().base().dimension().height(), DoubleEq(0.4));
  ASSERT_THAT(trafficSign1.main_sign().base().orientation().yaw(), DoubleNear(0.1, 1e-4));
  ASSERT_THAT(trafficSign1.main_sign().base().orientation().pitch(), DoubleNear(-0.2, 1e-4));
  ASSERT_THAT(trafficSign1.main_sign().base().orientation().roll(), DoubleNear(-0.1, 1e-4));

  const auto &trafficSign2 = groundTruth.traffic_sign(2);
  ASSERT_THAT(trafficSign2.main_sign().base().position().x(), DoubleEq(35));
  ASSERT_THAT(trafficSign2.main_sign().base().position().y(), DoubleEq(-0.5));
  ASSERT_THAT(trafficSign2.main_sign().base().position().z(), DoubleEq(1.7));
  ASSERT_THAT(trafficSign2.main_sign().base().dimension().width(), DoubleEq(0.4));
  ASSERT_THAT(trafficSign2.main_sign().base().dimension().height(), DoubleEq(0.4));
  ASSERT_THAT(trafficSign2.main_sign().base().orientation().yaw(), DoubleNear(-M_PI + 0.1, 1e-4));
  ASSERT_THAT(trafficSign2.main_sign().base().orientation().pitch(), DoubleNear(0.0, 1e-4));
  ASSERT_THAT(trafficSign2.main_sign().base().orientation().roll(), DoubleNear(0.0, 1e-4));

  const auto &trafficSign3 = groundTruth.traffic_sign(3);
  ASSERT_THAT(trafficSign3.main_sign().base().position().x(), DoubleEq(36));
  ASSERT_THAT(trafficSign3.main_sign().base().position().y(), DoubleEq(-0.5));
  ASSERT_THAT(trafficSign3.main_sign().base().position().z(), DoubleEq(2.0));
  ASSERT_THAT(trafficSign3.main_sign().base().dimension().width(), DoubleEq(0.5));
  ASSERT_THAT(trafficSign3.main_sign().base().dimension().height(), DoubleEq(1.0));
  ASSERT_THAT(trafficSign3.main_sign().base().orientation().yaw(), DoubleNear(0.0, 1e-4));
  ASSERT_THAT(trafficSign3.main_sign().base().orientation().pitch(), DoubleNear(0.0, 1e-4));
  ASSERT_THAT(trafficSign3.main_sign().base().orientation().roll(), DoubleNear(0.0, 1e-4));

  const auto &trafficSign4 = groundTruth.traffic_sign(4);
  ASSERT_THAT(trafficSign4.main_sign().base().position().x(), DoubleEq(40));
  ASSERT_THAT(trafficSign4.main_sign().base().position().y(), DoubleEq(-0.5));
  ASSERT_THAT(trafficSign4.main_sign().base().position().z(), DoubleEq(1.7));
  ASSERT_THAT(trafficSign4.main_sign().base().dimension().width(), DoubleEq(0.4));
  ASSERT_THAT(trafficSign4.main_sign().base().dimension().height(), DoubleEq(0.4));
  ASSERT_THAT(trafficSign4.main_sign().base().orientation().yaw(), DoubleNear(0.0, 1e-4));
  ASSERT_THAT(trafficSign4.main_sign().base().orientation().pitch(), DoubleNear(0.0, 1e-4));
  ASSERT_THAT(trafficSign4.main_sign().base().orientation().roll(), DoubleNear(0.0, 1e-4));
  ASSERT_THAT(trafficSign4.supplementary_sign(0).base().position().x(), DoubleEq(40));
  ASSERT_THAT(trafficSign4.supplementary_sign(0).base().position().y(), DoubleEq(-0.5));
  ASSERT_THAT(trafficSign4.supplementary_sign(0).base().position().z(), DoubleEq(1.3));
  ASSERT_THAT(trafficSign4.supplementary_sign(0).base().dimension().width(), DoubleEq(0.4));
  ASSERT_THAT(trafficSign4.supplementary_sign(0).base().dimension().height(), DoubleEq(0.2));
  ASSERT_THAT(trafficSign4.supplementary_sign(0).base().orientation().yaw(), DoubleNear(0.0, 1e-4));
  ASSERT_THAT(trafficSign4.supplementary_sign(0).base().orientation().pitch(), DoubleNear(0.0, 1e-4));
  ASSERT_THAT(trafficSign4.supplementary_sign(0).base().orientation().roll(), DoubleNear(0.0, 1e-4));

  ASSERT_THAT(groundTruth.road_marking_size(), Eq(2));

  const auto &roadMarking1 = groundTruth.road_marking(0);
  ASSERT_THAT(roadMarking1.base().position().x(), DoubleEq(10));
  ASSERT_THAT(roadMarking1.base().position().y(), DoubleEq(7.5));
  ASSERT_THAT(roadMarking1.base().position().z(), DoubleEq(0.0));
  ASSERT_THAT(roadMarking1.base().dimension().width(), DoubleEq(8.0));
  ASSERT_THAT(roadMarking1.base().dimension().length(), DoubleEq(4.0));
  ASSERT_THAT(roadMarking1.base().orientation().yaw(), DoubleNear(0.0, 1e-4));
  ASSERT_THAT(
      roadMarking1.classification().traffic_main_sign_type(),
      Eq(osi3::TrafficSign_MainSign_Classification_Type::TrafficSign_MainSign_Classification_Type_TYPE_ZEBRA_CROSSING));

  const auto &roadMarking2 = groundTruth.road_marking(1);
  ASSERT_THAT(roadMarking2.base().position().x(), DoubleEq(41));
  ASSERT_THAT(roadMarking2.base().position().y(), DoubleEq(0.5));
  ASSERT_THAT(roadMarking2.base().position().z(), DoubleEq(0.0));
  ASSERT_THAT(roadMarking2.base().dimension().width(), DoubleEq(4.0));
  ASSERT_THAT(roadMarking2.base().dimension().height(), DoubleEq(0.0));
  ASSERT_THAT(roadMarking2.base().orientation().yaw(), DoubleNear(0.0, 1e-4));
  ASSERT_THAT(roadMarking2.classification().traffic_main_sign_type(),
              Eq(osi3::TrafficSign_MainSign_Classification_Type::TrafficSign_MainSign_Classification_Type_TYPE_STOP));
}

TEST(SceneryImporter_IntegrationTests, SingleRoad_ImportWithCorrectTrafficLightGeometries)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("IntegrationTestScenery.xodr"), IsTrue());

  auto &world = tsf.world;

  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());
  const auto &groundTruth = worldData->GetOsiGroundTruth();

  ASSERT_THAT(groundTruth.traffic_light_size(), Eq(6));
  const auto &trafficLight0 = groundTruth.traffic_light(0);
  EXPECT_THAT(trafficLight0.base().position().x(), DoubleEq(60));
  EXPECT_THAT(trafficLight0.base().position().y(), DoubleEq(-1.5));
  EXPECT_THAT(trafficLight0.base().position().z(), DoubleEq(3.25));
  EXPECT_THAT(trafficLight0.base().dimension().width(), DoubleEq(0.3));
  EXPECT_THAT(trafficLight0.base().dimension().height(), DoubleEq(0.1));
  EXPECT_THAT(trafficLight0.base().orientation().yaw(), DoubleNear(0.1, 1e-4));
  EXPECT_THAT(trafficLight0.base().orientation().pitch(), DoubleNear(-0.2, 1e-4));
  EXPECT_THAT(trafficLight0.base().orientation().roll(), DoubleNear(0.3, 1e-4));

  const auto &trafficLight1 = groundTruth.traffic_light(1);
  EXPECT_THAT(trafficLight1.base().position().x(), DoubleEq(60));
  EXPECT_THAT(trafficLight1.base().position().y(), DoubleEq(-1.5));
  EXPECT_THAT(trafficLight1.base().position().z(), DoubleEq(3.15));
  EXPECT_THAT(trafficLight1.base().dimension().width(), DoubleEq(0.3));
  EXPECT_THAT(trafficLight1.base().dimension().height(), DoubleEq(0.1));
  EXPECT_THAT(trafficLight1.base().orientation().yaw(), DoubleNear(0.1, 1e-4));
  EXPECT_THAT(trafficLight1.base().orientation().pitch(), DoubleNear(-0.2, 1e-4));
  EXPECT_THAT(trafficLight1.base().orientation().roll(), DoubleNear(0.3, 1e-4));

  const auto &trafficLight2 = groundTruth.traffic_light(2);
  EXPECT_THAT(trafficLight2.base().position().x(), DoubleEq(60));
  EXPECT_THAT(trafficLight2.base().position().y(), DoubleEq(-1.5));
  EXPECT_THAT(trafficLight2.base().position().z(), DoubleEq(3.05));
  EXPECT_THAT(trafficLight2.base().dimension().width(), DoubleEq(0.3));
  EXPECT_THAT(trafficLight2.base().dimension().height(), DoubleEq(0.1));
  EXPECT_THAT(trafficLight2.base().orientation().yaw(), DoubleNear(0.1, 1e-4));
  EXPECT_THAT(trafficLight2.base().orientation().pitch(), DoubleNear(-0.2, 1e-4));
  EXPECT_THAT(trafficLight2.base().orientation().roll(), DoubleNear(0.3, 1e-4));

  const auto &trafficLight3 = groundTruth.traffic_light(3);
  EXPECT_THAT(trafficLight3.base().position().x(), DoubleEq(65));
  EXPECT_THAT(trafficLight3.base().position().y(), DoubleEq(-0.5));
  EXPECT_THAT(trafficLight3.base().position().z(), DoubleEq(3.25));
  EXPECT_THAT(trafficLight3.base().dimension().width(), DoubleEq(0.3));
  EXPECT_THAT(trafficLight3.base().dimension().height(), DoubleEq(0.1));
  EXPECT_THAT(trafficLight3.base().orientation().yaw(), DoubleNear(M_PI - 0.1, 1e-4));
  EXPECT_THAT(trafficLight3.base().orientation().pitch(), DoubleNear(0.2, 1e-4));
  EXPECT_THAT(trafficLight3.base().orientation().roll(), DoubleNear(-0.3, 1e-4));

  const auto &trafficLight4 = groundTruth.traffic_light(4);
  EXPECT_THAT(trafficLight4.base().position().x(), DoubleEq(65));
  EXPECT_THAT(trafficLight4.base().position().y(), DoubleEq(-0.5));
  EXPECT_THAT(trafficLight4.base().position().z(), DoubleEq(3.15));
  EXPECT_THAT(trafficLight4.base().dimension().width(), DoubleEq(0.3));
  EXPECT_THAT(trafficLight4.base().dimension().height(), DoubleEq(0.1));
  EXPECT_THAT(trafficLight4.base().orientation().yaw(), DoubleNear(M_PI - 0.1, 1e-4));
  EXPECT_THAT(trafficLight4.base().orientation().pitch(), DoubleNear(0.2, 1e-4));
  EXPECT_THAT(trafficLight4.base().orientation().roll(), DoubleNear(-0.3, 1e-4));

  const auto &trafficLight5 = groundTruth.traffic_light(5);
  EXPECT_THAT(trafficLight5.base().position().x(), DoubleEq(65));
  EXPECT_THAT(trafficLight5.base().position().y(), DoubleEq(-0.5));
  EXPECT_THAT(trafficLight5.base().position().z(), DoubleEq(3.05));
  EXPECT_THAT(trafficLight5.base().dimension().width(), DoubleEq(0.3));
  EXPECT_THAT(trafficLight5.base().dimension().height(), DoubleEq(0.1));
  EXPECT_THAT(trafficLight5.base().orientation().yaw(), DoubleNear(M_PI - 0.1, 1e-4));
  EXPECT_THAT(trafficLight5.base().orientation().pitch(), DoubleNear(0.2, 1e-4));
  EXPECT_THAT(trafficLight5.base().orientation().roll(), DoubleNear(-0.3, 1e-4));
}

bool operator==(const osi3::Identifier &lhs, const osi3::Identifier &rhs)
{
  return lhs.value() == rhs.value();
}

MATCHER_P(ContainsId, id, "ContainsId " + std::to_string(id.value()))
{
  return std::find_if(
             arg.cbegin(), arg.cend(), [&](const osi3::Identifier &element) { return element.value() == id.value(); })
      != arg.cend();
}

MATCHER_P(ContainsLogicalLaneAssignment,
          assignment,
          "ContainsLogicalLaneAssignment " + std::to_string(assignment.assigned_lane_id().value()))
{
  constexpr static const double EPS = 1e-3;
  return std::find_if(arg.cbegin(),
                      arg.cend(),
                      [&](const osi3::LogicalLaneAssignment &element)
                      {
                        return element.assigned_lane_id().value() == assignment.assigned_lane_id().value()
                            && std::abs(element.s_position() - assignment.s_position()) < EPS
                            && std::abs(element.t_position() - assignment.t_position()) < EPS
                            && std::abs(element.angle_to_lane() - assignment.angle_to_lane()) < EPS;
                      })
      != arg.cend();
}

TEST(SceneryImporter_IntegrationTests, SingleRoad_ImportWithCorrectTrafficSignLaneAssignment)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("IntegrationTestScenery.xodr"), IsTrue());

  auto &world = tsf.world;

  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());
  const auto &groundTruth = worldData->GetOsiGroundTruth();

  const auto &lanes2 = worldData->GetRoads().at("1")->GetSections().at(1)->GetLanes();
  const auto *lane2Minus1 = GetLaneById(lanes2, -1);
  const auto osiLane2Minus1 = GetOsiLane(lane2Minus1);
  const auto osiLogicalLane2Minus1 = GetOsiLogicalLane(lane2Minus1);
  const auto *lane2Minus2 = GetLaneById(lanes2, -2);
  const auto osiLane2Minus2 = GetOsiLane(lane2Minus2);
  const auto osiLogicalLane2Minus2 = GetOsiLogicalLane(lane2Minus2);

  const auto &lanes3 = worldData->GetRoads().at("1")->GetSections().at(2)->GetLanes();
  const auto *lane3Minus2 = GetLaneById(lanes3, -2);
  const auto osiLane3Minus2 = GetOsiLane(lane3Minus2);
  const auto osiLogicalLane3Minus2 = GetOsiLogicalLane(lane3Minus2);

  const auto &trafficSign0 = groundTruth.traffic_sign(0);
  EXPECT_THAT(trafficSign0.main_sign().classification().assigned_lane_id_size(), Eq(2));
  EXPECT_THAT(trafficSign0.main_sign().classification().assigned_lane_id(), ContainsId(osiLane2Minus1.id()));
  EXPECT_THAT(trafficSign0.main_sign().classification().assigned_lane_id(), ContainsId(osiLane2Minus2.id()));
  osi3::LogicalLaneAssignment expectedAssignment0_Minus1;
  expectedAssignment0_Minus1.mutable_assigned_lane_id()->set_value(osiLogicalLane2Minus1.id().value());
  expectedAssignment0_Minus1.set_s_position(15);
  expectedAssignment0_Minus1.set_t_position(-8);
  expectedAssignment0_Minus1.set_angle_to_lane(0);
  osi3::LogicalLaneAssignment expectedAssignment0_Minus2;
  expectedAssignment0_Minus2.mutable_assigned_lane_id()->set_value(osiLogicalLane2Minus2.id().value());
  expectedAssignment0_Minus2.set_s_position(15);
  expectedAssignment0_Minus2.set_t_position(-8);
  expectedAssignment0_Minus2.set_angle_to_lane(0);
  EXPECT_THAT(trafficSign0.main_sign().classification().logical_lane_assignment_size(), Eq(2));
  EXPECT_THAT(trafficSign0.main_sign().classification().logical_lane_assignment(),
              ContainsLogicalLaneAssignment(expectedAssignment0_Minus1));
  EXPECT_THAT(trafficSign0.main_sign().classification().logical_lane_assignment(),
              ContainsLogicalLaneAssignment(expectedAssignment0_Minus2));

  const auto &roadMarking1 = groundTruth.road_marking(0);
  EXPECT_THAT(roadMarking1.classification().assigned_lane_id_size(), Eq(2));
  EXPECT_THAT(roadMarking1.classification().assigned_lane_id(), ContainsId(osiLane2Minus1.id()));
  EXPECT_THAT(roadMarking1.classification().assigned_lane_id(), ContainsId(osiLane2Minus2.id()));
  osi3::LogicalLaneAssignment expectedAssignment1_Minus1;
  expectedAssignment1_Minus1.mutable_assigned_lane_id()->set_value(osiLogicalLane2Minus1.id().value());
  expectedAssignment1_Minus1.set_s_position(10);
  expectedAssignment1_Minus1.set_t_position(0);
  expectedAssignment1_Minus1.set_angle_to_lane(0);
  osi3::LogicalLaneAssignment expectedAssignment1_Minus2;
  expectedAssignment1_Minus2.mutable_assigned_lane_id()->set_value(osiLogicalLane2Minus2.id().value());
  expectedAssignment1_Minus2.set_s_position(10);
  expectedAssignment1_Minus2.set_t_position(0);
  expectedAssignment1_Minus2.set_angle_to_lane(0);
  EXPECT_THAT(roadMarking1.classification().logical_lane_assignment_size(), Eq(2));
  EXPECT_THAT(roadMarking1.classification().logical_lane_assignment(),
              ContainsLogicalLaneAssignment(expectedAssignment1_Minus1));
  EXPECT_THAT(roadMarking1.classification().logical_lane_assignment(),
              ContainsLogicalLaneAssignment(expectedAssignment1_Minus2));

  const auto &roadMarking2 = groundTruth.road_marking(1);
  EXPECT_THAT(roadMarking2.classification().assigned_lane_id_size(), Eq(1));
  EXPECT_THAT(roadMarking2.classification().assigned_lane_id(), ContainsId(osiLane3Minus2.id()));
  osi3::LogicalLaneAssignment expectedAssignment2_Minus2;
  expectedAssignment2_Minus2.mutable_assigned_lane_id()->set_value(osiLogicalLane3Minus2.id().value());
  expectedAssignment2_Minus2.set_s_position(41);
  expectedAssignment2_Minus2.set_t_position(-7);
  expectedAssignment2_Minus2.set_angle_to_lane(0);
  EXPECT_THAT(roadMarking2.classification().logical_lane_assignment_size(), Eq(1));
  EXPECT_THAT(roadMarking2.classification().logical_lane_assignment(),
              ContainsLogicalLaneAssignment(expectedAssignment2_Minus2));
}

TEST(SceneryImporter_IntegrationTests, SingleRoad_ImportWithCorrectTrafficSignSourceReferenceAndCode)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("IntegrationTestScenery.xodr"), IsTrue());

  auto &world = tsf.world;

  auto *worldData = static_cast<OWL::Interfaces::WorldData *>(world.GetWorldData());
  const auto &groundTruth = worldData->GetOsiGroundTruth();

  ASSERT_THAT(groundTruth.traffic_sign_size(), Eq(5));
  const auto &trafficSign0 = groundTruth.traffic_sign(0);
  EXPECT_THAT(trafficSign0.source_reference(0).type(), Eq("net.asam.opendrive"));
  EXPECT_THAT(trafficSign0.source_reference(0).identifier(0), Eq("21"));
  EXPECT_THAT(trafficSign0.main_sign().classification().country(), Eq("DE"));
  EXPECT_THAT(trafficSign0.main_sign().classification().country_revision(), Eq("2017"));
  EXPECT_THAT(trafficSign0.main_sign().classification().code(), Eq("274"));
  EXPECT_THAT(trafficSign0.main_sign().classification().sub_code(), Eq("50"));

  const auto &trafficSign1 = groundTruth.traffic_sign(1);
  EXPECT_THAT(trafficSign1.source_reference(0).type(), Eq("net.asam.opendrive"));
  EXPECT_THAT(trafficSign1.source_reference(0).identifier(0), Eq("22"));
  EXPECT_THAT(trafficSign1.main_sign().classification().country(), Eq("DE"));
  EXPECT_THAT(trafficSign1.main_sign().classification().country_revision(), Eq("2017"));
  EXPECT_THAT(trafficSign1.main_sign().classification().code(), Eq("276"));
  EXPECT_THAT(trafficSign1.main_sign().classification().sub_code(), Eq("-1"));

  const auto &trafficSign2 = groundTruth.traffic_sign(2);
  EXPECT_THAT(trafficSign2.source_reference(0).type(), Eq("net.asam.opendrive"));
  EXPECT_THAT(trafficSign2.source_reference(0).identifier(0), Eq("23"));
  EXPECT_THAT(trafficSign2.main_sign().classification().country(), Eq("DE"));
  EXPECT_THAT(trafficSign2.main_sign().classification().country_revision(), Eq("2017"));
  EXPECT_THAT(trafficSign2.main_sign().classification().code(), Eq("274.1"));
  EXPECT_THAT(trafficSign2.main_sign().classification().sub_code(), Eq("-1"));

  const auto &trafficSign3 = groundTruth.traffic_sign(3);
  EXPECT_THAT(trafficSign3.source_reference(0).type(), Eq("net.asam.opendrive"));
  EXPECT_THAT(trafficSign3.source_reference(0).identifier(0), Eq("24"));
  EXPECT_THAT(trafficSign3.main_sign().classification().country(), Eq("DE"));
  EXPECT_THAT(trafficSign3.main_sign().classification().country_revision(), Eq("2017"));
  EXPECT_THAT(trafficSign3.main_sign().classification().code(), Eq("531"));
  EXPECT_THAT(trafficSign3.main_sign().classification().sub_code(), Eq("21"));

  const auto &trafficSign4 = groundTruth.traffic_sign(4);
  EXPECT_THAT(trafficSign4.source_reference(0).type(), Eq("net.asam.opendrive"));
  EXPECT_THAT(trafficSign4.source_reference(0).identifier(0), Eq("25"));
  EXPECT_THAT(trafficSign4.main_sign().classification().country(), Eq("DE"));
  EXPECT_THAT(trafficSign4.main_sign().classification().country_revision(), Eq("2017"));
  EXPECT_THAT(trafficSign4.main_sign().classification().code(), Eq("206"));
  EXPECT_THAT(trafficSign4.main_sign().classification().sub_code(), Eq("-1"));

  ASSERT_THAT(groundTruth.road_marking_size(), Eq(2));

  const auto &roadMarking1 = groundTruth.road_marking(0);
  EXPECT_THAT(roadMarking1.source_reference(0).type(), Eq("net.asam.opendrive"));
  EXPECT_THAT(roadMarking1.source_reference(0).identifier(0), Eq("11"));

  const auto &roadMarking2 = groundTruth.road_marking(1);
  EXPECT_THAT(roadMarking2.source_reference(0).type(), Eq("net.asam.opendrive"));
  EXPECT_THAT(roadMarking2.source_reference(0).identifier(0), Eq("26"));
  EXPECT_THAT(roadMarking2.classification().country(), Eq("DE"));
  EXPECT_THAT(roadMarking2.classification().country_revision(), Eq("2017"));
  EXPECT_THAT(roadMarking2.classification().code(), Eq("294"));
  EXPECT_THAT(roadMarking2.classification().sub_code(), Eq("-1"));
}

TEST(SceneryImporter_IntegrationTests, TJunction_ImportWithCorrectConnectionsAndPriorities)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("TJunction.xodr"), IsTrue());

  auto &world = tsf.world;

  auto connections = world.GetConnectionsOnJunction("J0", "R1");
  ASSERT_THAT(connections, SizeIs(2));
  ASSERT_THAT(connections.at(0).connectingRoadId, Eq("R1-2"));
  ASSERT_THAT(connections.at(0).outgoingRoadId, Eq("R2"));
  ASSERT_THAT(connections.at(0).outgoingStreamDirection, Eq(false));
  ASSERT_THAT(connections.at(1).connectingRoadId, Eq("R1-3"));
  ASSERT_THAT(connections.at(1).outgoingRoadId, Eq("R3"));
  ASSERT_THAT(connections.at(1).outgoingStreamDirection, Eq(false));

  connections = world.GetConnectionsOnJunction("J0", "R2");
  ASSERT_THAT(connections, SizeIs(2));
  ASSERT_THAT(connections.at(0).connectingRoadId, Eq("R2-1"));
  ASSERT_THAT(connections.at(0).outgoingRoadId, Eq("R1"));
  ASSERT_THAT(connections.at(0).outgoingStreamDirection, Eq(false));
  ASSERT_THAT(connections.at(1).connectingRoadId, Eq("R2-3"));
  ASSERT_THAT(connections.at(1).outgoingRoadId, Eq("R3"));
  ASSERT_THAT(connections.at(1).outgoingStreamDirection, Eq(false));

  connections = world.GetConnectionsOnJunction("J0", "R3");
  ASSERT_THAT(connections, SizeIs(2));
  ASSERT_THAT(connections.at(0).connectingRoadId, Eq("R3-1"));
  ASSERT_THAT(connections.at(0).outgoingRoadId, Eq("R1"));
  ASSERT_THAT(connections.at(0).outgoingStreamDirection, Eq(false));
  ASSERT_THAT(connections.at(1).connectingRoadId, Eq("R3-2"));
  ASSERT_THAT(connections.at(1).outgoingRoadId, Eq("R2"));
  ASSERT_THAT(connections.at(1).outgoingStreamDirection, Eq(false));

  auto priorities = world.GetPrioritiesOnJunction("J0");
  std::sort(priorities.begin(),
            priorities.end(),
            [](const JunctionConnectorPriority &first, const JunctionConnectorPriority &second)
            { return first.high < second.high || (first.high == second.high && first.low < second.low); });
  ASSERT_THAT(priorities.at(0).high, Eq("R1-2"));
  ASSERT_THAT(priorities.at(0).low, Eq("R3-2"));
  ASSERT_THAT(priorities.at(1).high, Eq("R1-3"));
  ASSERT_THAT(priorities.at(1).low, Eq("R2-1"));
  ASSERT_THAT(priorities.at(2).high, Eq("R1-3"));
  ASSERT_THAT(priorities.at(2).low, Eq("R2-3"));
  ASSERT_THAT(priorities.at(3).high, Eq("R1-3"));
  ASSERT_THAT(priorities.at(3).low, Eq("R3-2"));
  ASSERT_THAT(priorities.at(4).high, Eq("R3-1"));
  ASSERT_THAT(priorities.at(4).low, Eq("R2-1"));
  ASSERT_THAT(priorities.at(5).high, Eq("R3-2"));
  ASSERT_THAT(priorities.at(5).low, Eq("R2-1"));
}

TEST(GetObstruction_IntegrationTests, AgentsOnStraightRoad)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("SceneryLeftLaneEnds.xodr"), IsTrue());

  auto &world = tsf.world;
  auto &agent0 = ADD_AGENT(world, 0, 10.0_m, 2.0_m);
  auto &agent1 = ADD_AGENT(world, 1, 100.0_m, 2.5_m, 2.0_m);
  world.SyncGlobalData();

  auto &egoAgent = agent0.GetEgoAgent();
  RoadGraph roadGraph;
  auto root = add_vertex(RouteElement{"1", true}, roadGraph);
  egoAgent.SetRoadGraph(std::move(roadGraph), root, root);
  const auto obstruction
      = egoAgent.GetObstruction(&agent1, {ObjectPointRelative::Leftmost, ObjectPointRelative::Rightmost});
  EXPECT_THAT(obstruction.lateralDistances.at(ObjectPointRelative::Leftmost).value(), DoubleEq(1.5));
  EXPECT_THAT(obstruction.lateralDistances.at(ObjectPointRelative::Rightmost).value(), DoubleEq(-0.5));
}

TEST(GetObstruction_IntegrationTests, AgentBehindJunction)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("TJunction.xodr"), IsTrue());

  auto &world = tsf.world;
  auto &agent0 = ADD_AGENT(world, 0, 10.0_m, -3.0_m);
  auto &agent1 = ADD_AGENT(world, 1, 203.5_m, -10.0_m, 1.0_m, 3.0_m);
  world.SyncGlobalData();

  auto &egoAgent = agent0.GetEgoAgent();
  RoadGraph roadGraph;
  auto node1 = add_vertex(RouteElement{"R1", true}, roadGraph);
  auto node2 = add_vertex(RouteElement{"R1-2", true}, roadGraph);
  auto node3 = add_vertex(RouteElement{"R2", false}, roadGraph);
  add_edge(node1, node2, roadGraph);
  add_edge(node2, node3, roadGraph);
  egoAgent.SetRoadGraph(std::move(roadGraph), node1, node3);
  const auto obstruction
      = egoAgent.GetObstruction(&agent1, {ObjectPointRelative::Leftmost, ObjectPointRelative::Rightmost});
  EXPECT_THAT(obstruction.lateralDistances.at(ObjectPointRelative::Leftmost).value(), DoubleNear(2.0, 1e-3));
  EXPECT_THAT(obstruction.lateralDistances.at(ObjectPointRelative::Rightmost).value(), DoubleNear(-1.0, 1e-3));
}

TEST(EgoAgent_IntegrationTests, GetWorldPosition_MultipleRoads)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("MultipleRoadsIntegrationScenery.xodr"), IsTrue());

  auto &world = tsf.world;
  auto &agent0 = ADD_AGENT(world, 0, 10.0_m, 2.0_m);
  world.SyncGlobalData();

  auto &egoAgent = agent0.GetEgoAgent();
  RoadGraph roadGraph;
  auto node1 = add_vertex(RouteElement{"1", true}, roadGraph);
  auto node2 = add_vertex(RouteElement{"2", false}, roadGraph);
  auto node3 = add_vertex(RouteElement{"3", true}, roadGraph);
  add_edge(node1, node2, roadGraph);
  add_edge(node2, node3, roadGraph);
  egoAgent.SetRoadGraph(std::move(roadGraph), node1, node3);

  const auto worldPosition1 = egoAgent.GetWorldPosition(500_m, -1_m, 0_rad).value();
  EXPECT_THAT(worldPosition1.xPos.value(), DoubleNear(510, 1e-3));
  EXPECT_THAT(worldPosition1.yPos.value(), DoubleNear(1, 1e-3));
  EXPECT_THAT(worldPosition1.yawAngle.value(), DoubleNear(0, 1e-3));

  const auto worldPosition2 = egoAgent.GetWorldPosition(1500_m, 2_m, 0_rad).value();
  EXPECT_THAT(worldPosition2.xPos.value(), DoubleNear(1510, 1e-3));
  EXPECT_THAT(worldPosition2.yPos.value(), DoubleNear(4, 1e-3));
  EXPECT_THAT(worldPosition2.yawAngle.value(), DoubleNear(0, 1e-3));

  const auto worldPosition3 = egoAgent.GetWorldPosition(3500_m, 2_m, 0_rad).value();
  EXPECT_THAT(worldPosition3.xPos.value(), DoubleNear(3510, 1e-3));
  EXPECT_THAT(worldPosition3.yPos.value(), DoubleNear(4, 1e-3));
  EXPECT_THAT(worldPosition3.yawAngle.value(), DoubleNear(0, 1e-3));
}

TEST(EgoAgent_IntegrationTests, GetWorldPosition_Junction)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("TJunction.xodr"), IsTrue());

  auto &world = tsf.world;
  auto &agent0 = ADD_AGENT(world, 0, 10.0_m, -1.0_m);
  world.SyncGlobalData();

  auto &egoAgent = agent0.GetEgoAgent();
  RoadGraph roadGraph;
  auto node1 = add_vertex(RouteElement{"R1", true}, roadGraph);
  auto node2 = add_vertex(RouteElement{"R1-2", true}, roadGraph);
  auto node3 = add_vertex(RouteElement{"R2", false}, roadGraph);
  add_edge(node1, node2, roadGraph);
  add_edge(node2, node3, roadGraph);
  egoAgent.SetRoadGraph(std::move(roadGraph), node1, node3);

  const auto worldPosition1 = egoAgent.GetWorldPosition(100_m, -1_m, 0_rad).value();
  EXPECT_THAT(worldPosition1.xPos.value(), DoubleNear(110, 1e-3));
  EXPECT_THAT(worldPosition1.yPos.value(), DoubleNear(-2, 1e-3));
  EXPECT_THAT(worldPosition1.yawAngle.value(), DoubleNear(0, 1e-3));

  const auto worldPosition2 = egoAgent.GetWorldPosition(209.424778_m, -1_m, 0_rad).value();
  EXPECT_THAT(worldPosition2.xPos.value(), DoubleNear(204, 1e-3));
  EXPECT_THAT(worldPosition2.yPos.value(), DoubleNear(-16, 1e-3));
  EXPECT_THAT(worldPosition2.yawAngle.value(), DoubleNear(-M_PI_2, 1e-3));
}

// Ensures that the length, color and type of each boundary in the scenery is correct
// and that each boundary can be accessed from a physical lane.
TEST(SceneryImporter_IntegrationTests, VerifyBoundaries)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("IntegrationTestScenery.xodr"), IsTrue());
  OWL::Interfaces::WorldData *worldData{static_cast<OWL::Interfaces::WorldData *>(tsf.world.GetWorldData())};

  const auto &groundTruth{worldData->GetOsiGroundTruth()};
  const OWL::Interfaces::Road &road{*worldData->GetRoads().at("1")};

  const auto getBoundary = [&groundTruth](uint64_t id)
  {
    return *std::find_if(groundTruth.lane_boundary().begin(),
                         groundTruth.lane_boundary().end(),
                         [id](const osi3::LaneBoundary &boundary) { return boundary.id().value() == id; });
  };

  ASSERT_THAT(road.GetSections(), SizeIs(5));
  {  // Section 1/5 (0m - 10m)
    const OWL::Interfaces::Lanes &lanes{road.GetSections().at(0)->GetLanes()};
    // Only lane of the section
    const osi3::Lane lane{GetOsiLane(GetLaneById(lanes, -1))};
    // Left boundary
    ASSERT_THAT(lane.classification().left_lane_boundary_id(), SizeIs(1));
    const osi3::LaneBoundary &leftBoundary{getBoundary(lane.classification().left_lane_boundary_id(0).value())};
    EXPECT_THAT(leftBoundary.boundary_line().begin()->position().x(), DoubleNear(0.0, 1e-3));
    EXPECT_THAT(leftBoundary.boundary_line().rbegin()->position().x(), DoubleNear(10.0, 1e-3));
    EXPECT_EQ(leftBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_SOLID_LINE);
    EXPECT_EQ(leftBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_WHITE);
    // Right boundary
    ASSERT_THAT(lane.classification().right_lane_boundary_id(), SizeIs(1));
    const osi3::LaneBoundary &rightBoundary{getBoundary(lane.classification().right_lane_boundary_id(0).value())};
    EXPECT_THAT(rightBoundary.boundary_line().begin()->position().x(), DoubleNear(0.0, 1e-3));
    EXPECT_THAT(rightBoundary.boundary_line().rbegin()->position().x(), DoubleNear(10.0, 1e-3));
    EXPECT_EQ(rightBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_DASHED_LINE);
    EXPECT_EQ(rightBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_YELLOW);
  }
  {  // Section 2/5 (10m - 30m)
    const OWL::Interfaces::Lanes &lanes{road.GetSections().at(1)->GetLanes()};
    ASSERT_THAT(lanes, SizeIs(2));
    {  // Lane 1/2 (leftmost)
      const osi3::Lane lane{GetOsiLane(GetLaneById(lanes, -1))};
      // Left boundaries
      ASSERT_THAT(lane.classification().left_lane_boundary_id(), SizeIs(2));
      {  // First left boundary
        const uint64_t id{lane.classification().left_lane_boundary_id(0).value()};
        const osi3::LaneBoundary &leftBoundary{getBoundary(id)};
        EXPECT_THAT(leftBoundary.boundary_line().begin()->position().x(), DoubleNear(10.0, 1e-3));
        EXPECT_THAT(leftBoundary.boundary_line().rbegin()->position().x(), DoubleNear(18.0, 1e-3));
        EXPECT_EQ(leftBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_NO_LINE);
        EXPECT_EQ(leftBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_WHITE);
      }
      {  // Second left boundary
        const uint64_t id{lane.classification().left_lane_boundary_id(1).value()};
        const osi3::LaneBoundary &leftBoundary{getBoundary(id)};
        EXPECT_THAT(leftBoundary.boundary_line().begin()->position().x(), DoubleNear(18.0, 1e-3));
        EXPECT_THAT(leftBoundary.boundary_line().rbegin()->position().x(), DoubleNear(30.0, 1e-3));
        EXPECT_EQ(leftBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_SOLID_LINE);
        EXPECT_EQ(leftBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_WHITE);
      }

      // The lane has two right RoadMarks in OpenDRIVE. These are of type "solid broken" and "broken solid", which both
      // translate to two parallel lane boundaries in OSI. Thus, there should be a total of four lane boundaries:

      // Right boundaries
      ASSERT_THAT(lane.classification().right_lane_boundary_id(), SizeIs(4));
      {  // First right boundary
        const uint64_t id{lane.classification().right_lane_boundary_id(0).value()};
        const osi3::LaneBoundary &rightBoundary{getBoundary(id)};
        EXPECT_THAT(rightBoundary.boundary_line().begin()->position().x(), DoubleNear(10.0, 1e-3));
        EXPECT_THAT(rightBoundary.boundary_line().rbegin()->position().x(), DoubleNear(21.0, 1e-3));
        EXPECT_EQ(rightBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_DASHED_LINE);
        EXPECT_EQ(rightBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_RED);
      }
      {  // Second right boundary
        const uint64_t id{lane.classification().right_lane_boundary_id(1).value()};
        const osi3::LaneBoundary &rightBoundary{getBoundary(id)};
        EXPECT_THAT(rightBoundary.boundary_line().begin()->position().x(), DoubleNear(10.0, 1e-3));
        EXPECT_THAT(rightBoundary.boundary_line().rbegin()->position().x(), DoubleNear(21.0, 1e-3));
        EXPECT_EQ(rightBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_SOLID_LINE);
        EXPECT_EQ(rightBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_RED);
      }
      {  // Third right boundary
        const uint64_t id{lane.classification().right_lane_boundary_id(2).value()};
        const osi3::LaneBoundary &rightBoundary{getBoundary(id)};
        EXPECT_THAT(rightBoundary.boundary_line().begin()->position().x(), DoubleNear(21.0, 1e-3));
        EXPECT_THAT(rightBoundary.boundary_line().rbegin()->position().x(), DoubleNear(30.0, 1e-3));
        EXPECT_EQ(rightBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_SOLID_LINE);
        EXPECT_EQ(rightBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_BLUE);
      }
      {  // Fourth right boundary
        const uint64_t id{lane.classification().right_lane_boundary_id(3).value()};
        const osi3::LaneBoundary &rightBoundary{getBoundary(id)};
        EXPECT_THAT(rightBoundary.boundary_line().begin()->position().x(), DoubleNear(21.0, 1e-3));
        EXPECT_THAT(rightBoundary.boundary_line().rbegin()->position().x(), DoubleNear(30.0, 1e-3));
        EXPECT_EQ(rightBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_DASHED_LINE);
        EXPECT_EQ(rightBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_BLUE);
      }
    }
    {  // Lane 2/2 (rightmost)
      const osi3::Lane lane{GetOsiLane(GetLaneById(lanes, -2))};
      // Right boundaries
      ASSERT_THAT(lane.classification().right_lane_boundary_id(), SizeIs(4));
      {  // First right boundary
        const uint64_t id{lane.classification().right_lane_boundary_id(0).value()};
        const osi3::LaneBoundary &rightBoundary{getBoundary(id)};
        EXPECT_THAT(rightBoundary.boundary_line().begin()->position().x(), DoubleNear(10.0, 1e-3));
        EXPECT_THAT(rightBoundary.boundary_line().rbegin()->position().x(), DoubleNear(15.0, 1e-3));
        EXPECT_EQ(rightBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_DASHED_LINE);
        EXPECT_EQ(rightBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_WHITE);
      }
      {  // Second right boundary
        const uint64_t id{lane.classification().right_lane_boundary_id(1).value()};
        const osi3::LaneBoundary &rightBoundary{getBoundary(id)};
        EXPECT_THAT(rightBoundary.boundary_line().begin()->position().x(), DoubleNear(10.0, 1e-3));
        EXPECT_THAT(rightBoundary.boundary_line().rbegin()->position().x(), DoubleNear(15.0, 1e-3));
        EXPECT_EQ(rightBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_DASHED_LINE);
        EXPECT_EQ(rightBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_WHITE);
      }
      {  // Third right boundary
        const uint64_t id{lane.classification().right_lane_boundary_id(2).value()};
        const osi3::LaneBoundary &rightBoundary{getBoundary(id)};
        EXPECT_THAT(rightBoundary.boundary_line().begin()->position().x(), DoubleNear(15.0, 1e-3));
        EXPECT_THAT(rightBoundary.boundary_line().rbegin()->position().x(), DoubleNear(30.0, 1e-3));
        EXPECT_EQ(rightBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_SOLID_LINE);
        EXPECT_EQ(rightBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_WHITE);
      }
      {  // Fourth right boundary
        const uint64_t id{lane.classification().right_lane_boundary_id(3).value()};
        const osi3::LaneBoundary &rightBoundary{getBoundary(id)};
        EXPECT_THAT(rightBoundary.boundary_line().begin()->position().x(), DoubleNear(15.0, 1e-3));
        EXPECT_THAT(rightBoundary.boundary_line().rbegin()->position().x(), DoubleNear(30.0, 1e-3));
        EXPECT_EQ(rightBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_SOLID_LINE);
        EXPECT_EQ(rightBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_WHITE);
      }
    }
  }
  {  // Section 3/5 (30m - 60m)
    const OWL::Interfaces::Lanes &lanes{road.GetSections().at(2)->GetLanes()};
    ASSERT_THAT(lanes, SizeIs(3));
    {  // Lane 1/3 (leftmost)
      const osi3::Lane lane{GetOsiLane(GetLaneById(lanes, -1))};
      // Left boundary
      ASSERT_THAT(lane.classification().left_lane_boundary_id(), SizeIs(1));
      {
        const uint64_t id{lane.classification().left_lane_boundary_id(0).value()};
        const osi3::LaneBoundary &leftBoundary{getBoundary(id)};
        EXPECT_THAT(leftBoundary.boundary_line().begin()->position().x(), DoubleNear(30.0, 1e-3));
        EXPECT_THAT(leftBoundary.boundary_line().rbegin()->position().x(), DoubleNear(60.0, 1e-3));
        EXPECT_EQ(leftBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_SOLID_LINE);
        EXPECT_EQ(leftBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_WHITE);
      }
      // Right boundary
      ASSERT_THAT(lane.classification().right_lane_boundary_id(), SizeIs(1));
      {
        const uint64_t id{lane.classification().right_lane_boundary_id(0).value()};
        const osi3::LaneBoundary &rightBoundary{getBoundary(id)};
        EXPECT_THAT(rightBoundary.boundary_line().begin()->position().x(), DoubleNear(30.0, 1e-3));
        EXPECT_THAT(rightBoundary.boundary_line().rbegin()->position().x(), DoubleNear(60.0, 1e-3));
        EXPECT_EQ(rightBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_NO_LINE);
        EXPECT_EQ(rightBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_OTHER);
      }
    }
    {  // Lane 2/3
      const osi3::Lane lane{GetOsiLane(GetLaneById(lanes, -2))};
      // Right boundary
      ASSERT_THAT(lane.classification().right_lane_boundary_id(), SizeIs(1));
      {
        const uint64_t id{lane.classification().right_lane_boundary_id(0).value()};
        const osi3::LaneBoundary &rightBoundary{getBoundary(id)};
        EXPECT_THAT(rightBoundary.boundary_line().begin()->position().x(), DoubleNear(30.0, 1e-3));
        EXPECT_THAT(rightBoundary.boundary_line().rbegin()->position().x(), DoubleNear(60.0, 1e-3));
        EXPECT_EQ(rightBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_DASHED_LINE);
        EXPECT_EQ(rightBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_WHITE);
      }
    }
    {  // Lane 3/3 (rightmost)
      const osi3::Lane lane{GetOsiLane(GetLaneById(lanes, -3))};
      // Right boundary
      ASSERT_THAT(lane.classification().right_lane_boundary_id(), SizeIs(1));
      {
        const uint64_t id{lane.classification().right_lane_boundary_id(0).value()};
        const osi3::LaneBoundary &rightBoundary{getBoundary(id)};
        EXPECT_THAT(rightBoundary.boundary_line().begin()->position().x(), DoubleNear(30.0, 1e-3));
        EXPECT_THAT(rightBoundary.boundary_line().rbegin()->position().x(), DoubleNear(60.0, 1e-3));
        EXPECT_EQ(rightBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_DASHED_LINE);
        EXPECT_EQ(rightBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_WHITE);
      }
    }
  }
  {  // Section 4/5 (60m - 100m)
    const OWL::Interfaces::Lanes &lanes{road.GetSections().at(3)->GetLanes()};
    ASSERT_THAT(lanes, SizeIs(3));
    {  // Lane 1/3 (leftmost)
      const osi3::Lane lane{GetOsiLane(GetLaneById(lanes, -1))};
      // Left boundary
      ASSERT_THAT(lane.classification().left_lane_boundary_id(), SizeIs(1));
      {
        const uint64_t id{lane.classification().left_lane_boundary_id(0).value()};
        const osi3::LaneBoundary &leftBoundary{getBoundary(id)};
        EXPECT_THAT(leftBoundary.boundary_line().begin()->position().x(), DoubleNear(60.0, 1e-3));
        EXPECT_THAT(leftBoundary.boundary_line().rbegin()->position().x(), DoubleNear(100.0, 1e-3));
        EXPECT_EQ(leftBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_SOLID_LINE);
        EXPECT_EQ(leftBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_WHITE);
      }
      // Right boundary
      ASSERT_THAT(lane.classification().right_lane_boundary_id(), SizeIs(1));
      {
        const uint64_t id{lane.classification().right_lane_boundary_id(0).value()};
        const osi3::LaneBoundary &rightBoundary{getBoundary(id)};
        EXPECT_THAT(rightBoundary.boundary_line().begin()->position().x(), DoubleNear(60.0, 1e-3));
        EXPECT_THAT(rightBoundary.boundary_line().rbegin()->position().x(), DoubleNear(100.0, 1e-3));
        EXPECT_EQ(rightBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_DASHED_LINE);
        EXPECT_EQ(rightBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_WHITE);
      }
    }
    {  // Lane 2/3
      const osi3::Lane lane{GetOsiLane(GetLaneById(lanes, -2))};
      // Right boundary
      ASSERT_THAT(lane.classification().right_lane_boundary_id(), SizeIs(1));
      {
        const uint64_t id{lane.classification().right_lane_boundary_id(0).value()};
        const osi3::LaneBoundary &rightBoundary{getBoundary(id)};
        EXPECT_THAT(rightBoundary.boundary_line().begin()->position().x(), DoubleNear(60.0, 1e-3));
        EXPECT_THAT(rightBoundary.boundary_line().rbegin()->position().x(), DoubleNear(100.0, 1e-3));
        EXPECT_EQ(rightBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_DASHED_LINE);
        EXPECT_EQ(rightBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_WHITE);
      }
    }
    {  // Lane 3/3 (rightmost)
      const osi3::Lane lane{GetOsiLane(GetLaneById(lanes, -3))};
      // Right boundary
      ASSERT_THAT(lane.classification().right_lane_boundary_id(), SizeIs(1));
      {
        const uint64_t id{lane.classification().right_lane_boundary_id(0).value()};
        const osi3::LaneBoundary &rightBoundary{getBoundary(id)};
        EXPECT_THAT(rightBoundary.boundary_line().begin()->position().x(), DoubleNear(60.0, 1e-3));
        EXPECT_THAT(rightBoundary.boundary_line().rbegin()->position().x(), DoubleNear(100.0, 1e-3));
        EXPECT_EQ(rightBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_DASHED_LINE);
        EXPECT_EQ(rightBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_WHITE);
      }
    }
  }
  {  // Section 5/5 (100m - 150m)
    const OWL::Interfaces::Lanes &lanes{road.GetSections().at(4)->GetLanes()};
    ASSERT_THAT(lanes, SizeIs(2));
    {  // Lane 1/2 (leftmost)
      const osi3::Lane lane{GetOsiLane(GetLaneById(lanes, -1))};
      // Left boundary
      ASSERT_THAT(lane.classification().left_lane_boundary_id(), SizeIs(1));
      {
        const uint64_t id{lane.classification().left_lane_boundary_id(0).value()};
        const osi3::LaneBoundary &leftBoundary{getBoundary(id)};
        EXPECT_THAT(leftBoundary.boundary_line().begin()->position().x(), DoubleNear(100.0, 1e-3));
        EXPECT_THAT(leftBoundary.boundary_line().rbegin()->position().x(), DoubleNear(150.0, 1e-3));
        EXPECT_EQ(leftBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_SOLID_LINE);
        EXPECT_EQ(leftBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_WHITE);
      }
      // Right boundary
      ASSERT_THAT(lane.classification().right_lane_boundary_id(), SizeIs(1));
      {
        const uint64_t id{lane.classification().right_lane_boundary_id(0).value()};
        const osi3::LaneBoundary &rightBoundary{getBoundary(id)};
        EXPECT_THAT(rightBoundary.boundary_line().begin()->position().x(), DoubleNear(100.0, 1e-3));
        EXPECT_THAT(rightBoundary.boundary_line().rbegin()->position().x(), DoubleNear(150.0, 1e-3));
        EXPECT_EQ(rightBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_DASHED_LINE);
        EXPECT_EQ(rightBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_WHITE);
      }
    }
    {  // Lane 2/2 (rightmost)
      const osi3::Lane lane{GetOsiLane(GetLaneById(lanes, -2))};
      // Right boundary
      ASSERT_THAT(lane.classification().right_lane_boundary_id(), SizeIs(1));
      {
        const uint64_t id{lane.classification().right_lane_boundary_id(0).value()};
        const osi3::LaneBoundary &rightBoundary{getBoundary(id)};
        EXPECT_THAT(rightBoundary.boundary_line().begin()->position().x(), DoubleNear(100.0, 1e-3));
        EXPECT_THAT(rightBoundary.boundary_line().rbegin()->position().x(), DoubleNear(150.0, 1e-3));
        EXPECT_EQ(rightBoundary.classification().type(), osi3::LaneBoundary_Classification_Type_TYPE_DASHED_LINE);
        EXPECT_EQ(rightBoundary.classification().color(), osi3::LaneBoundary_Classification_Color_COLOR_WHITE);
      }
    }
  }
}

// Check that adjacent lanes share the same boundaries
TEST(SceneryImporter_IntegrationTests, AdjacentLanesPointToSameBoundaries)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("IntegrationTestScenery.xodr"), IsTrue());
  OWL::Interfaces::WorldData *worldData{static_cast<OWL::Interfaces::WorldData *>(tsf.world.GetWorldData())};

  const OWL::Interfaces::Road &road{*worldData->GetRoads().at("1")};

  for (const OWL::Interfaces::Section *section : road.GetSections())
  {
    for (size_t i{1u}; i < section->GetLanes().size(); ++i)
    {
      const osi3::Lane leftPhysicalLane{GetOsiLane(GetLaneById(section->GetLanes(), -static_cast<int>(i)))};
      const osi3::Lane rightPhysicalLane{GetOsiLane(GetLaneById(section->GetLanes(), -static_cast<int>(i + 1)))};

      ASSERT_EQ(leftPhysicalLane.classification().right_lane_boundary_id_size(),
                rightPhysicalLane.classification().left_lane_boundary_id_size());
      for (size_t j{0u}; j < leftPhysicalLane.classification().right_lane_boundary_id_size(); ++j)
      {
        EXPECT_EQ(leftPhysicalLane.classification().right_lane_boundary_id(j).value(),
                  rightPhysicalLane.classification().left_lane_boundary_id(j).value());
      }
    }
  }
}

// There are two ways to access physical lane boundaries from a logical lane.
// Either loop through the logical lane's physical lane references and read the boundaries of those physical lanes
// or loop the the logical lane's logical boundaries and read the physical boundaries of those logical boundaries.
// This test verifies that both approaches are identical for all lanes in the test scenery.
TEST(SceneryImporter_IntegrationTests, BoundaryAccessCongruence)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("IntegrationTestScenery.xodr"), IsTrue());
  OWL::Interfaces::WorldData *worldData{static_cast<OWL::Interfaces::WorldData *>(tsf.world.GetWorldData())};

  const auto &groundTruth{worldData->GetOsiGroundTruth()};

  for (const osi3::LogicalLane &logicalLane : groundTruth.logical_lane())
  {
    const osi3::Lane &lane{*std::find_if(groundTruth.lane().begin(),
                                         groundTruth.lane().end(),
                                         [id = logicalLane.physical_lane_reference(0).physical_lane_id().value()](
                                             const osi3::Lane &lane) { return lane.id().value() == id; })};

    std::vector<const osi3::LogicalLaneBoundary *> leftBoundaries(logicalLane.left_boundary_id_size());
    std::transform(logicalLane.left_boundary_id().begin(),
                   logicalLane.left_boundary_id().end(),
                   leftBoundaries.begin(),
                   [&groundTruth](const osi3::Identifier &id)
                   {
                     return &*std::find_if(groundTruth.logical_lane_boundary().begin(),
                                           groundTruth.logical_lane_boundary().end(),
                                           [id = id.value()](const osi3::LogicalLaneBoundary &boundary)
                                           { return boundary.id().value() == id; });
                   });

    std::vector<const osi3::LogicalLaneBoundary *> rightBoundaries(logicalLane.right_boundary_id_size());
    std::transform(logicalLane.right_boundary_id().begin(),
                   logicalLane.right_boundary_id().end(),
                   rightBoundaries.begin(),
                   [&groundTruth](const osi3::Identifier &id)
                   {
                     return &*std::find_if(groundTruth.logical_lane_boundary().begin(),
                                           groundTruth.logical_lane_boundary().end(),
                                           [id = id.value()](const osi3::LogicalLaneBoundary &boundary)
                                           { return boundary.id().value() == id; });
                   });

    size_t leftBoundariesCount{0};
    for (const auto &laneBoundary : leftBoundaries)
    {
      EXPECT_THAT(laneBoundary->physical_boundary_id_size(), AllOf(Ge(1), Le(2)));
      leftBoundariesCount += laneBoundary->physical_boundary_id_size();
    }
    EXPECT_EQ(lane.classification().left_lane_boundary_id_size(), leftBoundariesCount);

    size_t rightBoundariesCount{0};
    for (const auto &laneBoundary : rightBoundaries)
    {
      EXPECT_THAT(laneBoundary->physical_boundary_id_size(), AllOf(Ge(1), Le(2)));
      rightBoundariesCount += laneBoundary->physical_boundary_id_size();
    }
    EXPECT_EQ(lane.classification().right_lane_boundary_id_size(), rightBoundariesCount);
  }
}

TEST(SceneryImporter_IntegrationTests, LogicalLaneMatchesPhyicalLaneReferenceStartAndEndS)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("IntegrationTestScenery.xodr"), IsTrue());
  OWL::Interfaces::WorldData *worldData{static_cast<OWL::Interfaces::WorldData *>(tsf.world.GetWorldData())};
  const osi3::GroundTruth &groundTruth{worldData->GetOsiGroundTruth()};
  for (const osi3::LogicalLane &lane : groundTruth.logical_lane())
  {
    EXPECT_NE(lane.physical_lane_reference(0).end_s(), lane.start_s());
    EXPECT_EQ(lane.physical_lane_reference(0).start_s(), lane.start_s());
    EXPECT_EQ(lane.physical_lane_reference().rbegin()->end_s(), lane.end_s());
  }
}

TEST(SceneryImporter_IntegrationTests, VerifyReferenceLine)
{
  TESTSCENERY_FACTORY tsf;
  ASSERT_THAT(tsf.instantiate("TJunction.xodr"), IsTrue());
  OWL::Interfaces::WorldData *worldData{static_cast<OWL::Interfaces::WorldData *>(tsf.world.GetWorldData())};

  const auto &groundTruth{worldData->GetOsiGroundTruth()};
  const OWL::Interfaces::Road &road{*worldData->GetRoads().at("R1-2")};

  const OWL::Interfaces::Lanes &lanes{road.GetSections().at(0)->GetLanes()};
  const osi3::LogicalLane &lane{GetOsiLogicalLane(GetLaneById(lanes, -1))};
  const osi3::Identifier &referenceLineId = lane.reference_line_id();

  const osi3::ReferenceLine &referenceLine = *std::find_if(groundTruth.reference_line().begin(),
                                                           groundTruth.reference_line().end(),
                                                           [referenceLineId](const osi3::ReferenceLine &reference_line)
                                                           { return reference_line.id() == referenceLineId; });

  constexpr static const double EPS = 1e-3;  // epsilon value for geometric comparisons

  EXPECT_THAT(referenceLine.type(), Eq(osi3::ReferenceLine_Type_TYPE_POLYLINE_WITH_T_AXIS));
  const auto &firstpoint = *referenceLine.poly_line().cbegin();
  EXPECT_THAT(firstpoint.world_position().x(), DoubleNear(200.0, EPS));
  EXPECT_THAT(firstpoint.world_position().y(), DoubleNear(0.0, EPS));
  EXPECT_THAT(firstpoint.s_position(), DoubleNear(0.0, EPS));
  EXPECT_THAT(firstpoint.t_axis_yaw(), DoubleNear(M_PI_2, EPS));

  const auto &lastpoint = *(referenceLine.poly_line().cend() - 1);
  EXPECT_THAT(lastpoint.world_position().x(), DoubleNear(206.0, EPS));
  EXPECT_THAT(lastpoint.world_position().y(), DoubleNear(-6.0, EPS));
  EXPECT_THAT(lastpoint.s_position(), DoubleNear(9.424778, EPS));
  EXPECT_THAT(lastpoint.t_axis_yaw(), DoubleNear(0.0, EPS));
}
