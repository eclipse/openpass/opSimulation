/********************************************************************************
 * Copyright (c) 2019-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>

#include "include/roadInterface/roadObjectInterface.h"

class FakeRoadObject : public RoadObjectInterface
{
public:
  MOCK_CONST_METHOD0(GetType, RoadObjectType());
  MOCK_CONST_METHOD0(GetId, std::string());
  MOCK_CONST_METHOD0(GetS, units::length::meter_t());
  MOCK_CONST_METHOD0(GetT, units::length::meter_t());
  MOCK_CONST_METHOD0(GetZOffset, units::length::meter_t());
  MOCK_CONST_METHOD1(IsValidForLane, bool(int64_t));
  MOCK_CONST_METHOD0(GetLength, units::length::meter_t());
  MOCK_CONST_METHOD0(GetWidth, units::length::meter_t());
  MOCK_CONST_METHOD0(GetHdg, units::angle::radian_t());
  MOCK_CONST_METHOD0(GetHeight, units::length::meter_t());
  MOCK_CONST_METHOD0(GetPitch, units::angle::radian_t());
  MOCK_CONST_METHOD0(GetRoll, units::angle::radian_t());
  MOCK_CONST_METHOD0(GetName, std::string());
  MOCK_CONST_METHOD0(IsContinuous, bool());
};
