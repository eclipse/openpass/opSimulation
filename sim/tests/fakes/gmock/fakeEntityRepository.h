/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>

#include <MantleAPI/Test/test_utils.h>

#include "include/entityRepositoryInterface.h"

class FakeEntityRepository : public core::EntityRepositoryInterface
{
public:
  MOCK_METHOD2(Create, mantle_api::IVehicle&(const std::string& name, const mantle_api::VehicleProperties& properties));
  MOCK_METHOD3(Create,
               mantle_api::IVehicle&(mantle_api::UniqueId id,
                                     const std::string& name,
                                     const mantle_api::VehicleProperties& properties));
  MOCK_METHOD2(Create,
               mantle_api::IPedestrian&(const std::string& name, const mantle_api::PedestrianProperties& properties));
  MOCK_METHOD3(Create,
               mantle_api::IPedestrian&(mantle_api::UniqueId id,
                                        const std::string& name,
                                        const mantle_api::PedestrianProperties& properties));
  MOCK_METHOD2(Create,
               mantle_api::IStaticObject&(const std::string& name,
                                          const mantle_api::StaticObjectProperties& properties));
  MOCK_METHOD3(Create,
               mantle_api::IStaticObject&(mantle_api::UniqueId id,
                                          const std::string& name,
                                          const mantle_api::StaticObjectProperties& properties));
  MOCK_METHOD0(GetHost, mantle_api::IVehicle&());
  MOCK_METHOD1(Get, std::optional<std::reference_wrapper<mantle_api::IEntity>>(const std::string& name));
  MOCK_CONST_METHOD1(Get, std::optional<std::reference_wrapper<const mantle_api::IEntity>>(const std::string& name));
  MOCK_METHOD1(Get, std::optional<std::reference_wrapper<mantle_api::IEntity>>(mantle_api::UniqueId id));
  MOCK_CONST_METHOD1(Get, std::optional<std::reference_wrapper<const mantle_api::IEntity>>(mantle_api::UniqueId id));
  MOCK_CONST_METHOD1(Contains, bool(mantle_api::UniqueId id));
  MOCK_METHOD1(Delete, void(const std::string& name));
  MOCK_METHOD1(Delete, void(mantle_api::UniqueId id));
  MOCK_CONST_METHOD0(GetEntities, const std::vector<std::unique_ptr<mantle_api::IEntity>>&());
  MOCK_METHOD1(RegisterEntityCreatedCallback, void(const std::function<void(mantle_api::IEntity&)>& callback));
  MOCK_METHOD1(RegisterEntityDeletedCallback, void(const std::function<void(const std::string&)>& callback));
  MOCK_METHOD1(RegisterEntityDeletedCallback, void(const std::function<void(mantle_api::UniqueId)>& callback));
  MOCK_METHOD0(SpawnReadyAgents, bool());
  MOCK_METHOD1(CreateCommon, mantle_api::IVehicle&(const mantle_api::VehicleProperties& properties));
  MOCK_METHOD0(ConsumeNewAgents, std::vector<core::Agent*>());
  MOCK_METHOD0(Reset, void());
};
