/********************************************************************************
 * Copyright (c) 2018-2020 in-tech GmbH
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>

class FakeObservation : public ObservationInterface
{
public:
  MOCK_METHOD0(OpSimulationPreHook, void());
  MOCK_METHOD0(OpSimulationPreRunHook, void());
  MOCK_METHOD2(OpSimulationUpdateHook, void(int, RunResultInterface&));
  MOCK_METHOD1(OpSimulationPostRunHook, void(const RunResultInterface&));
  MOCK_METHOD0(OpSimulationPostHook, void());
  MOCK_METHOD5(Insert, void(int, int, LoggingGroup, const std::string&, const std::string&));
};
