/********************************************************************************
 * Copyright (c) 2018-2020 in-tech GmbH
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>

#include "include/roadInterface/roadInterface.h"

class FakeOdRoad : public RoadInterface
{
public:
  MOCK_METHOD5(AddGeometryLine,
               void(units::length::meter_t s,
                    units::length::meter_t x,
                    units::length::meter_t y,
                    units::angle::radian_t hdg,
                    units::length::meter_t length));
  MOCK_METHOD6(AddGeometryArc,
               void(units::length::meter_t s,
                    units::length::meter_t x,
                    units::length::meter_t y,
                    units::angle::radian_t hdg,
                    units::length::meter_t length,
                    units::curvature::inverse_meter_t curvature));
  MOCK_METHOD7(AddGeometrySpiral,
               void(units::length::meter_t s,
                    units::length::meter_t x,
                    units::length::meter_t y,
                    units::angle::radian_t hdg,
                    units::length::meter_t length,
                    units::curvature::inverse_meter_t curvStart,
                    units::curvature::inverse_meter_t curvEnd));
  MOCK_METHOD9(AddGeometryPoly3,
               void(units::length::meter_t s,
                    units::length::meter_t x,
                    units::length::meter_t y,
                    units::angle::radian_t hdg,
                    units::length::meter_t length,
                    units::length::meter_t a,
                    double b,
                    units::unit_t<units::inverse<units::length::meter>> c,
                    units::unit_t<units::inverse<units::squared<units::length::meter>>> d));
  MOCK_METHOD6(AddGeometryParamPoly3,
               void(units::length::meter_t s,
                    units::length::meter_t x,
                    units::length::meter_t y,
                    units::angle::radian_t hdg,
                    units::length::meter_t length,
                    ParamPoly3Parameters parameters));
  MOCK_METHOD5(AddElevation,
               void(units::length::meter_t s,
                    units::length::meter_t a,
                    double b,
                    units::unit_t<units::inverse<units::length::meter>> c,
                    units::unit_t<units::inverse<units::squared<units::length::meter>>> d));
  MOCK_METHOD5(AddLaneOffset,
               void(units::length::meter_t s,
                    units::length::meter_t a,
                    double b,
                    units::unit_t<units::inverse<units::length::meter>> c,
                    units::unit_t<units::inverse<units::squared<units::length::meter>>> d));
  MOCK_METHOD4(AddLink, void(RoadLinkType, RoadLinkElementType, const std::string &, ContactPointType));
  MOCK_METHOD1(AddRoadLaneSection, void(units::length::meter_t start));
  MOCK_METHOD1(AddRoadSignal, void(const RoadSignalSpecification &signal));
  MOCK_METHOD1(AddRoadObject, void(const RoadObjectSpecification &object));
  MOCK_CONST_METHOD0(GetId, const std::string());
  MOCK_CONST_METHOD0(GetLength, double());
  MOCK_CONST_METHOD0(GetElevations, std::vector<std::unique_ptr<RoadElevation>> &());
  MOCK_CONST_METHOD0(GetLaneOffsets, const std::vector<std::unique_ptr<RoadLaneOffset>> &());
  MOCK_CONST_METHOD0(GetGeometries, std::vector<std::unique_ptr<RoadGeometryInterface>> &());
  MOCK_CONST_METHOD0(GetRoadLinks, std::vector<std::unique_ptr<RoadLinkInterface>> &());
  MOCK_CONST_METHOD0(GetLaneSections, std::vector<std::unique_ptr<RoadLaneSectionInterface>> &());
  MOCK_CONST_METHOD0(GetRoadSignals, std::vector<std::unique_ptr<RoadSignalInterface>> &());
  MOCK_CONST_METHOD0(GetRoadObjects, std::vector<std::unique_ptr<RoadObjectInterface>> &());
  MOCK_METHOD1(SetInDirection, void(bool inDirection));
  MOCK_CONST_METHOD0(GetInDirection, bool());
  MOCK_METHOD1(AddRoadType, void(const RoadTypeSpecification &));
  MOCK_CONST_METHOD1(GetRoadType, RoadTypeInformation(units::length::meter_t start));
  MOCK_CONST_METHOD0(GetJunctionId, const std::string());
  MOCK_METHOD1(SetJunctionId, void(const std::string &id));
};
