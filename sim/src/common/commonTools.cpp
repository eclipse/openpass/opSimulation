/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2018-2021 in-tech GmbH
 *               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "commonTools.h"

#include <algorithm>
#include <array>
#include <memory>

bool CommonHelper::IntersectionCalculation::OnEdge(const Common::Vector2d<units::length::meter_t> &A,
                                                   const Common::Vector2d<units::length::meter_t> &B,
                                                   const Common::Vector2d<units::length::meter_t> &P)
{
  const auto PA = P - A;
  const auto BA = B - A;

  if (units::math::abs(PA.x * BA.y - PA.y * BA.x).value() > CommonHelper::EPSILON)
  {
    return false;
  }

  if (std::abs(BA.y.value()) < CommonHelper::EPSILON)
  {
    return BA.x > 0_m ? A.x <= P.x && P.x <= B.x : B.x <= P.x && P.x <= A.x;
  }

  return BA.y > 0_m ? A.y <= P.y && P.y <= B.y : B.y <= P.y && P.y <= A.y;
}

bool CommonHelper::IntersectionCalculation::WithinBarycentricCoords(units::area::square_meter_t dot00,
                                                                    units::area::square_meter_t dot02,
                                                                    units::area::square_meter_t dot01,
                                                                    units::area::square_meter_t dot11,
                                                                    units::area::square_meter_t dot12)
{
  // Compute barycentric coordinates
  const auto denom = (dot00 * dot11 - dot01 * dot01);
  const auto u = (dot11 * dot02 - dot01 * dot12) / denom;
  const auto v = (dot00 * dot12 - dot01 * dot02) / denom;
  const units::dimensionless::scalar_t ZERO{0.0};
  const units::dimensionless::scalar_t ONE{1.0};

  return (u >= ZERO) && (v >= ZERO) && (u + v < ONE);
}

bool CommonHelper::IntersectionCalculation::IsWithin(const Common::Vector2d<units::length::meter_t> &A,
                                                     const Common::Vector2d<units::length::meter_t> &B,
                                                     const Common::Vector2d<units::length::meter_t> &C,
                                                     const Common::Vector2d<units::length::meter_t> &D,
                                                     const Common::Vector2d<units::length::meter_t> &P)
{
  // Triangle 1 (A, B, C)
  const auto BA = A - B;
  const auto BC = C - B;
  const auto BP = P - B;

  auto dot00 = BA.Dot(BA);
  auto dot01 = BA.Dot(BC);
  auto dot02 = BA.Dot(BP);
  auto dot11 = BC.Dot(BC);
  auto dot12 = BC.Dot(BP);

  if (WithinBarycentricCoords(dot00, dot02, dot01, dot11, dot12))
  {
    return true;
  }

  // Triangle 2 (C, B, D)
  const auto BD = D - B;

  dot00 = dot11;
  dot02 = dot12;
  dot01 = BC.Dot(BD);
  dot11 = BD.Dot(BD);
  dot12 = BD.Dot(BP);

  if (WithinBarycentricCoords(dot00, dot02, dot01, dot11, dot12))
  {
    return true;
  }

  // On an edge
  return OnEdge(A, B, P) ||  // along left edge
         OnEdge(B, D, P) ||  // along end of next element
         OnEdge(D, C, P) ||  // along right edge
         OnEdge(C, A, P) ||  // along start of current element
         OnEdge(B, C, P);    // along triangular split
}

//! Adds a new point to the list if it is not already in the list (or a point near it within a small epsilon)
void AddPointIfNotDuplicate(std::vector<Common::Vector2d<units::length::meter_t>> &points,
                            const Common::Vector2d<units::length::meter_t> &newPoint)
{
  //! Note: find uses the operator== which is defined for Vector2d taking a small epsilon into account
  //! This mitigates rounding errors
  if (std::find(points.cbegin(), points.cend(), newPoint) == points.cend())
  {
    points.emplace_back(newPoint);
  }
}

// NOLINTBEGIN(cppcoreguidelines-pro-bounds-constant-array-index)
// performance critical code
std::vector<Common::Vector2d<units::length::meter_t>> CommonHelper::IntersectionCalculation::GetIntersectionPoints(
    const std::vector<Common::Vector2d<units::length::meter_t>> &firstPoints,
    const std::vector<Common::Vector2d<units::length::meter_t>> &secondPoints,
    bool firstIsRectangular,
    bool secondIsRectangular)
{
  std::vector<Common::Vector2d<units::length::meter_t>> intersectionPoints{};
  intersectionPoints.reserve(8);
  std::array<Common::Vector2d<units::length::meter_t>, 4> firstEdges;
  firstEdges[0] = firstPoints[1] - firstPoints[0];
  firstEdges[1] = firstPoints[2] - firstPoints[1];
  firstEdges[2] = firstPoints[3] - firstPoints[2];
  firstEdges[3] = firstPoints[0] - firstPoints[3];
  std::array<Common::Vector2d<units::length::meter_t>, 4> secondEdges;
  secondEdges[0] = secondPoints[1] - secondPoints[0];
  secondEdges[1] = secondPoints[2] - secondPoints[1];
  secondEdges[2] = secondPoints[3] - secondPoints[2];
  secondEdges[3] = secondPoints[0] - secondPoints[3];

  std::array<std::array<units::dimensionless::scalar_t, 4>, 4> lambda{};
  std::array<std::array<units::dimensionless::scalar_t, 4>, 4> kappa{};
  const units::dimensionless::scalar_t ZERO{0.0};
  const units::dimensionless::scalar_t ONE{1.0};
  std::array<std::array<bool, 4>, 4> parallel{};
  for (size_t i = 0; i < 4; i++)
  {
    for (size_t k = 0; k < 4; k++)
    {
      units::area::square_meter_t determinant = secondEdges[i].x * firstEdges[k].y - secondEdges[i].y * firstEdges[k].x;
      parallel[i][k] = (std::abs(determinant.value()) < CommonHelper::EPSILON);
      lambda[i][k] = (-secondPoints[i].x * firstEdges[k].y + secondPoints[i].y * firstEdges[k].x
                      + firstPoints[k].x * firstEdges[k].y - firstPoints[k].y * firstEdges[k].x)
                   / determinant;
      kappa[i][k] = (-secondPoints[i].x * secondEdges[i].y + secondPoints[i].y * secondEdges[i].x
                     + firstPoints[k].x * secondEdges[i].y - firstPoints[k].y * secondEdges[i].x)
                  / determinant;
      if (lambda[i][k] > ZERO && lambda[i][k] < ONE && kappa[i][k] > ZERO && kappa[i][k] < ONE)
      {
        //0 < lambda < 1 and 0 < kappa < 1 => edges intersect. Intersection point is left hand side (and rhs) of the
        //above equation
        units::length::meter_t intersectionPointX = secondPoints[i].x + lambda[i][k] * secondEdges[i].x;
        units::length::meter_t intersectionPointY = secondPoints[i].y + lambda[i][k] * secondEdges[i].y;
        intersectionPoints.emplace_back(intersectionPointX, intersectionPointY);
      }
    }
  }
  // For each vertex of the first quadrangle check, if it is inside the second. If true, it is also part of the
  // intersection polygon.
  for (size_t k = 0; k < 4; k++)
  {
    if (secondIsRectangular)
    {
      // Quicker check for rectangle
      if ((!parallel[0][k] ? kappa[0][k] * kappa[2][k] <= ZERO
                           : (1 - kappa[0][(k - 1) % 4]) * (1 - kappa[2][(k - 1) % 4]) <= ZERO)
          && (!parallel[1][k] ? kappa[1][k] * kappa[3][k] <= ZERO
                              : (1 - kappa[1][(k - 1) % 4]) * (1 - kappa[3][(k - 1) % 4]) <= ZERO))
      {
        AddPointIfNotDuplicate(intersectionPoints, firstPoints[k]);
      }
    }
    else
    {
      if (IsWithin(secondPoints[1], secondPoints[2], secondPoints[0], secondPoints[3], firstPoints[k]))
      {
        AddPointIfNotDuplicate(intersectionPoints, firstPoints[k]);
      }
    }
  }
  // For each object vertex of the second quadrangle check, if it is inside the first. If true, it is also part of the
  // intersection polygon.
  for (size_t i = 0; i < 4; i++)
  {
    if (firstIsRectangular)
    {
      // Quicker check for rectangle
      if ((!parallel[i][0] ? lambda[i][0] * lambda[i][2] <= ZERO
                           : (1 - lambda[(i - 1) % 4][0]) * (1 - lambda[(i - 1) % 4][2]) <= ZERO)
          && (!parallel[i][1] ? lambda[i][1] * lambda[i][3] <= ZERO
                              : (1 - lambda[(i - 1) % 4][1]) * (1 - lambda[(i - 1) % 4][3]) <= ZERO))
      {
        AddPointIfNotDuplicate(intersectionPoints, secondPoints[i]);
      }
    }
    else
    {
      if (IsWithin(firstPoints[1], firstPoints[2], firstPoints[0], firstPoints[3], secondPoints[i]))
      {
        AddPointIfNotDuplicate(intersectionPoints, secondPoints[i]);
      }
    }
  }
  return intersectionPoints;
}
// NOLINTEND(cppcoreguidelines-pro-bounds-constant-array-index)

std::vector<Common::Vector2d<units::length::meter_t>> CommonHelper::IntersectionCalculation::GetIntersectionPoints(
    const polygon_t &firstPolygon, const polygon_t &secondPolygon, bool firstIsRectangular, bool secondIsRectangular)
{
  std::vector<Common::Vector2d<units::length::meter_t>> firstPoints;
  std::transform(firstPolygon.outer().begin(),
                 firstPolygon.outer().end(),
                 std::back_insert_iterator(firstPoints),
                 [&](const auto &point)
                 {
                   return Common::Vector2d<units::length::meter_t>{units::length::meter_t(bg::get<0>(point)),
                                                                   units::length::meter_t(bg::get<1>(point))};
                 });
  firstPoints.pop_back();

  std::vector<Common::Vector2d<units::length::meter_t>> secondPoints;
  std::transform(secondPolygon.outer().begin(),
                 secondPolygon.outer().end(),
                 std::back_insert_iterator(secondPoints),
                 [&](const auto &point)
                 {
                   return Common::Vector2d<units::length::meter_t>{units::length::meter_t(bg::get<0>(point)),
                                                                   units::length::meter_t(bg::get<1>(point))};
                 });
  secondPoints.pop_back();

  return GetIntersectionPoints(firstPoints, secondPoints, firstIsRectangular, secondIsRectangular);
}
