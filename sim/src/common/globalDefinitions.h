/********************************************************************************
 * Copyright (c) 2018-2019 AMFD GmbH
 *               2020 HLRS, University of Stuttgart
 *               2016-2021 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  globalDefinitions.h
//! @brief This file contains several classes for global purposes
//-----------------------------------------------------------------------------

#pragma once

#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <array>
#include <list>
#include <map>
#include <string>
#include <tuple>
#include <vector>

#include "common/opMath.h"

// the following is a temporary workaround until the contribution is merged into osi
#if defined(_WIN32) && !defined(NODLL)
#define OSIIMPORT __declspec(dllimport)  ///< DLL import macro
#define OSIEXPORT __declspec(dllexport)  ///< DLL export macro

#elif (defined(__GNUC__) && __GNUC__ >= 4 || defined(__clang__))
#define OSIEXPORT __attribute__((visibility("default")))  ///< DLL import/export macro
#define OSIIMPORT OSIEXPORT                               ///< DLL import/export macro

#else
#define OSIIMPORT  ///< DLL import macro
#define OSIEXPORT  ///< DLL export macro
#endif

#if defined(open_simulation_interface_EXPORTS)
#define OSI_EXPORT OSIEXPORT  ///< DLL import/export macro
#else
#define OSI_EXPORT OSIIMPORT  ///< DLL import/export macro
#endif

//-----------------------------------------------------------------------------
//! @brief Containing the three possible states regarding lane change
//-----------------------------------------------------------------------------
enum class LaneChangeState
{
  NoLaneChange = 0,
  LaneChangeLeft,
  LaneChangeRight
};

//-----------------------------------------------------------------------------
//! weekday type
//-----------------------------------------------------------------------------
enum class Weekday
{
  Undefined = 0,
  Monday,
  Tuesday,
  Wednesday,
  Thursday,
  Friday,
  Saturday,
  Sunday
};

//-----------------------------------------------------------------------------
//! Agent category classification
//-----------------------------------------------------------------------------
enum class AgentCategory
{
  Ego = 0,
  Scenario,
  Common,
  Any
};

namespace units
{
//! Adds a single new unit to the given namespace, as well as a literal definition and `cout` support based on the given
//! `abbreviation`.
UNIT_ADD(
    curvature, inverse_meter, inverse_meters, i_m, units::unit<std::ratio<1>, units::inverse<units::length::meter>>)

namespace category
{
typedef base_unit<detail::meter_ratio<-1>> curvature_unit;
}

UNIT_ADD_CATEGORY_TRAIT(curvature)

using inertia = unit_t<compound_unit<mass::kilogram, squared<length::meter>>>;  //!< The unit of inertia
using impulse = unit_t<compound_unit<force::newton, time::second>>;             //!< The unit of impulse

/// Radian are by default unitless, but the units.h explicitly defines it as a unit. This leads to several typematch
/// errors in calculations. Therefore the inverse_radian is used to remove the radian type.
using inverse_radian = units::unit_t<units::inverse<units::angle::radian>>;
}  // namespace units

namespace openpass::utils
{

/// @brief constexpr map for transforming the a corresponding enumeration into
///        a string representation: try to_cstr(EnumType) or to_string(EnumType)
static constexpr std::array<const char *, 4> AgentCategoryMapping{"Ego", "Scenario", "Common", "Any"};

/**
 * @brief Convert AgentCategory to cstr (constexpr)
 *
 * @param agentCategory Agent category classification
 * @return Returns category of the agent as string representation
 */
constexpr const char *to_cstr(AgentCategory agentCategory)
{
  return AgentCategoryMapping[static_cast<std::size_t>(agentCategory)];
}

/**
 * @brief Convert AgentCategory to std::string
 *
 * @param agentCategory Agent category classification
 * @return Returns category of the agent as string
 */
inline std::string to_string(AgentCategory agentCategory) noexcept
{
  return std::string(to_cstr(agentCategory));
}

}  // namespace openpass::utils

//-----------------------------------------------------------------------------
//! Entity type classification
//-----------------------------------------------------------------------------
namespace openpass::utils
{

/// @brief constexpr map for transforming the a corresponding enumeration into
///        a string representation: try to_cstr(EnumType) or to_string(EnumType)
static constexpr std::array<const char *, 4> EntityTypeMapping{"Other", "Vehicle", "Pedestrian", "Animal"};

static constexpr std::array<const char *, 16> VehicleClassificationMapping{"Other",
                                                                           "Small_car",
                                                                           "Compact_car",
                                                                           "Medium_car",
                                                                           "Luxury_car",
                                                                           "Delivery_van",
                                                                           "Heavy_truck",
                                                                           "Semitrailer",
                                                                           "Trailer",
                                                                           "Motorbike",
                                                                           "Bicycle",
                                                                           "Bus",
                                                                           "Tram",
                                                                           "Train",
                                                                           "Wheelchair",
                                                                           "Invalid"};

/**
 * @brief Convert mantle_api::EntityType to cstr (constexpr)
 *
 * @param entityType    Entity type
 * @return Returns type of the entity as string representation
 */
constexpr const char *to_cstr(mantle_api::EntityType entityType)
{
  return EntityTypeMapping[static_cast<std::size_t>(entityType) - 1];
}

/**
 * @brief Convert mantle_api::VehicleClass to cstr (constexpr)
 *
 * @param vehicleClassification Vehicle classification
 * @return Returns classification of the vehicle as string representation
 */
constexpr const char *to_cstr(mantle_api::VehicleClass vehicleClassification)
{
  if (vehicleClassification == mantle_api::VehicleClass::kInvalid)
  {
    return VehicleClassificationMapping.back();
  }
  else
  {
    return VehicleClassificationMapping[static_cast<std::size_t>(vehicleClassification) - 1];
  }
}

/**
 * @brief Convert mantle_api::EntityType to std::string
 *
 * @param entityType    Entity type
 * @return Returns type of the entity as string
 */
inline std::string to_string(mantle_api::EntityType entityType) noexcept
{
  return std::string(to_cstr(entityType));
}

/**
 * @brief Convert mantle_api::VehicleClass to std::string
 *
 * @param vehicleClassification Vehicle classification
 * @return Returns classification of the vehicle as string
 */
inline std::string to_string(mantle_api::VehicleClass vehicleClassification) noexcept
{
  return std::string(to_cstr(vehicleClassification));
}

//! @brief Method which checks if vehicle classification is marked as the car
//!
//! @param vehicleClassification Vehicle classification
//! @return true if vehicle classification is marked as the car
inline bool IsCar(mantle_api::VehicleClass vehicleClassification)
{
  if (vehicleClassification == mantle_api::VehicleClass::kSmall_car
      || vehicleClassification == mantle_api::VehicleClass::kCompact_car
      || vehicleClassification == mantle_api::VehicleClass::kMedium_car
      || vehicleClassification == mantle_api::VehicleClass::kLuxury_car)
  {
    return true;
  }
  else
  {
    return false;
  }
}

//! @brief Method which checks if vehicle classification is marked as the truck
//!
//! @param vehicleClassification Vehicle classification
//! @return true if vehicle classification is marked as the truck
inline bool IsTruck(mantle_api::VehicleClass vehicleClassification)
{
  if (vehicleClassification == mantle_api::VehicleClass::kHeavy_truck
      || vehicleClassification == mantle_api::VehicleClass::kSemitrailer)
  {
    return true;
  }
  else
  {
    return false;
  }
}

}  // namespace openpass::utils

//! State of indicator lever
enum class IndicatorLever
{
  IndicatorLever_Off = 0,
  IndicatorLever_Left = 1,
  IndicatorLever_Right = -1
};

/// state of the indicator
enum class IndicatorState
{
  IndicatorState_Off = 0,
  IndicatorState_Left = 1,
  IndicatorState_Right = 2,
  IndicatorState_Warn = 3
};

/// @brief Position of the object
struct Position
{
  Position() {}

  /**
   * @brief Construct a new Position object
   *
   * @param x             x coordinate
   * @param y             y coordinate
   * @param yaw           yaw angle
   * @param curvature     curvature
   */
  Position(units::length::meter_t x,
           units::length::meter_t y,
           units::angle::radian_t yaw,
           units::curvature::inverse_meter_t curvature)
      : xPos(x), yPos(y), yawAngle(yaw), curvature(curvature)
  {
  }

  /// x coordinate of the position
  units::length::meter_t xPos{0};
  /// y coordinate of the position
  units::length::meter_t yPos{0};
  /// yaw angle
  units::angle::radian_t yawAngle{0};
  /// curvature
  units::curvature::inverse_meter_t curvature{0};
};

//! Enum of potential types of marks.
enum class MarkType
{
  NONE,
  CONTINUOUS,
  INTERRUPTED_LONG,
  INTERRUPTED_SHORT,
  ROADSIDE,
  NumberOfMarkTypes
};

//! Enum of potential types of objects.
enum class ObjectType
{
  NONE,
  OBJECT,
  VIEWOBJECT,
  NumberOfObjectTypes
};

/// OSI object type
enum ObjectTypeOSI : int
{
  None = 0x00,  // default at initialization
  Vehicle = 0x01,
  Object = 0x02,
  Any = Vehicle | Object
};

/// typedef collision partner as a pair of type of OSI object and the id associated to that object
using CollisionPartner = std::pair<ObjectTypeOSI, int>;

/// state of the light
enum class LightState
{
  Off = 0,
  LowBeam,
  FogLight,
  HighBeam,
  Flash,
  NumberOfLightStates
};

//! Possibile direction of view angle with agent in center.
enum class AgentViewDirection
{
  none,
  front,
  left,
  back,
  right,
  NumberOfCarViewDirections
};

/// side whether left or right
enum class Side
{
  Left,
  Right
};

/// Type of the ADAS system
enum class AdasType
{
  Safety = 0,
  Comfort,
  Undefined
};

/// map adas type to the string
const std::map<AdasType, std::string> adasTypeToString
    = {{AdasType::Safety, "Safety"}, {AdasType::Comfort, "Comfort"}, {AdasType::Undefined, "Undefined"}};
/// Type of the component
enum class ComponentType
{
  Driver = 0,
  TrajectoryFollower,
  VehicleComponent,
  Undefined
};

/// category for lanes
enum class LaneCategory
{
  Undefined = 0,
  RegularLane,
  RightMostLane
};

//! Defines which traffic rules are in effect
struct TrafficRules
{
  double openSpeedLimit;          //!< maximum allowed speed if not restricted by signs
  double openSpeedLimitTrucks;    //!< maximum allowed speed for trucks if not restricted by signs
  double openSpeedLimitBuses;     //!< maximum allowed speed for buses if not restricted by signs
  bool keepToOuterLanes;          //!< if true, vehicles must use the outermost free lane for driving
  bool dontOvertakeOnOuterLanes;  //!< if true, it is prohibited to overtake another vehicle, that is driving further
                                  //!< left (or right for lefthand traffic)
  bool formRescueLane;  //!< if true, vehicles driving in a traffic jam must form a corridor for emergency vehicles
  bool zipperMerge;     //!< if true all merging shall be performed using zipper merge
};

//! Defines the weight of a path for randomized route generation
struct TurningRate
{
  std::string incoming{};  //!< id of the incoming road
  std::string outgoing{};  //!< id of the outgoing road (i.e. connector)
  double weight{1.};       //!< weight relative to all paths with the same incoming road
};

/// list of turning rate
using TurningRates = std::vector<TurningRate>;

//-----------------------------------------------------------------------------
//! Representation of an agent as defined in the run configuration.
//-----------------------------------------------------------------------------
class AgentSpawnItem
{
public:
  /**
   * @brief Construct a new Agent Spawn Item object
   *
   * @param id        Id of the agent spawn item
   * @param reference Reference of the agent spawn item
   */
  AgentSpawnItem(int id, int reference) : id(id), reference(reference) {}
  AgentSpawnItem(const AgentSpawnItem &) = delete;
  AgentSpawnItem(AgentSpawnItem &&) = delete;
  AgentSpawnItem &operator=(const AgentSpawnItem &) = delete;
  AgentSpawnItem &operator=(AgentSpawnItem &&) = delete;
  virtual ~AgentSpawnItem() = default;

  /*!
   * \brief GetId function to get id of the agent
   *
   * \return Id of the agent spawn item
   */
  int GetId() const { return id; }

  /*!
   * \brief GetReference function to get reference id of the agent
   *
   * \return Reference of the agent spawn item
   */
  int GetReference() const { return reference; }

  /*!
   * \brief GetVehicleClassification function to get vehicle classification
   *
   * \return Vehicle classification of the agent
   */
  mantle_api::VehicleClass GetVehicleClassification() const { return vehicleClassification; }

  /*!
   * \brief GetWidth function to get width
   *
   * \return width of the agent
   */
  units::length::meter_t GetWidth() const { return width; }

  /*!
   * \brief GetLength function to get length
   *
   * \return length of the agent
   */
  units::length::meter_t GetLength() const { return length; }

  /*!
   * \brief GetDistanceCOGtoFrontAxle function to get distance from COG to front axle
   *
   * \return distance of center of gravity to front axle of the agent
   */
  units::length::meter_t GetDistanceCOGtoFrontAxle() const { return distanceCOGtoFrontAxle; }

  /*!
   * \brief GetWeight function to get width
   *
   * \return width of the agent
   */
  units::mass::kilogram_t GetWeight() const { return weight; }

  /*!
   * \brief GetHeightCOG function to get height
   *
   * \return height of COG
   */
  units::length::meter_t GetHeightCOG() const { return heightCOG; }

  /*!
   * \brief GetWheelbase function to get wheel base
   *
   * \return wheelbase of the agent
   */
  units::length::meter_t GetWheelbase() const { return wheelbase; }

  /*!
   * \brief GetMomentInertiaRoll function to get rolling moment of inertia
   *
   * \return rolling moment of inertia of the agent
   */
  double GetMomentInertiaRoll() const { return momentInertiaRoll; }

  /*!
   * \brief GetMomentInertiaPitch function to get pitching moment of inertia
   *
   * \return moment of inertia during pitching of the agent
   */
  double GetMomentInertiaPitch() const { return momentInertiaPitch; }

  /*!
   * \brief GetMomentInertiaYaw function to get yaw moment of inertia
   *
   * \return yaw moment of inertia of the agent
   */
  double GetMomentInertiaYaw() const { return momentInertiaYaw; }

  /*!
   * \brief GetFrictionCoeff function to get coefficient of friction
   *
   * \return coefficient of friction
   */
  double GetFrictionCoeff() const { return frictionCoeff; }

  /*!
   * \brief GetTrackWidth function to get track width
   *
   * \return track width of the agent
   */
  units::length::meter_t GetTrackWidth() const { return trackWidth; }

  /*!
   * \brief GetDistanceCOGtoLeadingEdge function to get distance from COG to LeadingEdge
   *
   * \return distance of COG to LeadingEdge of the agent
   */
  units::length::meter_t GetDistanceCOGtoLeadingEdge() const { return distanceCOGtoLeadingEdge; }

  /*!
   * \brief SetVehicleClassification function to set vehicle classification
   *
   * \param vehicleClassification classification of the agent
   */
  void SetVehicleClassification(mantle_api::VehicleClass vehicleClassification)
  {
    this->vehicleClassification = vehicleClassification;
  }

  /*!
   * \brief SetWidth function to set width
   *
   * \param width width of the agent
   */
  void SetWidth(units::length::meter_t width) { this->width = width; }

  /*!
   * \brief SetLength function to length vehicle type
   *
   * \param length length of the agent
   */
  void SetLength(units::length::meter_t length) { this->length = length; }

  /*!
   * \brief SetDistanceCOGtoFrontAxle function to set distance from COG to FrontAxle type
   *
   * \param distanceCOGtoFrontAxle distance from COG to FrontAxle of the agent
   */
  void SetDistanceCOGtoFrontAxle(units::length::meter_t distanceCOGtoFrontAxle)
  {
    this->distanceCOGtoFrontAxle = distanceCOGtoFrontAxle;
  }

  /*!
   * \brief SetWeight function to set weight
   *
   * \param weight of the agent
   */
  void SetWeight(units::mass::kilogram_t weight) { this->weight = weight; }

  /*!
   * \brief SetHeightCOG function to set height of COG
   *
   * \param heightCOG height of COG
   */
  void SetHeightCOG(units::length::meter_t heightCOG) { this->heightCOG = heightCOG; }

  /*!
   * \brief SetWheelbase function to set wheel base
   *
   * \param wheelbase wheel base of the agent
   */
  void SetWheelbase(units::length::meter_t wheelbase) { this->wheelbase = wheelbase; }

  /*!
   * \brief SetMomentInertiaRoll function to set rolling moment of inertia
   *
   * \param momentInertiaRoll rolling moment of inertia of the agent
   */
  void SetMomentInertiaRoll(double momentInertiaRoll) { this->momentInertiaRoll = momentInertiaRoll; }

  /*!
   * \brief SetMomentInertiaPitch function to set pitch moment of inertia
   *
   * \param momentInertiaPitch pitch moment of inertia
   */
  void SetMomentInertiaPitch(double momentInertiaPitch) { this->momentInertiaPitch = momentInertiaPitch; }

  /*!
   * \brief SetMomentInertiaYaw function to set yaw moment of inertia
   *
   * \param momentInertiaYaw yaw moment of inertia of the agent
   */
  void SetMomentInertiaYaw(double momentInertiaYaw) { this->momentInertiaYaw = momentInertiaYaw; }

  /*!
   * \brief SetFrictionCoeff function to set coefficient of friction
   *
   * \param frictionCoeff coefficient of friction
   */
  void SetFrictionCoeff(double frictionCoeff) { this->frictionCoeff = frictionCoeff; }

  /*!
   * \brief SetTrackWidth function to set track width
   *
   * \param trackWidth track width of the agent
   */
  void SetTrackWidth(units::length::meter_t trackWidth) { this->trackWidth = trackWidth; }

  /*!
   * \brief SetDistanceCOGtoLeadingEdge function to set distance of COG to leading edge
   *
   * \param distanceCOGtoLeadingEdge distance of COG to the leading edge of the agent
   */
  void SetDistanceCOGtoLeadingEdge(units::length::meter_t distanceCOGtoLeadingEdge)
  {
    this->distanceCOGtoLeadingEdge = distanceCOGtoLeadingEdge;
  }

private:
  int id;
  int reference;
  mantle_api::VehicleClass vehicleClassification;
  units::length::meter_t positionX;
  units::length::meter_t positionY;
  units::length::meter_t width;
  units::length::meter_t length;
  double velocityX;
  double velocityY;
  units::length::meter_t distanceCOGtoFrontAxle;
  units::mass::kilogram_t weight;
  units::length::meter_t heightCOG;
  units::length::meter_t wheelbase;
  double momentInertiaRoll;
  double momentInertiaPitch;
  double momentInertiaYaw;
  double frictionCoeff;
  units::length::meter_t trackWidth;
  units::length::meter_t distanceCOGtoLeadingEdge;
  double accelerationX;
  double accelerationY;
  double yawAngle;
};

struct PostCrashVelocity  ///< Post crach velocity parameters
{
  bool isActive = false;                                           //!< activity flag
  units::velocity::meters_per_second_t velocityAbsolute{0.0};      //!< post crash velocity, absolute [m/s]
  units::angle::radian_t velocityDirection{0.0};                   //!< post crash velocity direction [rad]
  units::angular_velocity::radians_per_second_t yawVelocity{0.0};  //!< post crash yaw velocity [rad/s]
};

/*!
 * For definitions see http://indexsmart.mirasmart.com/26esv/PDFfiles/26ESV-000177.pdf
 */
struct CollisionAngles
{
  units::angle::radian_t OYA{0.0};    //!< opponent yaw angle
  units::angle::radian_t HCPAo{0.0};  //!< original host collision point angle
  units::angle::radian_t OCPAo{0.0};  //!< original opponent collision point angle
  units::angle::radian_t HCPA{0.0};   //!< transformed host collision point angle
  units::angle::radian_t OCPA{0.0};   //!< transformed opponent collision point angle
};

class AgentInterface;
/// list of agent interface pointers
using AgentInterfaces = std::vector<const AgentInterface *>;

//! This struct contains all values regarding the dynamic calculation of the agent
struct DynamicsInformation
{
  units::acceleration::meters_per_second_squared_t acceleration{NAN};  //!< Acceleration of the agent
  units::acceleration::meters_per_second_squared_t centripetalAcceleration{
      NAN};                                                             //!< Centripetal acceleration of the agent
  units::acceleration::meters_per_second_squared_t accelerationX{NAN};  //!< Acceleration of the agent in x direction
  units::acceleration::meters_per_second_squared_t accelerationY{NAN};  //!< Acceleration of the agent in y direction
  units::velocity::meters_per_second_t velocityX{0.0};                  //!< Velocity of the agent in x direction
  units::velocity::meters_per_second_t velocityY{0.0};                  //!< Velocity of the agent in y direction
  units::length::meter_t positionX{0.0};                                //!< x-position of agent
  units::length::meter_t positionY{0.0};                                //!< y-position of agent
  units::angle::radian_t yaw{0.0};                                      //!< Yaw angle of agent
  units::angular_velocity::radians_per_second_t yawRate{0.0};           //!< Yaw rate of the agent
  units::angular_acceleration::radians_per_second_squared_t yawAcceleration{0.0};  //!< Yaw acceleration of the agent
  units::angle::radian_t roll{0.0};                                                //!< Roll angle of agent
  units::angle::radian_t pitch{0.0};                                               //!< Pitch angle of agent
  units::angle::radian_t steeringWheelAngle{0.0};  //!< New angle of the steering wheel angle
  std::vector<units::angular_velocity::radians_per_second_t> wheelRotationRate;  //!< Wheel rotation rate
  std::vector<units::angle::radian_t> wheelRoll;                                 //!< Roll angle of wheels
  std::vector<units::angle::radian_t> wheelPitch;                                //!< Pitch angle of wheels
  std::vector<units::angle::radian_t> wheelYaw;                                  //!< Yaw angle of wheels
  units::length::meter_t travelDistance{0.0};  //!< Distance traveled by the agent during this timestep
};
