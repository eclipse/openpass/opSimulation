/********************************************************************************
 * Copyright (c) 2018 AMFD GmbH
 *               2017-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  sampler.h
//! @brief This file contains the Sampler, which samples parameters and
//!  probabilities.
//-----------------------------------------------------------------------------

#pragma once

#include <stdexcept>
#include <vector>

#include "common/opExport.h"
#include "common/stochasticDefinitions.h"
#include "include/stochasticsInterface.h"

//-----------------------------------------------------------------------------
/** \brief Sampler samples probabilities and SpawnPoint and World parameters.
 *   \details This class contains all stochastics functions of the SpawnPoint and the world
 *               It samples parameters according to probabilities specified in the configurations
 *
 */
//-----------------------------------------------------------------------------
namespace Sampler
{
/// @brief Make a coin flip with given probability
/// @param chance probability of true
/// @param stochastics Pointer to the stochastics interface
/// @return result of coin flip
OPENPASSCOMMONEXPORT bool RollFor(double chance, StochasticsInterface* stochastics);

/// @brief Roll for the value of a stochastic attribute
/// @param distribution stochastic distribution of the value
/// @param stochastics Pointer to the stochastics interface
/// @return rolled value
OPENPASSCOMMONEXPORT double RollForStochasticAttribute(const openpass::parameter::StochasticDistribution& distribution,
                                                       StochasticsInterface* stochastics);

/// @brief  Roll for a random value from a set of weighted values
/// @tparam T               value type
/// @param weightedValues   values with weigths
/// @param stochastics      Pointer to the stochastics interface
/// @return rolled value
template <typename T>
T Sample(std::vector<std::pair<T, double>> weightedValues, StochasticsInterface* stochastics)
{
  double sumOfWeights{0.0};
  for (const auto& [value, weight] : weightedValues)
  {
    sumOfWeights += weight;
  }
  auto roll = stochastics->GetUniformDistributed(0, sumOfWeights);
  for (const auto& [value, weight] : weightedValues)
  {
    roll -= weight;
    if (roll <= 0)
    {
      return value;
    }
  }
  throw std::runtime_error("Invalid roll in Sampler");
}

};  //namespace Sampler
