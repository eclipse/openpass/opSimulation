/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @brief Transport a vector of strings
//-----------------------------------------------------------------------------

#pragma once

#include "include/modelInterface.h"

//! @brief Transport a vector of strings
class StringSignal : public ComponentStateSignalInterface
{
public:
  /// component name
  static constexpr const char* COMPONENTNAME{"StringSignal"};

  /// constructor
  StringSignal() : payload{} { componentState = ComponentState::Disabled; }

  /**
   * @brief Construct a new String Signal object
   *
   * @param other Another string signal
   */
  StringSignal(StringSignal& other) : StringSignal(other.componentState, other.payload) {}

  /**
   * @brief Construct a new String Signal object
   *
   * @param componentState State of the component
   * @param payload        Pay load
   */
  StringSignal(ComponentState componentState, const std::string payload) : payload{std::move(payload)}
  {
    this->componentState = componentState;
  }

  virtual ~StringSignal() = default;

  //! @brief Conversion method for printing
  //! @return Payload of the signal as string
  virtual operator std::string() const { return payload; }

  /// payload
  const std::string payload;
};
