/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "DetectedObject.h"

#include <utility>

const WorldObjectInterface *DetectedObject::GetWorldObject() const
{
  return worldObject;
}

void DetectedObject::SetSensorMetadata(std::string typeOfSensor, int Id)
{
  sensorType = std::move(typeOfSensor);
  sensorId = Id;
}

std::string DetectedObject::GetSensorType() const
{
  return sensorType;
}

int DetectedObject::GetSensorId() const
{
  return sensorId;
}

bool operator==(const DetectedObject &dObj1, const DetectedObject &dObj2)
{
  //compare members
  if (!(dObj1.worldObject == dObj2.worldObject)) return false;

  if (!(dObj1.sensorType == dObj2.sensorType && dObj1.sensorId == dObj2.sensorId)) return false;

  return true;
}
