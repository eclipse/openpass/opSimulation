/********************************************************************************
 * Copyright (c) 2019 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <cstdint>
#include <filesystem>
#include <functional>
#include <string>

namespace openpass::common
{

using SetCurrentRunFct = std::function<void(uint64_t)>;
using SetTotalRunsFct = std::function<void(uint64_t)>;

/// @brief RuntimeInformation
struct RuntimeInformation
{
  uint64_t currentRun{};  ///< Current run number
  uint64_t totalRuns{};   ///< Total number of runs

  /// directory informations
  struct Directories
  {
    /// Target of the /proc/self/exe symlink on Linux, canonical path of executable on Windows
    std::string executionPath;
    /// configuration directory
    std::string configuration;
    /// output directory
    std::string output;
    /// library directory
    std::string library;
  } directories;  ///< directories information
};

inline std::string GetRunIdWithLeadingZeros(const openpass::common::RuntimeInformation& runtimeInformation)
{
  auto runPrefixLength = std::to_string(runtimeInformation.totalRuns - 1).length()
                       - std::to_string(runtimeInformation.currentRun).length();
  std::string runPrefix(runPrefixLength, '0');
  return runPrefix + std::to_string(runtimeInformation.currentRun);
}

inline std::filesystem::path GenerateEntityOutputDir(const openpass::common::RuntimeInformation& runtimeInformation,
                                                     const std::string& entityId)
{
  return std::filesystem::path(runtimeInformation.directories.output)
       / ("run" + openpass::common::GetRunIdWithLeadingZeros(runtimeInformation)) / ("entity" + entityId);
}

}  // namespace openpass::common
