/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! \file  compCtrlToAgentCompSignal.h
//! \brief This file contains all functions for class CompCtrlToAgentCompSignal
//!
//! This class contains all functionality of the module.
//-----------------------------------------------------------------------------

#pragma once

#include <map>
#include <string>

#include "common/driverWarning.h"
#include "common/globalDefinitions.h"
#include "include/signalInterface.h"

/// using warnings as a map of string and list of component warning information
using Warnings = std::map<std::string, std::vector<ComponentWarningInformation>>;

//! Information about the current state of a vehicle component
struct VehicleComponentState
{
  ComponentType componentType;    //!< type of the component
  ComponentState componentState;  //!< current state of the component
  MovementDomain movementDomain;  //!< current movement domain of the component
};

/*!
 * --------------------------------------------------------------------------------
 * \brief The CompCtrlToAgentCompSignal class facilitates communication with the
 *        agent component, defining the max reachable state of the component as
 *        well as the states of other components connected to the
 *        ComponentController
 * --------------------------------------------------------------------------------
 */
class CompCtrlToAgentCompSignal : public SignalInterface
{
public:
  /// component name
  static constexpr const char* COMPONENTNAME{"CompCtrlToAgentCompSignal"};
  /**
   * @brief Construct a new Comp Ctrl To Agent Comp Signal object
   *
   * @param maxReachableState         maximum reachable state of the component
   * @param vehicleComponentStates    vehicle component states
   */
  CompCtrlToAgentCompSignal(const ComponentState& maxReachableState,
                            const std::map<std::string, VehicleComponentState>& vehicleComponentStates)
      : maxReachableState(maxReachableState), vehicleComponentStates(vehicleComponentStates)
  {
  }

  CompCtrlToAgentCompSignal() = delete;
  CompCtrlToAgentCompSignal(const CompCtrlToAgentCompSignal&) = delete;
  CompCtrlToAgentCompSignal(CompCtrlToAgentCompSignal&&) = delete;
  CompCtrlToAgentCompSignal& operator=(const CompCtrlToAgentCompSignal&) = delete;
  CompCtrlToAgentCompSignal& operator=(CompCtrlToAgentCompSignal&&) = delete;

  virtual ~CompCtrlToAgentCompSignal() = default;

  //-----------------------------------------------------------------------------
  //! Returns the content/payload of the signal as an std::string
  //!
  //! \return     Content/payload of the signal as an std::string
  //-----------------------------------------------------------------------------
  virtual operator std::string() const { return ""; }

  /*!
   * \brief GetMaxReachableState returns the max reachable state of the target component as defined by the ComponentController
   *
   * \return The max reachable state of the target component as defined by the ComponentController as a ComponentState
   */
  ComponentState GetMaxReachableState() const { return maxReachableState; }

  /*!
   * \brief GetVehicleComponentStates returns a map linking vehicle component names to types and states
   *
   * \return A map of vehicle component names to pairs of types and states as std::map<std::string, std::pair<AdasType, ComponentState>>
   */
  const std::map<std::string, VehicleComponentState>& GetVehicleComponentStates() const
  {
    return vehicleComponentStates;
  }

private:
  const ComponentState maxReachableState;                                     //!< Highest reachable state
  const std::map<std::string, VehicleComponentState> vehicleComponentStates;  //!< Current state of all components
};

/*!
 * --------------------------------------------------------------------------------
 * \brief The CompCtrlToDriverCompSignal class facilitates communication with the
 *        driver component, allowing for warnings to be forwarded to the driver
 *        alongside standard agent component information.
 * --------------------------------------------------------------------------------
 */
class CompCtrlToDriverCompSignal : public CompCtrlToAgentCompSignal
{
public:
  /// component name
  static constexpr const char* COMPONENTNAME{"CompCtrlToDriverCompSignal"};
  /**
   * @brief Construct a new Comp Ctrl To Driver Comp Signal object
   *
   * @param maxReachableState         Maximum reachable state
   * @param vehicleComponentStates    Vehicle component states
   * @param warnings                  Warnings
   */
  CompCtrlToDriverCompSignal(const ComponentState& maxReachableState,
                             const std::map<std::string, VehicleComponentState>& vehicleComponentStates,
                             const Warnings& warnings)
      : CompCtrlToAgentCompSignal(maxReachableState, vehicleComponentStates), warnings(warnings)
  {
  }

  CompCtrlToDriverCompSignal() = delete;
  CompCtrlToDriverCompSignal(const CompCtrlToDriverCompSignal&) = delete;
  CompCtrlToDriverCompSignal(CompCtrlToDriverCompSignal&&) = delete;
  CompCtrlToDriverCompSignal& operator=(const CompCtrlToDriverCompSignal&) = delete;
  CompCtrlToDriverCompSignal& operator=(CompCtrlToDriverCompSignal&&) = delete;
  virtual ~CompCtrlToDriverCompSignal() = default;

  /*!
   * \brief GetWarnings gets all warnings from the signal (if any exist), by component name
   *
   * \return all warnings for the driver known by the ComponentController (if any exist), by component name
   */
  const Warnings& GetWarnings() const { return warnings; }

private:
  const Warnings warnings;  //!< Contains all warnings for the driver known by the ComponentController (if any exist) by
                            //!< component name
};
