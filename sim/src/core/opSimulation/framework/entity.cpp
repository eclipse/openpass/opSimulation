/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "entity.h"

#include <MantleAPI/Common/bounding_box.h>
#include <MantleAPI/Common/route_definition.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/i_entity.h>
#include <utility>
#include <variant>

#include "common/log.h"
#include "common/vector2d.h"
#include "framework/geometryHelper.h"
#include "framework/routeSampler.h"
#include "include/agentInterface.h"
#include "include/egoAgentInterface.h"
#include "include/scenarioControlInterface.h"
#include "modelElements/scenarioControl.h"

namespace core
{
Entity::Entity(mantle_api::UniqueId id,
               std::string name,
               const RouteSamplerInterface *routeSampler,
               std::shared_ptr<mantle_api::EntityProperties> properties,
               AgentCategory agentCategory)
    : id(id),
      name(std::move(name)),
      routeSampler(routeSampler),
      properties(std::move(properties)),
      agentCategory(agentCategory),
      scenarioControl(std::make_shared<ScenarioControl>())
{
}

mantle_api::UniqueId Entity::GetUniqueId() const
{
  return id;
}

void Entity::SetName(const std::string &name)
{
  this->name = name;
}

const std::string &Entity::GetName() const
{
  return name;
}

void Entity::SetPosition(const mantle_api::Vec3<units::length::meter_t> &inert_pos)
{
  GeometryHelper geometryHelper;
  if (!agent)
  {
    auto position = geometryHelper.TranslateGlobalPositionLocally(
        inert_pos, spawnParameter.orientation, -properties->bounding_box.geometric_center);
    spawnParameter.position = position;
    spawnParameter.route = routeSampler->Sample(inert_pos, spawnParameter.orientation.yaw);
    shouldBeSpawned = true;
  }
  else
  {
    auto position = geometryHelper.TranslateGlobalPositionLocally(
        inert_pos, {agent->GetYaw(), 0_rad, 0_rad}, -properties->bounding_box.geometric_center);
    agent->SetPositionX(position.x);
    agent->SetPositionY(position.y);
  }
}

mantle_api::Vec3<units::length::meter_t> Entity::GetPosition() const
{
  GeometryHelper geometryHelper;
  if (!agent)
  {
    return geometryHelper.TranslateGlobalPositionLocally(
        spawnParameter.position, spawnParameter.orientation, properties->bounding_box.geometric_center);
  }
  return geometryHelper.TranslateGlobalPositionLocally({agent->GetPositionX(), agent->GetPositionY(), 0_m},
                                                       {agent->GetYaw(), 0_rad, 0_rad},
                                                       properties->bounding_box.geometric_center);
}

void Entity::SetVelocity(const mantle_api::Vec3<units::velocity::meters_per_second_t> &velocity)
{
  if (!agent)
  {
    spawnParameter.velocity = velocity.Length();
  }
  else
  {
    agent->SetVelocityVector(velocity);
  }
}

mantle_api::Vec3<units::velocity::meters_per_second_t> Entity::GetVelocity() const
{
  if (!agent)
  {
    return {spawnParameter.velocity * units::math::cos(spawnParameter.orientation.yaw),
            spawnParameter.velocity * units::math::sin(spawnParameter.orientation.yaw),
            0_mps};
  }
  const auto velocity = agent->GetVelocity();
  return {velocity.x, velocity.y, 0_mps};
}

void Entity::SetAcceleration(const mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> &acceleration)
{
  if (!agent)
  {
    // spawn acceleration not implemented
  }
  else
  {
    // TODO (revised on 12.10.2023): Sets the magnitude of the given components x, y which
    //      is "good enough" for the current use case. If keeping of the individual components
    //      is necessary, SetAccelerationX and SetAccelerationY needs to be implemented.
    const auto absAcceleration = units::math::hypot(acceleration.x, acceleration.y);
    agent->SetAcceleration(absAcceleration);
  }
}

mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> Entity::GetAcceleration() const
{
  if (!agent)
  {
    return {0_mps_sq, 0_mps_sq, 0_mps_sq};
  }
  const auto acceleration = agent->GetAcceleration();
  return {acceleration.x, acceleration.y, 0_mps_sq};
}

void Entity::SetOrientation(const mantle_api::Orientation3<units::angle::radian_t> &orientation)
{
  if (!agent)
  {
    GeometryHelper geometryHelper;
    auto referencePosition = geometryHelper.TranslateGlobalPositionLocally(
        spawnParameter.position, spawnParameter.orientation, properties->bounding_box.geometric_center);
    auto position = geometryHelper.TranslateGlobalPositionLocally(
        referencePosition, orientation, -properties->bounding_box.geometric_center);
    spawnParameter.position = position;
    spawnParameter.orientation = orientation;
  }
  else
  {
    agent->SetYaw(orientation.yaw);
  }
}

mantle_api::Orientation3<units::angle::radian_t> Entity::GetOrientation() const
{
  if (!agent)
  {
    return spawnParameter.orientation;
  }
  return {agent->GetYaw(), 0_rad, 0_rad};
}

void Entity::SetOrientationRate(
    const mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> &orientation_rate)
{
  if (!agent)
  {
  }
  else
  {
    agent->SetYawRate(orientation_rate.yaw);
  }
}

mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> Entity::GetOrientationRate() const
{
  if (!agent)
  {
    return {0_rad_per_s, 0_rad_per_s, 0_rad_per_s};
  }
  return {agent->GetYawRate(), 0_rad_per_s, 0_rad_per_s};
}

void Entity::SetOrientationAcceleration(
    const mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t> &orientation_acceleration)
{
  if (!agent)
  {
  }
  else
  {
    agent->SetYawAcceleration(orientation_acceleration.yaw);
  }
}

mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t> Entity::GetOrientationAcceleration()
    const
{
  if (!agent)
  {
    return {0_rad_per_s_sq, 0_rad_per_s_sq, 0_rad_per_s_sq};
  }
  return {agent->GetYawAcceleration(), 0_rad_per_s_sq, 0_rad_per_s_sq};
}

void Entity::SetAssignedLaneIds(const std::vector<uint64_t> &assigned_lane_ids) {}

std::vector<uint64_t> Entity::GetAssignedLaneIds() const
{
  return {};
}

void Entity::SetVisibility(const mantle_api::EntityVisibilityConfig &visibility) {}

mantle_api::EntityVisibilityConfig Entity::GetVisibility() const
{
  return {};
}

void Entity::SetSystem(const System &system)
{
  this->system = system;
}

AgentBuildInstructions Entity::GetAgentBuildInstructions()
{
  return {name, agentCategory, properties, system, spawnParameter, scenarioControl};
}

void Entity::SetAgent(AgentInterface *agent)
{
  this->agent = agent;
}

std::shared_ptr<ScenarioControlInterface> const Entity::GetScenarioControl() const
{
  return scenarioControl;
}

bool Entity::ShouldBeSpawned()
{
  return shouldBeSpawned;
}

void Entity::AssignRoute(const mantle_api::RouteDefinition &routeDefinition)
{
  std::static_pointer_cast<ScenarioControl>(scenarioControl)->AddCommand(ScenarioCommand::AssignRoute{routeDefinition});
  if (!agent)
  {
    spawnParameter.route = routeSampler->CalculateRouteFromWaypoints(routeDefinition.waypoints);
    spawnParameter.routeDefinition = routeDefinition;
  }
  else
  {
    auto egoRoute = routeSampler->CalculateRouteFromWaypoints(routeDefinition.waypoints);
    agent->GetEgoAgent().SetRoadGraph(std::move(egoRoute.roadGraph), egoRoute.root, egoRoute.target);
  }
}

bool Entity::IsAgentInWorld()
{
  if (!agent)
  {
    return true;  //Don't delete if not spawned yet
  }
  return agent->IsAgentInWorld();
}

void Entity::SetVehicleLightState(const mantle_api::LightState &lightState, const mantle_api::LightType &lightType)
{
  const auto &lightMode = lightState.light_mode;
  auto indicatorState = std::visit(
      [&lightMode](auto &&lightType)
      {
        using T = std::decay_t<decltype(lightType)>;

        if constexpr (std::is_same_v<T, mantle_api::VehicleLightType>)
        {
          switch (lightType)
          {
            case mantle_api::VehicleLightType::kIndicatorLeft:
              return lightMode == mantle_api::LightMode::kOn ? IndicatorState::IndicatorState_Left
                                                             : IndicatorState::IndicatorState_Off;
            case mantle_api::VehicleLightType::kIndicatorRight:
              return lightMode == mantle_api::LightMode::kOn ? IndicatorState::IndicatorState_Right
                                                             : IndicatorState::IndicatorState_Off;
            case mantle_api::VehicleLightType::kWarningLights:
              return lightMode == mantle_api::LightMode::kOn ? IndicatorState::IndicatorState_Warn
                                                             : IndicatorState::IndicatorState_Off;
            default:
              LogErrorAndThrow(
                  "Unsupported vehicle light type in LightStateAction. Only indicatorLeft, indicatorRight and "
                  "warningLights are supported");
              return IndicatorState::IndicatorState_Off;
          }
        }
        LogErrorAndThrow(
            "Unsupported vehicle light type in LightStateAction. Only indicatorLeft, indicatorRight and "
            "warningLights are supported");
        return IndicatorState::IndicatorState_Off;
      },
      lightType);

  if (!agent)
  {
    spawnParameter.indicatorState = indicatorState;
  }
  else
  {
    agent->SetIndicatorState(indicatorState);
  }
}
}  // namespace core
