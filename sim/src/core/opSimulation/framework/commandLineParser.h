/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2017-2019 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <boost/program_options.hpp>
#include <string>
#include <vector>

#include "common/opExport.h"

namespace po = boost::program_options;

/// Information about command line arguments
struct SIMULATIONCOREEXPORT CommandLineArguments
{
  bool clearResultsPath{false};             ///< Clear results path
  int logLevel{0};                          ///< Level of log
  std::string libPath{SUBDIR_LIB_MODULES};  ///< Path to the simulator libraries
  std::string logFile{"opSimulation.log"};  ///< Name of log file
  std::string configsPath{"configs"};       ///< Path to configs
  std::string resultsPath{"results"};       ///< Path to store results
};

/// Class to parse command line
class SIMULATIONCOREEXPORT CommandLineParser
{
public:
  /// @brief Parse the command line arguments
  /// @param argc Number of arguments
  /// @param argv Argument vector
  /// @param version Version string
  /// @return Command line arguments
  static CommandLineArguments Parse(int argc, char* argv[], const std::string& version);
};
