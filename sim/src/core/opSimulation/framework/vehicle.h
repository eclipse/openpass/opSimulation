/*******************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/i_entity.h>
#include <memory>
#include <string>

#include "common/globalDefinitions.h"
#include "framework/entity.h"

class RouteSamplerInterface;

namespace core
{

//! This class represents a vehicle in a scenario
class Vehicle : public Entity, public mantle_api::IVehicle
{
public:
  //! Vehicle constructor
  //!
  //! @param[in] id           Unique id
  //! @param[in] name         Name of the vehicle
  //! @param[in] properties   Additional properties that describe scenario vehicle
  //! @param[in] routeSampler Route sampler
  //! @param[in] category     Agent category classification
  Vehicle(mantle_api::UniqueId id,
          const std::string &name,
          const std::shared_ptr<mantle_api::VehicleProperties> &properties,
          const RouteSamplerInterface *routeSampler,
          AgentCategory category);

  /// @brief Sets the properties that describe scenario vehicle
  ///
  /// @param properties Basic properties that describe scenario vehicle
  void SetProperties(std::unique_ptr<mantle_api::EntityProperties> properties) override;
  mantle_api::VehicleProperties *GetProperties() const override;

  void SetIndicatorState(mantle_api::IndicatorState state) override;
  mantle_api::IndicatorState GetIndicatorState() const override;

private:
  std::shared_ptr<mantle_api::VehicleProperties> properties;
  mantle_api::IndicatorState indicatorState{mantle_api::IndicatorState::kUnknown};
};
}  // namespace core
