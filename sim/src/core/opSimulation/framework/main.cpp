/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <chrono>
#include <filesystem>
#include <iostream>
#include <string>
#include <system_error>
#include <unistd.h>
#include <unordered_map>

#include "common/callbacks.h"
#include "common/log.h"
#include "common/runtimeInformation.h"
#include "common/version.h"
#include "compatibility.h"
#include "framework/commandLineParser.h"
#include "framework/configurationContainer.h"
#include "framework/directories.h"
#include "framework/frameworkModuleContainer.h"
#include "framework/runInstantiator.h"
#include "importer/configurationFiles.h"
#include "importer/frameworkModules.h"
#include "include/agentTypeInterface.h"
#include "include/simulationConfigInterface.h"

using namespace core;

//-----------------------------------------------------------------------------
//! \brief SetupLogging Sets up the logging
//! \param[in] executionPath path of the executable (ignored if logFileString is absolute)
//! \param[in] logLevel 0 (none) to 5 (debug core)
//! \param[in] logFileString  name/path of the logfile
//-----------------------------------------------------------------------------
static void SetupLogging(const std::filesystem::path& executionPath,
                         LogLevel logLevel,
                         const std::string& logFileString);

//-----------------------------------------------------------------------------
//! \brief  Check several parameters of a directory
//! \param  directory
//! \param  prefix   for readable logging in case of an error
//! \return true, if everything directory exists and really is a directory, false otherwise
//-----------------------------------------------------------------------------
static bool CheckDir(const std::string& directory, const std::string& prefix);

//-----------------------------------------------------------------------------
//! \brief  Check several parameters of a readable directory
//! \param  directory
//! \param  prefix   for readable logging in case of an error
//! \return true, if \c CheckDir returns true and the directory is readable, false otherwise
//-----------------------------------------------------------------------------
static bool CheckReadableDir(const std::string& directory, const std::string& prefix);

//-----------------------------------------------------------------------------
//! \brief  Check several parameters of a writeable directory
//! \param  directory
//! \param  prefix   for readable logging in case of an error
//! \return true, if \c CheckDir returns true and the directory is writable, false otherwise
//-----------------------------------------------------------------------------
static bool CheckWritableDir(const std::string& directory, const std::string& prefix);

/// @brief Prepare directories and logging
/// @param directories      Directories use by the simulator
/// @param clearResultsPath Clear results path before simulation if set
/// @param logLevel         Selected Loglevel
/// @param logFileString    Path to the logfile
/// @returns true on success, false on error
static bool PrepareDirectoriesAndLogging(const openpass::core::Directories& directories,
                                         bool clearResultsPath,
                                         LogLevel logLevel,
                                         const std::string& logFileString);

//-----------------------------------------------------------------------------
//! \brief   Get the absolute directory of the given execuable
//! \param   executable Used for resolution on windows, as a fallback on Linux
//! \return  Target of the /proc/self/exe symlink on Linux, canonical path of executable on Windows.
//-----------------------------------------------------------------------------
static std::filesystem::path ResolveExecutionPath(const char* executable);

//-----------------------------------------------------------------------------
//! Entry point of program.
//!
//! Setup of program parameters and framework infrastructure.
//! Startup of servers.
//!
//! @param[in]     argc    number of program arguments
//! @param[in]     argv    program arguments
//! @return                program error code / result
//-----------------------------------------------------------------------------
int main(int argc, char* argv[])  // NOLINT(cppcoreguidelines-avoid-c-arrays, modernize-avoid-c-arrays)
{
  const auto opsimulationStart = std::chrono::high_resolution_clock::now();
  const auto parsedArguments = CommandLineParser::Parse(argc, argv, openpass::common::Version::framework.str());

  const auto executionPath = ResolveExecutionPath(argv[0]);  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)

  openpass::core::Directories directories(
      executionPath.string(), parsedArguments.libPath, parsedArguments.configsPath, parsedArguments.resultsPath);
  if (!PrepareDirectoriesAndLogging(directories,
                                    parsedArguments.clearResultsPath,
                                    static_cast<LogLevel>(parsedArguments.logLevel),
                                    parsedArguments.logFile))
  {
    exit(EXIT_FAILURE);
  }

  ConfigurationFiles configurationFiles{
      directories.configurationDir, "systemConfigBlueprint.xml", "simulationConfig.xml"};
  openpass::common::RuntimeInformation runtimeInformation;
  runtimeInformation.directories
      = {executionPath.string(), directories.configurationDir, directories.outputDir, directories.libraryDir};

  Configuration::ConfigurationContainer configurationContainer(configurationFiles, runtimeInformation);
  if (!configurationContainer.ImportAllConfigurations(executionPath))
  {
    LOG_INTERN(LogLevel::Error) << "Failed to import all configurations";
    exit(EXIT_FAILURE);
  }

  const auto& libraries = configurationContainer.GetSimulationConfig()->GetExperimentConfig().libraries;
  FrameworkModules frameworkModules{parsedArguments.logLevel,
                                    directories.libraryDir,
                                    libraries.at("DataBufferLibrary"),
                                    configurationContainer.GetSimulationConfig()->GetObservationConfig(),
                                    libraries.at("StochasticsLibrary"),
                                    libraries.at("WorldLibrary"),
                                    configurationContainer.GetSimulationConfig()->GetSpawnPointsConfig()};

  SimulationCommon::Callbacks callbacks;
  FrameworkModuleContainer frameworkModuleContainer(frameworkModules, runtimeInformation, &callbacks);

  auto setCurrentRun = [&runtimeInformation](uint64_t run) { runtimeInformation.currentRun = run; };
  auto setTotalRuns = [&runtimeInformation](uint64_t runs) { runtimeInformation.totalRuns = runs; };
  RunInstantiator runInstantiator(
      configurationContainer, frameworkModuleContainer, frameworkModules, setCurrentRun, setTotalRuns);

  if (runInstantiator.ExecuteRun())
  {
    LOG_INTERN(LogLevel::DebugCore) << "simulation finished successfully";
  }
  else
  {
    LOG_INTERN(LogLevel::Error) << "simulation failed";
    exit(EXIT_FAILURE);
  }

  const auto opsimulationEnd = std::chrono::high_resolution_clock::now();
  const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(opsimulationEnd - opsimulationStart);

  std::cout << "Simulation time elapsed: " << duration.count() << " ms" << std::endl;
  LOG_INTERN(LogLevel::DebugCore) << "Simulation time elapsed: " << duration.count() << " ms";
  LOG_INTERN(LogLevel::DebugCore) << "end simulation";

#ifdef CROSS_COMPILING
  // Code specific to cross-compiling
  _exit(0);
#else
  return (0);
#endif
}

std::filesystem::path ResolveExecutionPath(const char* executable)
{
#ifndef WIN32
  try
  {
    return std::filesystem::canonical("/proc/self/exe").parent_path();
  }
  catch (const std::filesystem::filesystem_error&)
#endif
  {
    return std::filesystem::canonical(executable).parent_path();
  }
}

void SetupLogging(const std::filesystem::path& executionPath, LogLevel logLevel, const std::string& logFileString)
{
  namespace fs = std::filesystem;
  const auto logFile = fs::path(logFileString);
  const auto logPath = logFile.is_absolute()  //
                         ? logFile
                         : executionPath / logFile;

  if (!fs::exists(logPath.parent_path()))
  {
    std::error_code errorCode;
    fs::create_directories(logPath.parent_path(), errorCode);

    if (errorCode)
    {
      std::cerr << "Unable to create directory for log file: " << errorCode.message() << std::endl;
      exit(EXIT_FAILURE);
    }
  }

  // Set the log file path using LogOutputPolicy
  LogOutputPolicy::SetFile(logPath.string());
  LogFile::ReportingLevel() = logLevel;
  LOG_INTERN(LogLevel::DebugCore) << "\n\n### simulation start ##";
}

/// @brief Removes all files and directories in the given directory
/// @param dir path to the directory, which shall be cleared (recursive)
/// @returns true on success, false on errors (reported to cerr)
bool ClearDir(const std::filesystem::path& dir) [[nothrow]]
{
  namespace fs = std::filesystem;

  auto reportError = [](const fs::path& dir, const std::string& additionalInfo)
  { std::cerr << "Failed to clean \"" << dir.string() << "\": " << additionalInfo << '\n'; };

  try
  {
    for (const auto& entry : fs::directory_iterator(dir))
    {
      fs::remove_all(entry.path());
    }
    if (!fs::is_empty(dir))
    {
      reportError(dir, "Path not empty after calling \"remove_all\".");
      return false;
    }
  }
  catch (const fs::filesystem_error& e)
  {
    reportError(dir, e.what());
    return false;
  }

  return true;
}

bool PrepareDirectoriesAndLogging(const openpass::core::Directories& directories,
                                  bool clearResultsPath,
                                  LogLevel logLevel,
                                  const std::string& logFileString)
{
  // Setup is done after the output directory has been cleaned
  // for the case that logFileString is set to the output folder
  // itself, preventing proper cleanup of the output folder

  if (!std::filesystem::exists(directories.outputDir))
  {
    std::filesystem::create_directory(directories.outputDir);
  }

  if (clearResultsPath)
  {
    if (!ClearDir(directories.outputDir))
    {
      return false;
    }
  }

  SetupLogging(directories.baseDir, logLevel, logFileString);
  LOG_INTERN(LogLevel::DebugCore) << "base path: " << directories.baseDir;
  LOG_INTERN(LogLevel::DebugCore) << "library path: " << directories.libraryDir;
  LOG_INTERN(LogLevel::DebugCore) << "configuration path: " << directories.configurationDir;
  LOG_INTERN(LogLevel::DebugCore) << "output path: " << directories.outputDir;

  auto statusLibrary = CheckReadableDir(directories.libraryDir, "Library");
  auto statusConfig = CheckReadableDir(directories.configurationDir, "Configuration");
  auto statusOutput = CheckWritableDir(directories.outputDir, "Output");

  // return after probing, so all directories are reported at once
  return statusConfig && statusLibrary && statusOutput;
}

bool CheckDir(const std::string& directory, const std::string& prefix)
{
  namespace fs = std::filesystem;

  const fs::path dir(directory);

  if (!fs::exists(dir))
  {
    LOG_INTERN(LogLevel::Error) << prefix << " directory " << directory << " does not exist";
    return false;
  }

  if (!fs::is_directory(dir))
  {
    LOG_INTERN(LogLevel::Error) << prefix << " directory " << directory << " is not a directory";
    return false;
  }

  return true;
}

bool CheckReadableDir(const std::string& directory, const std::string& prefix)
{
  CheckDir(directory, prefix);

  if (int ret = access(directory.c_str(), R_OK))
  {
    LOG_INTERN(LogLevel::Error) << prefix << " directory " << directory << " is not readable (code " << ret << ")";
    return false;
  }

  return true;
}

bool CheckWritableDir(const std::string& directory, const std::string& prefix)
{
  CheckDir(directory, prefix);

  if (int ret = access(directory.c_str(), W_OK))
  {
    LOG_INTERN(LogLevel::Error) << prefix << " directory " << directory << " is not writable (code " << ret << ")";
    return false;
  }

  return true;
}
