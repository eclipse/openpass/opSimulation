/*******************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Traffic/i_entity.h>
#include <functional>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "common/globalDefinitions.h"
#include "common/opExport.h"
#include "include/entityRepositoryInterface.h"
#include "include/idManagerInterface.h"

class RouteSamplerInterface;
class WorldInterface;
namespace mantle_api
{
struct PedestrianProperties;
struct StaticObjectProperties;
struct VehicleProperties;
}  // namespace mantle_api

namespace core
{
class Agent;
class AgentFactoryInterface;
class Entity;
class Pedestrian;
class Vehicle;

//! This class provides CRUD functionality for scenario entities
class SIMULATIONCOREEXPORT EntityRepository final : public EntityRepositoryInterface
{
public:
  //! EntityRepository constructor
  //!
  //! @param[in] world        Pointer to the world
  //! @param[in] idManager    References the idManager that handles unique IDs
  //! @param[in] agentFactory Pointer to the agent factory
  //! @param[in] routeSampler Pointer to the route sampler
  EntityRepository(WorldInterface *world,
                   IdManagerInterface &idManager,
                   core::AgentFactoryInterface *agentFactory,
                   const RouteSamplerInterface *routeSampler);

  mantle_api::IVehicle &Create(const std::string &name, const mantle_api::VehicleProperties &properties) override;

  /// @brief Creates a new dynamic scenario entity of vehicle type
  ///
  /// @param id         Unique ID of the entity
  /// @param name       The name of the entity to be created
  /// @param properties Vehicle properties
  /// @return newly created dynamic scenario entity of vehicle type
  mantle_api::IVehicle &Create([[maybe_unused]] mantle_api::UniqueId id,
                               [[maybe_unused]] const std::string &name,
                               [[maybe_unused]] const mantle_api::VehicleProperties &properties) override;
  mantle_api::IPedestrian &Create(const std::string &name, const mantle_api::PedestrianProperties &properties) override;

  /// @brief Creates a new scenario entity of pedestrian type
  ///
  /// @param id         Unique ID of the entity
  /// @param name       The name of the entity to be created
  /// @param properties Pedestrian properties
  /// @return newly created scenario entity of pedestrian type
  mantle_api::IPedestrian &Create([[maybe_unused]] mantle_api::UniqueId id,
                                  [[maybe_unused]] const std::string &name,
                                  [[maybe_unused]] const mantle_api::PedestrianProperties &properties) override;

  /// @brief Creates a new scenario entity of static object type
  ///
  /// @param name       The name of the entity to be created
  /// @param properties Static object properties
  /// @return newly created scenario entity of static object type
  mantle_api::IStaticObject &Create([[maybe_unused]] const std::string &name,
                                    [[maybe_unused]] const mantle_api::StaticObjectProperties &properties) override;

  /// @brief Creates a new scenario entity of static object type
  ///
  /// @param id         Unique ID of the entity
  /// @param name       The name of the entity to be created
  /// @param properties Static object properties
  /// @return newly created scenario entity of static object type
  mantle_api::IStaticObject &Create([[maybe_unused]] mantle_api::UniqueId id,
                                    [[maybe_unused]] const std::string &name,
                                    [[maybe_unused]] const mantle_api::StaticObjectProperties &properties) override;

  mantle_api::IVehicle &GetHost() override;
  std::optional<std::reference_wrapper<mantle_api::IEntity>> Get(const std::string &name) override;
  std::optional<std::reference_wrapper<const mantle_api::IEntity>> Get(const std::string &name) const override;
  std::optional<std::reference_wrapper<mantle_api::IEntity>> Get(mantle_api::UniqueId id) override;
  std::optional<std::reference_wrapper<const mantle_api::IEntity>> Get(mantle_api::UniqueId id) const override;
  bool Contains(mantle_api::UniqueId id) const override;

  void Delete(const std::string &name) override;
  void Delete(mantle_api::UniqueId id) override;

  void Reset() override;

  const std::vector<std::unique_ptr<mantle_api::IEntity>> &GetEntities() const override;

  void RegisterEntityCreatedCallback(const std::function<void(mantle_api::IEntity &)> &callback) override {}
  void RegisterEntityDeletedCallback(const std::function<void(const std::string &)> &callback) override {}

  /// @brief Allows an external component to react on deletion of an entity, such as cleaning up invalid references
  /// pointing to it
  ///
  /// @param callback Function to be called when deleting an entity with UniqueId parameter
  void RegisterEntityDeletedCallback(const std::function<void(mantle_api::UniqueId)> &callback) override {}

  //! @brief Method which returns the entity by given unique ID
  //!
  //! @param[in] id   Unique ID
  //! @return The entity by given unique ID
  Entity &GetEntity(mantle_api::UniqueId id);

  //! @brief Method which returns the entity by given unique ID
  //!
  //! @param[in] id   Unique ID
  //! @return The entity by given unique ID
  const Entity &GetEntity(mantle_api::UniqueId id) const;

  bool SpawnReadyAgents() override;

  mantle_api::IVehicle &CreateCommon(const mantle_api::VehicleProperties &properties) override;

  std::vector<Agent *> ConsumeNewAgents() override;

private:
  //! @brief Emplaces an entity of given type into the internal management containers
  //!
  //! @tparam EntityType   Type of the entity to emplace
  //! @param[in] id        Unique id of the entity
  //! @param[in] name      Name of the entity
  //! @param[in] entity    The entity to emplace
  //!
  //! @returns Reference to the emplaced entity as given type
  template <typename EntityType>
  EntityType &EmplaceEntity(mantle_api::UniqueId id,
                            const std::string &name,
                            std::unique_ptr<mantle_api::IEntity> &entity)
  {
    EntityType &entityOfType = *(dynamic_cast<EntityType *>(entity.get()));

    entitiesById.insert({id, entityOfType});
    entitiesByName.insert({name, entityOfType});
    unspawnedEntities.push_back(entityOfType);

    return entityOfType;
  }

  //! @brief Creates a new vehicle
  //!
  //! @param[in] id             Unique ID
  //! @param[in] name           entity name
  //! @param[in] agentCategory  OpenPASS agent category
  //! @param[in] properties     vehicle properties
  //! @return newly created vehicle
  Vehicle &CreateVehicle(mantle_api::UniqueId id,
                         const std::string &name,
                         AgentCategory agentCategory,
                         const mantle_api::VehicleProperties &properties);

  //! @brief Creates a new pedestrian
  //!
  //! @param[in] id           Unique ID
  //! @param[in] name         entity name
  //! @param[in] properties   vehicle properties
  //! @return newly created pedestrian
  Pedestrian &CreatePedestrian(mantle_api::UniqueId id,
                               const std::string &name,
                               const mantle_api::PedestrianProperties &properties);

  WorldInterface *world{nullptr};
  IdManagerInterface &idManager;
  mantle_api::UniqueId nextCommonID{0};
  std::vector<std::unique_ptr<mantle_api::IEntity>> entities;
  std::map<mantle_api::UniqueId, std::reference_wrapper<Entity>> entitiesById;
  std::map<std::string, std::reference_wrapper<Entity>> entitiesByName;
  std::vector<std::reference_wrapper<Entity>> unspawnedEntities;
  core::AgentFactoryInterface *agentFactory{nullptr};
  const RouteSamplerInterface *routeSampler;
  std::vector<Agent *> newAgents;
};
}  // namespace core
