/*******************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "controllerRepository.h"

#include <MantleAPI/Traffic/i_controller_config.h>
#include <ostream>
#include <string>
#include <utility>

#include "common/commonTools.h"
#include "common/log.h"
#include "framework/agentBlueprintProvider.h"
#include "framework/controller.h"
#include "include/agentBlueprintInterface.h"

namespace core
{

mantle_api::IController& ControllerRepository::Create(std::unique_ptr<mantle_api::IControllerConfig> config)
{
  auto createController = [&](const std::string& name, System&& system) -> mantle_api::IController&
  {
    auto uniqueId = idManager.Generate(EntityType::kController);
    auto controller = std::make_shared<Controller>(uniqueId, name, std::move(system));
    controllers.emplace(uniqueId, controller);
    return *(std::dynamic_pointer_cast<mantle_api::IController>(controller));
  };

  if (dynamic_cast<mantle_api::InternalControllerConfig*>(config.get()))
  {
    return createController(config->name, agentBlueprintProvider.get().GetDefaultSystem());
  }
  if (const auto* controllerConfig = dynamic_cast<mantle_api::ExternalControllerConfig*>(config.get()))
  {
    auto agentProfile = helper::map::query(controllerConfig->parameters, "AgentProfile");
    ThrowIfFalse(agentProfile.has_value(), "ControllerRepository::Create Missing property AgentProfile");
    return createController(config->name, agentBlueprintProvider.get().SampleSystem(agentProfile.value()));
  }

  LogErrorAndThrow("ControllerRepository: Invalid controller config");
}

std::optional<std::reference_wrapper<mantle_api::IController>> ControllerRepository::Get(mantle_api::UniqueId id)
{
  if (!Contains(id))
  {
    return std::nullopt;
  }
  return *((std::static_pointer_cast<mantle_api::IController>(controllers.at(id))));
}

bool ControllerRepository::Contains(mantle_api::UniqueId id) const
{
  return controllers.find(id) != controllers.end();
}

void ControllerRepository::Delete(mantle_api::UniqueId id)
{
  LOG_INTERN(LogLevel::Warning) << "ControllerRepository: Call to unimplemented method Delete for entity "
                                << std::to_string(id);
}

void ControllerRepository::Reset()
{
  controllers.clear();
  idManager.Reset(EntityType::kController);
};

}  // namespace core
