/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  agentBlueprintProvider.h
//-----------------------------------------------------------------------------

#pragma once

#include <map>
#include <memory>
#include <string>
#include <unordered_map>

#include "common/opExport.h"
#include "include/agentBlueprintInterface.h"
#include "include/agentBlueprintProviderInterface.h"

class ConfigurationContainerInterface;
class ProfilesInterface;
class StochasticsInterface;
class SystemConfigInterface;
struct AgentBuildInformation;
struct AgentProfile;

/// This file contains the AgentBlueprintProvider, which creates agentBlueprints according to
///  either a given SceneryConfiguration or based on a sampled agent.
class SIMULATIONCOREEXPORT AgentBlueprintProvider : public AgentBlueprintProviderInterface
{
public:
  AgentBlueprintProvider() = default;

  /**
   * @brief AgentBlueprintProvider constructor
   *
   * @param[in] configurationContainer    Pointer to the configuration container
   * @param[in] stochastics               Pointer to the stochastic
   */
  AgentBlueprintProvider(ConfigurationContainerInterface* configurationContainer, StochasticsInterface* stochastics);

  //! Initialize the parameters as in the original constructor
  //!
  //! @param[in] configurationContainer    Pointer to the configuration container
  //! @param[in] stochastics               Pointer to the stochastic
  void Init(ConfigurationContainerInterface* configurationContainer, StochasticsInterface* stochastics);

  System GetDefaultSystem() const override;

  /*!
   * \brief Samples an entire agent
   *
   * \details First samples the agent profile and then depending on whether it is a static or dynamic profile samples the dynamic
   * profiles and builds an AgentType from this or loads the AgentType from the specified SystemConfig
   *
   * \param    agentProfileName    name of agent profile to sample
   *
   * \return   sampled system if successful
   */
  System SampleSystem(const std::string& agentProfileName) const override;

  std::string SampleVehicleModelName(const std::string& agentProfileName) const override;

  /*!
   * \brief Store sampled information in System for dynamic AgentProfile

   * \param agentBuildInformation          AgentBuildInformation with information to store into the System
   * \param agentProfileName               Name of agent profile
   * \param driverProfileName              Name of sampled DriverProfile
   * \param vehicleComponentProfileNames   Name for the component profile name for each vehicle component
   *
   * \return  System
   */
  System GenerateDynamicAgentBlueprint(
      const AgentBuildInformation& agentBuildInformation,
      const std::string& agentProfileName,
      const std::string& driverProfileName,
      const std::unordered_map<std::string, std::string>& vehicleComponentProfileNames) const;

  /*!
   * \brief Store sampled information in System for static AgentProfile
   *
   * @param[in]        systemConfigName        Name of systemConfiguration holding the system information
   * @param[in]        systemId                Id of system to build (referes to given systemConfig
   * @param[in]        agentProfileName        Name of agent profile
   *
   * \return  System
   */
  System GenerateStaticAgentBlueprint(const std::string& systemConfigName,
                                      int systemId,
                                      const std::string& agentProfileName) const;

private:
  StochasticsInterface* stochastics{nullptr};
  const ProfilesInterface* profiles{nullptr};
  const std::unordered_map<std::string, AgentProfile>* agentProfiles{nullptr};
  std::shared_ptr<SystemConfigInterface> systemConfigBlueprint{nullptr};
  const std::map<std::string, std::shared_ptr<SystemConfigInterface>>* systemConfigs{nullptr};
};
