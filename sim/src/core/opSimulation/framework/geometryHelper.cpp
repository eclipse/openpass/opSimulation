/*******************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "geometryHelper.h"

#include <algorithm>
#include <iterator>

#include "common/log.h"

using namespace units::literals;

namespace core
{

mantle_api::Vec3<units::length::meter_t> GeometryHelper::TranslateGlobalPositionLocally(
    const mantle_api::Vec3<units::length::meter_t> &global_position,
    const mantle_api::Orientation3<units::angle::radian_t> &local_orientation,
    const mantle_api::Vec3<units::length::meter_t> &local_translation) const
{
  const units::length::meter_t x = global_position.x + units::math::cos(local_orientation.yaw) * local_translation.x
                                 - units::math::sin(local_orientation.yaw) * local_translation.y;
  const units::length::meter_t y = global_position.y + units::math::sin(local_orientation.yaw) * local_translation.x
                                 + units::math::cos(local_orientation.yaw) * local_translation.y;
  const units::length::meter_t z = global_position.z;
  return {x, y, z};
}

std::vector<mantle_api::Vec3<units::length::meter_t>> GeometryHelper::TransformPolylinePointsFromWorldToLocal(
    const std::vector<mantle_api::Vec3<units::length::meter_t>> &polyline_points,
    const mantle_api::Vec3<units::length::meter_t> &local_origin,
    const mantle_api::Orientation3<units::angle::radian_t> &local_orientation) const
{
  std::vector<mantle_api::Vec3<units::length::meter_t>> local_polyline_points;
  local_polyline_points.reserve(polyline_points.size());
  std::transform(begin(polyline_points),
                 end(polyline_points),
                 std::back_insert_iterator(local_polyline_points),
                 [&](auto &&point)
                 { return TransformPositionFromWorldToLocal(point, local_origin, local_orientation); });
  return local_polyline_points;
}

mantle_api::Vec3<units::length::meter_t> GeometryHelper::TransformPositionFromWorldToLocal(
    const mantle_api::Vec3<units::length::meter_t> &world_position,
    const mantle_api::Vec3<units::length::meter_t> &local_origin,
    const mantle_api::Orientation3<units::angle::radian_t> &local_orientation) const
{
  const auto p = world_position - local_origin;

  //https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions#Euler_angles_(z-y%E2%80%B2-x%E2%80%B3_intrinsic)_%E2%86%92_rotation_matrix
  const auto sin_yaw = units::math::sin(local_orientation.yaw);
  const auto cos_yaw = units::math::cos(local_orientation.yaw);

  const auto sin_pitch = units::math::sin(local_orientation.pitch);
  const auto cos_pitch = units::math::cos(local_orientation.pitch);

  const auto sin_roll = units::math::sin(local_orientation.roll);
  const auto cos_roll = units::math::cos(local_orientation.roll);

  return {cos_pitch * cos_yaw * p.x + cos_pitch * sin_yaw * p.y - sin_pitch * p.z,
          (sin_roll * sin_pitch * cos_yaw - cos_roll * sin_yaw) * p.x
              + (sin_roll * sin_pitch * sin_yaw + cos_roll * cos_yaw) * p.y + sin_roll * cos_pitch * p.z,
          (cos_roll * sin_pitch * cos_yaw + sin_roll * sin_yaw) * p.x
              + (cos_roll * sin_pitch * sin_yaw - sin_roll * cos_yaw) * p.y + cos_roll * cos_pitch * p.z};
}

bool GeometryHelper::AreOrientedSimilarly(
    [[maybe_unused]] const mantle_api::Orientation3<units::angle::radian_t> &orientation1,
    [[maybe_unused]] const mantle_api::Orientation3<units::angle::radian_t> &orientation2) const
{
  LogErrorAndThrow("Method AreOrientedSimilarly in GeometryHelper is not yet implemented");
}

}  // namespace core