/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "agentFactory.h"

#include <MantleAPI/Common/bounding_box.h>
#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <cstddef>
#include <map>
#include <optional>
#include <sstream>
#include <stdexcept>
#include <string>
#include <units.h>
#include <unordered_map>
#include <utility>

#include "bindings/stochastics.h"
#include "common/globalDefinitions.h"
#include "common/log.h"
#include "common/parameter.h"
#include "common/sensorDefinitions.h"
#include "include/agentBlueprintInterface.h"
#include "include/componentInterface.h"
#include "include/dataBufferInterface.h"
#include "modelElements/agent.h"
#include "modelElements/channel.h"

class WorldInterface;
namespace core
{
class ChannelBuffer;
}  // namespace core

const std::map<AgentCategory, std::string> AgentCategories = {{AgentCategory::Ego, "Ego"},
                                                              {AgentCategory::Scenario, "Scenario"},
                                                              {AgentCategory::Common, "Common"},
                                                              {AgentCategory::Any, "Any"}};

namespace core
{

AgentFactory::AgentFactory(ModelBinding *modelBinding,
                           WorldInterface *world,
                           Stochastics *stochastics,
                           ObservationNetworkInterface *observationNetwork,
                           DataBufferWriteInterface *dataBuffer)
    : modelBinding(modelBinding),
      world(world),
      stochastics(stochastics),
      observationNetwork(observationNetwork),
      dataBuffer(dataBuffer)
{
}

void AgentFactory::Clear()
{
  agentList.clear();
}

Agent *AgentFactory::AddAgent(mantle_api::UniqueId id, const AgentBuildInstructions &agentBuildInstructions)
{
  try
  {
    auto agent = CreateAgent(id, agentBuildInstructions);
    PublishProperties(agentBuildInstructions, agent->GetId());
    agentList.push_back(std::move(agent));
    return agentList.back().get();
  }
  catch (const std::runtime_error &e)
  {
    LOG_INTERN(LogLevel::Error) << "could not create agent: " << e.what();
    return nullptr;
  }
}

std::unique_ptr<Agent> AgentFactory::CreateAgent(mantle_api::UniqueId id,
                                                 const AgentBuildInstructions &agentBuildInstructions)
{
  LOG_INTERN(LogLevel::DebugCore) << "create new agent";

  auto agent = std::make_unique<Agent>(id, world, agentBuildInstructions);

  if (!agent)
  {
    LOG_INTERN(LogLevel::Error) << "agent creation failed";
    return nullptr;
  }

  LOG_INTERN(LogLevel::DebugCore) << "agent created (" << agent->GetId() << ")";

  if (!agent->Instantiate(agentBuildInstructions, modelBinding, stochastics, observationNetwork, dataBuffer))
  {
    LOG_INTERN(LogLevel::Error) << "agent could not be instantiated";
    return nullptr;
  }

  // link agent internal components
  if (!ConnectAgentLinks(agent.get()))
  {
    LOG_INTERN(LogLevel::Error) << "agent channels could not be created";
    return nullptr;
  }

  return agent;
}

bool AgentFactory::ConnectAgentLinks(Agent *agent)
{
  for (const auto &[_, component] : agent->GetComponents())
  {
    for (const auto &[outputLinkId, channel] : component->GetOutputLinks())
    {
      if (!channel)
      {
        return false;
      }

      ComponentInterface *source = channel->GetSource();
      if (!source)
      {
        return false;
      }

      ChannelBuffer *buffer = source->CreateOutputBuffer(outputLinkId);
      if (!buffer || !(channel->AttachSourceBuffer(buffer)))
      {
        return false;
      }

      // channel buffer is now attached to channel and will be released when deleting the agent
      for (const auto &item : channel->GetTargets())
      {
        int targetLinkId = std::get<static_cast<size_t>(Channel::TargetLinkType::LinkId)>(item);
        ComponentInterface *targetComponent = std::get<static_cast<size_t>(Channel::TargetLinkType::Component)>(item);
        targetComponent->SetInputBuffer(targetLinkId, buffer);
      }
    }
  }

  return true;
}

void AgentFactory::PublishProperties(const AgentBuildInstructions &agentBuildInstructions, int agentId)
{
  const std::string keyPrefix = "Agents/" + std::to_string(agentId) + "/";
  dataBuffer->PutStatic(keyPrefix + "AgentTypeGroupName", AgentCategories.at(agentBuildInstructions.agentCategory));
  dataBuffer->PutStatic(keyPrefix + "AgentTypeName", agentBuildInstructions.system.agentProfileName);
  dataBuffer->PutStatic(keyPrefix + "VehicleModelType", agentBuildInstructions.entityProperties->model);
  dataBuffer->PutStatic(keyPrefix + "DriverProfileName", agentBuildInstructions.system.driverProfileName);
  for (const auto &[vehicleComponent, profileName] : agentBuildInstructions.system.vehicleComponentProfileNames)
  {
    dataBuffer->PutStatic(std::string(keyPrefix).append("VehicleComponents/").append(vehicleComponent), profileName);
  }
  dataBuffer->PutStatic(
      keyPrefix + "EntityType",
      std::string(openpass::utils::to_cstr(
          agentBuildInstructions.entityProperties->type)));  // std::string for compatibility with gcc-9 std::ariant

  const auto &entityProperties = agentBuildInstructions.entityProperties;
  if (entityProperties->type == mantle_api::EntityType::kVehicle)
  {
    dataBuffer->PutStatic(keyPrefix + "VehicleClassification",
                          std::string(openpass::utils::to_cstr(
                              std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(entityProperties)
                                  ->classification)));  // std::string for compatibility with gcc-9 std::ariant
  }

  dataBuffer->PutStatic(keyPrefix + "Vehicle/Width",
                        units::unit_cast<double>(entityProperties->bounding_box.dimension.width));
  dataBuffer->PutStatic(keyPrefix + "Vehicle/Length",
                        units::unit_cast<double>(entityProperties->bounding_box.dimension.length));
  dataBuffer->PutStatic(keyPrefix + "Vehicle/Height",
                        units::unit_cast<double>(entityProperties->bounding_box.dimension.height));
  dataBuffer->PutStatic(keyPrefix + "Vehicle/LongitudinalPivotOffset",
                        units::unit_cast<double>(-entityProperties->bounding_box.geometric_center.x));

  for (const auto &sensor : agentBuildInstructions.system.sensorParameters)
  {
    const auto sensorPosition = openpass::sensors::GetPosition(
        sensor.positionName, *std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(entityProperties));
    const std::string sensorKeyPrefix = keyPrefix + "Vehicle/Sensors/" + std::to_string(sensor.id) + "/";
    dataBuffer->PutStatic(sensorKeyPrefix + "Type", sensor.profile.type);
    dataBuffer->PutStatic(sensorKeyPrefix + "Mounting/Position/Longitudinal",
                          units::unit_cast<double>(sensorPosition.pose.position.x));
    dataBuffer->PutStatic(sensorKeyPrefix + "Mounting/Position/Lateral",
                          units::unit_cast<double>(sensorPosition.pose.position.y));
    dataBuffer->PutStatic(sensorKeyPrefix + "Mounting/Position/Height",
                          units::unit_cast<double>(sensorPosition.pose.position.z));
    dataBuffer->PutStatic(sensorKeyPrefix + "Mounting/Orientation/Yaw",
                          units::unit_cast<double>(sensorPosition.pose.orientation.yaw));
    dataBuffer->PutStatic(sensorKeyPrefix + "Mounting/Orientation/Pitch",
                          units::unit_cast<double>(sensorPosition.pose.orientation.pitch));
    dataBuffer->PutStatic(sensorKeyPrefix + "Mounting/Orientation/Roll",
                          units::unit_cast<double>(sensorPosition.pose.orientation.roll));

    const auto &parameters = sensor.profile.parameter;

    if (auto latency = openpass::parameter::Get<double>(parameters, "Latency"))
    {
      dataBuffer->PutStatic(sensorKeyPrefix + "Parameters/Latency", latency.value());
    }

    if (auto openingAngleH = openpass::parameter::Get<double>(parameters, "OpeningAngleH"))
    {
      dataBuffer->PutStatic(sensorKeyPrefix + "Parameters/OpeningAngleH", openingAngleH.value());
    }

    if (auto openingAngleV = openpass::parameter::Get<double>(parameters, "OpeningAngleV"))
    {
      dataBuffer->PutStatic(sensorKeyPrefix + "Parameters/OpeningAngleV", openingAngleV.value());
    }

    if (auto detectionRange = openpass::parameter::Get<double>(parameters, "DetectionRange"))
    {
      dataBuffer->PutStatic(sensorKeyPrefix + "Parameters/Range", detectionRange.value());
    }
  }
}

}  // namespace core
