/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Common/i_identifiable.h>
#include <cstddef>
#include <map>
#include <stdexcept>

#include "common/opExport.h"
#include "include/dataBufferInterface.h"
#include "include/idManagerInterface.h"

namespace core
{

/// @brief Handles unique IDs and reports creation to the entityInfoPublisher
class SIMULATIONCOREEXPORT IdManager final : public IdManagerInterface
{
  static constexpr size_t MAX_ENTITIES_PER_GROUP{1000000};

  /// @brief Defines an index group for entities, such as "moving objects from 0 to 10000"
  class EntityGroup
  {
  public:
    /// @brief Create a group
    /// @param capacity    maximum number of entities within the group
    /// @param offset      initial index
    /// @param persistent true if group shall not be affected by a reset (e.g. lanes)
    constexpr EntityGroup(size_t capacity, size_t offset) noexcept
        : offset{offset}, end{offset + capacity}, nextId{offset}
    {
    }

    /// @brief Retrieve the next index
    /// @note  Throws if too many indices are retrieved
    /// @returns initial index or incremented by one w.r.t to last call
    [[nodiscard]] size_t GetNextIndex()
    {
      if (nextId == end)
      {
        throw std::runtime_error("Too many entities for current group");
      }
      return nextId++;
    }

    /// @brief Resets the index to the original offset
    /// @note  If group is persistent, the index remains unchanged
    void Reset() noexcept { nextId = offset; }

  private:
    size_t offset;
    size_t end;
    size_t nextId;
  };

public:
  /// @brief Create a new IdManager
  IdManager();

  ~IdManager() override;

  [[nodiscard]] mantle_api::UniqueId Generate() override;
  [[nodiscard]] mantle_api::UniqueId Generate(EntityType entityType) override;
  void Reset(EntityType entityType) override;

private:
  /// @brief Registers an index group for the given entity type
  void RegisterGroup(EntityType entityType);

  std::map<EntityType, EntityGroup> _repository;
};

}  // namespace core
