/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  runInstantiator.h
//! @brief This file contains the component which triggers the simulation runs.
//-----------------------------------------------------------------------------

#pragma once

#include <memory>
#include <mutex>
#include <string>
#include <tuple>
#include <utility>

#include "common/opExport.h"
#include "common/runtimeInformation.h"
#include "common/spawnPointLibraryDefinitions.h"
#include "framework/agentBlueprintProvider.h"
#include "framework/environment.h"
#include "framework/logger.h"
#include "importer/frameworkModules.h"
#include "include/configurationContainerInterface.h"
#include "include/frameworkModuleContainerInterface.h"
#include "include/parameterInterface.h"
#include "include/profilesInterface.h"
#include "include/simulationConfigInterface.h"

class DataBufferInterface;
class StochasticsInterface;
class WorldInterface;
#include "include/stochasticsInterface.h"
#include "logger.h"

namespace core
{
class AgentFactoryInterface;
class CollisionDetectionInterface;
class ObservationNetworkInterface;
class SpawnPointNetworkInterface;

/// class representing the component which triggers the simulation runs
class SIMULATIONCOREEXPORT RunInstantiator
{
public:
  /**
   * @brief RunInstantiator constructor
   *
   * @param[in] configurationContainer    Pointer to the configuration container
   * @param[in] frameworkModuleContainer  Pointer to the framework module container
   * @param[in] frameworkModules          Framework modules
   * @param[in] setCurrentRunFct          Function to set the current run
   * @param[in] setTotalRunsFct           Function to set the total number of runs
   */
  RunInstantiator(ConfigurationContainerInterface &configurationContainer,
                  FrameworkModuleContainerInterface &frameworkModuleContainer,
                  FrameworkModules &frameworkModules,
                  openpass::common::SetCurrentRunFct setCurrentRunFct,
                  openpass::common::SetCurrentRunFct setTotalRunsFct)
      : configurationContainer(configurationContainer),
        observationNetwork(*frameworkModuleContainer.GetObservationNetwork()),
        agentFactory(*frameworkModuleContainer.GetAgentFactory()),
        world(*frameworkModuleContainer.GetWorld()),
        spawnPointNetwork(*frameworkModuleContainer.GetSpawnPointNetwork()),
        stochastics(*frameworkModuleContainer.GetStochastics()),
        collisionDetection(*frameworkModuleContainer.GetCollisionDetection()),
        dataBuffer(*frameworkModuleContainer.GetDataBuffer()),
        frameworkModules(frameworkModules),
        logger(std::make_shared<Logger>(frameworkModules.logLevel, frameworkModuleContainer.GetCallbacks())),
        environment(std::make_shared<Environment>(
            configurationContainer.GetRuntimeInformation().directories.configuration,
            agentBlueprintProvider,
            &agentFactory,
            &stochastics,
            &world,
            configurationContainer.GetSimulationConfig()->GetEnvironmentConfig().turningRates,
            frameworkModuleContainer.GetCallbacks(),
            *frameworkModuleContainer.GetIdManager())),
        setCurrentRun(std::move(setCurrentRunFct)),
        setTotalRuns(std::move(setTotalRunsFct))
  {
  }

  //-----------------------------------------------------------------------------
  //! @brief Executes the run by preparing the stochastics, world and observation
  //!     network instances, then scheduling for each run invocation an update
  //!     on the observation network, finalizing the run invocation and, after all
  //!     invocations have terminated, the observation network itself.
  //!
  //! Executes the run by initializing the stochastics generator with the random
  //! seed from the run configuration, instantiating and initializing the observation
  //! network with the observation instances from the run configuration and the
  //! observation result path from the framework configuration. For each run
  //! invocation:
  //! - configure the world paremeters from the run configuration
  //! - import the scenery file and the world global objects
  //! - init the observation network run
  //! - instantiate the world's spawn point network
  //! - start the scheduling with the observation networks's UpdateTimeStep as
  //!     update callback
  //! - finalize the run invocation using the result on the observation network
  //!
  //! Finally, finalize the observation network and clear teh world.
  //!
  //! @return                             Flag if the update was successful
  //-----------------------------------------------------------------------------
  bool ExecuteRun();

  //-----------------------------------------------------------------------------
  //! Stops the current run.
  //-----------------------------------------------------------------------------
  //void StopRun();

private:
  bool InitPreRun();
  bool InitRun(const EnvironmentConfig &environmentConfig, const ProfilesInterface &profiles);
  void InitializeSpawnPointNetwork();
  std::unique_ptr<ParameterInterface> SampleWorldParameters(
      const EnvironmentConfig &environmentConfig,
      const ProfileGroup &trafficRules,
      StochasticsInterface *stochastics,
      const openpass::common::RuntimeInformation &runtimeInformation);

  void ClearRun();

  std::mutex stopMutex;
  bool stopped = true;

  ConfigurationContainerInterface &configurationContainer;
  ObservationNetworkInterface &observationNetwork;
  AgentFactoryInterface &agentFactory;

  SpawnPointNetworkInterface &spawnPointNetwork;
  StochasticsInterface &stochastics;
  CollisionDetectionInterface &collisionDetection;
  WorldInterface &world;
  DataBufferInterface &dataBuffer;
  FrameworkModules &frameworkModules;

  AgentBlueprintProvider agentBlueprintProvider;

  std::unique_ptr<ParameterInterface> worldParameter;
  std::shared_ptr<Logger> logger;
  std::shared_ptr<Environment> environment{nullptr};
  std::shared_ptr<Vehicles> vehicles{};

  openpass::common::SetCurrentRunFct setCurrentRun;
  openpass::common::SetTotalRunsFct setTotalRuns;
};

}  // namespace core
