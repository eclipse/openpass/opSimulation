/*******************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "entityRepository.h"

#include <MantleAPI/Traffic/entity_properties.h>
#include <algorithm>
#include <stdexcept>
#include <utility>

#include "framework/entity.h"
#include "framework/pedestrian.h"
#include "framework/vehicle.h"
#include "include/agentFactoryInterface.h"
#include "include/worldInterface.h"
#include "modelElements/agent.h"

class RouteSamplerInterface;

namespace core
{
EntityRepository::EntityRepository(WorldInterface *world,
                                   IdManagerInterface &idManager,
                                   AgentFactoryInterface *agentFactory,
                                   const RouteSamplerInterface *routeSampler)
    : world(world), idManager(idManager), agentFactory(agentFactory), routeSampler(routeSampler)
{
}

Vehicle &EntityRepository::CreateVehicle(mantle_api::UniqueId id,
                                         const std::string &name,
                                         AgentCategory agentCategory,
                                         const mantle_api::VehicleProperties &properties)
{
  // This is to verify if scenarioEngine set the correct EntityType when creating Entities
  if (properties.type != mantle_api::EntityType::kVehicle)
  {
    throw std::runtime_error("Vehicle could not be created. Vehicle requires EntityType: Vehicle.");
  }

  auto &entity = entities.emplace_back(std::move(std::make_unique<Vehicle>(
      id, name, std::make_shared<mantle_api::VehicleProperties>(properties), routeSampler, agentCategory)));

  return EmplaceEntity<Vehicle>(id, name, entity);
}

Pedestrian &EntityRepository::CreatePedestrian(mantle_api::UniqueId id,
                                               const std::string &name,
                                               const mantle_api::PedestrianProperties &properties)
{
  // This is to verify if scenarioEngine set the correct EntityType when creating Entities
  if (properties.type != mantle_api::EntityType::kPedestrian)
  {
    throw std::runtime_error("Pedestrian could not be created. Pedestrian requires EntityType: kPedestrian.");
  }

  auto &entity = entities.emplace_back(std::move(std::make_unique<Pedestrian>(
      id, name, std::make_shared<mantle_api::PedestrianProperties>(properties), routeSampler)));

  return EmplaceEntity<Pedestrian>(id, name, entity);
}

mantle_api::IVehicle &EntityRepository::Create(const std::string &name, const mantle_api::VehicleProperties &properties)
{
  auto id = idManager.Generate(EntityType::kVehicle);

  auto agentCategory = properties.is_host ? AgentCategory::Ego : AgentCategory::Scenario;

  return CreateVehicle(id, name, agentCategory, properties);
}

mantle_api::IVehicle &EntityRepository::CreateCommon(const mantle_api::VehicleProperties &properties)
{
  auto id = idManager.Generate(EntityType::kVehicle);

  std::string name = "Common" + std::to_string(nextCommonID);
  ++nextCommonID;

  return CreateVehicle(id, name, AgentCategory::Common, properties);
}

mantle_api::IVehicle &EntityRepository::Create([[maybe_unused]] mantle_api::UniqueId id,
                                               [[maybe_unused]] const std::string &name,
                                               [[maybe_unused]] const mantle_api::VehicleProperties &properties)
{
  throw std::runtime_error("Creating entities with given id is deprecated");
}

mantle_api::IPedestrian &EntityRepository::Create(const std::string &name,
                                                  const mantle_api::PedestrianProperties &properties)
{
  auto id = idManager.Generate(EntityType::kVehicle);
  return CreatePedestrian(id, name, properties);
}

mantle_api::IPedestrian &EntityRepository::Create([[maybe_unused]] mantle_api::UniqueId id,
                                                  [[maybe_unused]] const std::string &name,
                                                  [[maybe_unused]] const mantle_api::PedestrianProperties &properties)
{
  throw std::runtime_error("Creating entities with given id is deprecated");
}

mantle_api::IStaticObject &EntityRepository::Create(
    [[maybe_unused]] const std::string &name, [[maybe_unused]] const mantle_api::StaticObjectProperties &properties)
{
  throw std::runtime_error("EntityRepository::Create from StaticObjectProperties currently not supported.");
}

mantle_api::IStaticObject &EntityRepository::Create(
    [[maybe_unused]] mantle_api::UniqueId id,
    [[maybe_unused]] const std::string &name,
    [[maybe_unused]] const mantle_api::StaticObjectProperties &properties)
{
  throw std::runtime_error("EntityRepository::Create from StaticObjectProperties currently not supported.");
}

mantle_api::IVehicle &EntityRepository::GetHost()
{
  throw std::runtime_error("EntityRepository::GetHost currently not supported.");
}

std::optional<std::reference_wrapper<mantle_api::IEntity>> EntityRepository::Get(const std::string &name)
{
  if (auto iter = entitiesByName.find(name); iter != entitiesByName.end())
  {
    return iter->second;
  }

  return std::nullopt;
}

std::optional<std::reference_wrapper<const mantle_api::IEntity>> EntityRepository::Get(const std::string &name) const
{
  if (auto iter = entitiesByName.find(name); iter != entitiesByName.end())
  {
    return iter->second;
  }

  return std::nullopt;
}

std::optional<std::reference_wrapper<mantle_api::IEntity>> EntityRepository::Get(mantle_api::UniqueId id)
{
  if (auto iter = entitiesById.find(id); iter != entitiesById.end())
  {
    return iter->second;
  }
  return std::nullopt;
}

std::optional<std::reference_wrapper<const mantle_api::IEntity>> EntityRepository::Get(mantle_api::UniqueId id) const
{
  if (auto iter = entitiesById.find(id); iter != entitiesById.end())
  {
    return iter->second;
  }
  return std::nullopt;
}

bool EntityRepository::Contains(mantle_api::UniqueId id) const
{
  return entitiesById.find(id) != entitiesById.end();
}

void EntityRepository::Delete(const std::string &name)
{
  if (auto iter = entitiesByName.find(name); iter != entitiesByName.end())
  {
    Delete(iter->second.get().GetUniqueId());
  }
}

void EntityRepository::Delete(mantle_api::UniqueId id)
{
  if (auto iter = entitiesById.find(id); iter != entitiesById.end())
  {
    auto &entity = iter->second.get();
    auto *agent = world->GetAgent(static_cast<int>(id));  // TODO: align datatypes
    world->QueueAgentRemove(&(*agent));
    auto entity_ptr = std::find_if(entities.begin(), entities.end(), [&](auto &ptr) { return ptr.get() == &entity; });
    entitiesById.erase(id);
    entitiesByName.erase(entity.GetName());
    entities.erase(entity_ptr);
  }
}

void EntityRepository::Reset()
{
  world->ClearAgents();
  idManager.Reset(EntityType::kVehicle);
  nextCommonID = 0;
  entities.clear();
  entitiesById.clear();
  entitiesByName.clear();
  unspawnedEntities.clear();
  newAgents.clear();
}

const std::vector<std::unique_ptr<mantle_api::IEntity>> &EntityRepository::GetEntities() const
{
  return entities;
}

Entity &EntityRepository::GetEntity(mantle_api::UniqueId id)
{
  return entitiesById.at(id);
}

const Entity &EntityRepository::GetEntity(mantle_api::UniqueId id) const
{
  return entitiesById.at(id);
}

bool EntityRepository::SpawnReadyAgents()
{
  bool successfullySpawnedAgents = true;
  std::vector<std::reference_wrapper<Entity>> stillUnspawnedVehicles;
  for (auto vehicle : unspawnedEntities)
  {
    if (vehicle.get().ShouldBeSpawned())
    {
      auto *newAgent = agentFactory->AddAgent(vehicle.get().GetUniqueId(), vehicle.get().GetAgentBuildInstructions());

      if (newAgent == nullptr)
      {
        successfullySpawnedAgents = false;
      }
      else
      {
        newAgents.push_back(newAgent);
        vehicle.get().SetAgent(newAgent->GetAgentAdapter());
      }
    }
    else
    {
      stillUnspawnedVehicles.push_back(vehicle);
    }
  }
  unspawnedEntities = stillUnspawnedVehicles;
  return successfullySpawnedAgents;
}

std::vector<Agent *> EntityRepository::ConsumeNewAgents()
{
  std::vector<Agent *> tmpAgents;
  std::swap(tmpAgents, newAgents);
  return tmpAgents;
}
}  // namespace core
