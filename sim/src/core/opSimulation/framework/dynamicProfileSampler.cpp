/********************************************************************************
 * Copyright (c) 2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "dynamicProfileSampler.h"

#include <stdexcept>
#include <utility>
#include <vector>

#include "common/sampler.h"
#include "include/profilesInterface.h"

class StochasticsInterface;

DynamicProfileSampler& DynamicProfileSampler::SampleDriverProfile()
{
  const auto probabilities = profiles->GetDriverProbabilities(agentProfileName);
  sampledProfiles.driverProfileName = Sampler::Sample(probabilities, &stochastics);
  return *this;
}

DynamicProfileSampler& DynamicProfileSampler::SampleSystemProfile()
{
  const auto probabilities = profiles->GetSystemProfileProbabilities(agentProfileName);
  sampledProfiles.systemProfileName = Sampler::Sample(probabilities, &stochastics);
  return *this;
}

DynamicProfileSampler& DynamicProfileSampler::SampleVehicleComponentProfiles()
{
  const auto find_result = profiles->GetSystemProfiles().find(sampledProfiles.systemProfileName);
  if (find_result == profiles->GetSystemProfiles().end())
  {
    throw std::runtime_error("No vehicle profile with name \"" + sampledProfiles.systemProfileName + "\" defined");
  }
  const SystemProfile& systemProfile = find_result->second;
  for (const VehicleComponent& vehicleComponentInProfile : systemProfile.vehicleComponents)
  {
    std::string vehicleComponentName = Sampler::Sample(vehicleComponentInProfile.componentProfiles, &stochastics);
    if (vehicleComponentName != "")
    {
      sampledProfiles.vehicleComponentProfileNames.insert(
          std::make_pair(vehicleComponentInProfile.type, vehicleComponentName));
    }
  }
  return *this;
}

DynamicProfileSampler SampledProfiles::make(std::string agentProfileName,
                                            StochasticsInterface& stochastics,
                                            const ProfilesInterface* profiles)
{
  return {std::move(agentProfileName), stochastics, profiles};
}
