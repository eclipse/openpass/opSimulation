/********************************************************************************
 * Copyright (c) 2017-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "frameworkModuleContainer.h"

#include <vector>

#include "common/observationLibraryDefinitions.h"
#include "common/spawnPointLibraryDefinitions.h"
#include "importer/frameworkModules.h"

class CallbackInterface;
class DataBufferInterface;
class StochasticsInterface;
class WorldInterface;

namespace openpass::common
{
struct RuntimeInformation;
}  // namespace openpass::common

namespace core
{
class AgentFactoryInterface;
class CollisionDetectionInterface;
class ObservationNetworkInterface;
class SpawnPointNetworkInterface;

FrameworkModuleContainer::FrameworkModuleContainer(const FrameworkModules &frameworkModules,
                                                   const openpass::common::RuntimeInformation &runtimeInformation,
                                                   CallbackInterface *callbacks)
    : callbacks(callbacks),
      dataBufferBinding(frameworkModules.dataBufferLibrary, runtimeInformation, callbacks),
      dataBuffer(&dataBufferBinding),
      coreDataPublisher(&dataBuffer),
      stochasticsBinding(callbacks),
      stochastics(&stochasticsBinding),
      worldBinding(frameworkModules.worldLibrary, callbacks, &idManager, &stochastics, &dataBuffer),
      world(&worldBinding),
      observationNetwork(&observationBindings),
      collisionDetector(&world, &coreDataPublisher),
      modelBinding(frameworkModules.libraryDir, runtimeInformation, callbacks),
      agentFactory(&modelBinding, &world, &stochastics, &observationNetwork, &dataBuffer),
      spawnPointNetwork(&spawnPointBindings, &world, runtimeInformation)
{
  for (const auto &libraryInfo : frameworkModules.spawnPointLibraries)
  {
    spawnPointBindings.emplace(libraryInfo.libraryName, SpawnPointBinding(callbacks));
  }

  for (const auto &libraryInfo : frameworkModules.observationLibraries)
  {
    observationBindings.emplace(libraryInfo.libraryName, ObservationBinding(runtimeInformation, callbacks));
  }
}

CallbackInterface *FrameworkModuleContainer::GetCallbacks()
{
  return callbacks;
}

IdManagerInterface *FrameworkModuleContainer::GetIdManager()
{
  return &idManager;
}

AgentFactoryInterface *FrameworkModuleContainer::GetAgentFactory()
{
  return &agentFactory;
}

DataBufferInterface *FrameworkModuleContainer::GetDataBuffer()
{
  return &dataBuffer;
}

CollisionDetectionInterface *FrameworkModuleContainer::GetCollisionDetection()
{
  return &collisionDetector;
}

ObservationNetworkInterface *FrameworkModuleContainer::GetObservationNetwork()
{
  return &observationNetwork;
}

SpawnPointNetworkInterface *FrameworkModuleContainer::GetSpawnPointNetwork()
{
  return &spawnPointNetwork;
}

StochasticsInterface *FrameworkModuleContainer::GetStochastics()
{
  return &stochastics;
}

WorldInterface *FrameworkModuleContainer::GetWorld()
{
  return &world;
}

}  //namespace core
