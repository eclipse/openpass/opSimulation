/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <filesystem>
#include <libxml/tree.h>
#include <string>
#include <vector>

#include "include/profilesInterface.h"

class Profiles;
namespace openpass
{
namespace sensors
{
struct Parameter;
}  // namespace sensors
}  // namespace openpass

//using namespace Configuration;

namespace Importer
{

/// Importer class for all profiles
class ProfilesImporter
{
public:
  /*!
   * \brief Imports the AgentProfiles section of the profiles catalog
   *
   * @param[in]     agentProfilesElement    Element containing the information
   * @param[out]    profiles                Class into which the values get saved
   */
  static void ImportAgentProfiles(xmlNodePtr agentProfilesElement, Profiles &profiles);

  /*!
   * \brief Imports all ProfileGroups defined in the profiles catalog
   * \param profiles Class into which the values get saved
   * \param profilesElement Map into which the values get saved
   */
  static void ImportProfileGroups(Profiles &profiles, xmlNodePtr profilesElement);

  /*!
   * \brief Imports the SystemProfiles section of the profiles catalog
   *
   * @param[in]     systemProfilesElement  Element containing the information
   * @param[out]    profiles               Class into which the values get saved
   */
  static void ImportSystemProfiles(xmlNodePtr systemProfilesElement, Profiles &profiles);

  /*!
   * \brief Imports a single SystemProfile
   * \param systemProfileElement  Element containing the information
   * \return SystemProfile
   */
  static SystemProfile ImportSystemProfile(xmlNodePtr systemProfileElement);

  /*!
   * \brief Imports all VehicleComponentes contained in one SystemProfile
   * \param systemProfileElement  Element containing the information
   * \param systemProfile         SystemProfile to fill
   */
  static void ImportAllVehicleComponentsOfSystemProfile(xmlNodePtr systemProfileElement, SystemProfile &systemProfile);

  /*!
   * \brief Imports a single VehicleComponentes contained in one SystemProfile
   * \param vehicleComponentElement  Element containing the information
   * \param vehicleComponent         VehicleComponent to fill
   */
  static void ImportVehicleComponent(xmlNodePtr vehicleComponentElement, VehicleComponent &vehicleComponent);

  /*!
   * \brief Imports all SensorLinks of a VehicleComponents contained in one SystemProfile
   * \param sensorLinksElement      Element containing the information
   * \param sensorLinks             Map into which SensorLinks are saved
   */
  static void ImportSensorLinksOfComponent(xmlNodePtr sensorLinksElement, std::vector<SensorLink> &sensorLinks);

  /*!
   * \brief Imports all Sensor contained in one SystemProfiles
   * \param systemProfileElement  Element containing the information
   * \param systemProfile         SystemProfile to fill
   */
  static void ImportAllSensorsOfSystemProfile(xmlNodePtr systemProfileElement, SystemProfile &systemProfile);

  /*!
   * \brief Imports a single Sensor contained in one SystemProfile
   * \param sensorElement           Element containing the information
   * \param sensorParameter         SensorParameter to fill
   */
  static void ImportSensorParameters(xmlNodePtr sensorElement, openpass::sensors::Parameter &sensorParameter);

  //Overall import function
  /*!
   * \brief Imports the entire profiles catalog
   * \details Calls all sections specific methods and saves the result in the CombiantionConfig
   *
   * @param[in]   filename      Name of the ProfileConfig file
   * @param[out]  profiles      Class into which the values get saved
   * @param       executionPath Target of the /proc/self/exe symlink on Linux, canonical path of executable on Windows
   * @return      true, if successful
   */
  static bool Import(const std::string &filename, Profiles &profiles, const std::filesystem::path &executionPath);

private:
  static constexpr auto profilesCatalogFile = "ProfilesCatalog.xml";
  static constexpr auto supportedConfigVersion = "0.6.0";
};
}  //namespace Importer
