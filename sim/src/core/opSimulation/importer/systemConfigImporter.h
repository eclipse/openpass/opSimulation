/********************************************************************************
 * Copyright (c) 2017 ITK Engineering GmbH
 *               2019-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

////-----------------------------------------------------------------------------
////! @file  agentSystemImporter.h
////! @brief This file contains the importer of the agent system configuration.
////-----------------------------------------------------------------------------

#pragma once

#include <filesystem>
#include <libxml/tree.h>
#include <memory>
#include <string>

#include "common/parameter.h"
#include "importer/systemConfig.h"

using namespace Configuration;

namespace Importer
{
//-----------------------------------------------------------------------------
//! Class handles importing an agent system configuration.
//-----------------------------------------------------------------------------
class SystemConfigImporter
{
public:
  SystemConfigImporter(const SystemConfigImporter&) = delete;
  SystemConfigImporter(SystemConfigImporter&&) = delete;
  SystemConfigImporter& operator=(const SystemConfigImporter&) = delete;
  SystemConfigImporter& operator=(SystemConfigImporter&&) = delete;

  //-----------------------------------------------------------------------------
  //! Imports the XML agent system configuration from the provided file path and
  //! stores the results in the provided mapping.
  //!
  //! @param[in]  filename        Path to the file with the XML agent system config
  //! @param[out] systemConfig    Agent system configuration
  //! @param      executionPath   Target of the /proc/self/exe symlink on Linux, canonical path of executable on Windows
  //!
  //! @return                     False if an error occurred, true otherwise
  //-----------------------------------------------------------------------------
  static bool Import(const std::string& filename,
                     const std::shared_ptr<SystemConfig>& systemConfig,
                     const std::filesystem::path& executionPath);

  //! Gets SystemConfig parameters from OpenSCENARIO DOM
  //! Public for testing
  //!
  //! @param[in] parametersElement    DOM element containing the SystemConfig parameters
  //! @return                         The parameter set to be imported
  static openpass::parameter::ParameterSetLevel1 ImportSystemParameters(xmlNodePtr parametersElement);

private:
  static xmlDocPtr ImportSystemConfigContent(const std::string& filename);
};

}  //namespace Importer
