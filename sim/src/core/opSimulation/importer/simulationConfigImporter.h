/********************************************************************************
 * Copyright (c) 2017-2018 ITK Engineering GmbH
 *               2018-2020 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  simulationConfigImporter.h
//! @brief This file contains the importer of the simulation configuration.
//-----------------------------------------------------------------------------
#pragma once

#include <filesystem>
#include <libxml/tree.h>
#include <string>

#include "common/globalDefinitions.h"
#include "include/simulationConfigInterface.h"

namespace Configuration
{
class SimulationConfig;
}  // namespace Configuration

namespace Importer
{

/**
 * @brief This class povides the import functionality
 * @details All methods in this class are static
 *
 * @ingroup Importers
 */
class SimulationConfigImporter
{
public:
  /**
   * @brief Imports the libraries used by the simulator
   *
   * @param[in]     rootElement   Element containing the libraries information
   * @return        Libraries (defaulted if element or single elements are missing)
   * @see           defaultLibraryMapping
   */
  static ExperimentConfig::Libraries ImportLibraries(xmlNodePtr rootElement);

  /// Importing specific sections of the SimulationConfig
  /**
   * @brief Imports the Experiment section of the simulation configuration
   *
   * @param[in]     experimentElement  Element containing the information
   * @param[out]    simulationConfig   Class into which the values get saved
   */
  static void ImportExperiment(xmlNodePtr experimentElement, Configuration::SimulationConfig& simulationConfig);

  /**
   * @brief Imports the Scenario section of the simulation configuration
   *
   * @param[in]     scenarioElement    Element containing the information
   * @param[in]     configurationDir   Path of the configuration files
   * @param[out]    simulationConfig   Class into which the values get saved
   */
  static void ImportScenario(xmlNodePtr scenarioElement,
                             const std::string& configurationDir,
                             Configuration::SimulationConfig& simulationConfig);

  /**
   * @brief Imports the Environment section of the simulation configuration
   *
   * @param[in]     environmentElement     Element containing the information
   * @param[out]    simulationConfig       Class into which the values get saved
   */
  static void ImportEnvironment(xmlNodePtr environmentElement, Configuration::SimulationConfig& simulationConfig);

  /**
   * @brief Imports the Spawners section of the simulation configuration
   *
   * @param spawnersElement   Element containing the information
   * @param simulationConfig  Class into which the values get saved
   */
  static void ImportSpawners(xmlNodePtr spawnersElement, Configuration::SimulationConfig& simulationConfig);

  /**
   * @brief Imports the Observations section of the simulation configuration
   *
   * @param observationsElement   Element containing the information
   * @param simulationConfig      Class into which the values get saved
   */
  static void ImportObservations(xmlNodePtr observationsElement, Configuration::SimulationConfig& simulationConfig);

  /**
   * @brief Imports the TurningRates section of the simulation configuration
   *
   * @param turningRatesElement   Element containing the information
   * @param turningRates          Class into which the values get saved
   */
  static void ImportTurningRates(xmlNodePtr turningRatesElement, TurningRates& turningRates);

  /// Overall import function
  /**
   * @brief Imports the entire simulation configuration
   * @details Calls all sections specific methods and saves the result in the CombinationConfig
   *
   * @param[in]     configurationDir       Path of the configuration files
   * @param[in]     simulationConfigFile   Name of the SimulationConfig file
   * @param[out]    simulationConfig       Class into which the values get saved
   * @param         executionPath Target of the /proc/self/exe symlink on Linux, canonical path of executable on Windows
   * @return        true, if successful
   */
  static bool Import(const std::string& configurationDir,
                     const std::string& simulationConfigFile,
                     Configuration::SimulationConfig& simulationConfig,
                     const std::filesystem::path& executionPath);

private:
  static std::string GetLibrary(xmlNodePtr root, const std::string& key, const std::string& tag);
  static constexpr auto supportedConfigVersion = "0.8.2";

  //! \brief Identifier with correspondiong default values for mandatory libraries
  //! \note: The identifier is identical to the XML tag
  inline static const ExperimentConfig::Libraries defaultLibraryMapping
      = {{"DataBufferLibrary", "BasicDataBuffer"}, {"WorldLibrary", "World"}, {"StochasticsLibrary", "Stochastics"}};
};

}  //namespace Importer
