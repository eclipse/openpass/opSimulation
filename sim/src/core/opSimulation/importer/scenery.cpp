/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2020 HLRS, University of Stuttgart
 *               2016-2018 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "scenery.h"

#include <algorithm>
#include <cassert>
#include <memory>
#include <new>
#include <ostream>
#include <utility>

#include "common/log.h"
#include "importer/junction.h"
#include "importer/road.h"
#include "include/roadInterface/roadInterface.h"

class JunctionInterface;

namespace Configuration
{

RoadInterface *Scenery::AddRoad(const std::string &id, const double length)
{
  if (auto road = roads.try_emplace(id, std::make_unique<Road>(id, length)); road.second)
  {
    return road.first->second.get();
  }

  LOG_INTERN(LogLevel::Error) << "roads must be unique";
  return nullptr;
}

JunctionInterface *Scenery::AddJunction(const std::string &id)
{
  if (auto junction = junctions.try_emplace(id, std::make_unique<Junction>(id)); junction.second)
  {
    return junction.first->second.get();
  }

  LOG_INTERN(LogLevel::Error) << "junctions must be unique";
  return nullptr;
}

}  //namespace Configuration
