/********************************************************************************
 * Copyright (c) 2017 ITK Engineering GmbH
 *               2019-2020 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "systemConfigImporter.h"

#include <algorithm>
#include <iostream>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <locale>
#include <map>
#include <optional>
#include <stdexcept>
#include <utility>
#include <vector>

#include "common/commonTools.h"
#include "common/log.h"
#include "common/stochasticDefinitions.h"
#include "common/xmlParser.h"
#include "importer/importerCommon.h"
#include "importer/importerLoggingHelper.h"
#include "modelElements/agentType.h"
#include "modelElements/componentType.h"
#include "modelElements/parameters.h"
#include "systemConfig.h"

using namespace SimulationCommon;

namespace TAG = openpass::importer::xml::systemConfigImporter::tag;

namespace Importer
{

openpass::parameter::ParameterSetLevel1 SystemConfigImporter::ImportSystemParameters(xmlNodePtr parametersElement)
{
  openpass::parameter::ParameterSetLevel1 param;
  xmlNodePtr parameterElement = GetFirstChildElement(parametersElement, "parameter");
  while (parameterElement)
  {
    if (xmlStrEqual(parameterElement->name, toXmlChar(TAG::parameter)))
    {
      //int id{};
      //ParseInt(parameterElement, "id", id);
      std::string id;
      ParseString(parameterElement, "id", id);

      std::string type{};
      ParseString(parameterElement, "type", type);
      std::string value{};
      ParseString(parameterElement, "value", value);

      if (type == "int")
      {
        param.emplace_back(id, std::stoi(value));
      }
      else if (type == "double")
      {
        param.emplace_back(id, stod(value));
      }
      else if (type == "bool")
      {
        std::transform(value.begin(), value.end(), value.begin(), ::tolower);
        bool valueRobust = false;
        if (value == "true" || (value.size() == 1 && stoi(value) > 0))
        {
          valueRobust = true;
        }
        param.emplace_back(id, valueRobust);
      }
      else if (type == "string")
      {
        param.emplace_back(id, value);
      }
      else if (type == "intVector")
      {
        std::vector<int> vector{};
        std::stringstream valueStream(value);

        int item{};
        while (valueStream >> item)
        {
          vector.push_back(item);

          if (valueStream.peek() == ',')
          {
            valueStream.ignore();
          }
        }
        param.emplace_back(id, vector);
      }
      else if (type == "doubleVector")
      {
        std::vector<double> vector{};
        std::stringstream valueStream(value);

        double item{};
        while (valueStream >> item)
        {
          vector.push_back(item);

          if (valueStream.peek() == ',')
          {
            valueStream.ignore();
          }
        }
        param.emplace_back(id, vector);
      }
      else if (type == "boolVector")
      {
        std::vector<bool> vector{};

        const auto items{CommonHelper::TokenizeString(value)};

        for (const auto& item : items)
        {
          if (item == "0" || item == "false")
          {
            vector.push_back(false);
          }
          else if (item == "1" || item == "true")
          {
            vector.push_back(true);
          }
          else
          {
            throw std::runtime_error("Invalid boolean value in SystemConfig parameter at line "
                                     + std::to_string(parametersElement->line));
          }
        }

        param.emplace_back(id, vector);
      }
      else if (type == "normalDistribution")
      {
        xmlNodePtr valueElement = GetFirstChildElement(parameterElement, "value");
        ThrowIfFalse(valueElement, parameterElement, "Missing value");

        double min{}, max{}, mean{}, sd{};
        ThrowIfFalse(Parse(valueElement, "min", min), valueElement, "Missing min");
        ThrowIfFalse(Parse(valueElement, "max", max), valueElement, "Missing max");

        if (!Parse(valueElement, "mean", mean))
        {
          ThrowIfFalse(Parse(valueElement, "mu", min), valueElement, "Missing mean or mu");
        }

        if (!Parse(valueElement, "sd", sd))
        {
          ThrowIfFalse(Parse(valueElement, "sigma", sd), valueElement, "Missing sd or sigma");
        }

        openpass::parameter::NormalDistribution distribution{mean, sd, min, max};
        param.emplace_back(id, distribution);
      }
      else if (type == "logNormalDistribution")
      {
        xmlNodePtr valueElement = GetFirstChildElement(parameterElement, "value");
        ThrowIfFalse(valueElement, parameterElement, "Missing value");

        double min{}, max{}, mean{}, sd{0}, mu{0}, sigma{0};
        ThrowIfFalse(Parse(valueElement, "min", min), valueElement, "Missing min");
        ThrowIfFalse(Parse(valueElement, "max", max), valueElement, "Missing max");

        if (Parse(valueElement, "mean", mean) && Parse(valueElement, "sd", sd))
        {
          auto distribution = openpass::parameter::LogNormalDistribution::CreateWithMeanSd(mean, sd, min, max);
          param.emplace_back(id, distribution);
        }
        else
        {
          ThrowIfFalse(Parse(valueElement, "mu", mu) && Parse(valueElement, "sigma", sigma),
                       valueElement,
                       "Missing mean and sd or mu and sigma");

          openpass::parameter::LogNormalDistribution distribution{mu, sigma, min, max};
          param.emplace_back(id, distribution);
        }
      }
      else if (type == "exponentialDistribution")
      {
        xmlNodePtr valueElement = GetFirstChildElement(parameterElement, "value");
        ThrowIfFalse(valueElement, parameterElement, "Missing value");

        double min{}, max{}, lambda{}, mean{0};
        ThrowIfFalse(Parse(valueElement, "min", min), valueElement, "Missing min");
        ThrowIfFalse(Parse(valueElement, "max", max), valueElement, "Missing max");

        if (!Parse(valueElement, "lambda", lambda))
        {
          ThrowIfFalse(Parse(valueElement, "mean", mean), valueElement, "Missing mean or mu");
          lambda = 1 / mean;
        }

        openpass::parameter::ExponentialDistribution distribution{lambda, min, max};
        param.emplace_back(id, distribution);
      }
      else if (type == "uniformDistribution")
      {
        xmlNodePtr valueElement = GetFirstChildElement(parameterElement, "value");
        ThrowIfFalse(valueElement, parameterElement, "Missing value");

        double min{}, max{};
        ThrowIfFalse(Parse(valueElement, "min", min), valueElement, "Missing min");
        ThrowIfFalse(Parse(valueElement, "max", max), valueElement, "Missing max");

        openpass::parameter::UniformDistribution distribution{min, max};
        param.emplace_back(id, distribution);
      }
      else if (type == "gammaDistribution")
      {
        xmlNodePtr valueElement = GetFirstChildElement(parameterElement, "value");
        ThrowIfFalse(valueElement, parameterElement, "Missing value");

        double min{}, max{}, mean{}, sd{0}, shape{0}, scale{0};
        ThrowIfFalse(Parse(valueElement, "min", min), valueElement, "Missing min");
        ThrowIfFalse(Parse(valueElement, "max", max), valueElement, "Missing max");

        if (Parse(valueElement, "mean", mean) && Parse(valueElement, "sd", sd))
        {
          auto distribution = openpass::parameter::GammaDistribution::CreateWithMeanSd(mean, sd, min, max);
          param.emplace_back(id, distribution);
        }
        else
        {
          ThrowIfFalse(Parse(valueElement, "shape", shape) && Parse(valueElement, "scale", scale),
                       valueElement,
                       "Missing mean and sd or mu and sigma");

          openpass::parameter::GammaDistribution distribution{shape, scale, min, max};
          param.emplace_back(id, distribution);
        }
      }
    }
    parameterElement = xmlNextElementSibling(parameterElement);
  }
  return param;
}

xmlDocPtr SystemConfigImporter::ImportSystemConfigContent(const std::string& filename)
{
  xmlInitParser();

  if (!std::filesystem::exists(filename))
  {
    LOG_INTERN(LogLevel::Info) << filename + " does not exist";
    return nullptr;
  }

  xmlDocPtr document
      = xmlReadFile(filename.c_str(),
                    NULL,  // NOLINT(modernize-use-nullptr)
                           // https://gnome.pages.gitlab.gnome.org/libxml2/devhelp/libxml2-parser.html#xmlReadFile
                    0);
  ThrowIfFalse(document, "an error occurred during agent type import. Invalid xml file format of file " + filename);

  return document;
}

bool SystemConfigImporter::Import(const std::string& filename,
                                  const std::shared_ptr<Configuration::SystemConfig>& systemConfig,
                                  const std::filesystem::path& executionPath)
{
  try
  {
    xmlDocPtr document = ImportSystemConfigContent(filename);
    if (!document)
    {
      return false;
    }

    xmlNodePtr documentRoot = xmlDocGetRootElement(document);
    if (!documentRoot)
    {
      return false;
    }

    ImporterCommon::validateXMLSchema(filename, "SystemConfig.xsd", document, executionPath);

    // parse agents
    const auto& agentTypes = systemConfig->GetSystems();
    xmlNodePtr systemElement = GetFirstChildElement(documentRoot, TAG::system);
    while (systemElement)
    {
      if (xmlStrEqual(systemElement->name, toXmlChar(TAG::system)))
      {
        // retrieve agent id
        int agentId{};
        ThrowIfFalse(ParseInt(systemElement, "id", agentId), systemElement, "Unable to retrieve agent id.");
        LOG_INTERN(LogLevel::DebugCore) << "agent type id: " << agentId
                                        << " *********************************************************";

        // retrieve agent priority
        int agentPriority{};
        ThrowIfFalse(
            ParseInt(systemElement, "priority", agentPriority), systemElement, "Unable to retrieve agent priority.");
        ThrowIfFalse(0 <= agentPriority, "Invalid agent priority.");

        LOG_INTERN(LogLevel::DebugCore) << "agent type priority: " << agentPriority;

        // create agent
        ThrowIfFalse(0 == agentTypes.count(agentId), systemElement, "Duplicate agent id.");  // avoid duplicate types

        std::shared_ptr<core::AgentType> agent = std::make_shared<core::AgentType>();
        ThrowIfFalse(agent != nullptr, "Agent is null");

        ThrowIfFalse(systemConfig->AddSystem(agentId, agent), systemElement, "Unable to add agent.");

        // parse components
        xmlNodePtr componentsElement = GetFirstChildElement(systemElement, TAG::components);
        ThrowIfFalse(componentsElement, systemElement, "Tag " + std::string(TAG::components) + " is missing.");

        xmlNodePtr componentElement = GetFirstChildElement(componentsElement, TAG::component);

        while (componentElement)
        {
          if (xmlStrEqual(componentElement->name, toXmlChar(TAG::component)))
          {
            // retrieve component id
            std::string componentId;
            ThrowIfFalse(
                ParseString(componentElement, "id", componentId), componentElement, "Unable to retrieve component id.");
            LOG_INTERN(LogLevel::DebugCore)
                << "component type id: " << componentId << " ---------------------------------------------------------";

            // retrieve component library
            std::string library;
            ThrowIfFalse(ParseString(componentElement, "library", library),
                         componentElement,
                         "Unable to retrieve component library.");
            ThrowIfFalse(!library.empty(), componentElement, "Component library is empty.");
            LOG_INTERN(LogLevel::DebugCore) << "library: " << library;

            xmlNodePtr scheduleElement = GetFirstChildElement(componentElement, "schedule");

            // retrieve component priority
            int componentPriority{};
            ThrowIfFalse(ParseInt(scheduleElement, "priority", componentPriority),
                         scheduleElement,
                         "Unable to retrieve component priority.");
            ThrowIfFalse(0 <= componentPriority, scheduleElement, "Invalid component priority.");
            LOG_INTERN(LogLevel::DebugCore) << "component priority: " << componentPriority;

            // retrieve component offset time
            int offsetTime = 0;  // must be set to 0 for init tasks for scheduler
            ThrowIfFalse(ParseInt(scheduleElement, "offset", offsetTime),
                         scheduleElement,
                         "Unable to retrieve component offset time");
            ThrowIfFalse(0 <= offsetTime, scheduleElement, "Invalid component offset time.");
            LOG_INTERN(LogLevel::DebugCore) << "offset time: " << offsetTime;

            // retrieve component response time
            int responseTime = 0;  // must be set to 0 for init tasks for scheduler
            ThrowIfFalse(ParseInt(scheduleElement, "response", responseTime),
                         scheduleElement,
                         "Unable to retrieve component response time.");
            ThrowIfFalse(0 <= responseTime, scheduleElement, "Invalid component response time.");
            LOG_INTERN(LogLevel::DebugCore) << "response time: " << responseTime;

            // retrieve component cycle time
            int cycleTime = 0;  // must be set to 0 for init tasks for scheduler
            ThrowIfFalse(ParseInt(scheduleElement, "cycle", cycleTime),
                         scheduleElement,
                         "Unable to retrieve component cycle time.");
            ThrowIfFalse(0 <= cycleTime, scheduleElement, "Invalid component cycle time.");
            LOG_INTERN(LogLevel::DebugCore) << "cycle time: " << cycleTime;

            bool isInitComponent = false;
            if (cycleTime == 0)
            {
              isInitComponent = true;
            }

            auto component = std::make_shared<core::ComponentType>(
                componentId, isInitComponent, componentPriority, offsetTime, responseTime, cycleTime, library);
            ThrowIfFalse(component != nullptr, componentElement, "Component is null.");
            ThrowIfFalse(agent->AddComponent(component), componentElement, "Unable to add component.");

            // parse model parameters
            LOG_INTERN(LogLevel::DebugCore) << "import model parameters...";
            xmlNodePtr parametersElement = GetFirstChildElement(componentElement, TAG::parameters);

            ThrowIfFalse(parametersElement, componentElement, "Tag " + std::string(TAG::parameters) + " is missing.");

            try
            {
              component->SetModelParameter(ImportSystemParameters(parametersElement));
            }
            catch (const std::runtime_error& error)
            {
              LogErrorAndThrow("Unable to import system parameters: " + std::string(error.what()));
            }
          }

          componentElement = xmlNextElementSibling(componentElement);
        }  // if components exist

        // parse connections
        xmlNodePtr connectionsElement = GetFirstChildElement(systemElement, TAG::connections);
        ThrowIfFalse(connectionsElement, systemElement, "Tag " + std::string(TAG::connections) + " is missing.");

        std::map<std::pair<std::string, int>, int> channelMap;
        xmlNodePtr connectionElement = GetFirstChildElement(connectionsElement, TAG::connection);
        while (connectionElement)
        {
          if (xmlStrEqual(connectionElement->name, toXmlChar(TAG::connection)))
          {
            xmlNodePtr sourceElement = GetFirstChildElement(connectionElement, TAG::source);
            std::string sourceId;
            ParseString(sourceElement, TAG::component, sourceId);
            int sourceOutputId{};
            ParseInt(sourceElement, TAG::output, sourceOutputId);

            int channelId{};
            ParseInt(connectionElement, TAG::id, channelId);
            std::pair<std::string, int> componentPair = std::make_pair(sourceId, sourceOutputId);
            std::map<std::pair<std::string, int>, int>::iterator channelIterator;
            channelIterator = channelMap.find(componentPair);

            if (channelIterator == channelMap.end())
            {
              // channelId = channelMap.size();
              channelMap.emplace(componentPair, channelId);
              agent->AddChannel(channelId);

              auto source = helper::map::query(agent->GetComponents(), sourceId);
              ThrowIfFalse(source.has_value(),
                           "Can not add channel. No component with name \"" + sourceId + "\" defined.");
              source.value()->AddOutputLink(sourceOutputId, channelId);
            }
            else
            {
              // channelId = channelIterator->second;
            }

            xmlNodePtr targetElement = GetFirstChildElement(connectionElement, TAG::target);
            while (targetElement)
            {
              if (xmlStrEqual(targetElement->name, toXmlChar(TAG::target)))
              {
                std::string targetId;
                ParseString(targetElement, TAG::component, targetId);
                int targetInputId{};
                ParseInt(targetElement, TAG::input, targetInputId);
                auto target = helper::map::query(agent->GetComponents(), targetId);
                ThrowIfFalse(target.has_value(),
                             "Can not add channel. No component with name \"" + targetId + "\" defined.");
                target.value()->AddInputLink(targetInputId, channelId);
              }
              targetElement = xmlNextElementSibling(targetElement);
            }
          }
          connectionElement = xmlNextElementSibling(connectionElement);
        }
      }

      systemElement = xmlNextElementSibling(systemElement);
    }

    xmlFreeDoc(document);
    xmlCleanupParser();

    return true;
  }
  catch (const std::runtime_error& e)
  {
    LOG_INTERN(LogLevel::Info) << "SystemConfig import failed: " + std::string(e.what());
    return false;
  }
}

}  //namespace Importer
