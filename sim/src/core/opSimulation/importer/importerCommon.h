/********************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *               2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <filesystem>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <string>

#include "common/log.h"
#include "common/xmlParser.h"

/// validates a xml with a xml shema file which is located at the same location
class SIMULATIONCOREEXPORT ImporterCommon
{
public:
  /**
   * \brief
   * \details validates a xml with a xml shema file which is located at the same location
   * logs warnings
   * @param[in]     xmlFileName       Name of the Xml file to be validated
   * @param[in]     xmlSchemaFilename Name of the schema file
   * @param[in]     doc               Xml file data
   * @param         executionPath     Target of the /proc/self/exe symlink on Linux, canonical path of executable on
   * Windows
   * @return	     true, if validation successful
   */
  static bool validateXMLSchema(const std::string& xmlFileName,
                                const std::string& xmlSchemaFilename,
                                xmlDocPtr doc,
                                const std::filesystem::path& executionPath);
};

//! If the first parameter is false or null or empty pointer writes a message into the log including the line number of
//! the erronous xml element
//!
//! \param success      writes message if success is false
//! \param element      erronous xml element
//! \param message      message describing error
//! \param logFunction  function to use for logging
[[maybe_unused]] static void ThrowIfFalse(bool success, xmlNodePtr element, const std::string& message)
{
  if (!success)
  {
    LogErrorAndThrow("Could not import element " + SimulationCommon::toString(element->name) + " (line "
                     + std::to_string(element->line) + "): " + message);
  }
}
