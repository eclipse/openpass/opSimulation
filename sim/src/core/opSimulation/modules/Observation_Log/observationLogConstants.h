/********************************************************************************
 * Copyright (c) 2017-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

/// Struct represents output attributes
struct OutputAttributes
{
  static constexpr const char* FRAMEWORKVERSION{"FrameworkVersion"};  ///< Output framework version
  static constexpr const char* SCHEMAVERSION{"SchemaVersion"};        ///< Output schema version
  static constexpr const char* RUNID{"RunId"};                        ///< Id of the current run
  static constexpr const char* RANDOMSEED{"RandomSeed"};              ///< RandomSeed of the current run
  static constexpr const char* FAILUREPROBABITLITY{
      "FailureProbability"};                        ///< Probability object is not detected although it is visible
  static constexpr const char* LATENCY{"Latency"};  ///< Sensor latency
  static constexpr const char* OPENINGANGLEH{"OpeningAngleH"};  ///< Horizontal opening angle of the sensor
  static constexpr const char* OPENINGANGLEV{"OpeningAngleV"};  ///< Vertical opening angle of the sensor
  static constexpr const char* MOUNTINGPOSITIONLONGITUDINAL{
      "MountingPosLongitudinal"};  ///< Relative longitudinal position of the sensor in relation to the agent
  static constexpr const char* MOUNTINGPOSITIONLATERAL{
      "MountingPosLateral"};  ///< Relative lateral position of the sensor in relation to the agent
  static constexpr const char* MOUNTINGPOSITIONHEIGHT{
      "MountingPosHeight"};  ///< Relative height of the sensor in relation to the agent
  static constexpr const char* ORIENTATIONYAW{
      "OrientationYaw"};  ///< Yaw rotation of the sensor in relation to the agent
  static constexpr const char* ORIENTATIONPITCH{
      "OrientationPitch"};  ///< Pitch rotation of the sensor in relation to the agent
  static constexpr const char* ORIENTATIONROLL{
      "OrientationRoll"};  ///< Roll rotation of the sensor in relation to the agent
  static constexpr const char* DETECTIONRANGE{"DetectionRange"};        ///< Range how far the sensor reaches in m
  static constexpr const char* VEHICLEMODELTYPE{"VehicleModelType"};    ///< Name of the vehicle model
  static constexpr const char* DRIVERPROFILENAME{"DriverProfileName"};  ///< Name of the driver profile
  static constexpr const char* AGENTTYPEGROUPNAME{
      "AgentTypeGroupName"};  ///< The agent category. This can either be Ego, Scenario or Common
  static constexpr const char* AGENTTYPENAME{"AgentTypeName"};  ///< Name of the agent profile
  static constexpr const char* AGENTTYPE{"AgentType"};          ///< Type of the agent
  static constexpr const char* TIME{"Time"};                    ///< Time in milliseconds
  static constexpr const char* TYPE{"Type"};                    ///< Type of the component
  static constexpr const char* NAME{"Name"};                    ///< Name of the component
  static constexpr const char* PROFILE{"Profile"};              ///< Profile of the component
  static constexpr const char* KEY{"Key"};                      ///< Name of parameter as string
  static constexpr const char* VALUE{"Value"};                  ///< Value of parameter as string
  static constexpr const char* ID{"Id"};                        ///< Identification number
  static constexpr const char* SOURCE{"Source"};                ///< Name of the component which created the event
  static constexpr const char* WIDTH{"Width"};                  ///< Width of the vehicles bounding box
  static constexpr const char* LENGTH{"Length"};                ///< Length of the vehicles bounding box
  static constexpr const char* HEIGHT{"Height"};                ///< Height of the vehicles bounding box
  static constexpr const char* LONGITUDINALPIVOTOFFSET{
      "LongitudinalPivotOffset"};  ///< Distance between center of the bounding box and reference point of the agent
};

/// Struct represents output tags
struct OutputTags
{
  static constexpr const char* RUNRESULTS{
      "RunResults"};  ///< This section contains all RunResults that occurred during the invocations
  static constexpr const char* RUNRESULT{"RunResult"};  ///< This section contains information about the agents, their
                                                        ///< parameters, events related to the entities
  static constexpr const char* SIMULATIONOUTPUT{
      "SimulationOutput"};  ///< This section contains central information about all
                            ///< executed invocations within an experiment
  static constexpr const char* EVENTS{
      "Events"};  ///< This section contains all events that occurred during the invocation
  static constexpr const char* EVENT{"Event"};  ///< This section contains event related to the entity
  static constexpr const char* EVENTPARAMETER{
      "EventParameter"};  ///< This section contains generic key/value string pairs
  static constexpr const char* AGENTS{
      "Agents"};  ///< This section contains some information on how each agent is configured
  static constexpr const char* AGENT{
      "Agent"};  ///< This section contains some information on how the agent is configured
  static constexpr const char* COMPONTENTS{"Components"};  ///< This section contains all components and their profile
  static constexpr const char* COMPONTENT{"Component"};    ///< This section contains one components and their profile
  static constexpr const char* SENSORS{
      "Sensors"};                                 ///< This section contains all sensors of the agent and its parameters
  static constexpr const char* SENSOR{"Sensor"};  ///< This section contains sensor of the agent and its parameters
  static constexpr const char* CYCLICS{
      "Cyclics"};  ///< This section contains all logged parameters of the agents per time step
  static constexpr const char* CYCLICSFILE{"CyclicsFile"};  ///< Reference to the corresponding CSV file
  static constexpr const char* HEADER{"Header"};            ///< Layout of all samples
  static constexpr const char* SAMPLES{
      "Samples"};  ///< This section contains information for all timesteps for which samples exist
  static constexpr const char* SAMPLE{"Sample"};  ///< This section contains information for one specific time step
  static constexpr const char* SCENERYFILE{"SceneryFile"};  ///< Reference to the used OpenDRIVE scenery file
  static constexpr const char* VEHICLEATTRIBUTES{"VehicleAttributes"};  ///< Basic information of the vehicle parameters
  static constexpr const char* TRIGGERINGENTITIES{"TriggeringEntities"};  ///< Entity IDs triggering this event
  static constexpr const char* AFFECTEDENTITIES{"AffectedEntities"};      ///< Entity IDs affected by this event
};

namespace output::tag
{
static constexpr const char* ENTITY{"Entity"};
static constexpr const char* PARAMETERS{"Parameters"};
static constexpr const char* PARAMETER{"Parameter"};
}  // namespace output::tag

namespace output::attribute
{
static constexpr const char* ID{"Id"};
static constexpr const char* TIME{"Time"};
static constexpr const char* TYPE{"Type"};
static constexpr const char* NAME{"Name"};
static constexpr const char* KEY{"Key"};
static constexpr const char* VALUE{"Value"};
}  // namespace output::attribute

namespace output::event::tag
{
static constexpr const char* EVENTS{"NewEvents"};
static constexpr const char* EVENT{"Event"};
static constexpr const char* TRIGGERING_ENTITIES{"TriggeringEntities"};
static constexpr const char* AFFECTED_ENTITIES{"AffectedEntities"};
}  // namespace output::event::tag
