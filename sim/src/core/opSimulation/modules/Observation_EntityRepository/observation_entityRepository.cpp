/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  observation_entityRepository.cpp */
//-----------------------------------------------------------------------------

#include "observation_entityRepository.h"

#include <new>
#include <stdexcept>
#include <string>

#include "include/callbackInterface.h"
#include "include/observationInterface.h"
#include "observation_entityRepositoryImplementation.h"
#include "opSimulation/modules/Observation_EntityRepository/observation_entityRepositoryGlobal.h"

class DataBufferReadInterface;
class ParameterInterface;
class RunResultInterface;
class StochasticsInterface;
class WorldInterface;

const std::string Version = "0.2.0";  //!< The version of the current module - has to be incremented manually
static const CallbackInterface* Callbacks = nullptr;  // NOLINT[cppcoreguidelines-avoid-non-const-global-variables]

//-----------------------------------------------------------------------------
//! dll-function to obtain the version of the current module
//!
//! @return                       Version of the current module
//-----------------------------------------------------------------------------
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT const std::string& OpenPASS_GetVersion()
{
  return Version;
}

//-----------------------------------------------------------------------------
//! dll-function to create an instance of the module
//!
//! @param[in]     stochastics    Pointer to the stochastics class loaded by the framework
//! @param[in]     world          Pointer to the world
//! @param[in]     parameters     Pointer to the parameters of the module
//! @param[in]     callbacks      Pointer to the callbacks
//! @param[in]     dataBuffer     Pointer to the data buffer that provides read-only access to the data
//! @return                       A pointer to the created module instance
//-----------------------------------------------------------------------------
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT ObservationInterface* OpenPASS_CreateInstance(
    StochasticsInterface* stochastics,
    WorldInterface* world,
    const ParameterInterface* parameters,
    const CallbackInterface* callbacks,
    DataBufferReadInterface* dataBuffer)
{
  Callbacks = callbacks;

  try
  {
    return (ObservationInterface*)(new (std::nothrow) ObservationEntityRepositoryImplementation(
        stochastics, world, parameters, callbacks, dataBuffer));
  }
  catch (const std::runtime_error& ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return nullptr;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return nullptr;
  }
}

//-----------------------------------------------------------------------------
//! dll-function to destroy/delete an instance of the module
//!
//! @param[in]     implementation    The instance that should be freed
//-----------------------------------------------------------------------------
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT void OpenPASS_DestroyInstance(
    ObservationInterface* implementation)
{
  delete implementation;  // NOLINT(cppcoreguidelines-owning-memory)
}

/**
 * dll-function called by framework in simulation before all simulation runs start
 *
 * @param[in]     implementation    Pointer to the ObservationInterface
 * @return  returns true if the instance is invoked, false otherwise
 */
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT bool OpenPASS_OpSimulationPreHook(
    ObservationInterface* implementation)
{
  try
  {
    implementation->OpSimulationPreHook();
  }
  catch (const std::runtime_error& ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return false;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return false;
  }

  return true;
}

/**
 * dll-function to call an instance of the module in simulation before each simulation run starts be stored
 *
 * @param[in]     implementation    The instance that should be called
 * @return  returns true if the instance is invoked, false otherwise
 */
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT bool OpenPASS_OpSimulationPreRunHook(
    ObservationInterface* implementation)
{
  try
  {
    implementation->OpSimulationPreRunHook();
  }
  catch (const std::runtime_error& ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return false;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return false;
  }

  return true;
}

/**
 * dll-function to call an instance of the module in simulation at each time step.
 * Observation module can indicate end of simulation run here.
 *
 * @param[in]       implementation  The instance that should be called
 * @param[in]       time            Current scheduling time
 * @param[in,out]   runResult       Reference to run result
 * @return  returns true if the instance is invoked, false otherwise
 */
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT bool OpenPASS_OpSimulationUpdateHook(
    ObservationInterface* implementation, int time, RunResultInterface& runResult)
{
  try
  {
    implementation->OpSimulationUpdateHook(time, runResult);
  }
  catch (const std::runtime_error& ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return false;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return false;
  }

  return true;
}

/**
 * dll-function to call an instance of the module in simulation after each simulation run ends.
 * Observation module can observe the current simulation run here.
 *
 * @param[in]       implementation  The instance that should be called
 * @param[in,out]   runResult       Reference to run result
 * @return  returns true if the instance is invoked, false otherwise
 */
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT bool OpenPASS_OpSimulationPostRunHook(
    ObservationInterface* implementation, const RunResultInterface& runResult)
{
  try
  {
    implementation->OpSimulationPostRunHook(runResult);
  }
  catch (const std::runtime_error& ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return false;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return false;
  }

  return true;
}

/**
 * dll-function to call an instance of the module in simulation after all simulation runs end.
 *
 * @param[in]       implementation  The instance that should be called
 * @return  returns true if the instance is invoked, false otherwise
 */
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT bool OpenPASS_OpSimulationPostHook(
    ObservationInterface* implementation)
{
  try
  {
    implementation->OpSimulationPostHook();
  }
  catch (const std::runtime_error& ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return false;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return false;
  }

  return true;
}
