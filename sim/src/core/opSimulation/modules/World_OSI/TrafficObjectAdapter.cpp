/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2017-2020 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "TrafficObjectAdapter.h"

#include <MantleAPI/Common/dimension.h>
#include <map>
#include <stdexcept>
#include <utility>
#include <variant>

#include "OWL/DataTypes.h"
#include "WorldData.h"

class WorldObjectInterface;

TrafficObjectAdapter::TrafficObjectAdapter(const openpass::type::EntityId id,
                                           OWL::Interfaces::WorldData& worldData,
                                           const World::Localization::Localizer& localizer,
                                           mantle_api::Vec3<units::length::meter_t> position,
                                           mantle_api::Dimension3 dimension,
                                           mantle_api::Orientation3<units::angle::radian_t> orientation,
                                           OpenDriveId odId)  // NOLINT(performance-unnecessary-value-param)
    : WorldObjectAdapter{worldData.AddStationaryObject(
        id.value,
        static_cast<void*>(static_cast<WorldObjectInterface*>(
            this)))},  // objects passed as void * need to be casted to WorldObjectInterface*, because they are
                       // retrieved by casting to that class
      localizer{localizer},
      openDriveId{odId}
{
  // clang-format off
  static_cast<OWL::Interfaces::StationaryObject*>(&baseTrafficObject)->SetSourceReference(odId); // NOLINT(cppcoreguidelines-pro-type-static-cast-downcast)
  // clang-format on
  baseTrafficObject.SetReferencePointPosition(position);
  baseTrafficObject.SetDimension(dimension);
  baseTrafficObject.SetAbsOrientation(orientation);
  InitLaneDirection(orientation.yaw);
  Locate();
}

ObjectTypeOSI TrafficObjectAdapter::GetType() const
{
  return ObjectTypeOSI::Object;
}

bool TrafficObjectAdapter::GetIsCollidable() const
{
  return (GetHeight() > 0_m && GetLength() > 0_m && GetWidth() > 0_m);
}

void TrafficObjectAdapter::InitLaneDirection(units::angle::radian_t hdg)
{
  laneDirection = GetYaw() - hdg;
}

Common::Vector2d<units::velocity::meters_per_second_t> TrafficObjectAdapter::GetVelocity(
    [[maybe_unused]] ObjectPoint point) const
{
  //TrafficObjects don't move
  return {0.0_mps, 0.0_mps};
}

Common::Vector2d<units::acceleration::meters_per_second_squared_t> TrafficObjectAdapter::GetAcceleration(
    [[maybe_unused]] ObjectPoint point) const
{
  //TrafficObjects don't move
  return {0.0_mps_sq, 0.0_mps_sq};
}

units::angle::radian_t TrafficObjectAdapter::GetLaneDirection() const
{
  return laneDirection;
}

bool TrafficObjectAdapter::Locate()
{
  // reset on-demand values
  boundaryPoints.clear();

  locateResult = localizer.Locate(GetBoundingBox2D(), baseTrafficObject);

  return true;
}

void TrafficObjectAdapter::Unlocate()
{
  localizer.Unlocate(baseTrafficObject);
}

const RoadIntervals& TrafficObjectAdapter::GetTouchedRoads() const
{
  return locateResult.touchedRoads;
}

Common::Vector2d<units::length::meter_t> TrafficObjectAdapter::GetAbsolutePosition(const ObjectPoint& objectPoint) const
{
  units::length::meter_t longitudinal;
  units::length::meter_t lateral;
  if (std::holds_alternative<ObjectPointCustom>(objectPoint))
  {
    longitudinal = std::get<ObjectPointCustom>(objectPoint).longitudinal;
    lateral = std::get<ObjectPointCustom>(objectPoint).lateral;
  }
  else if (std::holds_alternative<ObjectPointPredefined>(objectPoint))
  {
    switch (std::get<ObjectPointPredefined>(objectPoint))
    {
      case ObjectPointPredefined::Reference:
      case ObjectPointPredefined::Center:
        longitudinal = 0_m;
        lateral = 0_m;
        break;
      case ObjectPointPredefined::FrontCenter:
        longitudinal = 0.5 * GetLength();
        lateral = 0_m;
        break;
      case ObjectPointPredefined::RearCenter:
        longitudinal = -0.5 * GetLength();
        lateral = 0_m;
        break;
      case ObjectPointPredefined::FrontLeft:
        longitudinal = 0.5 * GetLength();
        lateral = 0.5 * GetWidth();
        break;
      case ObjectPointPredefined::FrontRight:
        longitudinal = 0.5 * GetLength();
        lateral = -0.5 * GetWidth();
        break;
      case ObjectPointPredefined::RearLeft:
        longitudinal = -0.5 * GetLength();
        lateral = 0.5 * GetWidth();
        break;
      case ObjectPointPredefined::RearRight:
        longitudinal = -0.5 * GetLength();
        lateral = -0.5 * GetWidth();
        break;
    }
  }
  else
  {
    throw std::runtime_error("Unknown type of ObjectPoint");
  }
  const auto& referencePoint = baseTrafficObject.GetReferencePointPosition();
  const auto& yaw = baseTrafficObject.GetAbsOrientation().yaw;
  auto x = referencePoint.x + units::math::cos(yaw) * longitudinal - units::math::sin(yaw) * lateral;
  auto y = referencePoint.y + units::math::sin(yaw) * longitudinal + units::math::cos(yaw) * lateral;
  return {x, y};
}

const GlobalRoadPositions& TrafficObjectAdapter::GetRoadPosition(const ObjectPoint& point) const
{
  auto position = locateResult.points.find(point);
  if (position != locateResult.points.cend())
  {
    return position->second;
  }
  const auto globalPoint = GetAbsolutePosition(point);
  auto locatedPoint = localizer.Locate(globalPoint, GetYaw());
  auto [newElement, success] = locateResult.points.insert({point, locatedPoint});
  return newElement->second;
}

OpenDriveId TrafficObjectAdapter::GetOpenDriveId() const
{
  return openDriveId;
}

TrafficObjectAdapter::~TrafficObjectAdapter()
{
  Unlocate();
}
