/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2020 HLRS, University of Stuttgart
 *               2017-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SceneryConverter.h"

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>
#include <algorithm>
#include <cassert>
#include <cmath>
#include <exception>
#include <memory>
#include <optional>
#include <osi3/osi_trafficlight.pb.h>
#include <osi3/osi_trafficsign.pb.h>
#include <stdexcept>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>

#include "GeometryConverter.h"
#include "OWL/LaneGeometryElement.h"
#include "OWL/LaneGeometryJoint.h"
#include "OWL/OpenDriveTypeMapper.h"
#include "SceneryEntities.h"
#include "TrafficObjectAdapter.h"
#include "WorldData.h"
#include "WorldDataQuery.h"
#include "common/commonTools.h"
#include "common/hypot.h"
#include "common/vector2d.h"
#include "common/worldDefinitions.h"
#include "commonHelper.h"
#include "include/roadInterface/connectionInterface.h"
#include "include/roadInterface/junctionInterface.h"
#include "include/roadInterface/roadGeometryInterface.h"
#include "include/roadInterface/roadInterface.h"
#include "include/roadInterface/roadLaneInterface.h"
#include "include/roadInterface/roadLaneRoadMark.h"
#include "include/roadInterface/roadLaneSectionInterface.h"
#include "include/roadInterface/roadLinkInterface.h"
#include "include/roadInterface/roadObjectInterface.h"
#include "include/roadInterface/roadSignalInterface.h"
#include "include/sceneryInterface.h"
#include "namedType.h"

namespace Internal
{

ConversionStatus ConnectJunction(const SceneryInterface *scenery,
                                 const JunctionInterface *junction,
                                 const PathInJunctionConnector &connectPathInJunction)
{
  for (auto &entry : junction->GetConnections())
  {
    ConnectionInterface *connection = entry.second;

    const std::string &incomingRoadId = connection->GetIncommingRoadId();
    const auto *incomingRoad = scenery->GetRoad(incomingRoadId);

    const std::string &connectingId = connection->GetConnectingRoadId();
    const auto *connectingRoad = scenery->GetRoad(connectingId);

    std::string outgoingRoadId;

    ContactPointType connectingContactPoint = connection->GetContactPoint();
    ContactPointType incomingContactPoint{ContactPointType::Undefined};
    ContactPointType outgoingContactPoint{ContactPointType::Undefined};

    const auto incomingRoadLinkType
        = (connectingContactPoint == ContactPointType::Start ? RoadLinkType::Predecessor : RoadLinkType::Successor);
    const auto outgoingRoadLinkType
        = (connectingContactPoint == ContactPointType::Start ? RoadLinkType::Successor : RoadLinkType::Predecessor);
    for (const auto &roadLink : connectingRoad->GetRoadLinks())
    {
      if (roadLink->GetType() == incomingRoadLinkType)
      {
        incomingContactPoint = roadLink->GetContactPoint();
      }
      if (roadLink->GetType() == outgoingRoadLinkType)
      {
        outgoingRoadId = roadLink->GetElementId();
        outgoingContactPoint = roadLink->GetContactPoint();
      }
    }

    const auto *outgoingRoad = scenery->GetRoad(outgoingRoadId);

    if (!incomingRoad || !connectingRoad || !outgoingRoad)
    {
      return {false,
              "Road linkage of junction " + junction->GetId()
                  + ": Potentially wrong ID of incoming, connecting, or outgoing road."};
    }

    if (incomingRoad->GetLaneSections().empty())
    {
      return {false,
              "Road linkage of junction " + junction->GetId() + ": Incoming road (" + incomingRoad->GetId()
                  + ") has no sections"};
    }

    if (connectingRoad->GetLaneSections().empty())
    {
      return {false,
              "Road linkage of junction " + junction->GetId() + ": Connecting road (" + incomingRoad->GetId()
                  + ") has no sections"};
    }

    const auto &laneIdMapping = connection->GetLinks();
    connectPathInJunction(junction,
                          incomingRoad,
                          connectingRoad,
                          outgoingRoad,
                          incomingContactPoint,
                          connectingContactPoint,
                          outgoingContactPoint,
                          laneIdMapping);
  }

  return {true};
}
}  // namespace Internal

Position SceneryConverter::RoadCoord2WorldCoord(const RoadInterface *road,
                                                units::length::meter_t s,  // NOLINT(readability-identifier-length)
                                                units::length::meter_t t,  // NOLINT(readability-identifier-length)
                                                units::angle::radian_t yaw)
{
  const auto &geometries = road->GetGeometries();
  auto geometry = std::find_if(geometries.cbegin(),
                               geometries.cend(),
                               [&](const std::unique_ptr<RoadGeometryInterface> &geometry)
                               { return (geometry->GetS() <= s) && (geometry->GetS() + geometry->GetLength() >= s); });
  if (geometry == geometries.end())
  {
    throw std::runtime_error("GetPositionForRoadCoordinates: No matching geometry within given s-coordinate");
  }
  auto coordinates = (*geometry)->GetCoord(s - (*geometry)->GetS(), t);
  auto absoluteYaw = CommonHelper::SetAngleToValidRange((*geometry)->GetDir(s - (*geometry)->GetS()) + yaw);
  return {units::length::meter_t(coordinates.x), units::length::meter_t(coordinates.y), absoluteYaw, 0_i_m};
}

SceneryConverter::SceneryConverter(const SceneryInterface *scenery,
                                   IdManagerInterface *idManager,
                                   EntityInfoPublisher &entityInfoPublisher,
                                   OWL::Interfaces::WorldData &worldData,
                                   const World::Localization::Localizer &localizer,
                                   const CallbackInterface *callbacks)
    : scenery(scenery),
      idManager(idManager),
      entityInfoPublisher(entityInfoPublisher),
      worldData(worldData),
      localizer(localizer),
      callbacks(callbacks)
{
}

RoadLaneInterface *SceneryConverter::GetOtherLane(RoadLaneSectionInterface *otherSection, int otherId)
{
  // search for connected lane in OpenDRIVE data structures since LaneId of
  // LINEAR_LANE Objects might be reordered according to direction definition
  RoadLaneInterface *connectedRoadLane{nullptr};

  for (const auto &findIt : otherSection->GetLanes())
  {
    connectedRoadLane = findIt.second.get();
    if (connectedRoadLane->GetId() == otherId)
    {
      return connectedRoadLane;
    }
  }

  return nullptr;
}

RoadInterface *SceneryConverter::GetConnectedRoad(RoadLinkInterface *roadLink)
{
  if (ContactPointType::Start != roadLink->GetContactPoint() && ContactPointType::End != roadLink->GetContactPoint())
  {
    LOG(CbkLogLevel::Error, "no contact point defined for road link");
    return nullptr;
  }

  RoadInterface *connectedRoad = nullptr;
  for (const auto &[_, itemRoad] : scenery->GetRoads())
  {
    if (roadLink->GetElementId() == itemRoad->GetId())
    {
      connectedRoad = itemRoad;
      break;
    }
  }

  if (!connectedRoad)
  {
    LOG(CbkLogLevel::Error, "missing road for ElementId " + roadLink->GetElementId());
    return nullptr;
  }

  return connectedRoad;
}

void SceneryConverter::MarkDirectionRoad(RoadInterface *road, bool inDirection)
{
  LOG(CbkLogLevel::Debug, "direction of road " + road->GetId() + ": " + (inDirection ? "true" : "false"));

  road->SetInDirection(inDirection);

  for (const auto &roadLaneSection : road->GetLaneSections())
  {
    roadLaneSection->SetInDirection(inDirection);

    for (const auto &item : roadLaneSection->GetLanes())
    {
      RoadLaneInterface *roadLane = item.second.get();
      roadLane->SetInDirection(inDirection);
    }
  }
}

bool SceneryConverter::MarkDirections()
{
  std::list<RoadInterface *> pendingRoads;
  const auto &roads = scenery->GetRoads();
  std::transform(std::begin(roads),
                 std::end(roads),
                 std::back_inserter(pendingRoads),
                 [](const auto &item) { return item.second; });

  while (!pendingRoads.empty())  // proceed until all roads have been marked
  {
    RoadInterface *road = pendingRoads.front();

    MarkDirectionRoad(road, true);  // first road in cluster defines direction within cluster
    if (road->GetJunctionId() != "-1")
    {
      pendingRoads.pop_front();
      continue;
    }

    std::list<RoadInterface *> tmpCluster;  // contains currently processed cluster
    tmpCluster.splice(tmpCluster.end(), pendingRoads, pendingRoads.begin());

    LOG(CbkLogLevel::Debug, "process road cluster");

    while (!tmpCluster.empty())  // process current cluster
    {
      road = tmpCluster.front();
      tmpCluster.pop_front();

      if (road->GetLaneSections().empty())
      {
        LOG(CbkLogLevel::Error, "no sections given for road " + road->GetId());
        return false;
      }

      // collect all road links for this road and save them in this mapping
      std::map<std::tuple<RoadInterface *, RoadInterface *>, std::tuple<RoadLinkType, ContactPointType>>
          collectedRoadLinks;
      if (!CollectRoadLinks(road, collectedRoadLinks))
      {
        return false;
      }

      // process collected road links
      if (!ProcessCollectedRoadLinks(road, pendingRoads, collectedRoadLinks, tmpCluster))
      {
        return false;
      }

    }  // process next road within current cluster
  }    // for each cluster

  return true;
}

bool SceneryConverter::CollectRoadLinks(RoadInterface *road,
                                        std::map<std::tuple<RoadInterface *, RoadInterface *>,
                                                 std::tuple<RoadLinkType, ContactPointType>> &collectedRoadLinks)
{
  for (const auto &roadLink : road->GetRoadLinks())
  {
    if (RoadLinkElementType::Road == roadLink->GetElementType())
    {
      RoadInterface *connectedRoad = GetConnectedRoad(roadLink.get());
      if (!connectedRoad)
      {
        return false;
      }

      if (road == connectedRoad)
      {
        LOG(CbkLogLevel::Error, "self references are not supported, road " + road->GetId());
        return false;
      }

      // if the connection between these two roads is found for the first time,
      // add it to the mapping
      if (0 == collectedRoadLinks.count(std::make_tuple(road, connectedRoad))
          && 0 == collectedRoadLinks.count(std::make_tuple(connectedRoad, road)))
      {
        collectedRoadLinks.insert(
            {std::make_tuple(road, connectedRoad),
             std::make_tuple(roadLink->GetType(),
                             roadLink->GetContactPoint())});  // connectedRoad is not part of a junction
      }
    }
    else if (RoadLinkElementType::Junction == roadLink->GetElementType())
    {
      //handle junctions
    }
    else
    {
      assert(0);  // catched by parser
      return false;
    }
  }
  return true;
}

bool SceneryConverter::ProcessCollectedRoadLinks(
    RoadInterface *road,
    std::list<RoadInterface *> pendingRoads,
    std::map<std::tuple<RoadInterface *, RoadInterface *>, std::tuple<RoadLinkType, ContactPointType>>
        collectedRoadLinks,
    std::list<RoadInterface *> tmpCluster)
{
  for (auto &item : collectedRoadLinks)
  {
    RoadInterface *connectedRoad = std::get<1>(item.first);
    RoadLinkType connectedRoadLinkType = std::get<0>(item.second);
    ContactPointType connectedContactPointType = std::get<1>(item.second);

    auto findIt = std::find_if(pendingRoads.begin(),
                               pendingRoads.end(),
                               [&connectedRoad](RoadInterface *road) { return road == connectedRoad; });
    if (pendingRoads.end() == findIt)
    {
      continue;  // road already processed (no not overwrite content)
    }

    bool connectedRoadInDirection = false;

    if (RoadLinkType::Predecessor != connectedRoadLinkType
        && RoadLinkType::Successor != connectedRoadLinkType)  // catch neighbor road link type
    {
      LOG(CbkLogLevel::Error, "only predecessor and successor road links are supported");
      return false;
    }

    assert(ContactPointType::Start == connectedContactPointType
           || ContactPointType::End == connectedContactPointType);  // catched by importer

    // if the connected road is in direction, depends whether the
    // current road is in direction and how they are connected
    bool roadInDirection = road->GetInDirection();
    if (RoadLinkType::Predecessor == connectedRoadLinkType)
    {
      if (ContactPointType::Start == connectedContactPointType)
      {
        connectedRoadInDirection = !roadInDirection;
      }
      else if (ContactPointType::End == connectedContactPointType)
      {
        connectedRoadInDirection = roadInDirection;
      }
    }
    else if (RoadLinkType::Successor == connectedRoadLinkType)
    {
      if (ContactPointType::Start == connectedContactPointType)
      {
        connectedRoadInDirection = roadInDirection;
      }
      else if (ContactPointType::End == connectedContactPointType)
      {
        connectedRoadInDirection = !roadInDirection;
      }
    }

    MarkDirectionRoad(connectedRoad, connectedRoadInDirection);

    // update cluster
    auto findIter = std::find(pendingRoads.begin(), pendingRoads.end(), connectedRoad);
    if (pendingRoads.end() != findIter)
    {
      pendingRoads.remove(connectedRoad);
      tmpCluster.push_back(connectedRoad);
    }
  }  // for each road link
  return true;
}

bool SceneryConverter::IndexElements()
{
  int linearSectionId = 0;
  for (const auto &[_, road] : scenery->GetRoads())
  {
    for (const auto &roadLaneSection : road->GetLaneSections())
    {
      roadLaneSection->SetId(linearSectionId);
      ++linearSectionId;
    }
  }

  return true;
}

bool SceneryConverter::ConnectLaneToLane(RoadLaneInterface *currentLane,
                                         ContactPointType currentContactPoint,
                                         RoadLaneInterface *otherLane,
                                         ContactPointType otherContactPoint)
{
  // calculate direction parameters
  bool currentDestBegin = ContactPointType::Start == currentContactPoint;

  if (!currentDestBegin)
  {
    worldData.AddLaneSuccessor(*currentLane, *otherLane, otherContactPoint == ContactPointType::Start);
  }
  else
  {
    worldData.AddLanePredecessor(*currentLane, *otherLane, otherContactPoint == ContactPointType::Start);
  }

  return true;
}

bool SceneryConverter::ConnectLaneToSection(RoadLaneInterface *currentLane,
                                            ContactPointType currentContactPoint,
                                            RoadLaneSectionInterface *otherLaneSection,
                                            ContactPointType otherContactPoint)
{
  if (currentLane->GetId() == 0)
  {
    return true;
  }  // no center lanes

  if (currentContactPoint == ContactPointType::Start)
  {
    // process predecessor
    if (currentLane->GetPredecessor().empty())
    {
      return true;
    }
    RoadLaneInterface *otherLane = GetOtherLane(otherLaneSection, currentLane->GetPredecessor().front());
    if (otherLane && !ConnectLaneToLane(currentLane, currentContactPoint, otherLane, otherContactPoint))
    {
      LOG(CbkLogLevel::Error, "could not connect lanes");
      return false;
    }
  }
  else
  {
    // process successor
    if (currentLane->GetSuccessor().empty())
    {
      return true;
    }
    RoadLaneInterface *otherLane = GetOtherLane(otherLaneSection, currentLane->GetSuccessor().front());
    if (otherLane && !ConnectLaneToLane(currentLane, currentContactPoint, otherLane, otherContactPoint))
    {
      LOG(CbkLogLevel::Error, "could not connect lanes");
      return false;
    }
  }

  return true;
}

bool SceneryConverter::ConnectLanes(RoadLaneSectionInterface *firstLaneSection,
                                    ContactPointType firstContactPoint,
                                    RoadLaneSectionInterface *secondLaneSection,
                                    ContactPointType secondContactPoint)
{
  if (!std::all_of(firstLaneSection->GetLanes().begin(),
                   firstLaneSection->GetLanes().end(),
                   [&](const auto &item) {
                     return ConnectLaneToSection(
                         item.second.get(), firstContactPoint, secondLaneSection, secondContactPoint);
                   }))
  {
    return false;
  }

  if (!std::all_of(
          secondLaneSection->GetLanes().begin(),
          secondLaneSection->GetLanes().end(),
          [&](const auto &item)
          { return ConnectLaneToSection(item.second.get(), secondContactPoint, firstLaneSection, firstContactPoint); }))
  {
    return false;
  }

  return true;
}

bool SceneryConverter::ConnectRoadExternalWithElementTypeRoad(RoadInterface *road)
{
  for (const auto &roadLink : road->GetRoadLinks())
  {
    if (roadLink->GetElementType() != RoadLinkElementType::Road)
    {
      continue;
    }

    const auto *otherRoad = scenery->GetRoad(roadLink->GetElementId());
    RoadLaneSectionInterface *otherSection{nullptr};

    if (roadLink->GetContactPoint() == ContactPointType::Start)
    {
      otherSection = otherRoad->GetLaneSections().front().get();
    }
    else
    {
      otherSection = otherRoad->GetLaneSections().back().get();
    }

    if (roadLink->GetType() == RoadLinkType::Successor)
    {
      if (!ConnectExternalRoadSuccessor(road, otherRoad, otherSection, roadLink->GetContactPoint()))
      {
        return false;
      }
    }
    else
    {
      if (!ConnectExternalRoadPredecessor(road, otherRoad, otherSection, roadLink->GetContactPoint()))
      {
        return false;
      }
    }
  }

  return true;
}

bool SceneryConverter::ConnectExternalRoadSuccessor(const RoadInterface *currentRoad,
                                                    const RoadInterface *otherRoad,
                                                    RoadLaneSectionInterface *otherSection,
                                                    ContactPointType otherContactPoint)
{
  worldData.SetRoadSuccessor(*currentRoad, *otherRoad);
  RoadLaneSectionInterface *currentSection = currentRoad->GetLaneSections().back().get();
  worldData.SetSectionSuccessor(*currentSection, *otherSection);
  for (const auto &laneEntry : currentSection->GetLanes())
  {
    RoadLaneInterface *lane = laneEntry.second.get();
    auto successorLaneId = lane->GetSuccessor();
    if (successorLaneId.size() == 1)
    {
      try
      {
        RoadLaneInterface *successorLane = otherSection->GetLanes().at(successorLaneId.front()).get();
        worldData.AddLaneSuccessor(*lane, *successorLane, otherContactPoint == ContactPointType::Start);
      }
      catch (const std::out_of_range &)
      {
        LOG(CbkLogLevel::Error, "LaneSuccessorId not found");
        return false;
      }
    }
  }
  return true;
}

bool SceneryConverter::ConnectExternalRoadPredecessor(const RoadInterface *currentRoad,
                                                      const RoadInterface *otherRoad,
                                                      RoadLaneSectionInterface *otherSection,
                                                      ContactPointType otherContactPoint)
{
  worldData.SetRoadPredecessor(*currentRoad, *otherRoad);
  RoadLaneSectionInterface *currentSection = currentRoad->GetLaneSections().front().get();
  worldData.SetSectionPredecessor(*currentSection, *otherSection);
  for (const auto &laneEntry : currentSection->GetLanes())
  {
    RoadLaneInterface *lane = laneEntry.second.get();
    auto predecessorLaneId = lane->GetPredecessor();
    if (predecessorLaneId.size() == 1)
    {
      try
      {
        RoadLaneInterface *predecessorLane = otherSection->GetLanes().at(predecessorLaneId.front()).get();
        worldData.AddLanePredecessor(*lane, *predecessorLane, otherContactPoint == ContactPointType::Start);
      }
      catch (const std::out_of_range &)
      {
        LOG(CbkLogLevel::Error, "LanePredecessorId not found");
        return false;
      }
    }
  }
  return true;
}

bool SceneryConverter::ConnectRoadInternal(RoadInterface *road)
{
  // connect sections within a road
  auto iter = road->GetLaneSections().begin();
  RoadLaneSectionInterface *previousLaneSection = (*iter).get();
  ++iter;
  while (iter
         != road->GetLaneSections()
                .end())  // skipped for junctions since OpenDRIVE connecting roads contain only one lane section
  {
    RoadLaneSectionInterface *laneSection = (*iter).get();

    worldData.SetSectionSuccessor(*previousLaneSection, *laneSection);
    worldData.SetSectionPredecessor(*laneSection, *previousLaneSection);

    if (!ConnectLanes(previousLaneSection, ContactPointType::End, laneSection, ContactPointType::Start))
    {
      LOG(CbkLogLevel::Error, "could not connect sections");
      return false;
    }

    previousLaneSection = laneSection;
    ++iter;
  }

  return true;
}

bool SceneryConverter::ConnectJunction(const JunctionInterface *junction)
{
  worldData.AddJunction(junction);
  // this indirection to an internal function is a first step towards better testability. please do not remove.
  auto [status, error_message] = Internal::ConnectJunction(scenery,
                                                           junction,
                                                           [&](const JunctionInterface *junction,
                                                               const RoadInterface *incomingRoad,
                                                               const RoadInterface *connectingRoad,
                                                               const RoadInterface *outgoingRoad,
                                                               ContactPointType incomingContactPoint,
                                                               ContactPointType connectingContactPoint,
                                                               ContactPointType outgoingContactPoint,
                                                               const std::map<int, int> &laneIdMapping)
                                                           {
                                                             ConnectPathInJunction(junction,
                                                                                   incomingRoad,
                                                                                   connectingRoad,
                                                                                   outgoingRoad,
                                                                                   incomingContactPoint,
                                                                                   connectingContactPoint,
                                                                                   outgoingContactPoint,
                                                                                   laneIdMapping);
                                                           });
  if (status)
  {
    for (const auto &priority : junction->GetPriorities())
    {
      worldData.AddJunctionPriority(junction, priority.high, priority.low);
    }
    return true;
  }
  LOG(CbkLogLevel::Error, error_message);
  return false;
}

std::optional<int> GetOutgoingLaneId(const RoadLaneInterface &connectingLane, bool connectAtStart)
{
  if (connectAtStart ? connectingLane.GetSuccessor().empty() : connectingLane.GetPredecessor().empty())
  {
    return std::nullopt;
  }
  return connectAtStart ? connectingLane.GetSuccessor().front() : connectingLane.GetPredecessor().front();
}

void SceneryConverter::ConnectPathInJunction(const JunctionInterface *junction,
                                             const RoadInterface *incomingRoad,
                                             const RoadInterface *connectingRoad,
                                             const RoadInterface *outgoingRoad,
                                             ContactPointType incomingContactPoint,
                                             ContactPointType connectingContactPoint,
                                             ContactPointType outgoingContactPoint,
                                             const std::map<int, int> &laneIdMapping)
{
  bool connectAtStart = (connectingContactPoint == ContactPointType::Start);
  if (incomingContactPoint == ContactPointType::Start)
  {
    worldData.SetRoadPredecessorJunction(*incomingRoad, junction);
  }
  else
  {
    worldData.SetRoadSuccessorJunction(*incomingRoad, junction);
  }

  if (outgoingContactPoint == ContactPointType::Start)
  {
    worldData.SetRoadPredecessorJunction(*outgoingRoad, junction);
  }
  else
  {
    worldData.SetRoadSuccessorJunction(*outgoingRoad, junction);
  }

  RoadLaneSectionInterface *incomingRoadSection{nullptr};
  if (incomingContactPoint == ContactPointType::Start)
  {
    incomingRoadSection = incomingRoad->GetLaneSections().front().get();
  }
  else
  {
    incomingRoadSection = incomingRoad->GetLaneSections().back().get();
  }

  RoadLaneSectionInterface *connectingRoadFirstSection = connectAtStart
                                                           ? connectingRoad->GetLaneSections().front().get()
                                                           : connectingRoad->GetLaneSections().back().get();
  RoadLaneSectionInterface *connectingRoadLastSection = connectAtStart
                                                          ? connectingRoad->GetLaneSections().back().get()
                                                          : connectingRoad->GetLaneSections().front().get();
  RoadLaneSectionInterface *outgoingRoadSection{nullptr};
  if (outgoingContactPoint == ContactPointType::Start)
  {
    outgoingRoadSection = outgoingRoad->GetLaneSections().front().get();
  }
  else
  {
    outgoingRoadSection = outgoingRoad->GetLaneSections().back().get();
  }

  for (auto lanePair : laneIdMapping)
  {
    RoadLaneInterface *incomingLane = incomingRoadSection->GetLanes().at(lanePair.first).get();
    RoadLaneInterface *connectingLane = connectingRoadFirstSection->GetLanes().at(lanePair.second).get();
    ConnectLaneToLane(incomingLane, incomingContactPoint, connectingLane, connectingContactPoint);
  }

  for (const auto &[connectingLaneId, connectingLane] : connectingRoadLastSection->GetLanes())
  {
    auto outgoingLaneId = GetOutgoingLaneId(*connectingLane, connectAtStart);
    if (!outgoingLaneId.has_value() || connectingLaneId == 0)
    {
      continue;
    }
    RoadLaneInterface *outgoingLane = outgoingRoadSection->GetLanes().at(outgoingLaneId.value()).get();
    //The contact point of the connecting road to the outgoing road is the opposite of the contact point of the
    //connecting road to the incoming road
    ConnectLaneToLane(outgoingLane,
                      outgoingContactPoint,
                      connectingLane.get(),
                      connectAtStart ? ContactPointType::End : ContactPointType::Start);
  }
}

bool SceneryConverter::ConnectRoads()
{
  for (const auto &[_, road] : scenery->GetRoads())
  {
    if (!ConnectRoadExternalWithElementTypeRoad(road))
    {
      LOG(CbkLogLevel::Error, "could not connect external road " + road->GetId());
      return false;
    }

    if (!ConnectRoadInternal(road))
    {
      LOG(CbkLogLevel::Error, "could not connect internal road " + road->GetId());
      return false;
    }
  }

  for (const auto &item : scenery->GetJunctions())
  {
    if (!ConnectJunction(item.second))
    {
      return false;
    }
  }
  for (const auto &[_, lane] : worldData.GetLanes())
  {
    lane->SetLanePairings();
  }
  return true;
}

bool SceneryConverter::ConvertRoads()
{
  // define a unique directions of roads/lanes within each road cluster
  if (!MarkDirections())
  {
    return false;
  }

  CreateRoads();

  // connect roads in data layer
  if (!ConnectRoads())
  {
    return false;
  }

  try
  {
    GeometryConverter::Convert(*scenery, worldData);
  }
  catch (const std::runtime_error &e)
  {
    LOGERROR(e.what());
    return false;
  }
  return true;
}

void SceneryConverter::ConvertObjects()
{
  try
  {
    CreateObjects();
  }
  catch (const std::runtime_error &e)
  {
    throw std::runtime_error("SceneryConverter: Creation of objects failed\n" + std::string(e.what()));
  }

  try
  {
    CreateRoadSignals();
  }
  catch (const std::exception &e)
  {
    throw std::runtime_error("SceneryConverter: Creation of road signals failed\n" + std::string(e.what()));
  }
}

void SceneryConverter::CreateObjects()
{
  for (const auto &[_, road] : scenery->GetRoads())
  {
    for (const auto &object : road->GetRoadObjects())
    {
      const auto *section = worldDataQuery.GetSectionByDistance(road->GetId(), object->GetS());

      if (section == nullptr)  // potential error in OpenDRIVE file
      {
        LOG(CbkLogLevel::Info, "Object ignored: s-coordinate not within road");
        continue;
      }

      const auto position = RoadCoord2WorldCoord(road, object->GetS(), object->GetT(), object->GetHdg());

      if (object->GetType() == RoadObjectType::crosswalk)
      {
        CreateRoadMarking(object.get(), position, section->GetLanes());
      }
      else if (object->IsContinuous())
      {
        CreateContinuousObject(object.get(), road);
      }
      else
      {
        CreateObject(object.get(), position);
      }
    }
  }
}

void SceneryConverter::CreateObject(const RoadObjectInterface *object, const Position &position)
{
  mantle_api::Vec3<units::length::meter_t> absPos{
      position.xPos, position.yPos, object->GetZOffset() + 0.5 * object->GetHeight()};
  mantle_api::Dimension3 dim{object->GetLength(), object->GetWidth(), object->GetHeight()};
  mantle_api::Orientation3<units::angle::radian_t> orientation{
      position.yawAngle, object->GetPitch(), object->GetRoll()};
  const auto id = GenerateIdAndPublishEntity(openpass::utils::GetEntityInfo(*object), EntityType::kObject);

  trafficObjects.emplace_back(
      std::make_unique<TrafficObjectAdapter>(id, worldData, localizer, absPos, dim, orientation, object->GetId()));
}

void SceneryConverter::CreateContinuousObject(const RoadObjectInterface *object, const RoadInterface *road)
{
  const auto *section = worldDataQuery.GetSectionByDistance(road->GetId(), object->GetS());
  const auto sStart = object->GetS();
  const auto sEnd = sStart + object->GetLength();
  const auto laneElements = section->GetLanes().front()->GetLaneGeometryElements();
  for (const auto &laneElement : laneElements)
  {
    const auto elementStart = laneElement->joints.current.sOffset;
    const auto elementEnd = laneElement->joints.next.sOffset;
    const auto objectStart = units::math::max(sStart, elementStart);
    const auto objectEnd = units::math::min(sEnd, elementEnd);
    if (objectStart < objectEnd)
    {
      const auto startPosition = RoadCoord2WorldCoord(road, objectStart, object->GetT(), 0_rad);
      const auto endPosition = RoadCoord2WorldCoord(road, objectEnd, object->GetT(), 0_rad);
      const auto deltaX = endPosition.xPos - startPosition.xPos;
      const auto deltaY = endPosition.yPos - startPosition.yPos;
      const auto length = openpass::hypot(deltaX, deltaY);
      const auto yaw = units::math::atan2(deltaY, deltaX);

      mantle_api::Vec3<units::length::meter_t> pos{(startPosition.xPos + endPosition.xPos) / 2.0,
                                                   (startPosition.yPos + endPosition.yPos) / 2.0,
                                                   0.5 * object->GetHeight() + object->GetZOffset()};
      mantle_api::Dimension3 dim{length, object->GetWidth(), object->GetHeight()};
      mantle_api::Orientation3<units::angle::radian_t> orientation{yaw, object->GetPitch(), object->GetRoll()};
      const auto id = GenerateIdAndPublishEntity(openpass::utils::GetEntityInfo(*object), EntityType::kObject);

      trafficObjects.emplace_back(
          std::make_unique<TrafficObjectAdapter>(id, worldData, localizer, pos, dim, orientation, object->GetId()));
    }
  }
}

Internal::LaneBoundaries SceneryConverter::CreateLaneBoundaries(RoadLaneInterface &odLane,
                                                                RoadLaneSectionInterface &odSection)
{
  Internal::LaneBoundaries laneBoundaries;

  if (odLane.GetRoadMarks().empty() || odLane.GetRoadMarks().front()->GetSOffset().value() > CommonHelper::EPSILON)
  {
    //If there are no RoadMarks defined in OpenDRIVE or the first RoadMark does not start at s=0,
    //there should still be a LaneBoundary, so we treat it as if there was a RoadMark of type none
    RoadLaneRoadMark defaultRoadMark(0_m,
                                     RoadLaneRoadDescriptionType::Center,
                                     RoadLaneRoadMarkType::None,
                                     RoadLaneRoadMarkColor::Undefined,
                                     RoadLaneRoadMarkLaneChange::Both,
                                     RoadLaneRoadMarkWeight::Undefined);
    if (!odLane.GetRoadMarks().empty())
    {
      defaultRoadMark.LimitSEnd(odLane.GetRoadMarks().front()->GetSOffset());
    }
    const auto id = GenerateIdAndPublishEntity(openpass::utils::GetEntityInfo(defaultRoadMark));
    worldData.AddLaneBoundary(id, defaultRoadMark, odSection.GetStart(), OWL::LaneMarkingSide::Single);
    laneBoundaries.laneBoundaries.push_back(id);

    const auto logicalLaneBoundaryId = GenerateIdAndPublishEntity(openpass::utils::GetEntityInfo(odLane));
    worldData.AddLogicalLaneBoundary(logicalLaneBoundaryId, defaultRoadMark, odSection.GetStart());
    laneBoundaries.logicalLaneBoundaries.push_back(logicalLaneBoundaryId);
    worldData.GetLogicalLaneBoundary(logicalLaneBoundaryId).SetPhysicalBoundaryIds({static_cast<size_t>(id)});
  }

  for (const auto &roadMarkEntry : odLane.GetRoadMarks())
  {
    const auto logicalLaneBoundaryId = GenerateIdAndPublishEntity(openpass::utils::GetEntityInfo(odLane));
    worldData.AddLogicalLaneBoundary(logicalLaneBoundaryId, *roadMarkEntry, odSection.GetStart());
    laneBoundaries.logicalLaneBoundaries.push_back(logicalLaneBoundaryId);

    const auto roadMarkType = roadMarkEntry->GetType();

    if (roadMarkType == RoadLaneRoadMarkType::Solid_Solid || roadMarkType == RoadLaneRoadMarkType::Solid_Broken
        || roadMarkType == RoadLaneRoadMarkType::Broken_Solid || roadMarkType == RoadLaneRoadMarkType::Broken_Broken)
    {
      const auto idLeft = GenerateIdAndPublishEntity(openpass::utils::GetEntityInfo(*roadMarkEntry));
      const auto idRight = GenerateIdAndPublishEntity(openpass::utils::GetEntityInfo(*roadMarkEntry));

      worldData.AddLaneBoundary(idLeft, *roadMarkEntry, odSection.GetStart(), OWL::LaneMarkingSide::Left);
      worldData.AddLaneBoundary(idRight, *roadMarkEntry, odSection.GetStart(), OWL::LaneMarkingSide::Right);

      laneBoundaries.laneBoundaries.push_back(idLeft);
      laneBoundaries.laneBoundaries.push_back(idRight);

      worldData.GetLogicalLaneBoundary(logicalLaneBoundaryId)
          .SetPhysicalBoundaryIds({static_cast<size_t>(idLeft), static_cast<size_t>(idRight)});
    }
    else
    {
      const auto idSingle = GenerateIdAndPublishEntity(openpass::utils::GetEntityInfo(*roadMarkEntry));
      worldData.AddLaneBoundary(idSingle, *roadMarkEntry, odSection.GetStart(), OWL::LaneMarkingSide::Single);
      laneBoundaries.laneBoundaries.push_back(idSingle);

      worldData.GetLogicalLaneBoundary(logicalLaneBoundaryId).SetPhysicalBoundaryIds({static_cast<size_t>(idSingle)});
    }
  }

  return laneBoundaries;
}

void SceneryConverter::CreateRoadSignals()
{
  for (const auto &[_, road] : scenery->GetRoads())
  {
    std::vector<std::pair<RoadSignalInterface *, std::vector<std::string>>> dependentSignals;

    for (const auto &signal : road->GetRoadSignals())
    {
      // Gather all dependent signals
      if (!signal->GetDependencies().empty())
      {
        dependentSignals.emplace_back(signal.get(), signal->GetDependencies());
        continue;
      }

      const auto *section = worldDataQuery.GetSectionByDistance(road->GetId(), signal->GetS());

      if (section == nullptr)  // potential error in OpenDRIVE file
      {
        LOG(CbkLogLevel::Warning, "Object ignored: s-coordinate not within road");
        continue;
      }

      auto yaw = signal->GetHOffset() + (signal->GetOrientation() ? 0_rad : 1_rad * M_PI);
      auto position = RoadCoord2WorldCoord(road, signal->GetS(), signal->GetT(), yaw);
      const auto *signalsMapping = OpenDriveTypeMapper::GetSignalsMapping(signal->GetCountry());
      if (signalsMapping->roadMarkings.find(signal->GetType()) != signalsMapping->roadMarkings.end())
      {
        CreateRoadMarking(signal.get(), position, section->GetLanes());
      }
      else if (OpenDriveTypeMapper::ThreeSignalsTrafficLightsIcons.find(signal->GetType())
                   != OpenDriveTypeMapper::ThreeSignalsTrafficLightsIcons.end()
               || OpenDriveTypeMapper::TwoSignalsTrafficLightsIcons.find(signal->GetType())
                      != OpenDriveTypeMapper::TwoSignalsTrafficLightsIcons.end())
      {
        CreateTrafficLight(signal.get(), position, section->GetLanes());
      }
      else
      {
        CreateTrafficSign(signal.get(), position, section->GetLanes());
      }
    }

    // First instantiate all signals and then add dependencies accordingly afterwards
    for (const auto &[supplementarySign, parentIds] : dependentSignals)
    {
      for (const auto &parentId : parentIds)
      {
        const auto owlId = helper::map::query(worldData.GetTrafficSignIdMapping(), parentId);
        if (!owlId.has_value())
        {
          LOGWARN("Parent id '" + parentId
                  + "' not found in world data's traffic sign id mapping for supplementary sign's id '"
                  + supplementarySign->GetId() + "'");
          continue;
        }
        if (worldData.GetSignalType(owlId.value()) != OWL::SignalType::TrafficSign)
        {
          throw std::runtime_error("Signal with id \"" + parentId + "\" is not a traffic sign");
        }
        auto &parentSign = worldData.GetTrafficSign(owlId.value());

        auto yaw = supplementarySign->GetHOffset() + (supplementarySign->GetOrientation() ? 0_rad : 1_rad * M_PI);
        auto position = RoadCoord2WorldCoord(road, supplementarySign->GetS(), supplementarySign->GetT(), yaw);
        if (!parentSign.AddSupplementarySign(supplementarySign, position))
        {
          LOGWARN("Unsupported supplementary sign type " + supplementarySign->GetType()
                  + " (id: " + supplementarySign->GetId() + ")");
        }
      }
    }
  }
}

void SceneryConverter::CreateTrafficSign(RoadSignalInterface *signal,
                                         Position position,
                                         const OWL::Interfaces::Lanes &lanes)
{
  const auto id = GenerateIdAndPublishEntity(openpass::utils::GetEntityInfo(*signal));
  OWL::Interfaces::TrafficSign &trafficSign = worldData.AddTrafficSign(id, signal->GetId());

  trafficSign.SetS(signal->GetS());

  try
  {
    if (!trafficSign.SetSpecification(signal, position))
    {
      const std::string message = "Unsupported traffic sign type: " + signal->GetType() + "-" + signal->GetSubType()
                                + " (id: " + signal->GetId() + ")";
      LOG(CbkLogLevel::Warning, message);
    }
  }
  catch (const std::invalid_argument &e)
  {
    LOG(CbkLogLevel::Warning, e.what());
  }

  for (const auto *lane : lanes)
  {
    OWL::OdId odId = lane->GetOdId();
    if (signal->IsValidForLane(odId))
    {
      worldData.AssignTrafficSignToLane(lane->GetId(), trafficSign, *signal);
    }
  }
}

void SceneryConverter::CreateRoadMarking(RoadSignalInterface *signal,
                                         Position position,
                                         const OWL::Interfaces::Lanes &lanes)
{
  const auto id = GenerateIdAndPublishEntity(openpass::utils::GetEntityInfo(*signal));
  OWL::Interfaces::RoadMarking &roadMarking = worldData.AddRoadMarking(id);

  roadMarking.SetS(signal->GetS());

  if (!roadMarking.SetSpecification(signal, position))
  {
    const std::string message
        = "Unsupported traffic sign type: " + signal->GetType() + (" (id: " + signal->GetId() + ")");
    LOG(CbkLogLevel::Warning, message);
    return;
  }

  for (const auto *lane : lanes)
  {
    OWL::OdId odId = lane->GetOdId();
    if (signal->IsValidForLane(odId))
    {
      worldData.AssignRoadMarkingToLane(lane->GetId(), roadMarking, *signal);
    }
  }
}

void SceneryConverter::CreateRoadMarking(RoadObjectInterface *object,
                                         Position position,
                                         const OWL::Interfaces::Lanes &lanes)
{
  const auto id = GenerateIdAndPublishEntity(openpass::utils::GetEntityInfo(*object));
  OWL::Interfaces::RoadMarking &roadMarking = worldData.AddRoadMarking(id);

  roadMarking.SetS(object->GetS());

  if (!roadMarking.SetSpecification(object, position))
  {
    const std::string message = "Unsupported traffic sign type: (id: " + object->GetId() + ")";
    LOG(CbkLogLevel::Warning, message);
    return;
  }

  for (const auto *lane : lanes)
  {
    OWL::OdId odId = lane->GetOdId();
    if (object->IsValidForLane(odId))
    {
      worldData.AssignRoadMarkingToLane(lane->GetId(), roadMarking, *object);
    }
  }
}

void SceneryConverter::CreateTrafficLight(RoadSignalInterface *signal,
                                          Position position,
                                          const OWL::Interfaces::Lanes &lanes)
{
  std::vector<OWL::Id> trafficOsiIds{};
  trafficOsiIds.emplace_back(GenerateIdAndPublishEntity(openpass::utils::GetEntityInfo(*signal)));
  trafficOsiIds.emplace_back(GenerateIdAndPublishEntity(openpass::utils::GetEntityInfo(*signal)));
  trafficOsiIds.emplace_back(GenerateIdAndPublishEntity(openpass::utils::GetEntityInfo(*signal)));

  OWL::Interfaces::TrafficLight &trafficLight
      = worldData.AddTrafficLight(trafficOsiIds, signal->GetId(), signal->GetType());

  trafficLight.SetS(signal->GetS());

  if (!trafficLight.SetSpecification(signal, position))
  {
    const std::string message
        = "Unsupported traffic sign type: " + signal->GetType() + (" (id: " + signal->GetId() + ")");
    LOG(CbkLogLevel::Warning, message);
    return;
  }

  trafficLight.SetState(CommonTrafficLight::State::Red);

  for (const auto *lane : lanes)
  {
    OWL::OdId odId = lane->GetOdId();
    if (signal->IsValidForLane(odId))
    {
      worldData.AssignTrafficLightToLane(lane->GetId(), trafficLight, *signal);
    }
  }
}

void SceneryConverter::CreateRoads()
{
  for (const auto &[_, odRoad] : scenery->GetRoads())
  {
    worldData.AddRoad(*odRoad);

    for (const auto &section : odRoad->GetLaneSections())
    {
      RoadLaneSectionInterface &odSection = *section;

      worldData.AddSection(*odRoad, odSection);

      for (const auto &laneEntry : odSection.GetLanes())
      {
        RoadLaneInterface &odLane = *(laneEntry.second);
        if (odLane.GetId() == 0)
        {
          auto laneBoundaries = CreateLaneBoundaries(odLane, odSection);
          const auto referenceLineId = GenerateIdAndPublishEntity(openpass::utils::GetEntityInfo(odLane));
          worldData.AddReferenceLine(referenceLineId);
          worldData.SetCenterLaneBoundary(
              odSection, laneBoundaries.laneBoundaries, laneBoundaries.logicalLaneBoundaries, referenceLineId);
        }
      }
      for (const auto &laneEntry : odSection.GetLanes())
      {
        RoadLaneInterface &odLane = *(laneEntry.second);
        if (odLane.GetId() != 0)
        {
          auto laneBoundaries = CreateLaneBoundaries(odLane, odSection);
          const auto laneId = GenerateIdAndPublishEntity(openpass::utils::GetEntityInfo(odLane));
          const auto logicalLaneId = GenerateIdAndPublishEntity(openpass::utils::GetEntityInfo(odLane));
          worldData.AddLane(laneId,
                            logicalLaneId,
                            odSection,
                            odLane,
                            laneBoundaries.laneBoundaries,
                            laneBoundaries.logicalLaneBoundaries);
        }
      }
    }
  }
}

void SceneryConverter::SetProjectionString()
{
  worldData.SetProjectionString(scenery->GetGeoReference());
}

std::pair<RoadGraph, RoadGraphVertexMapping> RoadNetworkBuilder::Build()
{
  RoadGraph roadGraph;
  RoadGraphVertexMapping vertices;

  for (const auto &[roadId, road] : scenery.GetRoads())
  {
    bool hasLeftLanes = road->GetLaneSections().front()->GetLanes().crbegin()->first > 0;
    bool hasRightLanes = road->GetLaneSections().front()->GetLanes().cbegin()->first < 0;
    if (hasLeftLanes)
    {
      RouteElement routeElement{roadId, false};
      vertices[routeElement] = add_vertex(routeElement, roadGraph);
    }
    if (hasRightLanes)
    {
      RouteElement routeElement{roadId, true};
      vertices[routeElement] = add_vertex(routeElement, roadGraph);
    }
  }

  for (const auto &[roadId, road] : scenery.GetRoads())
  {
    bool hasLeftLanes = road->GetLaneSections().front()->GetLanes().crbegin()->first > 0;
    bool hasRightLanes = road->GetLaneSections().front()->GetLanes().cbegin()->first < 0;
    for (const auto &link : road->GetRoadLinks())
    {
      if (link->GetElementType() == RoadLinkElementType::Road)
      {
        if (link->GetType() == RoadLinkType::Successor)
        {
          const auto &successorId = link->GetElementId();
          if (link->GetContactPoint() == ContactPointType::Start)
          {
            if (hasRightLanes)
            {
              add_edge(vertices[{roadId, true}], vertices[{successorId, true}], roadGraph);
            }
            if (hasLeftLanes)
            {
              add_edge(vertices[{successorId, false}], vertices[{roadId, false}], roadGraph);
            }
          }
          else
          {
            if (hasRightLanes)
            {
              add_edge(vertices[{roadId, true}], vertices[{successorId, false}], roadGraph);
            }
            if (hasLeftLanes)
            {
              add_edge(vertices[{successorId, true}], vertices[{roadId, false}], roadGraph);
            }
          }
        }
        if (link->GetType() == RoadLinkType::Predecessor)
        {
          const auto &predecessorId = link->GetElementId();
          if (link->GetContactPoint() == ContactPointType::Start)
          {
            if (hasRightLanes)
            {
              add_edge(vertices[{predecessorId, false}], vertices[{roadId, true}], roadGraph);
            }
            if (hasLeftLanes)
            {
              add_edge(vertices[{roadId, false}], vertices[{predecessorId, true}], roadGraph);
            }
          }
          else
          {
            if (hasRightLanes)
            {
              add_edge(vertices[{predecessorId, true}], vertices[{roadId, true}], roadGraph);
            }
            if (hasLeftLanes)
            {
              add_edge(vertices[{roadId, false}], vertices[{predecessorId, false}], roadGraph);
            }
          }
        }
      }
    }
  }

  return {roadGraph, vertices};
}

mantle_api::UniqueId SceneryConverter::GenerateIdAndPublishEntity(const openpass::type::EntityInfo &entityInfo,
                                                                  EntityType entityType)
{
  const auto id = idManager->Generate(entityType);
  entityInfoPublisher.Publish(static_cast<int>(id), entityInfo, true);  // TODO: align datatypes
  return id;
}
