/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  AgentNetwork.h
//! @brief This file contains the representation of the agent network during a
//!        simulation run.
//-----------------------------------------------------------------------------

#pragma once

#include <functional>
#include <list>
#include <map>
#include <string>
#include <vector>

#include "AgentAdapter.h"
#include "Localization.h"
#include "common/openPassTypes.h"
#include "include/callbackInterface.h"

class WorldImplementation;
class AgentInterface;
class WorldInterface;
namespace OWL
{
namespace Interfaces
{
class MovingObject;
}  // namespace Interfaces
}  // namespace OWL

/// Publisher
using Publisher = std::function<void(
    openpass::type::EntityId id, openpass::type::FlatParameterKey key, openpass::type::FlatParameterValue value)>;

/*!
 * \brief network of agents
 *
 * This class stores all agents in a network. It is used to synchronize the update of all
 * values of all agents.
 */
class AgentNetwork final
{
public:
  /**
   * @brief AgentNetwork constructor
   *
   * @param[in]   world           Pointer to the world
   * @param[in]   callbacks       Pointer to the callbacks
   */
  AgentNetwork(WorldImplementation *world, const CallbackInterface *callbacks);
  ~AgentNetwork();

  /*!
   * Add an agent to the agent network
   * All added agents must have unique ids.
   * Otherwise the method throws.
   *
   * \param movingObject     moving object
   * \param world            world interface
   * \param callbacks        callback interface
   * \param localizer        world localizer
   * \return                 agent adapter
   */
  AgentAdapter &CreateAgent(OWL::Interfaces::MovingObject &movingObject,
                            WorldInterface *world,
                            const CallbackInterface *callbacks,
                            const World::Localization::Localizer &localizer);

  /*!
   * \brief Clear
   * Clear map of agent network
   */
  void Clear();

  /*!
   * \brief QueueAgentUpdate
   * This function is used to store operations on the agents in a list.
   * At the end of each time step all queued operations will be executed.
   *
   * \param[in] func      function which is to stored to be executed later
   */
  void QueueAgentUpdate(std::function<void()> func);

  /*!
   * \brief QueueAgentRemove
   *
   * This function queues agents in a list that will be removed during the next syncronization.
   * \param agent agent which shall be removed
   */
  void QueueAgentRemove(const AgentInterface *agent);

  //! Remove agents queued for removal
  void RemoveAgents();

  //!
  //! Removes an agent from the network
  //! \param  agent    agent which shall be removed
  //! \return iterator agent adapter iterator
  //!
  std::list<AgentAdapter>::iterator RemoveAgent(const std::list<AgentAdapter>::iterator agent);

  /*!
   * \brief Publishes the general observations about current agents
   *
   * \param publish call
   */
  void PublishGlobalData(const Publisher &publish);

  /*!
   * \brief SyncGlobalData
   * This function is called after each timestep and executes all update function
   * and removes all agents in the remove list.
   */
  void SyncGlobalData();

  /*!
   * \brief GetAgent
   *
   * Retrieves specific agent by id
   * \param[in]  id              Agent id
   * \return     AgentInterface  Agent reference
   */
  AgentInterface *GetAgent(int id) const;

  /*!
   * \brief GetAgents
   * Retrieves all agents that currently exist
   *
   * \return  AgentAdapter   Mapping of ids to agents
   */
  virtual std::list<AgentAdapter> &GetAgents();

  /*!
   * \brief GetRemovedAgents
   * Retrieves agents that were removed during the previous timestep and clears the list
   *
   * \return   List   List of agent references
   */
  virtual const std::vector<int> GetRemovedAgentsInPreviousTimestep();

protected:
  /*!
   * \brief Provides callback to LOG() macro
   *
   * \param[in]     logLevel    Importance of log
   * \param[in]     file        Name of file where log is called
   * \param[in]     line        Line within file where log is called
   * \param[in]     message     Message to log
   */
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message)
  {
    if (callbacks)
    {
      callbacks->Log(logLevel, file, line, message);
    }
  }

private:
  WorldImplementation *world;
  std::list<AgentAdapter> agents;
  std::map<int, AgentInterface *> agentsById;
  std::vector<std::function<void()>> updateQueue;
  std::vector<int> removeQueue;
  std::vector<int> removedAgentsPrevious;

  const CallbackInterface *callbacks;
};
