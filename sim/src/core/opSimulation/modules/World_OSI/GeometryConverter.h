/********************************************************************************
 * Copyright (c) 2016-2017 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  GeometryConverter.h
//! @brief This file contains the converter of the scenery geometry.
//-----------------------------------------------------------------------------

#pragma once

#include <functional>
#include <map>
#include <optional>
#include <string>
#include <units.h>
#include <utility>
#include <vector>

#include "JointsBuilder.h"
#include "OWL/DataTypes.h"
#include "WorldData.h"
#include "boostGeometryCommon.h"
#include "common/worldDefinitions.h"

class RoadElevation;
class RoadGeometryInterface;
class RoadInterface;
class RoadLaneInterface;
class RoadLaneOffset;
class RoadLaneRoadMark;
class RoadLaneSectionInterface;
class RoadLaneWidth;
class SceneryInterface;
namespace OWL
{
namespace Primitive
{
struct LaneGeometryElement;
}  // namespace Primitive
}  // namespace OWL

//! Builds LaneGeometryPolygon from a LaneGeometryElement for a specific roadId and laneId
struct LaneGeometryPolygon
{
  std::string roadId;  //!< Id of the road for which the LaneGeometryPolygons will be built
  OWL::Id laneId;      //!< Id of the lane for which the LaneGeometryPolygons will be built
  const OWL::Primitive::LaneGeometryElement* const laneGeometryElement;  //!< LaneGeometryElement
  polygon_t polygon;                                                     //!< Polygons
};
using RoadPolygons = std::pair<std::string, std::vector<LaneGeometryPolygon>>;  //!< The pair of the road id to the
                                                                                //!< collection of polygons for the road
using JunctionPolygons
    = std::map<std::string,
               std::vector<LaneGeometryPolygon>>;  //!< The map of road ids to LaneGeometryPolygons for the junction

//-----------------------------------------------------------------------------
//! Functions for the convertion of the road geometries in a section. First, the roads,
//! lane sections and lanes have to be converted using SceneryConverter, which then
//! provides the mappings from OpenDRIVE to OSI of those elements.
//-----------------------------------------------------------------------------
namespace GeometryConverter
{
//-----------------------------------------------------------------------------
//! \brief Converts OpenDRIVE road network to OSI objects
//!
//! This function reads from the imported OpenDRIVE scenery and adds all roads,
//! sections, lanes and junctions to the WorldData by calling the respective
//! functions of the WorldData.
//!
//! \param  scenery     Scenery with the OpenDRIVE roads
//! \param  worldData   worldData that is built by this function
//-----------------------------------------------------------------------------
void Convert(const SceneryInterface& scenery, OWL::Interfaces::WorldData& worldData);

//! Converts the Roads section in OpenDRIVE to OSI
//!
//! \param  scenery     Scenery with the OpenDRIVE roads
//! \param  worldData   worldData that is built by this function
void CalculateRoads(const SceneryInterface& scenery, OWL::Interfaces::WorldData& worldData);

//! Converts a single section of an OpenDRIVE road to OSI
//!
//! \param  worldData       worldData that is built by this function
//! \param roadSectionStart start s coordinate of the section
//! \param roadSectionEnd   end s coordinate of the section
//! \param road             road the section is part of
//! \param roadSection      section to convert
void CalculateSection(OWL::Interfaces::WorldData& worldData,
                      units::length::meter_t roadSectionStart,
                      units::length::meter_t roadSectionEnd,
                      const RoadInterface* road,
                      const RoadLaneSectionInterface* roadSection);

//! Samples the lane boundary points of the lanes of one section between two consecutive
//! changes in the RoadMarks of the lanes with a defined maximum lateral error
//!
//! \param roadSectionStart start s coordinate of the section / first RoadMark change
//! \param roadSectionEnd   end s coordinate of the section / second RoadMark change
//! \param road             road the section is part of
//! \param roadSection      section to convert
//! \return                 sampled boundary points
SampledGeometry CalculateSectionBetweenRoadMarkChanges(units::length::meter_t roadSectionStart,
                                                       units::length::meter_t roadSectionEnd,
                                                       const RoadInterface* road,
                                                       const RoadLaneSectionInterface* roadSection);

//! Samples the lane boundary points of one geometry for one section with a defined maximum lateral error
//!
//! \param roadSectionStart start s coordinate of the section / first RoadMark change
//! \param roadSectionEnd       end s coordinate of the section / second RoadMark change
//! \param road                 road the section is part of
//! \param roadSection          section to convert
//! \param roadGeometry         geometry to sample
//! \param roadGeometryLength   length of the geometry
//! \return                 sampled boundary points
SampledGeometry CalculateGeometry(units::length::meter_t roadSectionStart,
                                  units::length::meter_t roadSectionEnd,
                                  const RoadInterface* road,
                                  const RoadLaneSectionInterface* roadSection,
                                  const RoadGeometryInterface* roadGeometry,
                                  units::length::meter_t roadGeometryLength);

//! Calculates the start s offset of a geometry inside a section
//!
//! \param roadSectionStart     start s coordinate of the section
//! \param roadGeometryStart    start s coordinate of the geometry
//! \return                     start s offset of the geometry in the section
units::length::meter_t CalculateGeometryOffsetStart(units::length::meter_t roadSectionStart,
                                                    units::length::meter_t roadGeometryStart);

//! Calculates the end s offset of a geometry inside a section
//!
//! \param roadSectionEnd       end s coordinate of the section
//! \param roadGeometryStart    start s coordinate of the geometry
//! \param roadGeometryEnd      end s coordinate of the section
//! \param roadGeometryLength   length of the geometry
//! \return                     end s offset of the geometry in the section
units::length::meter_t CalculateGeometryOffsetEnd(units::length::meter_t roadSectionEnd,
                                                  units::length::meter_t roadGeometryStart,
                                                  units::length::meter_t roadGeometryEnd,
                                                  units::length::meter_t roadGeometryLength);

//! Calculates the lane boundary points with a high sampling rate
//!
//! \param geometryOffsetStart  start s offset of the geometry in the section
//! \param geometryOffsetEnd    end s offset of the geometry in the section
//! \param roadSection          section to convert
//! \param road                 road the section is part of
//! \param roadGeometry         geometry to sample
//! \param roadGeometryStart    start s coordinate of the geometry
//! \return                     sampled lane boundary points
std::vector<BorderPoints> CalculateJoints(units::length::meter_t geometryOffsetStart,
                                          units::length::meter_t geometryOffsetEnd,
                                          const RoadLaneSectionInterface* roadSection,
                                          const RoadInterface* road,
                                          const RoadGeometryInterface* roadGeometry,
                                          units::length::meter_t roadGeometryStart);

//! Calculates the lane boundary points of a single joint (i.e. at one s coordinate)
//!
//! \param roadSection          section to convert
//! \param road                 road the section is part of
//! \param roadGeometry         geometry to sample
//! \param geometryOffset       s offset on the geometry
//! \param roadOffset           s coordinate on the road
//! \return                     sampled lane boundary points
BorderPoints CalculateBorderPoints(const RoadLaneSectionInterface* roadSection,
                                   const RoadInterface* road,
                                   const RoadGeometryInterface* roadGeometry,
                                   units::length::meter_t geometryOffset,
                                   units::length::meter_t roadOffset);

//! Adds the sampled joints to the world
//!
//! \param  worldData   worldData that is built by this function
//! \param joints       sampled joints
void AddPointsToWorld(OWL::Interfaces::WorldData& worldData, const Joints& joints);

//-----------------------------------------------------------------------------
//! Calculates height coordinate according to OpenDRIVE elevation profiles.
//!
//! @param[in]  road           OpenDRIVE road data structure
//! @param[in]  offset         Absolute offset on reference line within road
//! @return                    height coordinate
//-----------------------------------------------------------------------------
units::length::meter_t CalculateCoordZ(RoadInterface* road, units::length::meter_t offset);

//-----------------------------------------------------------------------------
//! Calculates slope according to OpenDRIVE elevation profiles.
//!
//! @param[in]  road           OpenDRIVE road data structure
//! @param[in]  offset         Absolute offset on reference line within road
//! @return                    slope
//-----------------------------------------------------------------------------
double CalculateSlope(RoadInterface* road, units::length::meter_t offset);

//-----------------------------------------------------------------------------
//! Calculates the width of the provided lane.
//!
//! @param[in]  roadLane            OpenDRIVE road lane (for width information)
//! @param[in]  sectionOffset       Offset within the OpenDRIVE section
//! @return                         Lane width, 0.0 if no width was specified
//-----------------------------------------------------------------------------
units::length::meter_t CalculateLaneWidth(const RoadLaneInterface* roadLane, units::length::meter_t sectionOffset);

//-----------------------------------------------------------------------------
//! Calculates the border of the provided lane.
//!
//! @param[in]  roadLane            OpenDRIVE road lane (for width information)
//! @param[in]  sectionOffset       Offset within the OpenDRIVE section
//! @return                         Lane border, 0.0 if no border was specified
//-----------------------------------------------------------------------------
units::length::meter_t CalculateLaneBorder(const RoadLaneInterface* roadLane, units::length::meter_t sectionOffset);

//! Checks if the lane has a negative width
//!
//! @param[in]  roadLane            OpenDRIVE road lane (for width information)
//! @param[in]  sectionOffset       Offset within the OpenDRIVE section
//! @param[in]  width               Lane width
void CheckLaneWidth(const RoadLaneInterface* roadLane,
                    units::length::meter_t sectionOffset,
                    units::length::meter_t width);

//! Calculates the offset of the provided lane
//!
//! @param[in]  road            OpenDRIVE road data structure
//! @param[in]  roadPosition    Position w.r.t. start of section
//! @return                     Lane offset, 0.0 if no offset was specified
units::length::meter_t CalculateLaneOffset(const RoadInterface* road, units::length::meter_t roadPosition);

//-----------------------------------------------------------------------------
//! Calculates the slope of a lane at a given position
//!
//! @param[in] elevation        elevation input data (has to match given position)
//! @param[in] position         position w.r.t. start of section
//! @return                     width at given position
//-----------------------------------------------------------------------------
double CalculateSlopeAtRoadPosition(const RoadElevation* elevation, units::length::meter_t position);

//! Calculates the offset of a lane at a given position
//!
//! @param[in] roadOffset       Lane offset, i.e. a lateral shift of the reference line, as a cubic polynomial
//! @param[in] position         Position w.r.t. start of section
//! @return                     Lane offset at given position
units::length::meter_t CalculateOffsetAtRoadPosition(const RoadLaneOffset* roadOffset, units::length::meter_t position);

//-----------------------------------------------------------------------------
//! Calculates the width of a lane at a given position
//!
//! @param[in] width            width input data (has to match given position)
//! @param[in] position         position w.r.t. start of section
//! @return                     width at given position
//-----------------------------------------------------------------------------
units::length::meter_t CalculateWidthAtSectionPosition(const RoadLaneWidth* width, units::length::meter_t position);

//-----------------------------------------------------------------------------
//! Get the RoadElevation which is relevant for the given position
//!
//! @param[in] sectionOffset    position w.r.t. start of section
//! @param[in] road             the RoadInterface input data
//! @return                     relevant RoadElevation
//-----------------------------------------------------------------------------
const RoadElevation* GetRelevantRoadElevation(units::length::meter_t sectionOffset, RoadInterface* road);

//-----------------------------------------------------------------------------
//! Get the RoadLaneWidth which is relevant for the given position
//!
//! @param[in] sectionOffset    position w.r.t. start of section
//! @param[in] widthsOrBorders  container with all potential RoadLaneWidth pointers
//! @return                     relevant RoadLaneWidth
//-----------------------------------------------------------------------------
const RoadLaneWidth* GetRelevantRoadLaneWidth(units::length::meter_t sectionOffset,
                                              const std::vector<std::unique_ptr<RoadLaneWidth>>& widthsOrBorders);

//! Get the RoadLaneOffset which is relevant for the given position
//!
//! @param[in] roadOffset       Position w.r.t. s coordinate on the road
//! @param[in] road             OpenDRIVE road data structure
//! @return                     Relevant RoadLaneOffset
const RoadLaneOffset* GetRelevantRoadLaneOffset(units::length::meter_t roadOffset, const RoadInterface* road);

//! Get the RoadLaneRoadMark which is relevant for the given position
//!
//! @param[in] sectionOffset    Position w.r.t. start of section
//! @param[in] roadLane         OpenDRIVE road lane (for width information)
//! @return                     Relevant RoadLaneRoadMark

const RoadLaneRoadMark* GetRelevantRoadLaneRoadMark(units::length::meter_t sectionOffset,
                                                    const RoadLaneInterface* roadLane);

//-----------------------------------------------------------------------------
//! \brief CalculateIntersections calculates any intersections on the
//!        scenery's junctions
//! \param [in] worldData       worldData that is built by this function
//-----------------------------------------------------------------------------
void CalculateIntersections(OWL::Interfaces::WorldData& worldData);

//-----------------------------------------------------------------------------
//! \brief BuildRoadPolygons builds all polygons for the road and pairs them
//!        with the road's id
//!
//! \param[in] road the road for which polygons will be built
//! \return the pair of the road id to the collection of polygons for the road
//-----------------------------------------------------------------------------
RoadPolygons BuildRoadPolygons(const OWL::Road* const road);

//-----------------------------------------------------------------------------
//! \brief CreateBuildPolygonFromLaneGeometryFunction creates a function that
//!        builds LaneGeometryPolygons from a LaneGeometryElement for a
//!        specific roadId and laneId
//!
//! \param[in] roadId the roadId for which the created function will build
//!            LaneGeometryPolygons
//! \param[in] laneId the laneId for which the created function will build
//!            LaneGeometryPolygons
//! \return a function that creates LaneGeometryPolygons from
//!         LaneGeometryElements for the lane at laneId on the road at roadId
//-----------------------------------------------------------------------------
std::function<LaneGeometryPolygon(const OWL::Primitive::LaneGeometryElement* const)>
CreateBuildPolygonFromLaneGeometryFunction(const std::string& roadId, const OWL::Id laneId);

//-----------------------------------------------------------------------------
//! \brief CalculateJunctionIntersectionsFromRoadPolygons calculates
//!        intersections for a junction from polygons created from that
//!        junction's roads
//!
//! \param[in] junctionPolygons the map of road ids to LaneGeometryPolygons for
//!            the junction
//! \param[in] junction the junction to which intersection information should
//!             be added, given that any interesections are found
//! \param[in] worldData that is built by this function
//-----------------------------------------------------------------------------
void CalculateJunctionIntersectionsFromRoadPolygons(const JunctionPolygons& junctionPolygons,
                                                    OWL::Junction* const junction,
                                                    OWL::Interfaces::WorldData& worldData);

//-----------------------------------------------------------------------------
//! \brief CalculateIntersectionInfoForRoadPolygons calculates intersection
//!        information between two sets of RoadPolygons
//!
//! \param[in] polygons the base road's set of polygons
//! \param[in] polygonsToCompare the potentially-intersecting road's set of
//!            polygons
//! \param[in] junction the junction for which the intersection info is
//!            calculated
//! \return the IntersectionInfo between the two sets of RoadPolygons, if an
//!         intersection exists between them
//-----------------------------------------------------------------------------
std::optional<OWL::IntersectionInfo> CalculateIntersectionInfoForRoadPolygons(const RoadPolygons& polygons,
                                                                              const RoadPolygons& polygonsToCompare,
                                                                              const OWL::Junction* const junction);

//-----------------------------------------------------------------------------
//! \brief GetRelativeRank Gets the relative rank of an intersecting road with
//!        respect to a road
//!
//! \param[in] roadId the roadId of the road whose relative rank to get
//! \param[in] intersectingRoadId the roadId of the intersecting road
//! \param[in] junction the junction in which the roads intersect
//! \return the relative rank of the intersecting road
//-----------------------------------------------------------------------------
IntersectingConnectionRank GetRelativeRank(const std::string& roadId,
                                           const std::string& intersectingRoadId,
                                           const OWL::Junction* const junction);

const units::length::meter_t SAMPLING_RATE{0.1};  //!< 1m sampling rate of reference line
constexpr static const double EPS = 1e-3;         //!< epsilon value for geometric comparisons
};                                                //namespace GeometryConverter
