/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "RadioImplementation.h"

#include <algorithm>
#include <cmath>

void RadioImplementation::Send(units::length::meter_t positionX,
                               units::length::meter_t postionY,
                               units::power::watt_t signalStrength,
                               osi3::MovingObject objectInformation)
{
  RadioSignal radioSignal{positionX, postionY, signalStrength, objectInformation};
  signalVector.push_back(radioSignal);
}

std::vector<osi3::MovingObject> RadioImplementation::Receive(units::length::meter_t positionX,
                                                             units::length::meter_t positionY,
                                                             units::sensitivity sensitivity)
{
  std::vector<osi3::MovingObject> detectedObjects{};
  for (const RadioSignal& radioSignal : signalVector)
  {
    auto deltaX = radioSignal.positionX - positionX;
    auto deltaY = radioSignal.positionY - positionY;
    auto distance = units::math::sqrt(deltaX * deltaX + deltaY * deltaY);
    if (CanHearSignal(radioSignal.signalStrength, distance, sensitivity))
    {
      detectedObjects.push_back(radioSignal.objectInformation);
    }
  }
  return detectedObjects;
}

bool RadioImplementation::CanHearSignal(units::power::watt_t signalStrength,
                                        units::length::meter_t distance,
                                        units::sensitivity sensitivity)
{
  units::sensitivity receivedSignalStrength = signalStrength / (4 * M_PI * distance * distance);
  return receivedSignalStrength >= sensitivity;
}

void RadioImplementation::Reset()
{
  signalVector.clear();
}
