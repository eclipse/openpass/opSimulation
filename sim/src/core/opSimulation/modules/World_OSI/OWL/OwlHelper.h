/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <cmath>
#include <units.h>

#include "DataTypes.h"

namespace OWL
{

//! Adds a OSI lane assignment for an object
//!
//! \param[out] object          object for which the lane assignments is set
//! \param[in] lane             lane to assign
//! \param[in] specification    OpenDRIVE specification of the object
//! \param[in] hdg              heading of the object on the lane
template <typename OsiObject, typename Specification>
void static AssignLaneAndLogicalLane(OsiObject* object,
                                     const OWL::Interfaces::Lane& lane,
                                     const Specification& specification,
                                     units::angle::radian_t hdg)
{
  object->mutable_classification()->add_assigned_lane_id()->set_value(lane.GetId());
  auto logicalLaneAssignment = object->mutable_classification()->add_logical_lane_assignment();
  logicalLaneAssignment->mutable_assigned_lane_id()->set_value(lane.GetLogicalLaneId());
  logicalLaneAssignment->set_s_position(specification.GetS().value());
  logicalLaneAssignment->set_t_position(specification.GetT().value());
  logicalLaneAssignment->set_angle_to_lane(hdg.value());
}

//! Sets the source reference as defined by OSI
//!
//! \param[out] object  object for which the source reference is set
//! \param[in] id       id of the object
template <typename Object, typename Specification>
void static SetSourceReference(Object* object, const Specification& specification)
{
  const auto externalReference = object->add_source_reference();
  externalReference->set_type("net.asam.opendrive");
  externalReference->add_identifier(specification->GetId());
}

//! Returns the heading of a signal
//!
//! \param[in] signal           OpenDRIVE specification of the signal
//! \return heading on the lane
auto static GetOrientedHOffset(const RoadSignalInterface* object)
{
  using namespace units::literals;
  return CommonHelper::SetAngleToValidRange(object->GetHOffset()
                                            + (object->GetOrientation() ? 0_rad : units::angle::radian_t(M_PI)));
}

//! Returns the heading of a signal
//!
//! \param[in] signal           OpenDRIVE specification of the signal
//! \return heading on the lane
auto static GetOrientedHOffset(const RoadSignalInterface& object)
{
  return GetOrientedHOffset(&object);
}

//! Set the OSI BaseStationary for an object
//!
//! \param[out] baseStationary  BaseStationary to set
//! \param[in] signal           OpenDRIVE specification of the object
//! \param[in] position         position of the object
void static SetBaseStationary(osi3::BaseStationary* baseStationary,
                              const RoadSignalInterface* signal,
                              const Position& position)
{
  baseStationary->mutable_position()->set_x(units::unit_cast<double>(position.xPos));
  baseStationary->mutable_position()->set_y(units::unit_cast<double>(position.yPos));
  baseStationary->mutable_position()->set_z(units::unit_cast<double>(signal->GetZOffset() + 0.5 * signal->GetHeight()));
  baseStationary->mutable_dimension()->set_width(units::unit_cast<double>(signal->GetWidth()));
  baseStationary->mutable_dimension()->set_height(units::unit_cast<double>(signal->GetHeight()));
  baseStationary->mutable_orientation()->set_yaw(
      units::unit_cast<double>(CommonHelper::SetAngleToValidRange(position.yawAngle)));
  baseStationary->mutable_orientation()->set_pitch(
      units::unit_cast<double>(CommonHelper::SetAngleToValidRange(signal->GetPitch())));
  baseStationary->mutable_orientation()->set_roll(
      units::unit_cast<double>(CommonHelper::SetAngleToValidRange(signal->GetRoll())));
}

//! Set the OSI country and code for an signal
//!
//! \param[out] classification  classification to set
//! \param[in] signal           OpenDRIVE specification of the object
template <typename Classification>
void static SetCountryCode(Classification* classification, const RoadSignalInterface* signal)
{
  classification->set_country(signal->GetCountry());
  classification->set_country_revision(signal->GetCountryRevision());
  classification->set_code(signal->GetType());
  classification->set_sub_code(signal->GetSubType());
}

}  //namespace OWL
