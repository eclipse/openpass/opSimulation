/********************************************************************************
 * Copyright (c) 2017-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  SceneryConverter.h
//! @brief This file contains the converter of the scenery configuration.
//-----------------------------------------------------------------------------

#pragma once

#include <functional>
#include <list>
#include <map>
#include <memory>
#include <osi3/osi_object.pb.h>
#include <stddef.h>
#include <string>
#include <tuple>
#include <units.h>
#include <utility>
#include <vector>

#include "EntityInfoPublisher.h"
#include "Localization.h"
#include "OWL/DataTypes.h"
#include "TrafficObjectAdapter.h"
#include "WorldDataQuery.h"
#include "common/globalDefinitions.h"
#include "common/worldDefinitions.h"
#include "include/callbackInterface.h"
#include "include/idManagerInterface.h"
#include "include/roadInterface/roadElementTypes.h"

class JunctionInterface;
class RoadInterface;
class RoadLaneInterface;
class RoadLaneSectionInterface;
class RoadLinkInterface;
class RoadObjectInterface;
class RoadSignalInterface;
class SceneryInterface;
namespace OWL
{
namespace Interfaces
{
class WorldData;
}  // namespace Interfaces
}  // namespace OWL
namespace osi3
{
class BaseMoving;
class BaseStationary;
}  // namespace osi3

namespace Internal
{
struct ConversionStatus  ///< Status of the conversion of scenery configuration.
{
  bool status;                  ///< True, if conversion is successful
  std::string error_message{};  ///< Error message, if conversion is unsuccessful
};

/// Ids of the generated LaneBoundaries
struct LaneBoundaries
{
  std::vector<OWL::Id> laneBoundaries;         ///< generated (physical) LaneBoundaries
  std::vector<OWL::Id> logicalLaneBoundaries;  ///< generated LogicalLaneBoundaries
};

/// Alias
using PathInJunctionConnector = std::function<void(const JunctionInterface *,
                                                   const RoadInterface *,
                                                   const RoadInterface *,
                                                   const RoadInterface *,
                                                   ContactPointType,
                                                   ContactPointType,
                                                   ContactPointType,
                                                   const std::map<int, int> &)>;

/// @brief Connect the junction
/// @param scenery                  Pointer to the scenery interface
/// @param junction                 Pointer to the junction interface
/// @param connectPathInJunction    Connct the path in junction
/// @return Status of the conversion of scenery configuration.
ConversionStatus ConnectJunction(const SceneryInterface *scenery,
                                 const JunctionInterface *junction,
                                 const PathInJunctionConnector &connectPathInJunction);

}  // namespace Internal

//-----------------------------------------------------------------------------
//! Class for the convertion of a scenery, i.e. the roads in it; the road geometry
//! is converted in class GeometryConverter.
//-----------------------------------------------------------------------------
class SceneryConverter
{
public:
  //! SceneryConverter constructor
  //!
  //! @param[in] scenery              Scenery with the OpenDRIVE roads
  //! @param[in] idManager            Core IdManager
  //! @param[in] entityInfoPublisher  Publisher for Entities
  //! @param[in] worldData            OWL WorldData
  //! @param[in] localizer            World localizer
  //! @param[in] callbacks            Callback interface
  SceneryConverter(const SceneryInterface *scenery,
                   IdManagerInterface *idManager,
                   EntityInfoPublisher &entityInfoPublisher,
                   OWL::Interfaces::WorldData &worldData,
                   const World::Localization::Localizer &localizer,
                   const CallbackInterface *callbacks);
  SceneryConverter(const SceneryConverter &) = delete;
  SceneryConverter(SceneryConverter &&) = delete;
  SceneryConverter &operator=(const SceneryConverter &) = delete;
  SceneryConverter &operator=(SceneryConverter &&) = delete;
  virtual ~SceneryConverter() = default;

  //-----------------------------------------------------------------------------
  //! Triggers the convertion process from OpenDRIVE to OSI in the following steps:
  //! - defines a unique direction within the road cluster (MarkDirections())
  //! - assigns IDs to OpenDRIVE lane sections
  //! - generates the mappings for lanes and lane sections from OpenDRIVE to OSI
  //! - then connects these sections
  //! - finally, triggers convertion of the road geometries
  //!
  //! @return                         False if an error occurred, true otherwise
  //-----------------------------------------------------------------------------
  bool ConvertRoads();

  //! Places all static traffic objects and all traffic signs in the world
  void ConvertObjects();

  /// Sets the osi groundtruth's projection string based on the scenery's geo reference
  void SetProjectionString();

  //! Convert road coordinates to world coordinate
  //!
  //! @param[in] road     OpenDRIVE road data structure
  //! @param[in] s        s coordinate of the road position
  //! @param[in] t        t coordinate of the road position
  //! @param[in] yaw      Heading angle
  //! @return Position in world coordinates
  static Position RoadCoord2WorldCoord(const RoadInterface *road,
                                       units::length::meter_t s,
                                       units::length::meter_t t,
                                       units::angle::radian_t yaw);

protected:
  //-----------------------------------------------------------------------------
  //! Provides callback to LOG() macro
  //!
  //! @param[in]     logLevel    Importance of log
  //! @param[in]     file        Name of file where log is called
  //! @param[in]     line        Line within file where log is called
  //! @param[in]     message     Message to log
  //-----------------------------------------------------------------------------
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message)
  {
    if (callbacks)
    {
      callbacks->Log(logLevel, file, line, message);
    }
  }

private:
  //-----------------------------------------------------------------------------
  //! Returns the lane with the provided ID in the provided lane section.
  //!
  //! @param[in]  otherSection        OpenDRIVE lane section containing the lane
  //! @param[in]  otherId             ID of the required lane
  //! @return                         OpenDRIVE lane with the provided ID, if it exists
  //!                                 in the provided lane section, nullptr otherwise
  //-----------------------------------------------------------------------------
  static RoadLaneInterface *GetOtherLane(RoadLaneSectionInterface *otherSection, int otherId);

  //-----------------------------------------------------------------------------
  //! Returns the road from the scenery to which the provided link links to.
  //!
  //! Preconditions:
  //! - Scenery has to be setup properly
  //!
  //! @param[in]  roadLink            OpenDRIVE road link to the desired road
  //! @return                         Road which the provided link connects to, if
  //!                                 it exists in the scenery, nullptr otherwise
  //-----------------------------------------------------------------------------
  RoadInterface *GetConnectedRoad(RoadLinkInterface *roadLink);

  //-----------------------------------------------------------------------------
  //! Marks the direction of the provided road, on all of its lane sections
  //! and lanes within these lane sections to the provided value.
  //!
  //! @param[in]  road                OpenDRIVE road on which to set if it is
  //!                                 in direction
  //! @param[in]  inDirection         Specifies if road is in direction
  //-----------------------------------------------------------------------------
  void MarkDirectionRoad(RoadInterface *road, bool inDirection);

  //-----------------------------------------------------------------------------
  //! Mark the directions of all roads in the scenery according to global direction
  //! definition.
  //!
  //! Notes:
  //! - The first road of each cluster implictely defines the driving direction
  //!   (OpenDRIVE lanes/roads which are connected with opposite directions will
  //!   be converted to OSI lanes/sections which point into the same direction)
  //!
  //! @return                         False if an error occurred, true otherwise
  //-----------------------------------------------------------------------------
  bool MarkDirections();

  //-----------------------------------------------------------------------------
  //! Assigns unique IDs (beginning with 0) to all OpenDRIVE lane sections for
  //! all roads in the scenery.
  //!
  //! @return                         True
  //-----------------------------------------------------------------------------
  bool IndexElements();

  //-----------------------------------------------------------------------------
  //! Connects the OSI representation of the two provided lanes.
  //!
  //! @param[in]  currentLane         First lane to connect
  //! @param[in]  currentContactPoint Contact point of the first lane
  //! @param[in]  otherLane           Second lane to connect
  //! @param[in]  otherContactPoint   Contact point of the second lane
  //! @return                         False, if an error occurred, true otherwise
  //-----------------------------------------------------------------------------
  bool ConnectLaneToLane(RoadLaneInterface *currentLane,
                         ContactPointType currentContactPoint,
                         RoadLaneInterface *otherLane,
                         ContactPointType otherContactPoint);

  //-----------------------------------------------------------------------------
  //! Connects a lane to a lane section in OSI by connecting the predecessor and
  //! successor of the provided lane, which are in the provided lane section, to
  //! the provided line.
  //!
  //! @param[in]  currentLane         Lane to connect
  //! @param[in]  currentContactPoint Contact point of the lane
  //! @param[in]  otherLaneSection    Lane section to connect
  //! @param[in]  otherContactPoint   Contact point of the lane section
  //! @return                         False, if an error occurred, true otherwise
  //-----------------------------------------------------------------------------
  bool ConnectLaneToSection(RoadLaneInterface *currentLane,
                            ContactPointType currentContactPoint,
                            RoadLaneSectionInterface *otherLaneSection,
                            ContactPointType otherContactPoint);

  //-----------------------------------------------------------------------------
  //! Connects a lane section to a lane section in OSI by first connecting all
  //! lanes in the first section to the second section, then connecting all lanes
  //! in the second section to the first.
  //!
  //! Notes:
  //! - OpenDRIVE center lanes are skipped (width=0 by convention)
  //!
  //! @param[in]  firstLaneSection    First lane section to connect
  //! @param[in]  currentContactPoint Contact point of the first lane section
  //! @param[in]  secondLaneSection   Lane section to connect
  //! @param[in]  secondContactPoint  Contact point of the second lane section
  //! @return                         False, if an error occurred, true otherwise
  //-----------------------------------------------------------------------------
  bool ConnectLanes(RoadLaneSectionInterface *firstLaneSection,
                    ContactPointType firstContactPoint,
                    RoadLaneSectionInterface *secondLaneSection,
                    ContactPointType secondContactPoint);

  //-----------------------------------------------------------------------------
  //! Connects a road in the scenery to its predecessor and successor roads in
  //! OSI by connecting their respective lane sections. Only connects roads with roadlinktype Road
  //!
  //! @param[in]  road                Road which should be connected to its predecessors
  //!                                 and successors
  //! @return                         False, if an error occurred, true otherwise
  //-----------------------------------------------------------------------------
  bool ConnectRoadExternalWithElementTypeRoad(RoadInterface *road);

  //-----------------------------------------------------------------------------
  //! Connects a road with another road by setting successor of road, section and lanes
  //!
  //! @param[in]  currentRoad         road which should be connected to its successor
  //! @param[in]  otherRoad           successor of this currentRoad
  //! @param[in]  otherSection        section on otherRoad to connect to
  //! @param[in]  otherContactPoint   Contact point of the lane section
  //! @return                         False, if an error occurred, true otherwise
  //-----------------------------------------------------------------------------
  bool ConnectExternalRoadSuccessor(const RoadInterface *currentRoad,
                                    const RoadInterface *otherRoad,
                                    RoadLaneSectionInterface *otherSection,
                                    ContactPointType otherContactPoint);

  //-----------------------------------------------------------------------------
  //! Connects a road with another road by setting predecessor of road, section and lanes
  //!
  //! @param[in]  currentRoad         road which should be connected to its predecessor
  //! @param[in]  otherRoad           predecessor of this currentRoad
  //! @param[in]  otherSection        section on otherRoad to connect to
  //! @param[in]  otherContactPoint   Contact point of the lane section
  //! @return                         False, if an error occurred, true otherwise
  //-----------------------------------------------------------------------------
  bool ConnectExternalRoadPredecessor(const RoadInterface *currentRoad,
                                      const RoadInterface *otherRoad,
                                      RoadLaneSectionInterface *otherSection,
                                      ContactPointType otherContactPoint);

  //-----------------------------------------------------------------------------
  //! Connects a road in the scenery internally in OSI by connecting all of its
  //! stored lane sections with their predecessors and successors.
  //!
  //! @param[in]  road                Road in which the lanes and lane sections
  //!                                 should be connected
  //! @return                         False, if an error occurred, true otherwise
  //-----------------------------------------------------------------------------
  bool ConnectRoadInternal(RoadInterface *road);

  //-----------------------------------------------------------------------------
  //!Connects the incoming and connecting roads of the junction
  //!
  //! @param[in]  junction            Junction which should be connected
  //! @return                         False, if an error occurred, true otherwise
  //-----------------------------------------------------------------------------
  bool ConnectJunction(const JunctionInterface *junction);

  //-----------------------------------------------------------------------------
  //! Connects a single path of a junction.
  //! It only sets the connections into the path, because the connections of the path
  //! itself are already set by ConnectRoadExternalWithElementTypeRoad
  //!
  //! @param[in]  incomingRoad            road going into the path
  //! @param[in]  connectingRoad          connecting road == path
  //! @param[in]  outgoingRoad            road going out of the path
  //! @param[in]  incomingContactPoint    contactPoint on the path connected to the incomingRoad
  //! @param[in]  connectingContactPoint  contactPoint of the connector to the incomingRoad
  //! @param[in]  outgoingContactPoint    contactPoint on the outgoing road connected to the path
  //! @param[in]  laneIdMapping           mapping of the lane ids between the incoming road and the path
  //! @return                         False, if an error occurred, true otherwise
  //-----------------------------------------------------------------------------
  void ConnectPathInJunction(const JunctionInterface *junction,
                             const RoadInterface *incomingRoad,
                             const RoadInterface *connectingRoad,
                             const RoadInterface *outgoingRoad,
                             ContactPointType incomingContactPoint,
                             ContactPointType connectingContactPoint,
                             ContactPointType outgoingContactPoint,
                             const std::map<int, int> &laneIdMapping);

  //-----------------------------------------------------------------------------
  //! Connects OSI sections for all roads in the scenery internally and externally.
  //!
  //!
  //! @return                         False, if an error occurred, true otherwise
  //-----------------------------------------------------------------------------
  bool ConnectRoads();

  //OSI Methods and Variables
  void CreateRoads();

  void CreateRoadSignals();

  //! Creates a traffic sign in OWL from an OpenDRIVE RoadSignal
  //!
  //! \param signal       OpenDRIVE specification of the sign
  //! \param position     position of the sign in the world
  //! \param lanes        lanes for which this sign is valid
  void CreateTrafficSign(RoadSignalInterface *signal, Position position, const OWL::Interfaces::Lanes &lanes);

  //! Creates a road marking in OWL from an OpenDRIVE RoadSignal
  //!
  //! \param signal       OpenDRIVE specification of the road marking
  //! \param position     position of the road marking in the world
  //! \param lanes        lanes for which this road marking is valid
  void CreateRoadMarking(RoadSignalInterface *signal, Position position, const OWL::Interfaces::Lanes &lanes);

  //! Creates a road marking in OWL from an OpenDRIVE RoadSignal
  //!
  //! \param object       OpenDRIVE specification of the road marking as object
  //! \param position     position of the road marking in the world
  //! \param lanes        lanes for which this road marking is valid
  void CreateRoadMarking(RoadObjectInterface *object, Position position, const OWL::Interfaces::Lanes &lanes);

  //! Creates a traffic light in OWL from an OpenDRIVE RoadSignal
  //!
  //! \param signal       OpenDRIVE specification of the traffic light
  //! \param position     position of the traffic light in the world
  //! \param lanes        lanes for which this traffic light is valid
  void CreateTrafficLight(RoadSignalInterface *signal, Position position, const OWL::Interfaces::Lanes &lanes);

  void CreateObjects();

  void CreateObject(const RoadObjectInterface *object, const Position &position);

  void CreateContinuousObject(const RoadObjectInterface *object, const RoadInterface *road);

  bool IsMovingObject(RoadObjectInterface *object);

  bool IsVehicle(RoadObjectInterface *object);

  osi3::StationaryObject_Classification_Type GetStationaryObjectType(RoadObjectInterface *object);

  osi3::MovingObject_Type GetMovingObjectType(RoadObjectInterface *object);

  osi3::MovingObject_VehicleClassification_Type GetVehicleType(RoadObjectInterface *object);

  void SetOsiPosition(osi3::BaseStationary *baseStationary,
                      osi3::BaseMoving *baseMoving,
                      RoadInterface *road,
                      units::length::meter_t s,
                      units::length::meter_t t,
                      units::length::meter_t length,
                      units::length::meter_t height,
                      units::length::meter_t width,
                      units::angle::radian_t yaw,
                      units::angle::radian_t pitch,
                      units::angle::radian_t roll);

  Internal::LaneBoundaries CreateLaneBoundaries(RoadLaneInterface &odLane, RoadLaneSectionInterface &odSection);

  bool CollectRoadLinks(RoadInterface *road,
                        std::map<std::tuple<RoadInterface *, RoadInterface *>,
                                 std::tuple<RoadLinkType, ContactPointType>> &collectedRoadLinks);

  bool ProcessCollectedRoadLinks(RoadInterface *road,
                                 std::list<RoadInterface *> pendingRoads,
                                 std::map<std::tuple<RoadInterface *, RoadInterface *>,
                                          std::tuple<RoadLinkType, ContactPointType>> collectedRoadLinks,
                                 std::list<RoadInterface *> tmpCluster);

  mantle_api::UniqueId GenerateIdAndPublishEntity(const openpass::type::EntityInfo &entityInfo,
                                                  EntityType entityType = EntityType::kOther);

  const SceneryInterface *scenery;
  IdManagerInterface *idManager;
  EntityInfoPublisher &entityInfoPublisher;
  OWL::Interfaces::WorldData &worldData;
  WorldDataQuery worldDataQuery{worldData};
  const World::Localization::Localizer &localizer;
  const CallbackInterface *callbacks;
  std::vector<std::unique_ptr<TrafficObjectAdapter>> trafficObjects;
};

//! Checks if a value is within the left closed interval
//!
//! @param[in] value    Value to check
//! @param[in] start    Start of the interval
//! @param[in] end      End of the interval
//! @return True, if a value is within the left closed interval
inline bool IsWithinLeftClosedInterval(double value, double start, double end)
{
  return value >= start && value < end;
}

//! Checks if a current element has a successor
//!
//! @param[in] vector           Vector of all elements
//! @param[in] currentIndex     Index of the current element
//! @return True, if current element has a successor
template <typename T, typename A>
inline bool HasSucceedingElement(std::vector<T, A> const &vector, size_t currentIndex)
{
  return vector.size() > currentIndex + 1;
}

//! This class converts the road network of OpenDRIVE into a graph representation
class RoadNetworkBuilder
{
  class DataBufferWriteInterface;

public:
  //! RoadNetworkBuilder constructor
  //!
  //! @param[in] scenery  Scenery with the OpenDRIVE roads
  RoadNetworkBuilder(const SceneryInterface &scenery) : scenery(scenery) {}

  //! Converts the road network of OpenDRIVE into a graph representation
  //!
  //! \return graph of the road network and mapping from roads (with direction) to the vertices
  //!
  std::pair<RoadGraph, RoadGraphVertexMapping> Build();

private:
  const SceneryInterface &scenery;
};
