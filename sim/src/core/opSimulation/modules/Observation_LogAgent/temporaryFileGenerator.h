/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <utility>

namespace fs = std::filesystem;

//! @class TemporaryFileGenerator
//! @brief A class for generating temporary files with unique names.
class TemporaryFileGenerator
{
public:
  //! @brief Constructor for TemporaryFileGenerator.
  //! @param filePath The base path for generating temporary file names.
  explicit TemporaryFileGenerator(fs::path filePath) : filePath(std::move(filePath)) {}

  ~TemporaryFileGenerator() = default;

  //! @brief Create a unique temporary file.
  //!
  //! If the provided file path exists, this method generates a new file name by appending
  //! a 6-digit number to the original file name.
  //!
  //! @return The generated unique temporary file.
  fs::path CreateTemporaryFile()
  {
    int counter = 0;
    do
    {
      // Generate a new filename with a 6-digit number appended
      std::ostringstream newFilenameStream;
      newFilenameStream << std::left << std::setw(6) << std::setfill('0') << counter % 1000000;
      std::string newFilename = filePath.stem().string() + "_" + newFilenameStream.str() + ".tmp";
      filePath.replace_filename(newFilename);
      counter++;
    } while (fs::exists(filePath));

    return filePath.filename();
  }

private:
  fs::path filePath;
};
