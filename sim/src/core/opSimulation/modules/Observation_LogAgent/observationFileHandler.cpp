/********************************************************************************
 * Copyright (c) 2017-2020 ITK Engineering GmbH
 *               2018-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  observationFileHandler.cpp */
//-----------------------------------------------------------------------------

#include "observationFileHandler.h"

#include <algorithm>
#include <chrono>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <libxml/xmlwriter.h>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>
#include <variant>

#include "common/namedType.h"
#include "common/openPassTypes.h"
#include "common/openPassUtils.h"
#include "common/sensorDefinitions.h"
#include "common/xmlParser.h"
#include "include/dataBufferInterface.h"
#include "opSimulation/modules/Observation_LogAgent/observationCyclics.h"
#include "opSimulation/modules/Observation_LogAgent/observationLogConstants.h"
#include "temporaryFileGenerator.h"

class RunResultInterface;

using namespace SimulationCommon;
namespace fs = std::filesystem;

ObservationFileHandler::ObservationFileHandler(const DataBufferReadInterface& dataBuffer) : dataBuffer{dataBuffer} {}

void ObservationFileHandler::WriteStartOfFile(const std::string& frameworkVersion)
{
  runNumber = 0;

  // setup environment
  auto folderPath = fs::path(folder);

  if (!fs::exists(folderPath))
  {
    throw std::runtime_error(std::string{COMPONENTNAME} + ": Output directory \"" + folderPath.string()
                             + "\" does not exist");
  }

  if (fs::exists(finalPath))
  {
    fs::remove(finalPath);
  }

  std::string filePath = folder.string() + "/" + "simulationOutput";
  TemporaryFileGenerator fileCreator(filePath);
  tmpFilePath = folder / fileCreator.CreateTemporaryFile();
  xmlFile.open(tmpFilePath.string());

  if (!xmlFile.is_open())
  {
    std::stringstream stream;
    stream << COMPONENTNAME << ": Unable to create temporary output file: " << tmpFilePath.filename().string();
    throw std::runtime_error(stream.str());
  }

  xmlFileStream = xmlNewTextWriterFilename(tmpFilePath.string().c_str(), 0);
  if (!xmlFileStream)
  {
    xmlFile.close();
    throw std::runtime_error("Could not create XML writer");
  }

  xmlTextWriterSetIndent(xmlFileStream, 1);
  xmlTextWriterStartDocument(xmlFileStream, "1.0", "UTF-8", nullptr);
  xmlTextWriterStartElement(xmlFileStream, toXmlChar(outputTags.SIMULATIONOUTPUT));
  xmlTextWriterWriteAttribute(xmlFileStream, toXmlChar(outputAttributes.FRAMEWORKVERSION), toXmlChar(frameworkVersion));
  xmlTextWriterWriteAttribute(xmlFileStream, toXmlChar(outputAttributes.SCHEMAVERSION), toXmlChar(outputFileVersion));
  xmlTextWriterStartElement(xmlFileStream, toXmlChar(outputTags.SCENERYFILE));
  xmlTextWriterWriteString(xmlFileStream, toXmlChar(sceneryFile));
  xmlTextWriterEndElement(xmlFileStream);
  xmlTextWriterStartElement(xmlFileStream, toXmlChar(outputTags.RUNRESULTS));
}

void ObservationFileHandler::WriteRun([[maybe_unused]] const RunResultInterface& runResult,
                                      std::uint32_t randomSeed,
                                      ObservationCyclics& cyclics,
                                      Events& events)
{
  std::stringstream stream;
  stream << COMPONENTNAME << " append log to file: " << tmpFilePath;
  // LOG(CbkLogLevel::Debug, ss.str());

  // init new run result
  xmlTextWriterStartElement(xmlFileStream, toXmlChar(outputTags.RUNRESULT));
  xmlTextWriterWriteAttribute(xmlFileStream, toXmlChar(outputAttributes.RUNID), toXmlChar(std::to_string(runNumber)));
  xmlTextWriterWriteAttribute(
      xmlFileStream, toXmlChar(outputAttributes.RANDOMSEED), toXmlChar(std::to_string(randomSeed)));

  AddEvents(events);
  AddAgents();

  // write CyclicsTag
  xmlTextWriterStartElement(xmlFileStream, toXmlChar(outputTags.CYCLICS));

  if (writeCyclicsToCsv)
  {
    std::string runPrefix;
    if (runNumber < 10)
    {
      runPrefix = "00";
    }
    else if (runNumber < 100)
    {
      runPrefix = "0";
    }
    std::string csvFilename = "Cyclics_Run_" + runPrefix + std::to_string(runNumber) + ".csv";

    AddReference(csvFilename);

    WriteCsvCyclics(csvFilename, cyclics);
  }
  else
  {
    AddHeader(cyclics);
    AddSamples(cyclics);
  }

  // close CyclicsTag
  xmlTextWriterEndElement(xmlFileStream);

  // close RunResultTag
  xmlTextWriterEndElement(xmlFileStream);

  ++runNumber;
}

void ObservationFileHandler::WriteEndOfFile()
{
  // close RunResultsTag
  xmlTextWriterEndElement(xmlFileStream);

  // close SimulationOutputTag
  xmlTextWriterEndElement(xmlFileStream);

  xmlTextWriterEndDocument(xmlFileStream);
  xmlFreeTextWriter(xmlFileStream);
  xmlFile.flush();
  xmlFile.close();

  // finalize results
  fs::rename(tmpFilePath, finalPath);
}

void ObservationFileHandler::AddEvents(Events& events)
{
  xmlTextWriterStartElement(xmlFileStream, toXmlChar(outputTags.EVENTS));

  for (const auto& event : events)
  {
    xmlTextWriterStartElement(xmlFileStream, toXmlChar(outputTags.EVENT));
    xmlTextWriterWriteAttribute(xmlFileStream, toXmlChar(outputAttributes.TIME), toXmlChar(std::to_string(event.time)));
    xmlTextWriterWriteAttribute(xmlFileStream, toXmlChar(outputAttributes.SOURCE), toXmlChar(event.dataRow.key));
    xmlTextWriterWriteAttribute(xmlFileStream, toXmlChar(outputAttributes.NAME), toXmlChar(event.dataRow.data.name));

    WriteEntities(outputTags.TRIGGERINGENTITIES, event.dataRow.data.triggeringEntities.entities, true);
    WriteEntities(outputTags.AFFECTEDENTITIES, event.dataRow.data.affectedEntities.entities, true);
    WriteParameter(event.dataRow.data.parameter, true);

    xmlTextWriterEndElement(xmlFileStream);  // event
  }

  xmlTextWriterEndElement(xmlFileStream);  // events
}

void ObservationFileHandler::AddAgents()
{
  xmlTextWriterStartElement(xmlFileStream, toXmlChar(outputTags.AGENTS));

  const auto agentIds = dataBuffer.GetKeys("Statics/Agents");

  for (const auto& agentId : agentIds)
  {
    AddAgent(agentId);
  }

  xmlTextWriterEndElement(xmlFileStream);
}

void ObservationFileHandler::AddAgent(const std::string& agentId)
{
  const std::string keyPrefix = "Agents/" + agentId + "/";

  xmlTextWriterStartElement(xmlFileStream, toXmlChar(outputTags.AGENT));

  xmlTextWriterWriteAttribute(xmlFileStream, toXmlChar(outputAttributes.ID), toXmlChar(agentId));
  xmlTextWriterWriteAttribute(
      xmlFileStream,
      toXmlChar(outputAttributes.AGENTTYPEGROUPNAME),
      toXmlChar(std::get<std::string>(dataBuffer.GetStatic(keyPrefix + "AgentTypeGroupName").at(0))));
  xmlTextWriterWriteAttribute(
      xmlFileStream,
      toXmlChar(outputAttributes.AGENTTYPENAME),
      toXmlChar(std::get<std::string>(dataBuffer.GetStatic(keyPrefix + "AgentTypeName").at(0))));
  xmlTextWriterWriteAttribute(
      xmlFileStream,
      toXmlChar(outputAttributes.VEHICLEMODELTYPE),
      toXmlChar(std::get<std::string>(dataBuffer.GetStatic(keyPrefix + "VehicleModelType").at(0))));
  xmlTextWriterWriteAttribute(
      xmlFileStream,
      toXmlChar(outputAttributes.DRIVERPROFILENAME),
      toXmlChar(std::get<std::string>(dataBuffer.GetStatic(keyPrefix + "DriverProfileName").at(0))));

  AddVehicleAttributes(agentId);
  AddSensors(agentId);

  xmlTextWriterEndElement(xmlFileStream);
}

void ObservationFileHandler::AddVehicleAttributes(const std::string& agentId)
{
  const std::string keyPrefix = "Agents/" + agentId + "/Vehicle/";

  xmlTextWriterStartElement(xmlFileStream, toXmlChar(outputTags.VEHICLEATTRIBUTES));

  xmlTextWriterWriteAttribute(
      xmlFileStream,
      toXmlChar(outputAttributes.WIDTH),
      toXmlChar(std::to_string(std::get<double>(dataBuffer.GetStatic(keyPrefix + "Width").at(0)))));
  xmlTextWriterWriteAttribute(
      xmlFileStream,
      toXmlChar(outputAttributes.LENGTH),
      toXmlChar(std::to_string(std::get<double>(dataBuffer.GetStatic(keyPrefix + "Length").at(0)))));
  xmlTextWriterWriteAttribute(
      xmlFileStream,
      toXmlChar(outputAttributes.HEIGHT),
      toXmlChar(std::to_string(std::get<double>(dataBuffer.GetStatic(keyPrefix + "Height").at(0)))));
  xmlTextWriterWriteAttribute(
      xmlFileStream,
      toXmlChar(outputAttributes.LONGITUDINALPIVOTOFFSET),
      toXmlChar(std::to_string(std::get<double>(dataBuffer.GetStatic(keyPrefix + "LongitudinalPivotOffset").at(0)))));

  xmlTextWriterEndElement(xmlFileStream);
}

void ObservationFileHandler::AddSensors(const std::string& agentId)
{
  const std::string keyPrefix = "Statics/Agents/" + agentId + "/Vehicle/Sensors";
  const auto& sensorIds = dataBuffer.GetKeys(keyPrefix);

  if (sensorIds.empty())
  {
    return;
  }

  xmlTextWriterStartElement(xmlFileStream, toXmlChar(outputTags.SENSORS));

  for (const auto& sensorId : sensorIds)
  {
    AddSensor(agentId, sensorId);
  }

  xmlTextWriterEndElement(xmlFileStream);
}

bool ObservationFileHandler::ContainsSensor(const openpass::sensors::Parameters& sensorParameters) const
{
  return !sensorParameters.empty();
}

void ObservationFileHandler::AddSensor(const std::string& agentId, const std::string& sensorId)
{
  const std::string sensorKeyPrefix = "Agents/" + agentId + "/Vehicle/Sensors/" + sensorId + "/";
  const std::string mountingKeyPrefix = sensorKeyPrefix + "Mounting/";

  xmlTextWriterStartElement(xmlFileStream, toXmlChar(outputTags.SENSOR));

  xmlTextWriterStartElement(xmlFileStream, toXmlChar(outputTags.SENSOR));

  xmlTextWriterWriteAttribute(xmlFileStream, toXmlChar(outputAttributes.ID), toXmlChar(sensorId));
  xmlTextWriterWriteAttribute(xmlFileStream,
                              toXmlChar(outputAttributes.TYPE),
                              toXmlChar(std::get<std::string>(dataBuffer.GetStatic(sensorKeyPrefix + "Type").at(0))));
  xmlTextWriterWriteAttribute(xmlFileStream,
                              toXmlChar(outputAttributes.MOUNTINGPOSITIONLONGITUDINAL),
                              toXmlChar(std::to_string(std::get<double>(
                                  dataBuffer.GetStatic(mountingKeyPrefix + "Position/Longitudinal").at(0)))));
  xmlTextWriterWriteAttribute(
      xmlFileStream,
      toXmlChar(outputAttributes.MOUNTINGPOSITIONLATERAL),
      toXmlChar(std::to_string(std::get<double>(dataBuffer.GetStatic(mountingKeyPrefix + "Position/Lateral").at(0)))));
  xmlTextWriterWriteAttribute(
      xmlFileStream,
      toXmlChar(outputAttributes.MOUNTINGPOSITIONHEIGHT),
      toXmlChar(std::to_string(std::get<double>(dataBuffer.GetStatic(mountingKeyPrefix + "Position/Height").at(0)))));
  xmlTextWriterWriteAttribute(
      xmlFileStream,
      toXmlChar(outputAttributes.ORIENTATIONYAW),
      toXmlChar(std::to_string(std::get<double>(dataBuffer.GetStatic(mountingKeyPrefix + "Orientation/Yaw").at(0)))));
  xmlTextWriterWriteAttribute(
      xmlFileStream,
      toXmlChar(outputAttributes.ORIENTATIONPITCH),
      toXmlChar(std::to_string(std::get<double>(dataBuffer.GetStatic(mountingKeyPrefix + "Orientation/Pitch").at(0)))));
  xmlTextWriterWriteAttribute(
      xmlFileStream,
      toXmlChar(outputAttributes.ORIENTATIONROLL),
      toXmlChar(std::to_string(std::get<double>(dataBuffer.GetStatic(mountingKeyPrefix + "Orientation/Roll").at(0)))));

  xmlTextWriterWriteAttribute(
      xmlFileStream,
      toXmlChar(outputAttributes.LATENCY),
      toXmlChar(std::to_string(std::get<double>(dataBuffer.GetStatic(sensorKeyPrefix + "Parameters/Latency").at(0)))));
  xmlTextWriterWriteAttribute(xmlFileStream,
                              toXmlChar(outputAttributes.OPENINGANGLEH),
                              toXmlChar(std::to_string(std::get<double>(
                                  dataBuffer.GetStatic(sensorKeyPrefix + "Parameters/OpeningAngleH").at(0)))));
  xmlTextWriterWriteAttribute(
      xmlFileStream,
      toXmlChar(outputAttributes.DETECTIONRANGE),
      toXmlChar(std::to_string(std::get<double>(dataBuffer.GetStatic(sensorKeyPrefix + "Parameters/Range").at(0)))));

  try
  {
    xmlTextWriterWriteAttribute(xmlFileStream,
                                toXmlChar(outputAttributes.OPENINGANGLEV),
                                toXmlChar(std::to_string(std::get<double>(
                                    dataBuffer.GetStatic(sensorKeyPrefix + "Parameters/OpeningAngleV").at(0)))));
  }
  catch (const std::out_of_range&)
  {
  }

  xmlTextWriterEndElement(xmlFileStream);
}

void ObservationFileHandler::AddHeader(ObservationCyclics& cyclics)
{
  xmlTextWriterStartElement(xmlFileStream, toXmlChar(outputTags.HEADER));
  xmlTextWriterWriteString(xmlFileStream, toXmlChar(cyclics.GetHeader()));
  xmlTextWriterEndElement(xmlFileStream);
}

void ObservationFileHandler::AddSamples(ObservationCyclics& cyclics)
{
  // write SamplesTag
  xmlTextWriterStartElement(xmlFileStream, toXmlChar(outputTags.SAMPLES));
  const auto& timeSteps = cyclics.GetTimeSteps();

  unsigned int timeStepNumber = 0;
  for (const auto timeStep : timeSteps)
  {
    xmlTextWriterStartElement(xmlFileStream, toXmlChar(outputTags.SAMPLE));
    xmlTextWriterWriteAttribute(xmlFileStream, toXmlChar(outputAttributes.TIME), toXmlChar(std::to_string(timeStep)));
    xmlTextWriterWriteString(xmlFileStream, toXmlChar(cyclics.GetSamplesLine(timeStepNumber)));

    // close SampleTag
    xmlTextWriterEndElement(xmlFileStream);

    ++timeStepNumber;
  }

  // close SamplesTag
  xmlTextWriterEndElement(xmlFileStream);
}

void ObservationFileHandler::AddReference(const std::string& filename)
{
  // write CyclicsFileTag
  xmlTextWriterStartElement(xmlFileStream, toXmlChar(outputTags.CYCLICSFILE));

  xmlTextWriterWriteString(xmlFileStream, toXmlChar(filename));

  // close CyclicsFileTag
  xmlTextWriterEndElement(xmlFileStream);
}

void ObservationFileHandler::WriteCsvCyclics(const std::filesystem::path& filename, ObservationCyclics& cyclics)
{
  fs::path path = folder / filename;

  csvFile.open(path.string());
  if (!csvFile.is_open())
  {
    std::stringstream stream;
    stream << COMPONENTNAME << " could not create file: " << tmpFilePath;
    throw std::runtime_error(stream.str());
  }

  csvFile << "Timestep,"
          << "AgentId," << cyclics.GetAgentHeader().c_str() << '\n';

  const auto& timeSteps = cyclics.GetTimeSteps();
  std::vector<std::string> agentSamplesLines;
  //for (unsigned int timeStepNumber = 0; timeStepNumber < timeSteps.size(); ++timeStepNumber)
  unsigned int timeStepNumber = 0;
  for (const auto timeStep : timeSteps)
  {
    agentSamplesLines = cyclics.GetAgentSamplesLine(timeStepNumber);
    for (size_t agentId = 0; agentId < agentSamplesLines.size(); ++agentId)
    {
      csvFile << std::to_string(timeStep).c_str() << "," << std::to_string(agentId).c_str() << ","
              << agentSamplesLines.at(agentId).c_str() << '\n';
    }
    ++timeStepNumber;
  }

  csvFile.flush();

  csvFile.close();
}

void ObservationFileHandler::WriteEntities(const char* tag, const openpass::type::EntityIds& entities, bool mandatory)
{
  if (!entities.empty())
  {
    xmlTextWriterStartElement(xmlFileStream, toXmlChar(tag));
    for (const auto& entity : entities)
    {
      xmlTextWriterStartElement(xmlFileStream, toXmlChar(output::tag::ENTITY));
      xmlTextWriterWriteAttribute(xmlFileStream, toXmlChar(output::attribute::ID), toXmlChar(std::to_string(entity)));
      xmlTextWriterEndElement(xmlFileStream);
    }
    xmlTextWriterEndElement(xmlFileStream);
  }
  else if (mandatory)
  {
    xmlTextWriterStartElement(xmlFileStream, toXmlChar(tag));
    xmlTextWriterEndElement(xmlFileStream);
  }
}

void ObservationFileHandler::WriteParameter(const openpass::type::FlatParameter& parameters, bool mandatory)
{
  constexpr auto tag = output::tag::PARAMETERS;
  if (!parameters.empty())
  {
    xmlTextWriterStartElement(xmlFileStream, toXmlChar(tag));

    // No structured binding on purpose:
    // see
    // https://stackoverflow.com/questions/46114214/lambda-implicit-capture-fails-with-variable-declared-from-structured-binding
    for (const auto& p : parameters)
    {
      auto parameterWriter = [&](const std::string& value)
      {
        xmlTextWriterStartElement(xmlFileStream, toXmlChar(output::tag::PARAMETER));
        xmlTextWriterWriteAttribute(xmlFileStream, toXmlChar(output::attribute::KEY), toXmlChar(p.first));
        xmlTextWriterWriteAttribute(xmlFileStream, toXmlChar(output::attribute::VALUE), toXmlChar(value));
        xmlTextWriterEndElement(xmlFileStream);
      };

      std::visit(openpass::utils::FlatParameter::to_string(parameterWriter), p.second);
    }
    xmlTextWriterEndElement(xmlFileStream);
  }
  else if (mandatory)
  {
    xmlTextWriterStartElement(xmlFileStream, toXmlChar(tag));
    xmlTextWriterEndElement(xmlFileStream);
  }
}
