/********************************************************************************
 * Copyright (c) 2017-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SpawnerPreRunCommonExport.h"

#include <memory>
#include <stdexcept>
#include <string>

#include "PreRunCommon/SpawnerPreRunCommonGlobal.h"
#include "SpawnerPreRunCommon.h"
#include "include/callbackInterface.h"
#include "include/spawnPointInterface.h"

struct SpawnPointDependencies;

const std::string Version = "0.0.1";
static const CallbackInterface *Callbacks = nullptr;  // NOLINT[cppcoreguidelines-avoid-non-const-global-variables]

extern "C" SPAWNPOINT_SHARED_EXPORT const std::string &OpenPASS_GetVersion()
{
  return Version;
}

extern "C" SPAWNPOINT_SHARED_EXPORT std::unique_ptr<SpawnPointInterface> OpenPASS_CreateInstance(
    const SpawnPointDependencies *dependencies, const CallbackInterface *callbacks)
{
  Callbacks = callbacks;

  try
  {
    return std::make_unique<SpawnerPreRunCommon>(dependencies, callbacks);
  }
  catch (const std::exception &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }

    return nullptr;
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }

    return nullptr;
  }
}

extern "C" SPAWNPOINT_SHARED_EXPORT void OpenPASS_Trigger(SpawnPointInterface *implementation, int time)
{
  try
  {
    implementation->Trigger(time);
  }
  catch (const std::exception &ex)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
    }
  }
  catch (...)
  {
    if (Callbacks != nullptr)
    {
      Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
    }
  }
}
