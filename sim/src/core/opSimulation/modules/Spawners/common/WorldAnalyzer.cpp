/********************************************************************************
 * Copyright (c) 2019-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "WorldAnalyzer.h"

#include <MantleAPI/Common/bounding_box.h>
#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <algorithm>
#include <limits>
#include <list>
#include <map>
#include <stdexcept>
#include <utility>

#include "common/RoutePlanning/RouteCalculation.h"
#include "common/SpawnerDefinitions.h"
#include "common/commonTools.h"
#include "common/globalDefinitions.h"
#include "common/vector2d.h"
#include "include/agentInterface.h"
#include "include/streamInterface.h"
#include "include/worldObjectInterface.h"

class StochasticsInterface;

namespace
{
const units::length::meter_t EPSILON{0.001};
const units::time::second_t TTC_THRESHHOLD{2.0};
const units::time::second_t TTC_ENDOFLANE{4.0};
const units::time::second_t ASSUMED_TTB{1.0};
const units::acceleration::meters_per_second_squared_t ASSUMED_BRAKING_ACCELERATION{-6.0};
const units::acceleration::meters_per_second_squared_t ASSUMED_FRONT_AGENT_ACCELERATION{-10};
static constexpr size_t MAX_ROADGRAPH_DEPTH = 10;  //! Limits search depths in case of cyclic network

static inline units::length::meter_t GetSeparation(const units::time::second_t gapInSeconds,
                                                   const units::velocity::meters_per_second_t velocity,
                                                   const units::length::meter_t consideredCarLengths,
                                                   const units::length::meter_t minimumSeparationBuffer)
{
  const auto minimumBuffer = minimumSeparationBuffer + consideredCarLengths;

  return units::math::max((gapInSeconds * velocity) + consideredCarLengths, minimumBuffer);
}
}  //namespace

bool WorldAnalyzer::ValidateRoadIdInDirection(const RoadId& roadId,
                                              const LaneId laneId,
                                              const units::length::meter_t distanceOnLane,
                                              const LaneTypes& validLaneTypes) const
{
  if (!world->IsDirectionalRoadExisting(roadId, laneId < 0))
  {
    loggingCallback("Invalid spawn information. RoadId: " + roadId
                    + " not existing for laneId:" + std::to_string(laneId) + " and will be ignored.");
    return false;
  }

  if (!world->IsLaneTypeValid(roadId, laneId, distanceOnLane, validLaneTypes))
  {
    return false;
  }

  return true;
}

Route WorldAnalyzer::SampleRoute(const RoadId& roadId, const LaneId laneId, StochasticsInterface* stochastics) const
{
  Route route;
  auto [roadGraph, root] = world->GetRoadGraph({roadId, laneId < 0}, MAX_ROADGRAPH_DEPTH);

  route.roadGraph = roadGraph;
  route.root = root;
  route.target = RouteCalculation::SampleRoute(roadGraph, root, world->GetEdgeWeights(roadGraph), *stochastics);

  return route;
}

Ranges GetRangesWithSupportedLaneType(const std::unique_ptr<LaneStreamInterface>& laneStream,
                                      const SPosition sStart,
                                      const SPosition sEnd,
                                      const LaneTypes& supportedLaneTypes)
{
  Ranges ranges;
  bool lastSupported = false;  // last lanetype was supported
  auto currentS = sStart;      // start of current range
  const auto laneTypes = laneStream->GetLaneTypes();
  for (const auto [s, laneType] : laneTypes)
  {
    if (s > sEnd)
    {
      break;
    }
    if (std::find(supportedLaneTypes.cbegin(), supportedLaneTypes.cend(), laneType) == supportedLaneTypes.cend())
    {
      if (lastSupported)
      {
        // If the lanetype is not supported, but the previous lanetype was, then the range ends at this s
        if (s > currentS)
        {
          ranges.emplace_back(currentS, s);
        }
        lastSupported = false;
      }
    }
    else
    {
      if (!lastSupported)
      {
        // If the lanetype is supported, but the previous lanetype was not, then a new range begins at this s
        if (s > currentS)
        {
          currentS = s;
        }
        lastSupported = true;
      }
    }
  }
  if (lastSupported)
  {
    // If the last lanetype is supported then the last range ends at sEnd
    ranges.emplace_back(currentS, std::min(sEnd, laneStream->GetLength()));
  }
  return ranges;
}

Ranges WorldAnalyzer::GetValidLaneSpawningRanges(const std::unique_ptr<LaneStreamInterface>& laneStream,
                                                 const SPosition sStart,
                                                 const SPosition sEnd,
                                                 const LaneTypes& supportedLaneTypes) const
{
  Ranges rangesWithSupportedLaneType = GetRangesWithSupportedLaneType(laneStream, sStart, sEnd, supportedLaneTypes);

  Ranges validLaneSpawningRanges;

  for (const auto& range : rangesWithSupportedLaneType)
  {
    const auto agentsInLane = laneStream->GetAgentsInRange({range.first, 0_m}, {range.second, 0_m});

    AgentInterfaces scenarioAgents{};
    for (const auto* agent : agentsInLane)
    {
      if (agent->GetAgentCategory() == AgentCategory::Ego || agent->GetAgentCategory() == AgentCategory::Scenario)
      {
        scenarioAgents.push_back(agent);
      }
    }

    if (scenarioAgents.empty())
    {
      validLaneSpawningRanges.emplace_back(range.first, range.second);
    }
    else
    {
      const auto* frontAgent = scenarioAgents.back();
      const auto frontAgentStreamPosition
          = laneStream->GetStreamPosition(frontAgent, ObjectPointPredefined::FrontCenter);
      const auto frontAgentDistance
          = frontAgentStreamPosition.has_value() ? frontAgentStreamPosition->s + EPSILON : range.second + EPSILON;

      const auto* rearAgent = scenarioAgents.front();
      const auto rearAgentStreamPosition = laneStream->GetStreamPosition(rearAgent, ObjectPointPredefined::RearCenter);
      const auto rearAgentDistance
          = rearAgentStreamPosition.has_value() ? rearAgentStreamPosition->s - EPSILON : -EPSILON;

      const auto validRangesBasedOnScenario
          = GetValidSpawningInformationForRange(range.first, range.second, rearAgentDistance, frontAgentDistance);

      validLaneSpawningRanges.insert(
          validLaneSpawningRanges.end(), validRangesBasedOnScenario.begin(), validRangesBasedOnScenario.end());
    }
  }

  return validLaneSpawningRanges;
}

std::optional<units::length::meter_t> WorldAnalyzer::GetNextSpawnPosition(
    const std::unique_ptr<LaneStreamInterface>& laneStream,
    const Range& bounds,
    const units::length::meter_t agentFrontLength,
    const units::length::meter_t agentRearLength,
    const units::velocity::meters_per_second_t intendedVelocity,
    const units::time::second_t gapInSeconds,
    const units::length::meter_t minimumSeparationBuffer) const
{
  units::length::meter_t spawnDistance;

  const auto maxSearchPosition = bounds.second + intendedVelocity * gapInSeconds;

  const auto downstreamObjects = laneStream->GetAgentsInRange({bounds.first, 0_m}, {maxSearchPosition, 0_m});
  const AgentInterface* firstDownstreamObject = nullptr;
  if (!downstreamObjects.empty())
  {
    firstDownstreamObject = downstreamObjects.front();
  }

  if (!firstDownstreamObject
      || laneStream->GetStreamPosition(firstDownstreamObject, ObjectPointPredefined::RearCenter)->s > maxSearchPosition)
  {
    spawnDistance = bounds.second - agentFrontLength;
  }
  else
  {
    const auto frontCarRearLength
        = firstDownstreamObject->GetLength() - firstDownstreamObject->GetDistanceReferencePointToLeadingEdge();
    const auto separation
        = GetSeparation(gapInSeconds, intendedVelocity, frontCarRearLength + agentFrontLength, minimumSeparationBuffer);

    const auto frontCarPosition
        = laneStream->GetStreamPosition(firstDownstreamObject, ObjectPointPredefined::RearCenter)->s;
    spawnDistance = frontCarPosition - separation;
  }

  if (spawnDistance - agentRearLength < bounds.first)
  {
    return {};
  }

  return spawnDistance;
}

units::length::meter_t WorldAnalyzer::CalculateAdjustedSpawnDistanceToEndOfLane(
    const LaneId laneId,
    const units::length::meter_t sCoordinate,
    const units::length::meter_t intendedSpawnPosition,
    const units::velocity::meters_per_second_t intendedVelocity,
    const Route& route,
    const LaneTypes& supportedLaneTypes) const
{
  const auto distanceToEndOfDrivingLane
      = world
            ->GetDistanceToEndOfLane(route.roadGraph,
                                     route.root,
                                     laneId,
                                     sCoordinate,
                                     units::length::meter_t(std::numeric_limits<double>::max()),
                                     supportedLaneTypes)
            .at(route.target);

  const auto minDistanceToEndOfLane = intendedVelocity * TTC_ENDOFLANE;

  if (distanceToEndOfDrivingLane < minDistanceToEndOfLane)
  {
    const auto difference = minDistanceToEndOfLane - distanceToEndOfDrivingLane;
    return intendedSpawnPosition - difference;
  }

  return intendedSpawnPosition;
}

units::velocity::meters_per_second_t WorldAnalyzer::CalculateSpawnVelocityToPreventCrashing(
    const LaneId laneId,
    const units::length::meter_t intendedSpawnPosition,
    const units::length::meter_t agentFrontLength,
    const units::length::meter_t agentRearLength,
    const units::velocity::meters_per_second_t intendedVelocity,
    const Route& route) const
{
  const auto& inOdDirection = get(RouteElement(), route.roadGraph, route.root).inOdDirection;
  const auto maxSearchDistance = agentFrontLength + (intendedVelocity * TTC_THRESHHOLD);

  const auto nextObjectsInLane
      = world
            ->GetAgentsInRange(
                route.roadGraph, route.root, laneId, intendedSpawnPosition - agentRearLength, 0_m, maxSearchDistance)
            .at(route.target);
  const AgentInterface* opponent = nullptr;
  if (!nextObjectsInLane.empty())
  {
    opponent = nextObjectsInLane.front();

    const auto& relativeVelocity = intendedVelocity - opponent->GetVelocity().Length();
    if (relativeVelocity <= 0.0_mps)
    {
      return intendedVelocity;
    }

    const auto& relativeDistance
        = inOdDirection
            ? world->ResolveRelativePoint(route.roadGraph, route.root, ObjectPointRelative::Rearmost, *opponent)
                      .at(route.target)
                      ->roadPosition.s
                  - intendedSpawnPosition - agentFrontLength
            : intendedSpawnPosition - agentFrontLength
                  - world->ResolveRelativePoint(route.roadGraph, route.root, ObjectPointRelative::Rearmost, *opponent)
                        .at(route.target)
                        ->roadPosition.s;

    if (relativeDistance / relativeVelocity < TTC_THRESHHOLD)
    {
      return opponent->GetVelocity().Length() + (relativeDistance / TTC_THRESHHOLD);
    }
  }

  return intendedVelocity;
}

units::velocity::meters_per_second_t WorldAnalyzer::CalculateSpawnVelocityToPreventCrashing(
    const std::unique_ptr<LaneStreamInterface>& laneStream,
    const units::length::meter_t intendedSpawnPosition,
    const units::length::meter_t agentFrontLength,
    const units::length::meter_t agentRearLength,
    const units::velocity::meters_per_second_t intendedVelocity) const
{
  const auto maxSearchDistance = agentFrontLength + (intendedVelocity * TTC_THRESHHOLD);

  const auto nextObjectsInLane
      = laneStream->GetObjectsInRange({intendedSpawnPosition - agentRearLength, 0_m}, {maxSearchDistance, 0_m});
  const WorldObjectInterface* opponent = nullptr;
  if (!nextObjectsInLane.empty())
  {
    opponent = nextObjectsInLane.front();

    const auto relativeVelocity = intendedVelocity - opponent->GetVelocity().Length();
    if (relativeVelocity <= 0.0_mps)
    {
      return intendedVelocity;
    }

    const auto relativeDistance = laneStream->GetStreamPosition(opponent, ObjectPointPredefined::RearCenter)->s
                                - intendedSpawnPosition - agentFrontLength;

    if (relativeDistance / relativeVelocity < TTC_THRESHHOLD)
    {
      return opponent->GetVelocity().Length() + (relativeDistance / TTC_THRESHHOLD);
    }
  }

  return intendedVelocity;
}

bool WorldAnalyzer::ValidMinimumSpawningDistanceToObjectInFront(
    const LaneId laneId,
    const SPosition sPosition,
    const Route& route,
    std::shared_ptr<const mantle_api::VehicleProperties>
        vehicleModelParameters,  // NOLINT(performance-unnecessary-value-param)
    const units::length::meter_t minimumSeparationBuffer) const
{
  const units::length::meter_t distanceReferencePointToFront
      = vehicleModelParameters->bounding_box.dimension.length * 0.5
      + vehicleModelParameters->bounding_box.geometric_center.x;
  const units::length::meter_t rearLength = vehicleModelParameters->bounding_box.dimension.length * 0.5
                                          - vehicleModelParameters->bounding_box.geometric_center.x;

  const auto agentsInRangeResult = world->GetObjectsInRange(route.roadGraph,
                                                            route.root,
                                                            laneId,
                                                            sPosition,
                                                            rearLength,
                                                            distanceReferencePointToFront + minimumSeparationBuffer);

  if (!agentsInRangeResult.at(route.target).empty())
  {
    loggingCallback("Minimum distance required to previous agent not valid on lane: " + std::to_string(laneId) + ".");
    return false;
  }

  return true;
}

bool WorldAnalyzer::AreSpawningCoordinatesValid(
    const RoadId& roadId,
    const LaneId laneId,
    const SPosition sPosition,
    const units::length::meter_t offset,
    const units::length::meter_t minimumSeparationBuffer,
    const Route& route,
    std::shared_ptr<const mantle_api::VehicleProperties>
        vehicleModelParameters)  // NOLINT(performance-unnecessary-value-param)
    const
{
  if (!world->IsSValidOnLane(roadId, laneId, sPosition))
  {
    loggingCallback("S is not valid for vehicle on lane: " + std::to_string(laneId)
                    + ". Invalid s: " + units::length::to_string(sPosition));
    return false;
  }

  if (!IsOffsetValidForLane(roadId, laneId, sPosition, offset, vehicleModelParameters->bounding_box.dimension.width))
  {
    loggingCallback("Offset is not valid for vehicle on lane: " + std::to_string(laneId)
                    + ". Invalid offset: " + units::length::to_string(offset));

    return false;
  }

  if (!ValidMinimumSpawningDistanceToObjectInFront(
          laneId, sPosition, route, vehicleModelParameters, minimumSeparationBuffer))
  {
    loggingCallback("New Agent does not fullfill the required minimum distance on lane: " + std::to_string(laneId)
                    + ".");
    return false;
  }

  return true;
}

Ranges WorldAnalyzer::GetValidSpawningInformationForRange(const units::length::meter_t sStart,
                                                          const units::length::meter_t sEnd,
                                                          const units::length::meter_t firstScenarioAgentSPosition,
                                                          const units::length::meter_t lastScenarioAgentSPosition)
{
  // if the stream segment this SpawnPoint is responsible for is surrounded by scenario agents,
  // no valid spawn locations exist here
  if (firstScenarioAgentSPosition < sStart && lastScenarioAgentSPosition > sEnd)
  {
    return {};
  }
  // if the stream segment this SpawnPoint is responsible for has one scenario agent within
  // its range and one without, only a portion of the specified range is valid
  Ranges validLaneSpawningInformation{};
  if ((firstScenarioAgentSPosition < sStart && lastScenarioAgentSPosition < sStart)
      || (firstScenarioAgentSPosition > sEnd && lastScenarioAgentSPosition > sEnd))
  {
    validLaneSpawningInformation.emplace_back(sStart, sEnd);
    return validLaneSpawningInformation;
  }
  // if the first scenario s position is within our range, exclude everything after that (except as below)
  if (firstScenarioAgentSPosition > sStart && firstScenarioAgentSPosition < sEnd)
  {
    validLaneSpawningInformation.emplace_back(sStart, firstScenarioAgentSPosition);
  }

  // if the last scenario s position is within our range, exclude everything before that (except as above)
  if (lastScenarioAgentSPosition > sStart && lastScenarioAgentSPosition < sEnd)
  {
    validLaneSpawningInformation.emplace_back(lastScenarioAgentSPosition, sEnd);
  }

  return validLaneSpawningInformation;
}

bool WorldAnalyzer::IsOffsetValidForLane(const RoadId& roadId,
                                         const LaneId laneId,
                                         const SPosition distanceFromStart,
                                         const units::length::meter_t offset,
                                         const units::length::meter_t vehicleWidth) const
{
  if (!world->IsSValidOnLane(roadId, laneId, distanceFromStart))
  {
    loggingCallback("Invalid offset. Lane is not available: " + std::to_string(laneId)
                    + ". Distance from start: " + units::length::to_string(distanceFromStart));
    return false;
  }

  //Check if lane width > vehicle width
  const auto laneWidth = world->GetLaneWidth(roadId, laneId, distanceFromStart);
  if (vehicleWidth > laneWidth + units::math::abs(offset))
  {
    loggingCallback("Invalid offset. Lane width < vehicle width: " + std::to_string(laneId)
                    + ". Distance from start: " + units::length::to_string(distanceFromStart)
                    + ". Lane width: " + units::length::to_string(laneWidth)
                    + ". Vehicle width: " + units::length::to_string(vehicleWidth));
    return false;
  }

  //Is vehicle completely inside the lane
  bool isVehicleCompletelyInLane = (laneWidth - vehicleWidth) * 0.5 >= units::math::abs(offset);
  if (isVehicleCompletelyInLane)
  {
    return true;
  }

  //Is vehicle more than 50 % on the lane
  units::length::meter_t allowedRange = laneWidth * 0.5;
  bool outsideAllowedRange = units::math::abs(offset) > allowedRange;
  if (outsideAllowedRange)
  {
    loggingCallback("Invalid offset. Vehicle not inside allowed range: " + std::to_string(laneId)
                    + ". Invalid offset: " + units::length::to_string(offset));
    return false;
  }

  //Is vehicle partly on invalid lane
  bool isOffsetToLeftLane = (offset >= 0_m);
  int otherLaneId = isOffsetToLeftLane ? (laneId + 1) : (laneId - 1);

  if (!world->IsSValidOnLane(roadId, otherLaneId, distanceFromStart))  //other lane is invalid
  {
    loggingCallback("Invalid offset. Other lane is invalid: " + std::to_string(laneId)
                    + ". Invalid offset: " + units::length::to_string(offset));
    return false;
  }

  return true;
}

bool WorldAnalyzer::SpawnWillCauseCrash(const std::unique_ptr<LaneStreamInterface>& laneStream,
                                        const SPosition sPosition,
                                        const units::length::meter_t agentFrontLength,
                                        const units::length::meter_t agentRearLength,
                                        const units::velocity::meters_per_second_t velocity,
                                        const Direction direction) const
{
  const auto searchForward = (direction == Direction::FORWARD);
  const auto nextObjectsInLane = laneStream->GetObjectsInRange(
      {sPosition - (searchForward ? 0_m : units::length::meter_t(std::numeric_limits<double>::infinity())), 0_m},
      {sPosition + (searchForward ? units::length::meter_t(std::numeric_limits<double>::infinity()) : 0_m), 0_m});

  const WorldObjectInterface* opponent = nullptr;
  if (!nextObjectsInLane.empty())
  {
    opponent = searchForward ? nextObjectsInLane.front() : nextObjectsInLane.back();
  }

  if (!opponent)
  {
    return false;
  }

  units::velocity::meters_per_second_t velocityOfRearObject;
  units::acceleration::meters_per_second_squared_t accelerationOfRearObject;

  units::velocity::meters_per_second_t velocityOfFrontObject;
  units::acceleration::meters_per_second_squared_t accelerationOfFrontObject;

  units::length::meter_t spaceBetweenObjects;

  if (searchForward)
  {
    velocityOfRearObject = velocity;
    accelerationOfRearObject = ASSUMED_BRAKING_ACCELERATION;
    spaceBetweenObjects = laneStream->GetStreamPosition(opponent, ObjectPointPredefined::RearCenter)->s
                        - (sPosition + agentFrontLength);
    velocityOfFrontObject = opponent->GetVelocity().Length();
    accelerationOfFrontObject = ASSUMED_FRONT_AGENT_ACCELERATION;
  }
  else
  {
    velocityOfRearObject = opponent->GetVelocity().Length();
    accelerationOfRearObject = ASSUMED_BRAKING_ACCELERATION;
    spaceBetweenObjects = (sPosition - agentRearLength)
                        - laneStream->GetStreamPosition(opponent, ObjectPointPredefined::FrontCenter)->s;
    velocityOfFrontObject = velocity;
    accelerationOfFrontObject = ASSUMED_FRONT_AGENT_ACCELERATION;
  }

  return TrafficHelperFunctions::WillCrash(spaceBetweenObjects,
                                           velocityOfRearObject,
                                           accelerationOfRearObject,
                                           velocityOfFrontObject,
                                           accelerationOfFrontObject,
                                           ASSUMED_TTB);
}

size_t WorldAnalyzer::GetRightLaneCount(const RoadId& roadId,
                                        const LaneId& laneId,
                                        const units::length::meter_t sPosition) const
{
  size_t rightLaneCount{0};
  RoadGraph roadGraph;
  const auto start = add_vertex(RouteElement{roadId, laneId < 0}, roadGraph);
  const auto relativeLanes = world->GetRelativeLanes(roadGraph, start, laneId, sPosition, 0_m).at(start);
  for (const auto& lane : relativeLanes.front().lanes)
  {
    if (lane.relativeId < 0 && lane.type == LaneType::Driving)
    {
      ++rightLaneCount;
    }
  }
  return rightLaneCount;
}

size_t WorldAnalyzer::GetRightLaneCount(const std::unique_ptr<LaneStreamInterface>& laneStream,
                                        const units::length::meter_t sPosition) const
{
  auto roadPosition = laneStream->GetRoadPosition({sPosition, 0_m});

  if (roadPosition.roadId.empty())
  {
    return 0;
  }

  // TODO: Align datatype of LaneId (int) with mantle_api::LaneId (int64_t))
  return GetRightLaneCount(roadPosition.roadId, static_cast<int>(roadPosition.laneId), roadPosition.roadPosition.s);
}

std::vector<RouteElement> WorldAnalyzer::SetWayToTargetRoute(RoadGraphVertex targetVertex,
                                                             RoadGraph roadGraph,
                                                             RoadGraphVertex current)
{
  // RoadGraphVertex current{0};
  RoadGraph wayToTarget{};
  std::vector<RouteElement> wayToTargetRoute{};  //! same information as wayToTarget but as vector
  RoadGraphVertex wayPoint = targetVertex;
  auto routeElement = get(RouteElement(), roadGraph, wayPoint);
  auto vertex1 = add_vertex(routeElement, wayToTarget);
  wayToTargetRoute.push_back(routeElement);
  while (wayPoint != current)
  {
    for (auto [edge, edgesEnd] = edges(roadGraph); edge != edgesEnd; ++edge)
    {
      if (target(*edge, roadGraph) == wayPoint)
      {
        wayPoint = source(*edge, roadGraph);
        auto routeElement = get(RouteElement(), roadGraph, wayPoint);
        auto vertex2 = add_vertex(routeElement, wayToTarget);
        add_edge(vertex2, vertex1, wayToTarget);
        wayToTargetRoute.insert(wayToTargetRoute.begin(), routeElement);
        vertex1 = vertex2;
        break;
      }
    }
  }

  return wayToTargetRoute;
}

mantle_api::RouteDefinition WorldAnalyzer::ConvertToMantleRouteWayPoints(const Route& route) const
{
  mantle_api::RouteDefinition routeDefinition;
  auto wayToTargetRoute = SetWayToTargetRoute(route.target, route.roadGraph, route.root);
  for (auto& routeElement : wayToTargetRoute)
  {
    mantle_api::RouteWaypoint routeWayPoint;
    RoadPosition roadPos{};

    auto roadLength = world->GetRoadLength(routeElement.roadId);
    roadPos.s = routeElement.inOdDirection ? std::max(roadLength - units::length::meter_t(0.5), roadLength / 2.0)
                                           : std::min(units::length::meter_t(0.5), roadLength / 2.0);
    roadPos.t = routeElement.inOdDirection ? -units::length::meter_t(0.5) : units::length::meter_t(0.5);

    auto worldPosition = world->RoadCoord2WorldCoord(roadPos, routeElement.roadId);
    if (worldPosition.has_value())
    {
      routeWayPoint.waypoint = {(*worldPosition).xPos, (*worldPosition).yPos, units::length::meter_t(0.0)};
      routeDefinition.waypoints.push_back(routeWayPoint);
    }
    else
    {
      throw std::runtime_error("Calculation of route failed; Road Id or s coordinate is not valid");
    }
  }

  return routeDefinition;
}
