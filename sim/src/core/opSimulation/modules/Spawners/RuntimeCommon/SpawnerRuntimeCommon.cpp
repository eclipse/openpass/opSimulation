/********************************************************************************
 * Copyright (c) 2019-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SpawnerRuntimeCommon.h"

#include <MantleAPI/Common/bounding_box.h>
#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/route_definition.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Execution/i_environment.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/i_entity_repository.h>
#include <cmath>
#include <memory>
#include <optional>
#include <ostream>
#include <stdexcept>
#include <utility>

#include "RuntimeCommon/SpawnerRuntimeCommonDefinitions.h"
#include "SpawnerRuntimeCommonParameterExtractor.h"
#include "common/commonTools.h"
#include "common/globalDefinitions.h"
#include "common/sampler.h"
#include "include/agentBlueprintInterface.h"
#include "include/agentBlueprintProviderInterface.h"
#include "include/callbackInterface.h"
#include "include/entityRepositoryInterface.h"

SpawnerRuntimeCommon::SpawnerRuntimeCommon(const SpawnPointDependencies *dependencies,
                                           const CallbackInterface *const callbacks)
    : SpawnPointInterface(dependencies->world, callbacks),
      dependencies(*dependencies),
      worldAnalyzer(dependencies->world),
      parameters(SpawnPointRuntimeCommonParameterExtractor::ExtractSpawnPointParameters(
          *(dependencies->parameters.value()), worldAnalyzer, supportedLaneTypes, callbacks))
{
  for (const auto &spawnPosition : parameters.spawnPositions)
  {
    queuedSpawnDetails.push_back(GenerateSpawnDetailsForLane(spawnPosition, 0));
  }
}

void SpawnerRuntimeCommon::Trigger(int time)
{
  for (size_t i = 0; i < parameters.spawnPositions.size(); ++i)
  {
    if (time >= queuedSpawnDetails[i].spawnTime
        && AreSpawningCoordinatesValid(queuedSpawnDetails[i], parameters.spawnPositions[i]))
    {
      AdjustVelocityForCrash(queuedSpawnDetails[i], parameters.spawnPositions[i]);

      auto entityId = SpawnPointDefinitions::CreateSpawnReadyEntity(queuedSpawnDetails[i].entityInformation,
                                                                    dependencies.environment);

      auto &entityRepository{
          dynamic_cast<core::EntityRepositoryInterface &>(dependencies.environment->GetEntityRepository())};
      entityRepository.SpawnReadyAgents();

      queuedSpawnDetails[i] = GenerateSpawnDetailsForLane(parameters.spawnPositions[i], time);
      dependencies.environment->AssignRoute(entityId,
                                            queuedSpawnDetails[i].entityInformation.spawnParameter.routeDefinition);
    }
  }
}

SpawnDetails SpawnerRuntimeCommon::GenerateSpawnDetailsForLane(const SpawnPosition &sceneryInformation, int time)
{
  auto rightLaneCount = worldAnalyzer.GetRightLaneCount(
      sceneryInformation.roadId, sceneryInformation.laneId, sceneryInformation.sPosition);
  const auto agentProfile = SampleAgentProfile(rightLaneCount == 0);

  try
  {
    const auto vehicleModelName = dependencies.agentBlueprintProvider->SampleVehicleModelName(agentProfile.name);
    const auto vehicleProperties = helper::map::query(*dependencies.vehicles, vehicleModelName);
    THROWIFFALSE(vehicleProperties,
                 "Unkown VehicleModel \"" + vehicleModelName + "\" in AgentProfile \"" + agentProfile.name + "\"");

    EntityInformation entityInformation(agentProfile.name, "", vehicleProperties.value(), {});

    const auto tGap = Sampler::RollForStochasticAttribute(agentProfile.tGap, dependencies.stochastics);

    auto velocity = Sampler::RollForStochasticAttribute(agentProfile.velocity, dependencies.stochastics);

    for (size_t i = 0; i < rightLaneCount; ++i)
    {
      double homogeneity = agentProfile.homogeneities.size() > i ? agentProfile.homogeneities[i] : 1.0;
      velocity *= 2.0 - homogeneity;
    }

    CalculateSpawnParameter(entityInformation.spawnParameter,
                            sceneryInformation.roadId,
                            sceneryInformation.laneId,
                            sceneryInformation.sPosition,
                            units::velocity::meters_per_second_t(velocity));

    return SpawnDetails{static_cast<int>(tGap * 1000) + time, entityInformation};
  }
  catch (const std::runtime_error &error)
  {
    LogError("Encountered an Error while generating Spawn Details: " + std::string(error.what()));
  }
}

void SpawnerRuntimeCommon::AdjustVelocityForCrash(SpawnDetails &spawnDetails,
                                                  const SpawnPosition &sceneryInformation) const
{
  const units::length::meter_t agentFrontLength
      = spawnDetails.entityInformation.vehicleProperties->bounding_box.dimension.length * 0.5
      + spawnDetails.entityInformation.vehicleProperties->bounding_box.geometric_center.x;
  const units::length::meter_t agentRearLength
      = spawnDetails.entityInformation.vehicleProperties->bounding_box.dimension.length * 0.5
      - spawnDetails.entityInformation.vehicleProperties->bounding_box.geometric_center.x;
  const auto intendedVelocity = spawnDetails.entityInformation.spawnParameter.velocity;

  spawnDetails.entityInformation.spawnParameter.velocity
      = worldAnalyzer.CalculateSpawnVelocityToPreventCrashing(sceneryInformation.laneId,
                                                              sceneryInformation.sPosition,
                                                              agentFrontLength,
                                                              agentRearLength,
                                                              intendedVelocity,
                                                              spawnDetails.entityInformation.spawnParameter.route);
}

bool SpawnerRuntimeCommon::AreSpawningCoordinatesValid(const SpawnDetails &spawnDetails,
                                                       const SpawnPosition &sceneryInformation) const
{
  const auto vehicleModelParameters = spawnDetails.entityInformation.vehicleProperties;

  return worldAnalyzer.AreSpawningCoordinatesValid(sceneryInformation.roadId,
                                                   sceneryInformation.laneId,
                                                   sceneryInformation.sPosition,
                                                   0_m /* offset */,
                                                   units::length::meter_t(GetStochasticOrPredefinedValue(
                                                       parameters.minimumSeparationBuffer, dependencies.stochastics)),
                                                   spawnDetails.entityInformation.spawnParameter.route,
                                                   vehicleModelParameters);
}

void SpawnerRuntimeCommon::CalculateSpawnParameter(SpawnParameter &spawnParameter,
                                                   const RoadId &roadId,
                                                   const LaneId laneId,
                                                   const SPosition sPosition,
                                                   const units::velocity::meters_per_second_t velocity) const
{
  Position pos = GetWorld()->LaneCoord2WorldCoord(sPosition, 0_m /* offset */, roadId, laneId).value();

  auto spawnV = velocity;

  //considers adjusted velocity in curvatures
  auto kappa = pos.curvature.value();

  // Note: This could falsify ego and scenario agents
  if (kappa != 0.0)
  {
    double curvatureVelocity = 160 * (1 / (std::sqrt((std::abs(kappa)) / 1000)));

    spawnV = units::math::min(spawnV, units::velocity::meters_per_second_t(curvatureVelocity));
  }

  spawnParameter.position = {pos.xPos, pos.yPos, 0_m};
  spawnParameter.orientation = {pos.yawAngle + (laneId < 0 ? 0.0_rad : units::angle::radian_t(M_PI)), 0_rad, 0_rad};
  spawnParameter.velocity = units::velocity::meters_per_second_t(spawnV);
  spawnParameter.acceleration = 0_mps_sq;
  spawnParameter.route = worldAnalyzer.SampleRoute(roadId, laneId, dependencies.stochastics);
  spawnParameter.routeDefinition = worldAnalyzer.ConvertToMantleRouteWayPoints(spawnParameter.route);
}

SpawningAgentProfile SpawnerRuntimeCommon::SampleAgentProfile(bool rightLane)
{
  return Sampler::Sample(
      rightLane ? parameters.agentProfileLaneMaps.rightLanes : parameters.agentProfileLaneMaps.leftLanes,
      dependencies.stochastics);
}

void SpawnerRuntimeCommon::LogError(const std::string &message) const
{
  std::stringstream log;
  log.str(std::string());
  log << COMPONENTNAME << " " << message;
  LOG(CbkLogLevel::Error, log.str());
  throw std::runtime_error(log.str());
}
