/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  observationBinding.h
//! @brief This file contains the interface to the observation module
//!        libraries.
//-----------------------------------------------------------------------------

#pragma once

#include <map>
#include <memory>
#include <string>

#include "bindings/observationLibrary.h"
#include "common/callbacks.h"
#include "common/opExport.h"
#include "common/parameter.h"

class CallbackInterface;
class DataBufferReadInterface;
class StochasticsInterface;
class WorldInterface;
namespace openpass
{
namespace common
{
struct RuntimeInformation;
}  // namespace common
}  // namespace openpass

namespace core
{
class ObservationModule;

//! This class represents the interface to the observation module libraries
class SIMULATIONCOREEXPORT ObservationBinding
{
public:
  /**
   * @brief ObservationBinding constructor
   *
   * @param[in] runtimeInformation    Common runtimeInformation
   * @param[in] callbacks             Pointer to the callbacks
   */
  ObservationBinding(const openpass::common::RuntimeInformation& runtimeInformation, CallbackInterface* callbacks);
  ObservationBinding(const ObservationBinding&) = delete;

  /// Default move constructor
  ObservationBinding(ObservationBinding&&) = default;
  ObservationBinding& operator=(const ObservationBinding&) = delete;
  ObservationBinding& operator=(ObservationBinding&&) = delete;

  /**
   * Creates an observation module based on the observation instance in the run
   * config and the observation library stored in the mapping for the library
   * name of the observation instance
   *
   * @param[in]     libraryPath          Path of the library
   * @param[in]     parameter            Observation parameter
   * @param[in]     stochastics          The stochastics interface
   * @param[in]     world                The world interface
   * @param[in]     dataBuffer           Pointer to the data buffer that provides read-only access to the data
   * @return                             Observation module created from the
   *                                     observation instance
   */
  std::unique_ptr<ObservationModule> Instantiate(const std::string& libraryPath,
                                                 const openpass::parameter::ParameterSetLevel1& parameter,
                                                 StochasticsInterface* stochastics,
                                                 WorldInterface* world,
                                                 DataBufferReadInterface* dataBuffer);

private:
  std::unique_ptr<ObservationLibrary> library;
  const openpass::common::RuntimeInformation& runtimeInformation;
  CallbackInterface* callbacks{nullptr};
};

}  // namespace core
