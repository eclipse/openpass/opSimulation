/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  observationLibrary.h
//! @brief This file contains the internal representation of the library of an
//!        observation module.
//-----------------------------------------------------------------------------

#pragma once

#include <boost/dll.hpp>
#include <boost/function.hpp>
#include <list>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "common/parameter.h"
#include "framework/observationModule.h"

class CallbackInterface;
class DataBufferReadInterface;
class ObservationInterface;
class ParameterInterface;
class RunResultInterface;
class StochasticsInterface;
class WorldInterface;

namespace openpass
{
namespace common
{
struct RuntimeInformation;
}  // namespace common
}  // namespace openpass

namespace core
{

//! This class represents the library of an observation module
class ObservationLibrary
{
public:
  //! Type representing the signature of a dll-function to obtain the version information of the module
  using ObservationInterface_GetVersionType = const std::string&();
  //! Type representing the signature of a dll-function to create an instance of the module
  using ObservationInterface_CreateInstanceType = ObservationInterface*(StochasticsInterface* stochastics,
                                                                        WorldInterface* world,
                                                                        const ParameterInterface* parameters,
                                                                        const CallbackInterface* callbacks,
                                                                        DataBufferReadInterface* const dataBuffer);
  //! Type representing the signature of a dll-function to destroy/delete an instance of the module
  using ObservationInterface_DestroyInstanceType = void(ObservationInterface* implementation);

  //! Type representing the signature of a dll-function to manage prehook of the simulation
  using ObservationInterface_OpSimulationPreHook = bool(ObservationInterface* implementation);

  //! Type representing the signature of a dll-function to manage pre run hook of the simulation
  using ObservationInterface_OpSimulationPreRunHook = bool(ObservationInterface* implementation);

  //! Type representing the signature of a dll-function to update hook of the simulation
  using ObservationInterface_OpSimulationUpdateHook
      = bool(ObservationInterface* implementation, int time, RunResultInterface& runResult);

  //! Type representing the signature of a dll-function to manage post run hook of the simulation
  using ObservationInterface_OpSimulationPostRunHook
      = bool(ObservationInterface* implementation, const RunResultInterface& runResult);

  //! Type representing the signature of a dll-function to manage post hook of the simulation
  using ObservationInterface_OpSimulationPostHook = bool(ObservationInterface* implementation);

  /**
   * @brief ObservationLibrary constructor
   *
   * @param[in] libraryPath   Path to the library
   * @param[in] callbacks     Pointer to the callbacks
   */
  ObservationLibrary(std::string libraryPath, CallbackInterface* callbacks)
      : libraryPath(std::move(libraryPath)), callbacks(callbacks)
  {
  }
  ObservationLibrary(const ObservationLibrary&) = delete;
  ObservationLibrary(ObservationLibrary&&) = delete;
  ObservationLibrary& operator=(const ObservationLibrary&) = delete;
  ObservationLibrary& operator=(ObservationLibrary&&) = delete;
  virtual ~ObservationLibrary();

  /**
   * Inits the observation library by obtaining the function pointers to all
   * "Interface.h" functions defined via typedef.
   *
   * @return     Flag if init was successful
   */
  bool Init();

  /**
   * Releases an observation module instance by calling the destroyInstance method
   * on the instance and removing the instance from the list of observation modules.
   *
   * @param[in]   observationModule   Observation module to release
   * @return     Flag if release was successful
   */
  bool ReleaseObservationModule(ObservationModule* observationModule);

  /**
   * Creates an observation module based on the observation instance in the run
   * config and the observation library stored in the mapping for the library
   * name of the observation instance.
   *
   * @param[in]     runtimeInformation   Common runtimeInformation
   * @param[in]     parameters           Observation parameters
   * @param[in]     stochastics          The stochastics interface
   * @param[in]     world                The world interface
   * @param[in]     dataBuffer           Pointer to the data buffer that provides read-only access to the data
   * @return                             Observation module created from the
   *                                     observation instance
   */
  std::unique_ptr<ObservationModule> CreateObservationModule(
      const openpass::common::RuntimeInformation& runtimeInformation,
      const openpass::parameter::ParameterSetLevel1& parameters,
      StochasticsInterface* stochastics,
      WorldInterface* world,
      DataBufferReadInterface* const dataBuffer);

  /// @brief Simulation prehook (once at simulation start) for the observation interface
  /// @param implementation Pointer to the observation interface
  /// @return True, when prehook is successfull
  bool SimulationPreHook(ObservationInterface* implementation) { return simulationPreHookFunc(implementation); }

  /// @brief Simulation prerunhook (once before each run) for the observation interface
  /// @param implementation Pointer to the observation interface
  /// @return True, when prehook is successfull
  bool SimulationPreRunHook(ObservationInterface* implementation) { return simulationPreRunHookFunc(implementation); }

  /// @brief Simulation updatehook (after each timestep) for the observation interface
  /// @param implementation Pointer to the observation interface
  /// @param time           Current time
  /// @param runResult      RunResult to write
  /// @return True, when updatehook is successfull
  bool SimulationUpdateHook(ObservationInterface* implementation, int time, RunResultInterface& runResult)
  {
    return simulationUpdateHookFunc(implementation, time, runResult);
  }

  /// @brief Simulation postrunhook (once after each run) for the observation interface
  /// @param implementation Pointer to the observation interface
  /// @param runResult      Result of the run
  /// @return True, when postrunhook is successfull
  bool SimulationPostRunHook(ObservationInterface* implementation, const RunResultInterface& runResult)
  {
    return simulationPostRunHookFunc(implementation, runResult);
  }

  /// @brief Simulation posthook (once at simulation end) for the observation interface
  /// @param implementation Pointer to the observation interface
  /// @return True, when posthook is successfull
  bool SimulationPostHook(ObservationInterface* implementation) { return simulationPostHookFunc(implementation); }

private:
  const std::string DllGetVersionId = "OpenPASS_GetVersion";
  const std::string DllCreateInstanceId = "OpenPASS_CreateInstance";
  const std::string DllDestroyInstanceId = "OpenPASS_DestroyInstance";
  const std::string DllOpSimulationPreHookId = "OpenPASS_OpSimulationPreHook";
  const std::string DllOpSimulationPreRunHookId = "OpenPASS_OpSimulationPreRunHook";
  const std::string DllOpSimulationUpdateHookId = "OpenPASS_OpSimulationUpdateHook";
  const std::string DllOpSimulationPostRunHookId = "OpenPASS_OpSimulationPostRunHook";
  const std::string DllOpSimulationPostHookId = "OpenPASS_OpSimulationPostHook";

  const std::string libraryPath;

  /// @brief List of observation modules generated from this library
  ///
  /// This list is used to check if modules belong to the library
  /// when they should be released. The responsibility for all modules
  /// is handed over to the ObservationNetwork
  std::vector<const ObservationModule*> observationModules;
  boost::dll::shared_library library;
  CallbackInterface* callbacks;

  boost::function<ObservationInterface_GetVersionType> getVersionFunc;
  boost::function<ObservationInterface_CreateInstanceType> createInstanceFunc;
  boost::function<ObservationInterface_DestroyInstanceType> destroyInstanceFunc;
  boost::function<ObservationInterface_OpSimulationPreHook> simulationPreHookFunc;
  boost::function<ObservationInterface_OpSimulationPreRunHook> simulationPreRunHookFunc;
  boost::function<ObservationInterface_OpSimulationUpdateHook> simulationUpdateHookFunc;
  boost::function<ObservationInterface_OpSimulationPostRunHook> simulationPostRunHookFunc;
  boost::function<ObservationInterface_OpSimulationPostHook> simulationPostHookFunc;
};

}  // namespace core
