/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  stochasticsLibrary.h
//! @brief This file contains the internal representation of the library of a
//!        stochasticsInterface.
//-----------------------------------------------------------------------------

#pragma once

#include <boost/dll.hpp>
#include <boost/function.hpp>
#include <string>
#include <utility>

class CallbackInterface;
class StochasticsInterface;

namespace core
{

//! This class represents the library of a stochasticsInterface
class StochasticsLibrary
{
public:
  //! Type representing the signature of a dll-function for obtaining the version information of the module
  using StochasticsInterface_GetVersionType = const std::string &();
  //! Type representing the signature of a dll-function for creating an instance of the module
  using StochasticsInterface_CreateInstanceType = StochasticsInterface *(const CallbackInterface *callbacks);
  //! Type representing the signature of a dll-function for destroying an instance of the module
  using StochasticsInterface_DestroyInstanceType = void(StochasticsInterface *implementation);

  /**
   * @brief StochasticsLibrary constructor
   *
   * @param[in] libraryPath   Path to the library
   * @param[in] callbacks     Pointer to the callbacks
   */
  StochasticsLibrary(std::string libraryPath, CallbackInterface *callbacks)
      : libraryPath(std::move(libraryPath)), callbacks(callbacks)
  {
  }
  StochasticsLibrary(const StochasticsLibrary &) = delete;
  StochasticsLibrary(StochasticsLibrary &&) = delete;
  StochasticsLibrary &operator=(const StochasticsLibrary &) = delete;
  StochasticsLibrary &operator=(StochasticsLibrary &&) = delete;

  //-----------------------------------------------------------------------------
  //! Destructor, deletes the stored library (unloads it if necessary)
  //-----------------------------------------------------------------------------
  virtual ~StochasticsLibrary();

  //-----------------------------------------------------------------------------
  //! Creates a boost::dll::shared_library based on the path from the constructor
  //! and stores boost::function for getting the library version, creating and
  //! destroying instances and setting the stochasticsInterface item
  //! (see typedefs for corresponding signatures).
  //!
  //! @return                 Null pointer
  //-----------------------------------------------------------------------------
  bool Init();

  //-----------------------------------------------------------------------------
  //! Delete the stochasticsInterface and the library
  //!
  //! @return                         Flag if the release was successful
  //-----------------------------------------------------------------------------
  bool ReleaseStochastics();

  //-----------------------------------------------------------------------------
  //! Make sure that the library exists and is loaded, then call the "create instance"
  //! function pointer using the parameters from the stochastics instance to get
  //! a stochastics interface, which is then used to instantiate a stochasticsInterface
  //! which is stored.
  //!
  //! @return                         stochasticsInterface created
  //-----------------------------------------------------------------------------
  StochasticsInterface *CreateStochastics();

private:
  const std::string DllGetVersionId = "OpenPASS_GetVersion";
  const std::string DllCreateInstanceId = "OpenPASS_CreateInstance";
  const std::string DllDestroyInstanceId = "OpenPASS_DestroyInstance";

  std::string libraryPath;
  StochasticsInterface *stochasticsInterface = nullptr;
  boost::dll::shared_library library;
  CallbackInterface *callbacks;
  boost::function<StochasticsInterface_GetVersionType> getVersionFunc;
  boost::function<StochasticsInterface_CreateInstanceType> createInstanceFunc;
  boost::function<StochasticsInterface_DestroyInstanceType> destroyInstanceFunc;
};

}  // namespace core
