/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "observationLibrary.h"

#include <algorithm>
#include <iostream>
#include <memory>
#include <new>
#include <sstream>
#include <stdexcept>
#include <string>

#include "common/log.h"
#include "framework/parameterbuilder.h"
#include "include/parameterInterface.h"
#include "modelElements/parameters.h"

class DataBufferReadInterface;
class ObservationInterface;
class StochasticsInterface;
class WorldInterface;

namespace openpass::common
{
struct RuntimeInformation;
}  // namespace openpass::common

namespace core
{

bool ObservationLibrary::Init()
{
  std::string suffix = DEBUG_POSTFIX;
  library = boost::dll::shared_library(
      libraryPath + suffix, boost::dll::load_mode::append_decorations | boost::dll::load_mode::search_system_folders);

  if (!library.is_loaded())
  {
    LOG_INTERN(LogLevel::Error) << "Failed to load library: " << libraryPath + suffix;
    return false;
  }

  getVersionFunc = library.get<ObservationInterface_GetVersionType>(DllGetVersionId);
  if (!getVersionFunc)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to resolve the symbol " << DllGetVersionId << " within the DLL located at "
                                << libraryPath + suffix;
    return false;
  }

  createInstanceFunc = library.get<ObservationInterface_CreateInstanceType>(DllCreateInstanceId);
  if (!createInstanceFunc)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to resolve the symbol " << DllCreateInstanceId
                                << " within the DLL located at " << libraryPath + suffix;
    return false;
  }

  destroyInstanceFunc = library.get<ObservationInterface_DestroyInstanceType>(DllDestroyInstanceId);
  if (!destroyInstanceFunc)
  {
    LOG_INTERN(LogLevel::Warning) << "Unable to resolve the symbol " << DllDestroyInstanceId
                                  << " within the DLL located at " << libraryPath + suffix;
    return false;
  }

  simulationPreHookFunc = library.get<ObservationInterface_OpSimulationPreHook>(DllOpSimulationPreHookId);
  if (!simulationPreHookFunc)
  {
    LOG_INTERN(LogLevel::Warning) << "Unable to resolve the symbol " << DllOpSimulationPreHookId
                                  << " within the DLL located at " << libraryPath + suffix;
    return false;
  }

  simulationPreRunHookFunc = library.get<ObservationInterface_OpSimulationPreRunHook>(DllOpSimulationPreRunHookId);
  if (!simulationPreRunHookFunc)
  {
    LOG_INTERN(LogLevel::Warning) << "Unable to resolve the symbol " << DllOpSimulationPreRunHookId
                                  << " within the DLL located at " << libraryPath + suffix;
    return false;
  }

  simulationUpdateHookFunc = library.get<ObservationInterface_OpSimulationUpdateHook>(DllOpSimulationUpdateHookId);
  if (!simulationPreRunHookFunc)
  {
    LOG_INTERN(LogLevel::Warning) << "Unable to resolve the symbol " << DllOpSimulationUpdateHookId
                                  << " within the DLL located at " << libraryPath + suffix;
    return false;
  }

  simulationPostRunHookFunc = library.get<ObservationInterface_OpSimulationPostRunHook>(DllOpSimulationPostRunHookId);
  if (!simulationPostRunHookFunc)
  {
    LOG_INTERN(LogLevel::Warning) << "Unable to resolve the symbol " << DllOpSimulationPostRunHookId
                                  << " within the DLL located at " << libraryPath + suffix;
    return false;
  }

  simulationPostHookFunc = library.get<ObservationInterface_OpSimulationPostHook>(DllOpSimulationPostHookId);
  if (!simulationPostHookFunc)
  {
    LOG_INTERN(LogLevel::Warning) << "Unable to resolve the symbol " << DllOpSimulationPostHookId
                                  << " within the DLL located at " << libraryPath + suffix;
    return false;
  }

  try
  {
    LOG_INTERN(LogLevel::DebugCore) << "Loaded observation library " << library.location().filename().string()
                                    << ", version " << getVersionFunc();
  }
  catch (std::runtime_error const& ex)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to retrieve version information from the DLL: " << ex.what();
    return false;
  }
  catch (...)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to retrieve version information from the DLL";
    return false;
  }

  return true;
}

ObservationLibrary::~ObservationLibrary()
{
  if (!(observationModules.empty()))
  {
    LOG_INTERN(LogLevel::Warning) << "unloading library which is still in use";
  }

  if (library.is_loaded())
  {
    LOG_INTERN(LogLevel::DebugCore) << "unloading observation library";
    library.unload();
  }
}

bool ObservationLibrary::ReleaseObservationModule(ObservationModule* observationModule)
{
  if (!library)
  {
    return false;
  }

  auto findIter = std::find(observationModules.begin(), observationModules.end(), observationModule);
  if (observationModules.end() == findIter)
  {
    LOG_INTERN(LogLevel::Warning) << "observation module doesn't belong to library";
    return false;
  }

  try
  {
    destroyInstanceFunc(observationModule->GetImplementation());
  }
  catch (std::runtime_error const& ex)
  {
    LOG_INTERN(LogLevel::Error) << "observation could not be released: " << ex.what();
    return false;
  }
  catch (...)
  {
    LOG_INTERN(LogLevel::Error) << "observation could not be released";
    return false;
  }

  observationModules.erase(findIter);
  return true;
}

std::unique_ptr<ObservationModule> ObservationLibrary::CreateObservationModule(
    const openpass::common::RuntimeInformation& runtimeInformation,
    const openpass::parameter::ParameterSetLevel1& parameters,
    StochasticsInterface* stochastics,
    WorldInterface* world,
    DataBufferReadInterface* const dataBuffer)
{
  if (!library.is_loaded())
  {
    return nullptr;
  }

  auto moduleParameters = openpass::parameter::make<SimulationCommon::Parameters>(runtimeInformation, parameters);

  ObservationInterface* observationInterface = nullptr;
  try
  {
    observationInterface = createInstanceFunc(stochastics, world, moduleParameters.get(), callbacks, dataBuffer);
  }
  catch (std::runtime_error const& ex)
  {
    LOG_INTERN(LogLevel::Error) << "could not create observation instance: " << ex.what();
    return nullptr;
  }
  catch (...)
  {
    LOG_INTERN(LogLevel::Error) << "could not create observation instance";
    return nullptr;
  }

  if (!observationInterface)
  {
    return nullptr;
  }

  auto observationModule = std::make_unique<ObservationModule>(observationInterface, std::move(moduleParameters), this);
  observationModules.emplace_back(observationModule.get());

  return std::move(observationModule);
}

}  // namespace core
