/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  modelLibrary.h
//! @brief This file contains the internal representation of the library of a
//!        model.
//-----------------------------------------------------------------------------

#pragma once

#include <boost/dll.hpp>
#include <boost/function.hpp>
#include <memory>
#include <string>
#include <utility>
#include <vector>

class PublisherInterface;
class AgentInterface;
class CallbackInterface;
class ModelInterface;
class ParameterInterface;
class ScenarioControlInterface;
class SignalInterface;
class StochasticsInterface;
class WorldInterface;
namespace openpass
{
namespace common
{
struct RuntimeInformation;
}  // namespace common
}  // namespace openpass

namespace core
{

class Agent;
class ComponentType;
class ComponentInterface;
class ObservationNetworkInterface;

//! This class represents the library of a model
class ModelLibrary
{
public:
  //! Type representing the signature of a dll-function for obtaining the version information of the module
  using ModelInterface_GetVersionType = const std::string &();
  //! Type representing the signature of a dll-function for creating an instance of the module
  using ModelInterface_CreateInstanceType = ModelInterface *(std::string componentName,
                                                             bool isInit,
                                                             int priority,
                                                             int offsetTime,
                                                             int responseTime,
                                                             int cycleTime,
                                                             StochasticsInterface *stochastics,
                                                             WorldInterface *world,
                                                             const ParameterInterface *parameters,
                                                             PublisherInterface *const publisher,
                                                             AgentInterface *agent,
                                                             const CallbackInterface *callbacks);
  //! Type of function pointer that points to dll-function that creates an instance of the module
  using UnrestrictedControllStrategyModelInterface_CreateInstanceType
      = ModelInterface *(*)(std::string componentName,
                            bool isInit,
                            int priority,
                            int offsetTime,
                            int responseTime,
                            int cycleTime,
                            StochasticsInterface *stochastics,
                            WorldInterface *world,
                            const ParameterInterface *parameters,
                            PublisherInterface *const publisher,
                            AgentInterface *agent,
                            const CallbackInterface *callbacks,
                            std::shared_ptr<ScenarioControlInterface> const scenarioControl);

  //! Type representing the signature of a dll-function for destroying an instance of the module
  using ModelInterface_DestroyInstanceType = void(ModelInterface *implementation);

  //! Type representing the signature of a dll-function for updating input type of the module
  using ModelInterface_UpdateInputType = bool(ModelInterface *implementation,
                                              int localLinkId,
                                              const std::shared_ptr<SignalInterface const> &data,
                                              int time);
  //! Type representing the signature of a dll-function for updating output type of the module
  using ModelInterface_UpdateOutputType
      = bool(ModelInterface *implementation, int localLinkId, std::shared_ptr<SignalInterface const> &data, int time);
  //! Type representing the signature of a dll-function for triggering the type of an instance of the module
  using ModelInterface_TriggerType = bool(ModelInterface *implementation, int time);

  /**
   * @brief ModelLibrary constructor
   *
   * @param[in] modelLibraryPath  Path to the model library
   * @param[in] modelLibraryName  Name of the library to load
   * @param[in] callbacks         Pointer to the callbacks
   */
  ModelLibrary(std::string modelLibraryPath, std::string modelLibraryName, CallbackInterface *callbacks)
      : modelLibraryPath(std::move(modelLibraryPath)),
        modelLibraryName(std::move(modelLibraryName)),
        callbacks(callbacks)
  {
  }
  ModelLibrary(const ModelLibrary &) = delete;
  ModelLibrary(ModelLibrary &&) = delete;
  ModelLibrary &operator=(const ModelLibrary &) = delete;
  ModelLibrary &operator=(ModelLibrary &&) = delete;
  virtual ~ModelLibrary();

  //-----------------------------------------------------------------------------
  //! Inits the model library by obtaining the function pointers to all
  //! "Interface.h" functions defined via typedef.
  //!
  //! @return     Flag if init was successful
  //-----------------------------------------------------------------------------
  bool Init();

  //-----------------------------------------------------------------------------
  //! Releases a component instance by calling the destroyInstance method
  //! on the instance and removing the instance from the list of components.
  //!
  //! @param  component   Component to release
  //! @return Flag if release was successful
  //-----------------------------------------------------------------------------
  bool ReleaseComponent(ComponentInterface *component);

  //-----------------------------------------------------------------------------
  //! @brief Creates a new agent component and its implementation and link all
  //!     observation modules in the observation network to it.
  //!
  //! Checks that the library exists and is loaded, then creates a new component
  //! using the agent and world parameter, set its model library to this object and
  //! link all observation modules in the observation networks to it. Finally,
  //! create a model interface using the "create instance" function pointer, set
  //! the component's implementation to this model interface and store the created
  //! component in the list of stored components and return it.
  //!
  //! @param[in]     componentType        Agent as defined in the agent config file
  //! @param[in]     componentName        Name of the component
  //! @param[in]     runtimeInformation   Static information from the simulation for the components
  //! @param[in]     stochastics          The stochastics interface
  //! @param[in]     world                The world interface
  //! @param[in]     observationNetwork   Network of the observation modules
  //! @param[in]     agent                Agent instance
  //! @param[in]     scenarioControl      scenarioControl of the entity
  //! @param[in]     publisher            Publisher instance
  //! @return
  //-----------------------------------------------------------------------------
  ComponentInterface *CreateComponent(const std::shared_ptr<ComponentType> &componentType,
                                      const std::string &componentName,
                                      const openpass::common::RuntimeInformation &runtimeInformation,
                                      StochasticsInterface *stochastics,
                                      WorldInterface *world,
                                      ObservationNetworkInterface *observationNetwork,
                                      Agent *agent,
                                      std::shared_ptr<ScenarioControlInterface> const &scenarioControl,
                                      PublisherInterface *publisher);

  //-----------------------------------------------------------------------------
  //! Calls the "update input" function pointer with the provided parameters and
  //! returns the result of this call.
  //!
  //! @param[in]     implementation       Model interface
  //! @param[in]     localLinkId          ID of the local link from the agent
  //!                                     configuration
  //! @param[in]     data                 Pointer to the signal interface
  //! @param[in]     time                 Current virtual simulation time
  //! @return                             Flag if update was successful?
  //-----------------------------------------------------------------------------
  bool UpdateInput(ModelInterface *implementation,
                   int localLinkId,
                   const std::shared_ptr<SignalInterface const> &data,
                   int time)
  {
    return updateInputFunc(implementation, localLinkId, data, time);
  }

  //-----------------------------------------------------------------------------
  //! Calls the "update output" function pointer with the provided parameters and
  //! returns the result of this call.
  //!
  //! @param[in]     implementation       Model interface
  //! @param[in]     localLinkId          ID of the local link from the agent
  //!                                     configuration
  //! @param[out]    data                 Pointer to the data as signal interface
  //! @param[in]     time                 Current virtual simulation time
  //! @return                             Flag if update was successful?
  //-----------------------------------------------------------------------------
  bool UpdateOutput(ModelInterface *implementation,
                    int localLinkId,
                    std::shared_ptr<SignalInterface const> &data,
                    int time)
  {
    return updateOutputFunc(implementation, localLinkId, data, time);
  }

  //-----------------------------------------------------------------------------
  //! Calls the "trigger" function pointer with the provided parameters and
  //! returns the result of this call.
  //!
  //! @param[in]     implementation       Model interface
  //! @param[in]     time                 Timepoint at which the trigger happens?
  //! @return                             Flag if triggering was successful?
  //-----------------------------------------------------------------------------
  bool Trigger(ModelInterface *implementation, int time) { return triggerFunc(implementation, time); }

private:
  const std::string DllGetVersionId = "OpenPASS_GetVersion";
  const std::string DllCreateInstanceId = "OpenPASS_CreateInstance";
  const std::string DllDestroyInstanceId = "OpenPASS_DestroyInstance";
  const std::string DllUpdateInputId = "OpenPASS_UpdateInput";
  const std::string DllUpdateOutputId = "OpenPASS_UpdateOutput";
  const std::string DllTriggerId = "OpenPASS_Trigger";

  std::string modelLibraryPath;
  std::string modelLibraryName;
  std::vector<ComponentInterface *> components;
  CallbackInterface *callbacks;
  boost::dll::shared_library library;
  boost::function<ModelInterface_GetVersionType> getVersionFunc;
  ModelInterface_CreateInstanceType *createInstanceFunc{
      nullptr};  // boost::function is limited to accept a maximum of 10 arguments
  boost::function<ModelInterface_DestroyInstanceType> destroyInstanceFunc;
  boost::function<ModelInterface_UpdateInputType> updateInputFunc;
  boost::function<ModelInterface_UpdateOutputType> updateOutputFunc;
  boost::function<ModelInterface_TriggerType> triggerFunc;
};

}  // namespace core
