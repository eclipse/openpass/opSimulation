/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "dataBufferBinding.h"

#include <new>
#include <utility>

#include "bindings/dataBufferLibrary.h"

class DataBufferInterface;

namespace openpass::common
{
struct RuntimeInformation;
}  // namespace openpass::common

namespace core
{

DataBufferBinding::DataBufferBinding(std::string libraryPath,
                                     const openpass::common::RuntimeInformation& runtimeInformation,
                                     CallbackInterface* callbacks)
    : libraryPath{std::move(libraryPath)}, callbacks{callbacks}, runtimeInformation{runtimeInformation}
{
}

DataBufferBinding::~DataBufferBinding()
{
  if (library)
  {
    library->ReleaseDataBuffer();
  }
}

DataBufferInterface* DataBufferBinding::Instantiate()
{
  if (!library)
  {
    // create new dataBuffer library
    library = std::make_unique<DataBufferLibrary>(libraryPath, callbacks);
  }

  // check if initialization is successful
  if (library->Init())
  {
    return library->CreateDataBuffer(runtimeInformation);
  }

  // dataBuffer library initialization failed
  return nullptr;
}

}  // namespace core
