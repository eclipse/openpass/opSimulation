/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2016-2018 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "stochasticsLibrary.h"

#include <ostream>
#include <stdexcept>
#include <string>

#include "common/log.h"

class StochasticsInterface;

namespace core
{

bool StochasticsLibrary::Init()
{
  std::string suffix = DEBUG_POSTFIX;
  library = boost::dll::shared_library(
      libraryPath + suffix, boost::dll::load_mode::append_decorations | boost::dll::load_mode::search_system_folders);

  if (!library.is_loaded())
  {
    LOG_INTERN(LogLevel::Error) << "Failed to load library: " << libraryPath + suffix;
    return false;
  }

  getVersionFunc = library.get<StochasticsInterface_GetVersionType>(DllGetVersionId);
  if (!getVersionFunc)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to resolve the symbol " << DllGetVersionId << " within the DLL located at "
                                << libraryPath + suffix;
    return false;
  }

  createInstanceFunc = library.get<StochasticsInterface_CreateInstanceType>(DllCreateInstanceId);
  if (!createInstanceFunc)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to resolve the symbol " << DllCreateInstanceId
                                << " within the DLL located at " << libraryPath + suffix;
    return false;
  }

  destroyInstanceFunc = library.get<StochasticsInterface_DestroyInstanceType>(DllDestroyInstanceId);
  if (!destroyInstanceFunc)
  {
    LOG_INTERN(LogLevel::Warning) << "Unable to resolve the symbol " << DllDestroyInstanceId
                                  << " within the DLL located at " << libraryPath + suffix;
    return false;
  }

  try
  {
    LOG_INTERN(LogLevel::DebugCore) << "Loaded stochastics library " << library.location().filename().string()
                                    << ", version " << getVersionFunc();
  }
  catch (std::runtime_error const &ex)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to retrieve version information from the DLL: " << ex.what();
    return false;
  }
  catch (...)
  {
    LOG_INTERN(LogLevel::Error) << "Unable to retrieve version information from the DLL";
    return false;
  }

  return true;
}

StochasticsLibrary::~StochasticsLibrary()
{
  if (stochasticsInterface)
  {
    LOG_INTERN(LogLevel::Warning) << "unloading library which is still in use";
  }

  if (library)
  {
    if (library.is_loaded())
    {
      LOG_INTERN(LogLevel::DebugCore) << "unloading library " << libraryPath;
      library.unload();
    }
  }
}

bool StochasticsLibrary::ReleaseStochastics()
{
  if (!stochasticsInterface)
  {
    return true;
  }

  if (!library)
  {
    return false;
  }

  try
  {
    destroyInstanceFunc(stochasticsInterface);
  }
  catch (std::runtime_error const &ex)
  {
    LOG_INTERN(LogLevel::Error) << "stochastics could not be released: " << ex.what();
    return false;
  }
  catch (...)
  {
    LOG_INTERN(LogLevel::Error) << "stochastics could not be released";
    return false;
  }

  stochasticsInterface = nullptr;

  return true;
}

StochasticsInterface *StochasticsLibrary::CreateStochastics()
{
  if (!library.is_loaded())
  {
    return nullptr;
  }

  stochasticsInterface = nullptr;
  try
  {
    stochasticsInterface = createInstanceFunc(callbacks);
  }
  catch (std::runtime_error const &ex)
  {
    LOG_INTERN(LogLevel::Error) << "could not create stochastics instance: " << ex.what();
    return nullptr;
  }
  catch (...)
  {
    LOG_INTERN(LogLevel::Error) << "could not create stochastics instance";
    return nullptr;
  }

  if (!stochasticsInterface)
  {
    return nullptr;
  }

  return stochasticsInterface;
}

}  // namespace core
