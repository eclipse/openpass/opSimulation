/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  worldLibrary.h
//! @brief This file contains the internal representation of the library of a
//!        worldInterface.
//-----------------------------------------------------------------------------

#pragma once

#include <boost/dll.hpp>
#include <boost/function.hpp>
#include <string>
#include <utility>

class CallbackInterface;
class DataBufferWriteInterface;
class IdManagerInterface;
class StochasticsInterface;
class WorldInterface;

namespace core
{

//! This class represents the library of a worldInterface
class WorldLibrary
{
public:
  //! Type representing the signature of a dll-function for obtaining the version information of the module
  using WorldInterface_GetVersionType = const std::string &();
  //! Type representing the signature of a dll-function for creating an instance of the module
  using WorldInterface_CreateInstanceType = WorldInterface *(const CallbackInterface *callbacks,
                                                             IdManagerInterface *idManager,
                                                             StochasticsInterface *stochastics,
                                                             DataBufferWriteInterface *dataBuffer);
  //! Type representing the signature of a dll-function for destroying an instance of the module
  using WorldInterface_DestroyInstanceType = void(WorldInterface *implementation);

  /**
   * @brief WorldLibrary constructor
   *
   * @param[in] worldLibraryPath  Path to the library
   * @param[in] callbacks         Pointer to the callbacks
   * @param[in] idManager         Pointer to the IdManager
   * @param[in] stochastics       Pointer to the stochastic
   * @param[in] dataBuffer        Pointer to the data buffer that provides write-only access to the data
   */
  WorldLibrary(std::string worldLibraryPath,
               CallbackInterface *callbacks,
               IdManagerInterface *idManager,
               StochasticsInterface *stochastics,
               DataBufferWriteInterface *dataBuffer)
      : worldLibraryPath(std::move(worldLibraryPath)),
        callbacks(callbacks),
        idManager(idManager),
        stochastics(stochastics),
        dataBuffer(dataBuffer)
  {
  }

  WorldLibrary(const WorldLibrary &) = delete;
  WorldLibrary(WorldLibrary &&) = delete;
  WorldLibrary &operator=(const WorldLibrary &) = delete;
  WorldLibrary &operator=(WorldLibrary &&) = delete;

  //-----------------------------------------------------------------------------
  //! Destructor, deletes the stored library (unloads it if necessary)
  //-----------------------------------------------------------------------------
  virtual ~WorldLibrary();

  //-----------------------------------------------------------------------------
  //! Creates a boost::dll::shared_library based on the path from the constructor
  //! and stores boost::function for getting the library version, creating and
  //! destroying instances and setting the stochasticsInterface item
  //! (see typedefs for corresponding signatures).
  //!
  //! @return                 Null pointer
  //-----------------------------------------------------------------------------
  bool Init();

  //-----------------------------------------------------------------------------
  //! Delete the worldInterface and the library
  //!
  //! @return                         Flag if the release was successful
  //-----------------------------------------------------------------------------
  bool ReleaseWorld();

  //-----------------------------------------------------------------------------
  //! Make sure that the library exists and is loaded, then call the "create instance"
  //! function pointer using the parameters from the stochastics instance to get
  //! a stochastics interface, which is then used to instantiate a stochasticsInterface
  //! which is stored.
  //!
  //! @return                         stochasticsInterface created
  //-----------------------------------------------------------------------------
  WorldInterface *CreateWorld();

private:
  const std::string DllGetVersionId = "OpenPASS_GetVersion";
  const std::string DllCreateInstanceId = "OpenPASS_CreateInstance";
  const std::string DllDestroyInstanceId = "OpenPASS_DestroyInstance";

  const std::string worldLibraryPath;
  WorldInterface *worldInterface = nullptr;
  boost::dll::shared_library library;
  CallbackInterface *callbacks;
  IdManagerInterface *idManager;
  StochasticsInterface *stochastics;
  DataBufferWriteInterface *dataBuffer;
  boost::function<WorldInterface_GetVersionType> getVersionFunc;
  boost::function<WorldInterface_CreateInstanceType> createInstanceFunc;
  boost::function<WorldInterface_DestroyInstanceType> destroyInstanceFunc;
};

}  // namespace core
