/********************************************************************************
 * Copyright (c) 2017-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  world.h
//-----------------------------------------------------------------------------

#pragma once

#include "bindings/worldBinding.h"
#include "include/worldInterface.h"

namespace core
{

//! @brief This file includes the static and dynamic objects used during a
//!        simulation run.
class World : public WorldInterface
{
public:
  /**
   * @brief WorldBinding constructor
   *
   * @param[in] worldBinding  Pointer to the world binding
   */
  World(WorldBinding* worldBinding) : worldBinding(worldBinding) {}

  World(const World&) = delete;
  World(World&&) = delete;
  World& operator=(const World&) = delete;
  World& operator=(World&&) = delete;

  ~World() = default;

  AgentInterface* GetAgent(int id) const override { return implementation->GetAgent(id); }

  AgentInterface* GetAgentByName(const std::string& scenarioName) override
  {
    return implementation->GetAgentByName(scenarioName);
  }

  const std::vector<const WorldObjectInterface*>& GetWorldObjects() const override
  {
    return implementation->GetWorldObjects();
  }

  std::map<int, AgentInterface*> GetAgents() override { return implementation->GetAgents(); }

  const std::vector<const TrafficObjectInterface*>& GetTrafficObjects() const override
  {
    return implementation->GetTrafficObjects();
  }

  const std::vector<int> GetRemovedAgentsInPreviousTimestep() override
  {
    return implementation->GetRemovedAgentsInPreviousTimestep();
  }

  // framework internal methods to access members without restrictions
  void ExtractParameter(ParameterInterface* parameters) override
  {
    return implementation->ExtractParameter(parameters);
  }

  void ClearAgents() override { return implementation->ClearAgents(); }

  std::string GetTimeOfDay() const override { return implementation->GetTimeOfDay(); }

  units::length::meter_t GetVisibilityDistance() const override { return implementation->GetVisibilityDistance(); }

  const TrafficRules& GetTrafficRules() const override { return implementation->GetTrafficRules(); }

  double GetFriction() const override { return implementation->GetFriction(); }

  void* GetOsiGroundTruth() const override { return implementation->GetOsiGroundTruth(); }

  void* GetWorldData() override { return implementation->GetWorldData(); }

  void QueueAgentUpdate(std::function<void()> func) override { return implementation->QueueAgentUpdate(func); }

  void QueueAgentRemove(const AgentInterface* agent) override { return implementation->QueueAgentRemove(agent); }

  void RemoveAgents() override { implementation->RemoveAgents(); }

  void PublishGlobalData() override { return implementation->PublishGlobalData(); }

  void SyncGlobalData() override { return implementation->SyncGlobalData(); }

  bool CreateScenery(const SceneryInterface* scenery, const TurningRates& turningRates) override
  {
    return implementation->CreateScenery(scenery, turningRates);
  }

  void SetWeather(const mantle_api::Weather& weather) override { return implementation->SetWeather(weather); }

  void SetTrafficSignalState(const std::string& traffic_signal_name, const std::string& traffic_signal_state) override
  {
    return implementation->SetTrafficSignalState(traffic_signal_name, traffic_signal_state);
  }

  AgentInterface* CreateAgentAdapterForAgent() override { return implementation->CreateAgentAdapterForAgent(); }

  AgentInterface& CreateAgentAdapter(mantle_api::UniqueId id, const AgentBuildInstructions& agentBlueprint) override
  {
    return implementation->CreateAgentAdapter(id, agentBlueprint);
  }

  AgentInterface* GetEgoAgent() override { return implementation->GetEgoAgent(); }

  RouteQueryResult<std::optional<GlobalRoadPosition>> ResolveRelativePoint(
      const RoadGraph& roadGraph,
      RoadGraphVertex startNode,
      ObjectPointRelative relativePoint,
      const WorldObjectInterface& object) const override
  {
    return implementation->ResolveRelativePoint(roadGraph, startNode, relativePoint, object);
  }

  RouteQueryResult<AgentInterfaces> GetAgentsInRange(const RoadGraph& roadGraph,
                                                     RoadGraphVertex startNode,
                                                     int laneId,
                                                     units::length::meter_t startDistance,
                                                     units::length::meter_t backwardRange,
                                                     units::length::meter_t forwardRange) const override
  {
    return implementation->GetAgentsInRange(roadGraph, startNode, laneId, startDistance, backwardRange, forwardRange);
  }

  RouteQueryResult<std::vector<const WorldObjectInterface*>> GetObjectsInRange(
      const RoadGraph& roadGraph,
      RoadGraphVertex startNode,
      int laneId,
      units::length::meter_t startDistance,
      units::length::meter_t backwardRange,
      units::length::meter_t forwardRange) const override
  {
    return implementation->GetObjectsInRange(roadGraph, startNode, laneId, startDistance, backwardRange, forwardRange);
  }

  AgentInterfaces GetAgentsInRangeOfJunctionConnection(std::string connectingRoadId,
                                                       units::length::meter_t range) const override
  {
    return implementation->GetAgentsInRangeOfJunctionConnection(connectingRoadId, range);
  }

  units::length::meter_t GetDistanceToConnectorEntrance(
      /*const ObjectPosition position,*/ std::string intersectingConnectorId,
      int intersectingLaneId,
      std::string ownConnectorId) const override
  {
    return implementation->GetDistanceToConnectorEntrance(
        /*position,*/ intersectingConnectorId, intersectingLaneId, ownConnectorId);
  }

  units::length::meter_t GetDistanceToConnectorDeparture(
      /*const ObjectPosition position,*/ std::string intersectingConnectorId,
      int intersectingLaneId,
      std::string ownConnectorId) const override
  {
    return implementation->GetDistanceToConnectorDeparture(
        /*position,*/ intersectingConnectorId, intersectingLaneId, ownConnectorId);
  }

  std::optional<Position> LaneCoord2WorldCoord(units::length::meter_t distance,
                                               units::length::meter_t offset,
                                               std::string roadId,
                                               int64_t laneId) const override
  {
    return implementation->LaneCoord2WorldCoord(distance, offset, roadId, laneId);
  }

  GlobalRoadPositions WorldCoord2LaneCoord(units::length::meter_t x,
                                           units::length::meter_t y,
                                           units::angle::radian_t heading) const override
  {
    return implementation->WorldCoord2LaneCoord(x, y, heading);
  }

  bool IsSValidOnLane(std::string roadId, int laneId, units::length::meter_t distance) override
  {
    return implementation->IsSValidOnLane(roadId, laneId, distance);
  }

  bool IsDirectionalRoadExisting(const std::string& roadId, bool inOdDirection) const override
  {
    return implementation->IsDirectionalRoadExisting(roadId, inOdDirection);
  }

  bool IsLaneTypeValid(const std::string& roadId,
                       const int laneId,
                       const units::length::meter_t distanceOnLane,
                       const LaneTypes& validLaneTypes) override
  {
    return implementation->IsLaneTypeValid(roadId, laneId, distanceOnLane, validLaneTypes);
  }

  units::curvature::inverse_meter_t GetLaneCurvature(std::string roadId,
                                                     int laneId,
                                                     units::length::meter_t position) const override
  {
    return implementation->GetLaneCurvature(roadId, laneId, position);
  }

  RouteQueryResult<std::optional<units::curvature::inverse_meter_t>> GetLaneCurvature(
      const RoadGraph& roadGraph,
      RoadGraphVertex startNode,
      int laneId,
      units::length::meter_t position,
      units::length::meter_t distance) const override
  {
    return implementation->GetLaneCurvature(roadGraph, startNode, laneId, position, distance);
  }

  units::length::meter_t GetLaneWidth(std::string roadId, int laneId, units::length::meter_t position) const override
  {
    return implementation->GetLaneWidth(roadId, laneId, position);
  }

  RouteQueryResult<std::optional<units::length::meter_t>> GetLaneWidth(const RoadGraph& roadGraph,
                                                                       RoadGraphVertex startNode,
                                                                       int laneId,
                                                                       units::length::meter_t position,
                                                                       units::length::meter_t distance) const override
  {
    return implementation->GetLaneWidth(roadGraph, startNode, laneId, position, distance);
  }

  units::angle::radian_t GetLaneDirection(std::string roadId,
                                          int64_t laneId,
                                          units::length::meter_t position) const override
  {
    return implementation->GetLaneDirection(roadId, laneId, position);
  }

  RouteQueryResult<std::optional<units::angle::radian_t>> GetLaneDirection(
      const RoadGraph& roadGraph,
      RoadGraphVertex startNode,
      int64_t laneId,
      units::length::meter_t position,
      units::length::meter_t distance) const override
  {
    return implementation->GetLaneDirection(roadGraph, startNode, laneId, position, distance);
  }

  RouteQueryResult<units::length::meter_t> GetDistanceToEndOfLane(
      const RoadGraph& roadGraph,
      RoadGraphVertex startNode,
      int laneId,
      units::length::meter_t initialSearchDistance,
      units::length::meter_t maximumSearchLength) const override
  {
    return implementation->GetDistanceToEndOfLane(
        roadGraph, startNode, laneId, initialSearchDistance, maximumSearchLength);
  }

  RouteQueryResult<units::length::meter_t> GetDistanceToEndOfLane(const RoadGraph& roadGraph,
                                                                  RoadGraphVertex startNode,
                                                                  int laneId,
                                                                  units::length::meter_t initialSearchDistance,
                                                                  units::length::meter_t maximumSearchLength,
                                                                  const LaneTypes& laneTypes) const override
  {
    return implementation->GetDistanceToEndOfLane(
        roadGraph, startNode, laneId, initialSearchDistance, maximumSearchLength, laneTypes);
  }

  LaneSections GetLaneSections(const std::string& roadId) const override
  {
    return implementation->GetLaneSections(roadId);
  }

  bool IntersectsWithAgent(units::length::meter_t x,
                           units::length::meter_t y,
                           units::angle::radian_t rotation,
                           units::length::meter_t length,
                           units::length::meter_t width,
                           units::length::meter_t center) override
  {
    return implementation->IntersectsWithAgent(x, y, rotation, length, width, center);
  }

  std::optional<Position> RoadCoord2WorldCoord(RoadPosition roadCoord, std::string roadID = "") const override
  {
    return implementation->RoadCoord2WorldCoord(roadCoord, roadID);
  }

  units::length::meter_t GetRoadLength(const std::string& roadId) const override
  {
    return implementation->GetRoadLength(roadId);
  }

  //-----------------------------------------------------------------------------
  //! Instantiate the world by creating a WorldInterface out of a world library
  //!
  //! @return                true if successful
  //-----------------------------------------------------------------------------
  bool Instantiate() override
  {
    if (!worldBinding)
    {
      return false;
    }
    else if (!implementation)
    {
      implementation = worldBinding->Instantiate();
      if (!implementation)
      {
        return false;
      }
    }
    return true;
  }

  //-----------------------------------------------------------------------------
  //! Returns true if world was already instantiated
  //!
  //! @return                true if world was already instantiated
  //-----------------------------------------------------------------------------
  bool isInstantiated() override { return implementation; }

  RouteQueryResult<std::vector<CommonTrafficSign::Entity>> GetTrafficSignsInRange(
      const RoadGraph& roadGraph,
      RoadGraphVertex startNode,
      int laneId,
      units::length::meter_t startDistance,
      units::length::meter_t searchRange) const override
  {
    return implementation->GetTrafficSignsInRange(roadGraph, startNode, laneId, startDistance, searchRange);
  }

  RouteQueryResult<std::vector<CommonTrafficSign::Entity>> GetRoadMarkingsInRange(
      const RoadGraph& roadGraph,
      RoadGraphVertex startNode,
      int laneId,
      units::length::meter_t startDistance,
      units::length::meter_t searchRange) const override
  {
    return implementation->GetRoadMarkingsInRange(roadGraph, startNode, laneId, startDistance, searchRange);
  }

  RouteQueryResult<std::vector<CommonTrafficLight::Entity>> GetTrafficLightsInRange(
      const RoadGraph& roadGraph,
      RoadGraphVertex startNode,
      int laneId,
      units::length::meter_t startDistance,
      units::length::meter_t searchRange) const override
  {
    return implementation->GetTrafficLightsInRange(roadGraph, startNode, laneId, startDistance, searchRange);
  }

  RouteQueryResult<std::vector<LaneMarking::Entity>> GetLaneMarkings(const RoadGraph& roadGraph,
                                                                     RoadGraphVertex startNode,
                                                                     int laneId,
                                                                     units::length::meter_t startDistance,
                                                                     units::length::meter_t range,
                                                                     Side side) const override
  {
    return implementation->GetLaneMarkings(roadGraph, startNode, laneId, startDistance, range, side);
  }

  [[deprecated]] RouteQueryResult<RelativeWorldView::Roads> GetRelativeJunctions(
      const RoadGraph& roadGraph,
      RoadGraphVertex startNode,
      units::length::meter_t startDistance,
      units::length::meter_t range) const override
  {
    return implementation->GetRelativeJunctions(roadGraph, startNode, startDistance, range);
  }

  RouteQueryResult<RelativeWorldView::Roads> GetRelativeRoads(const RoadGraph& roadGraph,
                                                              RoadGraphVertex startNode,
                                                              units::length::meter_t startDistance,
                                                              units::length::meter_t range) const override
  {
    return implementation->GetRelativeRoads(roadGraph, startNode, startDistance, range);
  }

  RouteQueryResult<RelativeWorldView::Lanes> GetRelativeLanes(const RoadGraph& roadGraph,
                                                              RoadGraphVertex startNode,
                                                              int laneId,
                                                              units::length::meter_t distance,
                                                              units::length::meter_t range,
                                                              bool includeOncoming = true) const override
  {
    return implementation->GetRelativeLanes(roadGraph, startNode, laneId, distance, range, includeOncoming);
  }

  RouteQueryResult<std::optional<int>> GetRelativeLaneId(const RoadGraph& roadGraph,
                                                         RoadGraphVertex startNode,
                                                         int laneId,
                                                         units::length::meter_t distance,
                                                         GlobalRoadPositions targetPosition) const override
  {
    return implementation->GetRelativeLaneId(roadGraph, startNode, laneId, distance, targetPosition);
  }

  std::vector<JunctionConnection> GetConnectionsOnJunction(std::string junctionId,
                                                           std::string incomingRoadId) const override
  {
    return implementation->GetConnectionsOnJunction(junctionId, incomingRoadId);
  }

  std::vector<IntersectingConnection> GetIntersectingConnections(std::string connectingRoadId) const override
  {
    return implementation->GetIntersectingConnections(connectingRoadId);
  }

  std::vector<JunctionConnectorPriority> GetPrioritiesOnJunction(std::string junctionId) const override
  {
    return implementation->GetPrioritiesOnJunction(junctionId);
  }

  RoadNetworkElement GetRoadSuccessor(std::string roadId) const override
  {
    return implementation->GetRoadSuccessor(roadId);
  }

  RoadNetworkElement GetRoadPredecessor(std::string roadId) const override
  {
    return implementation->GetRoadPredecessor(roadId);
  }

  std::pair<RoadGraph, RoadGraphVertex> GetRoadGraph(const RouteElement& start,
                                                     int maxDepth,
                                                     bool inDrivingDirection) const override
  {
    return implementation->GetRoadGraph(start, maxDepth, inDrivingDirection);
  }

  std::map<RoadGraphEdge, double> GetEdgeWeights(const RoadGraph& roadGraph) const override
  {
    return implementation->GetEdgeWeights(roadGraph);
  }

  std::unique_ptr<RoadStreamInterface> GetRoadStream(const std::vector<RouteElement>& route) const override
  {
    return implementation->GetRoadStream(route);
  }

  RouteQueryResult<Obstruction> GetObstruction(
      const RoadGraph& roadGraph,
      RoadGraphVertex startNode,
      const GlobalRoadPosition& ownPosition,
      const std::map<ObjectPoint, Common::Vector2d<units::length::meter_t>>& points,
      const RoadIntervals& touchedRoads) const override
  {
    return implementation->GetObstruction(roadGraph, startNode, ownPosition, points, touchedRoads);
  }

  void* GetGlobalDrivingView() const override { return implementation->GetGlobalDrivingView(); }
  void* GetGlobalObjects() const override { return implementation->GetGlobalObjects(); }
  void SetTimeOfDay(int timeOfDay) override { return implementation->SetTimeOfDay(timeOfDay); }
  void SetWeekday(Weekday weekday) override { return implementation->SetWeekday(weekday); }
  Weekday GetWeekday() const override { return implementation->GetWeekday(); }
  bool CreateGlobalDrivingView() override { return implementation->CreateGlobalDrivingView(); }
  const AgentInterface* GetSpecialAgent() const override { return implementation->GetSpecialAgent(); }
  const AgentInterface* GetLastCarInlane(int laneNumber) const override
  {
    return implementation->GetLastCarInlane(laneNumber);
  }
  const AgentInterface* GetBicycle() const override { return implementation->GetBicycle(); }
  void QueueAgentUpdate(std::function<void(double)> func, double val) override
  {
    return implementation->QueueAgentUpdate(func, val);
  }
  bool CreateWorldScenery(const std::string& sceneryFilename) override
  {
    return implementation->CreateWorldScenery(sceneryFilename);
  }
  bool CreateWorldScenario(const std::string& scenarioFilename) override
  {
    return implementation->CreateWorldScenario(scenarioFilename);
  }
  RouteQueryResult<std::optional<units::length::meter_t>> GetDistanceBetweenObjects(
      const RoadGraph& roadGraph,
      RoadGraphVertex startNode,
      units::length::meter_t ownPosition,
      const GlobalRoadPositions& target) const override
  {
    return implementation->GetDistanceBetweenObjects(roadGraph, startNode, ownPosition, target);
  }
  RadioInterface& GetRadio() override { return implementation->GetRadio(); }

  uint64_t GetUniqueLaneId(std::string roadId, int64_t laneId, units::length::meter_t position) const override
  {
    return implementation->GetUniqueLaneId(roadId, laneId, position);
  }

private:
  WorldBinding* worldBinding = nullptr;
  WorldInterface* implementation = nullptr;
};

}  // namespace core
