/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "modelBinding.h"

#include <memory>
#include <new>
#include <utility>

#include "bindings/modelLibrary.h"
#include "modelElements/componentType.h"

class CallbackInterface;
class ScenarioControlInterface;
class StochasticsInterface;
class WorldInterface;

namespace openpass::common
{
struct RuntimeInformation;
}  // namespace openpass::common

namespace core
{
class ComponentInterface;

ModelBinding::ModelBinding(std::string libraryPath,
                           const openpass::common::RuntimeInformation &runtimeInformation,
                           CallbackInterface *callbacks)
    : libraryPath{std::move(libraryPath)}, runtimeInformation{runtimeInformation}, callbacks{callbacks}
{
}

ModelBinding::~ModelBinding() = default;

ComponentInterface *ModelBinding::Instantiate(const std::shared_ptr<ComponentType> &componentType,
                                              const std::string &componentName,
                                              StochasticsInterface *stochastics,
                                              WorldInterface *world,
                                              ObservationNetworkInterface *observationNetwork,
                                              Agent *agent,
                                              std::shared_ptr<ScenarioControlInterface> const &scenarioControl,
                                              PublisherInterface *publisher)
{
  auto getOrCreateModelLibrary = [&](const std::string &name) -> ModelLibrary *
  {
    // check if model library is already loaded
    if (auto iter = modelLibraries.find(name); iter != modelLibraries.end())
    {
      return iter->second.get();
    }

    // create new model library and check if initialization is successful
    auto modelLibrary = std::make_unique<ModelLibrary>(libraryPath, name, callbacks);
    if (modelLibrary->Init())
    {
      return modelLibraries.emplace(name, std::move(modelLibrary)).first->second.get();
    }

    // initialization failed
    return nullptr;
  };

  if (auto *modelLibrary = getOrCreateModelLibrary(componentType->GetModelLibrary()))
  {
    return modelLibrary->CreateComponent(componentType,
                                         componentName,
                                         runtimeInformation,
                                         stochastics,
                                         world,
                                         observationNetwork,
                                         agent,
                                         scenarioControl,
                                         publisher);
  }

  // model library initialization failed
  return nullptr;
}

}  // namespace core
