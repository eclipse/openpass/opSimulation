/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  worldBinding.h
//! @brief This file contains the interface to the world library.
//-----------------------------------------------------------------------------

#pragma once

#include <memory>
#include <string>

#include "common/opExport.h"
class CallbackInterface;
class DataBufferWriteInterface;
class IdManagerInterface;
class StochasticsInterface;
class WorldInterface;

namespace core
{

class WorldLibrary;

//! This class represents the interface to the world library
class SIMULATIONCOREEXPORT WorldBinding
{
public:
  /**
   * @brief WorldBinding constructor
   *
   * @param[in] libraryPath   Path to the library
   * @param[in] callbacks     Pointer to the callbacks
   * @param[in] idManager     Pointer to the IdManager
   * @param[in] stochastics   Pointer to the stochastic
   * @param[in] dataBuffer    Pointer to the data buffer that provides write-only access to the data
   */
  WorldBinding(std::string libraryPath,
               CallbackInterface* callbacks,
               IdManagerInterface* idManager,
               StochasticsInterface* stochastics,
               DataBufferWriteInterface* dataBuffer);
  WorldBinding(const WorldBinding&) = delete;
  WorldBinding(WorldBinding&&) = delete;
  WorldBinding& operator=(const WorldBinding&) = delete;
  WorldBinding& operator=(WorldBinding&&) = delete;
  virtual ~WorldBinding();

  //-----------------------------------------------------------------------------
  //! Gets the world instance library and stores it,
  //! then creates a new worldInterface of the library.
  //!
  //! @return                         worldInterface created from the library
  //-----------------------------------------------------------------------------
  WorldInterface* Instantiate();

private:
  const std::string libraryPath;
  std::unique_ptr<WorldLibrary> library;
  CallbackInterface* callbacks;
  IdManagerInterface* idManager;
  StochasticsInterface* stochastics;
  DataBufferWriteInterface* dataBuffer;
};

}  // namespace core
