/********************************************************************************
 * Copyright (c) 2017 ITK Engineering GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "log.h"

std::map<std::thread::id, std::unique_ptr<std::ofstream>>
    LogOutputPolicy::logStreamMap;  // NOLINT[cppcoreguidelines-avoid-non-const-global-variables]
