/********************************************************************************
 * Copyright (c) 2018-2019 AMFD GmbH
 *               2020 HLRS, University of Stuttgart
 *               2018-2020 in-tech GmbH
 *               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//-----------------------------------------------------------------------------
//! @file  regularDriving.cpp
//! @brief This file contains the implementation header file
//-----------------------------------------------------------------------------

#include "regularDriving.h"

#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>
#include <algorithm>
#include <memory>
#include <new>
#include <optional>

#include "common/commonTools.h"
#include "common/dynamicsSignal.h"
#include "common/globalDefinitions.h"
#include "common/longitudinalSignal.h"
#include "common/parametersVehicleSignal.h"
#include "common/rollSignal.h"
#include "common/steeringSignal.h"
#include "common/vector2d.h"
#include "components/common/vehicleProperties.h"
#include "include/agentInterface.h"
#include "include/publisherInterface.h"
#include "include/worldInterface.h"

void DynamicsRegularDrivingImplementation::UpdateInput(int localLinkId,
                                                       const std::shared_ptr<SignalInterface const> &data,
                                                       [[maybe_unused]] int time)
{
  if (localLinkId == 0)
  {
    const std::shared_ptr<ComponentStateSignalInterface const> stateSignal
        = std::dynamic_pointer_cast<ComponentStateSignalInterface const>(data);
    if (stateSignal->componentState == ComponentState::Acting)
    {
      const std::shared_ptr<LongitudinalSignal const> signal
          = std::dynamic_pointer_cast<LongitudinalSignal const>(data);
      if (!signal)
      {
        const std::string msg = COMPONENTNAME + " invalid signaltype";
        LOG(CbkLogLevel::Debug, msg);
        throw std::runtime_error(msg);
      }
      in_accPedalPos = signal->accPedalPos;
      in_brakePedalPos = signal->brakePedalPos;
      ApplyPedalPositionLimits();
      in_gear = signal->gear;
      ApplyGearLimit();
      dynamicsSignal.longitudinalController = signal->source;
    }
  }
  else if (localLinkId == 1)
  {
    const std::shared_ptr<ComponentStateSignalInterface const> stateSignal
        = std::dynamic_pointer_cast<ComponentStateSignalInterface const>(data);
    if (stateSignal->componentState == ComponentState::Acting)
    {
      const std::shared_ptr<SteeringSignal const> signal = std::dynamic_pointer_cast<SteeringSignal const>(data);
      if (!signal)
      {
        const std::string msg = COMPONENTNAME + " invalid signaltype";
        LOG(CbkLogLevel::Debug, msg);
        throw std::runtime_error(msg);
      }
      in_steeringWheelAngle = signal->steeringWheelAngle;
      dynamicsSignal.lateralController = signal->source;
    }
  }
  else if (localLinkId == 2)
  {
    const std::shared_ptr<ComponentStateSignalInterface const> stateSignal
        = std::dynamic_pointer_cast<ComponentStateSignalInterface const>(data);
    if (stateSignal->componentState == ComponentState::Acting)
    {
      const std::shared_ptr<RollSignal const> signal = std::dynamic_pointer_cast<RollSignal const>(data);
      if (!signal)
      {
        const std::string msg = COMPONENTNAME + " invalid signaltype";
        LOG(CbkLogLevel::Debug, msg);
        throw std::runtime_error(msg);
      }
      dynamicsSignal.dynamicsInformation.roll = signal->rollAngle;
    }
  }
  else if (localLinkId == 100)
  {
    // from ParametersAgent
    const std::shared_ptr<ParametersVehicleSignal const> signal
        = std::dynamic_pointer_cast<ParametersVehicleSignal const>(data);
    if (!signal)
    {
      const std::string msg = COMPONENTNAME + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }

    vehicleModelParameters = signal->vehicleParameters;
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void DynamicsRegularDrivingImplementation::UpdateOutput(int localLinkId,
                                                        std::shared_ptr<SignalInterface const> &data,
                                                        [[maybe_unused]] int time)
{
  if (localLinkId == 0)
  {
    try
    {
      data = std::make_shared<DynamicsSignal const>(dynamicsSignal);
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg = COMPONENTNAME + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void DynamicsRegularDrivingImplementation::ApplyGearLimit()
{
  in_gear = std::min(std::max(in_gear, 1), static_cast<int>(GetVehicleProperty(Properties::Vehicle::NUMBER_OF_GEARS)));
}

void DynamicsRegularDrivingImplementation::ApplyPedalPositionLimits()
{
  in_accPedalPos = std::min(std::max(in_accPedalPos, 0.0), 1.0);
  in_brakePedalPos = std::min(std::max(in_brakePedalPos, 0.0), 1.0);
}

units::angular_velocity::revolutions_per_minute_t DynamicsRegularDrivingImplementation::GetEngineSpeedByVelocity(
    units::velocity::meters_per_second_t xVel, int gear)
{
  return 1_rad
       * (xVel * GetVehicleProperty(Properties::Vehicle::AXLE_RATIO)
          * GetVehicleProperty(Properties::Vehicle::GEAR_RATIO + std::to_string(gear)))
       / (vehicleModelParameters.rear_axle.wheel_diameter
          * 0.5);  // an dynamic wheel radius rDyn must actually be used here
}

units::torque::newton_meter_t DynamicsRegularDrivingImplementation::GetEngineMomentMax(
    units::angular_velocity::revolutions_per_minute_t engineSpeed)
{
  const units::torque::newton_meter_t maximumEngineTorque{
      GetVehicleProperty(Properties::Vehicle::MAXIMUM_ENGINE_TORQUE)};
  const units::angular_velocity::revolutions_per_minute_t maximumEngineSpeed{
      GetVehicleProperty(Properties::Vehicle::MAXIMUM_ENGINE_SPEED)};
  const units::angular_velocity::revolutions_per_minute_t minimumEngineSpeed{
      GetVehicleProperty(Properties::Vehicle::MINIMUM_ENGINE_SPEED)};

  auto torqueMax = maximumEngineTorque;  // initial value at max
  auto speed = engineSpeed;

  const bool isLowerSection = engineSpeed < minimumEngineSpeed + 1000.0_rpm;
  const bool isBeyondLowerSectionBorder = engineSpeed < minimumEngineSpeed;
  const bool isUpperSection = engineSpeed > maximumEngineSpeed - 1000.0_rpm;
  const bool isBeyondUpperSectionBorder = engineSpeed > maximumEngineSpeed;

  if (isLowerSection)
  {
    if (isBeyondLowerSectionBorder)  // not within limits
    {
      speed = minimumEngineSpeed;
    }
    torqueMax = units::inverse_radian(0.5 / M_PI) * (1000_rpm - (speed - minimumEngineSpeed))
                  * units::unit_t<units::compound_unit<units::torque::newton_meter, units::time::minute>>(-0.1)
              + maximumEngineTorque;
  }
  else if (isUpperSection)
  {
    if (isBeyondUpperSectionBorder)
    {
      speed = maximumEngineSpeed;
    }

    torqueMax = units::inverse_radian(0.5 / M_PI) * (speed - maximumEngineSpeed + 1000_rpm)
                  * units::unit_t<units::compound_unit<units::torque::newton_meter, units::time::minute>>(-0.04)
              + maximumEngineTorque;
  }
  return torqueMax;
}

units::acceleration::meters_per_second_squared_t
DynamicsRegularDrivingImplementation::GetAccelerationFromRollingResistance()
{
  const double rollingResistanceCoeff
      = .0125;  // Dummy value, get via vehicle Parameters (vehicleModelParameters.rollingDragCoefficient)
  const units::acceleration::meters_per_second_squared_t accDueToRollingResistance = -rollingResistanceCoeff * _oneG;
  return accDueToRollingResistance;
}

units::acceleration::meters_per_second_squared_t DynamicsRegularDrivingImplementation::GetAccelerationFromAirResistance(
    units::velocity::meters_per_second_t velocity)
{
  const units::force::newton_t forceAirResistance
      = -.5 * _rho * GetVehicleProperty(Properties::Vehicle::AIR_DRAG_COEFFICIENT)
      * units::area::square_meter_t(GetVehicleProperty(Properties::Vehicle::FRONT_SURFACE)) * velocity * velocity;
  const units::acceleration::meters_per_second_squared_t accDueToAirResistance
      = forceAirResistance / GetAgent()->GetVehicleModelParameters()->mass;
  return accDueToAirResistance;
}

units::torque::newton_meter_t DynamicsRegularDrivingImplementation::GetEngineMomentMin(
    units::angular_velocity::revolutions_per_minute_t engineSpeed)
{
  return GetEngineMomentMax(engineSpeed) * -.1;
}

double DynamicsRegularDrivingImplementation::GetFrictionCoefficient()
{
  return GetWorld()->GetFriction() * GetVehicleProperty(Properties::Vehicle::FRICTION_COEFFICIENT);
}

double DynamicsRegularDrivingImplementation::GetVehicleProperty(const std::string &propertyName)
{
  const auto property = helper::map::query(vehicleModelParameters.properties, propertyName);
  THROWIFFALSE(property.has_value(), "Vehicle property \"" + propertyName + "\" was not set in the VehicleCatalog");
  return std::stod(property.value());
}

units::torque::newton_meter_t DynamicsRegularDrivingImplementation::GetEngineMoment(double gasPedalPos, int gear)
{
  const auto xVel = GetAgent()->GetVelocity().Length();

  const auto engineSpeedAtGear = GetEngineSpeedByVelocity(xVel, gear);

  GetAgent()->SetEngineSpeed(engineSpeedAtGear);

  const auto max = GetEngineMomentMax(engineSpeedAtGear);
  const auto maxAccAtGear = GetAccFromEngineMoment(xVel, max, gear, GetCycleTime());

  GetAgent()->SetMaxAcceleration(maxAccAtGear * GetFrictionCoefficient());

  const auto min = GetEngineMomentMin(engineSpeedAtGear);

  return (units::math::fabs(min) + max) * gasPedalPos + min;
}

units::acceleration::meters_per_second_squared_t DynamicsRegularDrivingImplementation::GetAccFromEngineMoment(
    [[maybe_unused]] units::velocity::meters_per_second_t xVel,
    units::torque::newton_meter_t engineMoment,
    int chosenGear,
    [[maybe_unused]] int cycleTime)
{
  const units::torque::newton_meter_t wheelSetMoment
      = engineMoment
      * (GetVehicleProperty(Properties::Vehicle::AXLE_RATIO)
         * GetVehicleProperty(Properties::Vehicle::GEAR_RATIO + std::to_string(chosenGear)));
  const units::force::newton_t wheelSetForce = wheelSetMoment / (0.5 * vehicleModelParameters.rear_axle.wheel_diameter);

  const auto vehicleSetForce = wheelSetForce;
  const units::acceleration::meters_per_second_squared_t acc
      = vehicleSetForce / GetAgent()->GetVehicleModelParameters()->mass;

  return acc;
}

units::acceleration::meters_per_second_squared_t DynamicsRegularDrivingImplementation::GetAccVehicle(
    double accPedalPos, double brakePedalPos, int gear)
{
  units::acceleration::meters_per_second_squared_t resultAcc{0};

  const auto xVel = GetAgent()->GetVelocity().Length();

  if (brakePedalPos > 0.)  // Brake
  {
    const units::acceleration::meters_per_second_squared_t accelerationDueToPedal{brakePedalPos * _oneG * -1.};
    const units::angular_velocity::revolutions_per_minute_t engineSpeed{GetEngineSpeedByVelocity(xVel, gear)};
    auto engineDrag{GetEngineMomentMin(engineSpeed)};
    auto accelerationDueToDrag = GetAccFromEngineMoment(xVel, engineDrag, gear, GetCycleTime());
    if (accelerationDueToPedal > 0.0_mps_sq || accelerationDueToDrag > 0.0_mps_sq)
    {
      throw std::runtime_error("DynamicsRegularDrivingImplementation - Wrong sign for acceleration!");
    }

    resultAcc = accelerationDueToPedal + accelerationDueToDrag;

    const auto maxDecel = GetAgent()->GetMaxDeceleration();
    resultAcc = units::math::fmax(maxDecel, resultAcc);
  }
  else  // Gas
  {
    const auto engineMoment = GetEngineMoment(accPedalPos, gear);
    GetPublisher()->Publish("EngineMoment", engineMoment.value());
    resultAcc = GetAccFromEngineMoment(xVel, engineMoment, gear, GetCycleTime());
  }

  const auto accelerationDueToAirResistance = GetAccelerationFromAirResistance(xVel);
  const auto accelerationDueToRollingResistance = GetAccelerationFromRollingResistance();

  return resultAcc + accelerationDueToAirResistance + accelerationDueToRollingResistance;
}

void DynamicsRegularDrivingImplementation::Trigger([[maybe_unused]] int time)
{
  auto *const agent = GetAgent();

  //Lateral behavior
  const units::acceleration::meters_per_second_squared_t maxDecel = _oneG * GetFrictionCoefficient() * -1;
  agent->SetMaxDeceleration(maxDecel);

  units::velocity::meters_per_second_t velocity;
  const auto yawAngle = agent->GetYaw();

  auto accVehicle = GetAccVehicle(in_accPedalPos, in_brakePedalPos, in_gear);

  velocity = agent->GetVelocity().Length() + accVehicle * units::time::millisecond_t(GetCycleTime());

  if (velocity < VLowerLimit)
  {
    accVehicle = 0.0_mps_sq;
    velocity = VLowerLimit;
  }

  // change of path coordinate since last time step [m]
  const units::length::meter_t dsPath = velocity * units::time::millisecond_t(GetCycleTime());
  // change of inertial x-position due to ds and yaw [m]
  const units::length::meter_t dxPos = dsPath * units::math::cos(yawAngle);
  // change of inertial y-position due to ds and yaw [m]
  const units::length::meter_t dyPos = dsPath * units::math::sin(yawAngle);
  // new inertial x-position [m]
  auto xPos = agent->GetPositionX() + dxPos;
  // new inertial y-position [m]
  auto yPos = agent->GetPositionY() + dyPos;

  dynamicsSignal.dynamicsInformation.acceleration = accVehicle;
  dynamicsSignal.dynamicsInformation.velocityX = velocity * units::math::cos(yawAngle);
  dynamicsSignal.dynamicsInformation.velocityY = velocity * units::math::sin(yawAngle);
  dynamicsSignal.dynamicsInformation.positionX = xPos;
  dynamicsSignal.dynamicsInformation.positionY = yPos;
  dynamicsSignal.dynamicsInformation.travelDistance = dsPath;

  // convert steering wheel angle to steering angle of front wheels [radian]
  const auto steeringAngle = std::clamp(in_steeringWheelAngle / GetVehicleProperty(Properties::Vehicle::STEERING_RATIO),
                                        -vehicleModelParameters.front_axle.max_steering,
                                        vehicleModelParameters.front_axle.max_steering);
  dynamicsSignal.dynamicsInformation.steeringWheelAngle
      = steeringAngle * GetVehicleProperty(Properties::Vehicle::STEERING_RATIO);
  GetPublisher()->Publish("SteeringAngle", steeringAngle.value());
  const auto wheelbase = vehicleModelParameters.front_axle.bb_center_to_axle_center.x
                       - vehicleModelParameters.rear_axle.bb_center_to_axle_center.x;
  // calculate curvature (Ackermann model; reference point of yawing = rear axle!) [radian]
  const units::curvature::inverse_meter_t steeringCurvature = units::math::tan(steeringAngle) / wheelbase;
  // change of yaw angle due to ds and curvature [radian]
  const units::angle::radian_t dpsi{units::math::atan(steeringCurvature * dsPath)};
  dynamicsSignal.dynamicsInformation.yawRate = dpsi / units::time::millisecond_t(GetCycleTime());
  dynamicsSignal.dynamicsInformation.yawAcceleration
      = (dynamicsSignal.dynamicsInformation.yawRate - yawRatePrevious) / units::time::millisecond_t(GetCycleTime());
  yawRatePrevious = dynamicsSignal.dynamicsInformation.yawRate;
  dynamicsSignal.dynamicsInformation.centripetalAcceleration
      = units::inverse_radian(1) * dynamicsSignal.dynamicsInformation.yawRate * velocity;
  // new yaw angle in current time step [radian]
  const auto psi = agent->GetYaw() + dpsi;
  dynamicsSignal.dynamicsInformation.yaw = psi;

  //set wheel data
  auto rotationRateFront
      = units::angle::radian_t(1) * velocity / (vehicleModelParameters.front_axle.wheel_diameter / 2.0);
  auto rotationRateRear
      = units::angle::radian_t(1) * velocity / (vehicleModelParameters.rear_axle.wheel_diameter / 2.0);

  dynamicsSignal.dynamicsInformation.wheelRotationRate
      = {rotationRateFront, rotationRateFront, rotationRateRear, rotationRateRear};

  dynamicsSignal.dynamicsInformation.wheelYaw = {steeringAngle, steeringAngle, 0.0_rad, 0.0_rad};
}
