/********************************************************************************
 * Copyright (c) 2018 AMFD GmbH
 *               2017-2019 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/** \addtogroup Agent_Updater
 * @{
 * \brief This module gets the final values for the dynamic parameters of the agent and updates the agent accordingly
 *
 * \section AgentUpdater_Inputs Inputs
 * Input variables:
 * name | meaning
 * -----|------
 * dynamicsInformation | All values regarding the dynamic calculation of the agent
 *
 * Input channel IDs:
 * Input ID | signal class | contained variables
 * ------------|--------------|-------------
 * 0 | DynamicsSignal  | DynamicsInformation
 *
 * @} */

#pragma once

#include <cmath>
#include <memory>
#include <string>
#include <type_traits>
#include <units.h>

#include "common/globalDefinitions.h"
#include "include/agentInterface.h"
#include "include/callbackInterface.h"
#include "include/modelInterface.h"

class ParameterInterface;
class PublisherInterface;
class SignalInterface;
class StochasticsInterface;
class WorldInterface;

/**
 * \brief The class which is responsible for updating the agent in each time step.
 *
 * \ingroup AgentUpdater
 */
class AgentUpdaterImplementation : public UnrestrictedModelInterface
{
public:
  /// Component Name
  const std::string COMPONENTNAME = "AgentUpdater";

  /**
   * @brief Construct a new Agent Updater Implementation object
   *
   * @param[in]     componentName  Name of the component
   * @param[in]     isInit         Corresponds to "init" of "Component"
   * @param[in]     priority       Corresponds to "priority" of "Component"
   * @param[in]     offsetTime     Corresponds to "offsetTime" of "Component"
   * @param[in]     responseTime   Corresponds to "responseTime" of "Component"
   * @param[in]     cycleTime      Corresponds to "cycleTime" of "Component"
   * @param[in]     stochastics    Pointer to the stochastics class loaded by the framework
   * @param[in]     world          Pointer to the world interface
   * @param[in]     parameters     Pointer to the parameters of the module
   * @param[in]     publisher      Pointer to the publisher instance
   * @param[in]     callbacks      Pointer to the callbacks
   * @param[in]     agent          Pointer to agent instance
   */
  AgentUpdaterImplementation(std::string componentName,
                             bool isInit,
                             int priority,
                             int offsetTime,
                             int responseTime,
                             int cycleTime,
                             StochasticsInterface *stochastics,
                             WorldInterface *world,
                             const ParameterInterface *parameters,
                             PublisherInterface *const publisher,
                             const CallbackInterface *callbacks,
                             AgentInterface *agent)
      : UnrestrictedModelInterface(componentName,
                                   isInit,
                                   priority,
                                   offsetTime,
                                   responseTime,
                                   cycleTime,
                                   stochastics,
                                   world,
                                   parameters,
                                   publisher,
                                   callbacks,
                                   agent),
        publisher(publisher)
  {
  }
  AgentUpdaterImplementation(const AgentUpdaterImplementation &) = delete;
  AgentUpdaterImplementation(AgentUpdaterImplementation &&) = delete;
  AgentUpdaterImplementation &operator=(const AgentUpdaterImplementation &) = delete;
  AgentUpdaterImplementation &operator=(AgentUpdaterImplementation &&) = delete;
  virtual ~AgentUpdaterImplementation() = default;

  /*!
   * \brief Update Inputs
   *
   * Function is called by framework when another component delivers a signal over
   * a channel to this component (scheduler calls update taks of other component).
   *
   * Refer to module description for input channels and input ids.
   *
   * @param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
   * @param[in]     data           Referenced signal (copied by sending component)
   * @param[in]     time           Current scheduling time
   */
  void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time) override;

  /*!
   * \brief Update outputs.
   *
   * Function is called by framework when this component has to deliver a signal over
   * a channel to another component (scheduler calls update task of this component).
   *
   * Refer to module description for output channels and output ids.
   *
   * @param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
   * @param[out]    data           Referenced signal (copied by this component)
   * @param[in]     time           Current scheduling time
   */
  void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time) override;

  /*!
   * \brief Process data within component.
   *
   * Function is called by framework when the scheduler calls the trigger task
   * of this component
   *
   * @param[in]     time           Current scheduling time
   */
  void Trigger(int time) override;

  /*!
   * \brief Checks that the given value is not Nan and logs an error otherwise
   *
   * @param[in]    value           to be checked for NaN
   * @param[in]    description     for potential error message
   */
  template <typename T, class = typename std::enable_if_t<units::traits::is_unit_t<T>::value>>
  void Validate(T value, const std::string &description)
  {
    if (std::isnan(value.value()))
    {
      LOGERRORANDTHROW("AgentUpdater got NaN as value of " + description + " for Agent "
                       + std::to_string(GetAgent()->GetId()));
    }
  }

private:
  DynamicsInformation dynamicsInformation{};
  std::string longitudinalController{};  //!< Component that is active for longitudinal control
  std::string lateralController{};       //!< Component that is active for lateral control

  PublisherInterface *publisher;
};
