/********************************************************************************
 * Copyright (c) 2023-2024 Volkswagen AG
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/** \addtogroup Vehicle_Dynamics
 *@{
 * \addtogroup Steering_System
 * \brief  This module represents a simple steering system.
 *
 * \details Based on a steering angle input, a steering elasticity and a static toe parameterization,
 * the angle of each wheel is calculated by using the steering ratio. Front wheels are turned at the same angle.
 * There is no rear-axle steering implemented.
 *
 * \section Steering_System_Inputs Inputs
 * name | meaning
 * -----|---------
 * Steering Signal | current steering angle of vehicle
 *
 * \section Steering_System_Outputs Outputs
 * name | meaning
 * -----|---------
 * WheelAngle | current angle of each tire
 *
 * @} */

#ifndef STEERINGSYSTEM_H
#define STEERINGSYSTEM_H

#include <map>
#include <unordered_map>

#include "common/componentPorts.h"
#include "common/steeringSignal.h"
#include "common/vectorSignals.h"
#include "components/common/vehicleProperties.h"
#include "include/modelInterface.h"

/*!
 * \copydoc Steering_System
 * \ingroup Steering_System
 */
class ActionSteeringSystem : public ActionInterface
{
public:
  /// Component Name
  const std::string componentname = "Action_SteeringSystem";

  /**
   * @brief Construct a new Action Steering System Implementation object
   *
   * \param [in] componentName   Name of the component
   * \param [in] isInit          Query whether the component was just initialized
   * \param [in] priority        Priority of the component
   * \param [in] offsetTime      Offset time of the component
   * \param [in] responseTime    Response time of the component
   * \param [in] cycleTime       Cycle time of this components trigger task [ms]
   * \param [in] stochastics     Provides access to the stochastics functionality of the framework
   * \param [in] world           Pointer to the world interface
   * \param [in] parameters      Interface provides access to the configuration parameters
   * \param [in] publisher       Instance  provided by the framework
   * \param [in] callbacks       Interface for callbacks to framework
   * \param [in] agent           This interface provides access to agent parameters, properties, attributes and dynamic states
   */
  ActionSteeringSystem(std::string componentName,
                       bool isInit,
                       int priority,
                       int offsetTime,
                       int responseTime,
                       int cycleTime,
                       StochasticsInterface *stochastics,
                       WorldInterface *world,
                       const ParameterInterface *parameters,
                       PublisherInterface *publisher,
                       const CallbackInterface *callbacks,
                       AgentInterface *agent);

  /*!
   * \brief Update Inputs
   *
   * Function is called by framework when another component delivers a signal over
   * a channel to this component (scheduler calls update taks of other component).
   *
   * Refer to module description for input channels and input ids.
   *
   * \param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
   * \param[in]     data           Referenced signal (copied by sending component)
   * \param[in]     time           Current scheduling time
   */
  void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time) override;

  /*!
   * \brief Update Outputs
   *
   * Function is called by framework when this Component has to deliver a output signal over
   * a channel to another component (scheduler calls update task of this component).
   *
   * Refer to module description for output channels and output ids.
   *
   * \param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
   * \param[out]    data           Referenced signal (copied by this component)
   * \param[in]     time           Current scheduling time
   */
  void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time) override;

  /*!
   * \brief Process data within component.
   *
   * Function is called by framework when the scheduler calls the trigger task
   * of this component.
   *
   * Refer to module description for information about the module's task.
   *
   * \param[in]     time           Current scheduling time
   */
  void Trigger(int time) override;

private:
  /*!
   * ------------------------------------------------------------------------
   * \brief Reads the information of static toe from the
   *        ParameterInterface and stores them in the Parameters struct.
   *
   * \param parameters ParameterInterface
   * ------------------------------------------------------------------------
   */
  void ParseParameters(const ParameterInterface *parameters);

  /*!
   * \brief Calculates the angle of each wheel
   *
   */
  void CalculateWheelAngle();

  std::map<int, ComponentPort *> outputPorts{};  //!< map for all OutputPort
  OutputPort<SignalVectorDouble, std::vector<double>> WheelAngle{0,
                                                                 &outputPorts};  //!< OutputPort for wheel angle (id=0)

  units::angle::radian_t SteeringWheelAngle{};  //!< steering wheel angle of vehicle
  std::vector<units::force::newton_t> LateralTireForce{0.0_N, 0.0_N, 0.0_N, 0.0_N};  //!< lateral tire force

  std::vector<double> steeringElasticity{};      //!< constant steering elasticity of steering system
  std::vector<units::angle::radian_t> toe{};     //!< static toe of each tire
  std::vector<units::length::meter_t> caster{};  //!< total caster of steering system
  double SteeringRatio{};                        //!< constant steering ratio of steering system
};

#endif
