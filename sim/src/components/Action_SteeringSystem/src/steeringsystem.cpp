/********************************************************************************
 * Copyright (c) 2023-2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  steeringsystem.cpp */
//-----------------------------------------------------------------------------

#include "steeringsystem.h"

#include <exception>
#include <utility>

#include "common/primitiveSignals.h"
#include "include/parameterInterface.h"

ActionSteeringSystem::ActionSteeringSystem(std::string componentName,
                                           bool isInit,
                                           int priority,
                                           int offsetTime,
                                           int responseTime,
                                           int cycleTime,
                                           StochasticsInterface *stochastics,
                                           WorldInterface *world,
                                           const ParameterInterface *parameters,
                                           PublisherInterface *const publisher,
                                           const CallbackInterface *callbacks,
                                           AgentInterface *agent)
    : ActionInterface(std::move(componentName),
                      isInit,
                      priority,
                      offsetTime,
                      responseTime,
                      cycleTime,
                      stochastics,
                      world,
                      parameters,
                      publisher,
                      callbacks,
                      agent)
{
  // Get Steering ratio from vehicle model parameters
  const std::shared_ptr<const mantle_api::VehicleProperties> vehicleProperties
      = std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(GetAgent()->GetVehicleModelParameters());

  const auto steeringRatio = helper::map::query(vehicleProperties->properties, Properties::Vehicle::STEERING_RATIO);
  THROWIFFALSE(steeringRatio.has_value(), "SteeringRatio was not defined in VehicleCatalog");
  SteeringRatio = std::stod(steeringRatio.value());
  // get external parameters
  ParseParameters(parameters);
}

void ActionSteeringSystem::ParseParameters(const ParameterInterface *parameters)
{
  std::vector<double> toeTemp = parameters->GetParametersDoubleVector().at("Toe");
  toe = std::vector<units::angle::radian_t>(toeTemp.begin(), toeTemp.end());

  steeringElasticity = parameters->GetParametersDoubleVector().at("Elasticity");

  std::vector<double> casterTemp = parameters->GetParametersDoubleVector().at("Caster");
  caster = std::vector<units::length::meter_t>(casterTemp.begin(), casterTemp.end());
}

void ActionSteeringSystem::UpdateInput(int localLinkId,
                                       const std::shared_ptr<SignalInterface const> &data,
                                       [[maybe_unused]] int time)
{
  std::stringstream log;
  log << componentname << " (component " << GetComponentName() << ", agent " << std::to_string(GetAgent()->GetId())
      << ", input data for local link " << localLinkId << ": ";
  LOG(CbkLogLevel::Debug, log.str());

  if (localLinkId == 0)
  {
    const std::shared_ptr<SteeringSignal const> signal = std::dynamic_pointer_cast<SteeringSignal const>(data);
    if (!signal)
    {
      const std::string msg = componentname + "_" + std::to_string(GetAgent()->GetId()) + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
    SteeringWheelAngle = signal->steeringWheelAngle;
  }
  if (localLinkId == 1)
  {
    const std::shared_ptr<SignalVectorDouble const> signal = std::dynamic_pointer_cast<SignalVectorDouble const>(data);
    if (!signal)
    {
      const std::string msg = componentname + "_" + std::to_string(GetAgent()->GetId()) + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }

    LateralTireForce = std::vector<units::force::newton_t>(signal->value.begin(), signal->value.end());
  }
}

void ActionSteeringSystem::UpdateOutput(int localLinkId,
                                        std::shared_ptr<SignalInterface const> &data,
                                        [[maybe_unused]] int time)
{
  std::stringstream log;
  log << componentname << " UpdateOutput";
  LOG(CbkLogLevel::Debug, log.str());
  log.str(std::string());

  const bool success = outputPorts.at(localLinkId)->GetSignalValue(data);

  if (success)
  {
    log << componentname << " UpdateOutput successful";
    LOG(CbkLogLevel::Debug, log.str());
  }
  else
  {
    log << componentname << " UpdateOutput failed";
    LOG(CbkLogLevel::Error, log.str());
  }
}

void ActionSteeringSystem::CalculateWheelAngle()
{
  std::vector<double> wheelAngle;
  wheelAngle.resize(4);

  for (unsigned int idx = 0; idx < 4; idx++)
  {
    if (static_cast<int>(std::floor(idx / 2)) < toe.size())
    {
      if (idx % 2 == 0)
      {
        wheelAngle.at(idx) = -toe.at(static_cast<int>(std::floor(idx / 2))).value();
      }
      else
      {
        wheelAngle[idx] = toe.at(static_cast<int>(std::floor(idx / 2))).value();
      }
    }
    if (static_cast<int>(std::floor(idx / 2)) < std::min(steeringElasticity.size(), caster.size()))
    {
      wheelAngle.at(idx)
          -= (LateralTireForce.at(idx).value() * caster.at(static_cast<int>(std::floor(idx / 2))).value())
           / steeringElasticity.at(static_cast<int>(std::floor(idx / 2)));
    }
  }
  wheelAngle.at(0) += SteeringWheelAngle.value() / SteeringRatio;
  wheelAngle.at(1) += SteeringWheelAngle.value() / SteeringRatio;

  WheelAngle.SetValue(wheelAngle);
}

void ActionSteeringSystem::Trigger([[maybe_unused]] int time)
{
  // Set OSI steering wheel angle
  GetAgent()->SetSteeringWheelAngle(SteeringWheelAngle);
  // calculate wheel angle
  CalculateWheelAngle();
}
