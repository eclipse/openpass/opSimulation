/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  action_steeringsystem.h
//! @brief contains DLL export declarations
//-----------------------------------------------------------------------------

#pragma once

#include "sim/src/common/opExport.h"

#if defined(ACTION_STEERINGSYSTEM_LIBRARY)
#define ACTION_STEERINGSYSTEM_SHARED_EXPORT OPEXPORT  //!< Export of the dll-functions
#else
#define ACTION_STEERINGSYSTEM_SHARED_EXPORT OPIMPORT  //!< Import of the dll-functions
#endif
