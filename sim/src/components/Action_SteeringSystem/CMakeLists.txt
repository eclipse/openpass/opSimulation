################################################################################
# Copyright (c) 2023-2024 Volkswagen AG
#               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_NAME Action_SteeringSystem)

add_compile_definitions(ACTION_STEERINGSYSTEM_LIBRARY)

add_openpass_target(
  NAME ${COMPONENT_NAME} TYPE library LINKAGE shared COMPONENT module

  HEADERS
    action_steeringsystem.h
    src/steeringsystem.h

  SOURCES
    action_steeringsystem.cpp
    src/steeringsystem.cpp

  LIBRARIES
	  MantleAPI::MantleAPI
  
  GUIXML
    Action_SteeringSystem.xml  							   
)
