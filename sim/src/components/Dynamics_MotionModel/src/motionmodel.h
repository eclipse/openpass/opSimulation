/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023-2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef DYNAMICSMOTIONMODEL_IMPLEMENTATION_H
#define DYNAMICSMOTIONMODEL_IMPLEMENTATION_H

#include "common/componentPorts.h"
#include "common/dynamicsSignal.h"
#include "common/globalDefinitions.h"
#include "common/primitiveSignals.h"
#include "common/steeringSignal.h"
#include "common/vector2d.h"
#include "common/vectorSignals.h"
#include "include/modelInterface.h"
#include "include/parameterInterface.h"
#include "twotrackmodel.h"

/**
 * @{
 * \addtogroup Vehicle_Dynamics
 *
 * @{
 * \addtogroup Motion_Model
 * \brief Dynamic component to model the dynamic of a two track vehicle.
 *
 * \details open-loop two-track model.
 *
 *
 * \section Motion_Model_Inputs Inputs
 * name | meaning
 * -----|---------
 * longitudinalTireForces | longitudinal tire forces from tire model
 * lateralTireForces | lateral tire forces from tire model
 * wheelAngle | tire angle from steering system
 * wheelSelfAligningTorque | self aligning torque of each tire from tire model
 *
 *
 *
 *
 * @}
 * @}
 */

/*!
 * \copydoc Motion_Model
 * \ingroup Motion_Model
 */
class DynamicsMotionModelImplementation : public UnrestrictedModelInterface
{
public:
  /// @brief Component name
  const std::string componentname = "Dynamics_MotionModel_TwoTrack";

  //! Constructor
  //!
  //! @param[in]     componentName  Name of the component
  //! @param[in]     isInit         Corresponds to "init" of "Component"
  //! @param[in]     priority       Corresponds to "priority" of "Component"
  //! @param[in]     offsetTime     Corresponds to "offsetTime" of "Component"
  //! @param[in]     responseTime   Corresponds to "responseTime" of "Component"
  //! @param[in]     cycleTime      Corresponds to "cycleTime" of "Component"
  //! @param[in]     stochastics    Pointer to the stochastics class loaded by the framework
  //! @param[in]     world          Pointer to the world
  //! @param[in]     parameters     Pointer to the parameters of the module
  //! @param[in]     publisher      Pointer to the publisher instance
  //! @param[in]     callbacks      Pointer to the callbacks
  //! @param[in]     agent          Pointer to agent instance
  DynamicsMotionModelImplementation(std::string componentName,
                                    bool isInit,
                                    int priority,
                                    int offsetTime,
                                    int responseTime,
                                    int cycleTime,
                                    StochasticsInterface *stochastics,
                                    WorldInterface *world,
                                    const ParameterInterface *parameters,
                                    PublisherInterface *publisher,
                                    const CallbackInterface *callbacks,
                                    AgentInterface *agent);
  DynamicsMotionModelImplementation(const DynamicsMotionModelImplementation &) = delete;
  DynamicsMotionModelImplementation(DynamicsMotionModelImplementation &&) = delete;
  DynamicsMotionModelImplementation &operator=(const DynamicsMotionModelImplementation &) = delete;
  DynamicsMotionModelImplementation &operator=(DynamicsMotionModelImplementation &&) = delete;

  //! Destructor
  ~DynamicsMotionModelImplementation() override;

  //! Function is called by framework when another component delivers a signal over
  //! a channel to this component (scheduler calls update taks of other component).
  //!
  //! @param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
  //! @param[in]     data           Referenced signal (copied by sending component)
  //! @param[in]     time           Current scheduling time
  //! @return                       True on success
  void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time) override;

  //! Function is called by framework when this component has to deliver a signal over
  //! a channel to another component (scheduler calls update task of this component).
  //!
  //! @param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
  //! @param[out]    data           Referenced signal (copied by this component)
  //! @param[in]     time           Current scheduling time
  //! @return                       True on success
  void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time) override;

  //! Function is called by framework when the scheduler calls the trigger task
  //! of this component
  //!
  //! @param[in]     time           Current scheduling time
  //! @return                       True on success
  void Trigger(int time) override;

private:
  /** \addtogroup Motion_Model
   *  @{
   *      \name InputPorts
   *      All input ports with PortId
   *      @{
   */
  std::map<int, ComponentPort *> inputPorts;                                                  //!< map for all InputPort
  InputPort<SignalVectorDouble, std::vector<double>> longitudinalTireForces{0, &inputPorts};  //!< wheel drive torque
  InputPort<SignalVectorDouble, std::vector<double>> lateralTireForces{1, &inputPorts};       //!< Wheel brake torque
  InputPort<SignalVectorDouble, std::vector<double>> wheelAngle{2, &inputPorts};  //!< Steering Angle of the wheels
  InputPort<SignalVectorDouble, std::vector<double>> wheelSelfAligningTorque{
      3, &inputPorts};                                                                   //!< Wheel self aligning torque
  InputPort<SignalVectorDouble, std::vector<double>> wheelRotationRate{4, &inputPorts};  //!< wheel rotation rate
  units::angle::radian_t steeringWheelAngle{};                                           //!< steeringWheelAngle
  /**
   *    @}
   *  @}
   */

  /** \addtogroup Motion_Model
   *  @{
   *      \name Output
   *      All output signals
   *      @{
   */
  std::vector<double>
      forceGlobalInertia;         //!< inertia force on vehicle's Center of Gravity (Input for Dynamics Chassis)
  DynamicsSignal dynamicsSignal;  //!< agents dynamics information
                                  /**
                                   *    @}
                                   *  @}
                                   */

  /** \addtogroup Motion_Model
   *  @{
   *    \name Internal states
   *    @{
   */
  //! Time step as double in s
  double timeStep;
  /**
   *    @}
   *  @}
   */

  /** \addtogroup MotionModel
   *  @{
   *    \name Internal objects
   *    @{
   */
  //! two tracck vehicle
  TwoTrackVehicleModel *vehicle;
  /**
   *    @}
   *  @}
   */

  //! Update data on agent's actual position, velocity and acceleration
  void ReadPreviousState();

  //! Calculate next position, translation velocity and translation acceleration of the agent
  void NextStateTranslation();

  //! Calculate next yaw angle, rotation velocity and rotation acceleration of the agent
  void NextStateRotation();

  //! Write next position, velocity and acceleration of the agent
  void NextStateSet();
};

#endif  // DYNAMICS_TWOTRACK_IMPLEMENTATION_H
