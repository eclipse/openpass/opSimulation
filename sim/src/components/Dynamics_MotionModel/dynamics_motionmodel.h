/********************************************************************************
 * Copyright (c) 2023-2024 Volkswagen AG
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  dynamics_motionmodel.h
//! @brief This file contains exported interface for model construction and
//!        destruction.
//-----------------------------------------------------------------------------

#include "sim/src/common/opExport.h"

#if defined(DYNAMICS_MOTIONMODEL_LIBRARY)
#define DYNAMICS_MOTIONMODEL_SHARED_EXPORT OPEXPORT  //!< Export of the dll-functions
#else
#define DYNAMICS_MOTIONMODEL_SHARED_EXPORT OPIMPORT  //!< Import of the dll-functions
#endif
