/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  algorithm_RouteControl_global.h
//! @brief This file defines dynamic library import/export.
//-----------------------------------------------------------------------------

#ifndef ALGORITHM_ROUTECONTROL_GLOBAL_H
#define ALGORITHM_ROUTECONTROL_GLOBAL_H

#include "sim/src/common/opExport.h"

#if defined(ALGORITHM_ROUTECONTROL_LIBRARY)
#define ALGORITHM_ROUTECONTROL_SHARED_EXPORT OPEXPORT
#else
#define ALGORITHM_ROUTECONTROL_SHARED_EXPORT OPIMPORT  ///< RouteControl algorithm
#endif

#endif  // ALGORITHM_ROUTECONTROL_GLOBAL_H
