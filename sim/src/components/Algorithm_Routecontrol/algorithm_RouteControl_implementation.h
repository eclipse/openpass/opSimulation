/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023-2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef ALGORITHM_ROUTECONTROL_IMPLEMENTATION_H
#define ALGORITHM_ROUTECONTROL_IMPLEMENTATION_H

#include <map>
#include <memory>
#include <new>
#include <string>
#include <units.h>
#include <vector>

#include "common/componentPorts.h"
#include "common/longitudinalSignal.h"
#include "common/primitiveSignals.h"
#include "common/speedActionSignal.h"
#include "common/steeringSignal.h"
#include "common/trajectorySignal.h"
#include "common/vector2d.h"
#include "include/modelInterface.h"

class AgentInterface;
class CallbackInterface;
class ParameterInterface;
class PublisherInterface;
class RouteControl;
class ScenarioControlInterface;
class SignalInterface;
class StochasticsInterface;
class WorldInterface;
struct WaypointData;

using namespace Common;

/**
 * \addtogroup Components_Special openPASS components special
 * @{
 * \addtogroup Algorithm_RouteControl
 *
 * \brief Algorithm that controls the route of the vehice,
 * taking a collection of points as a reference,
 * which describes the desired trajectory,
 * that the vehicle should follow.
 *
 * \details Depending on the "aggressiveness" Parameter,
 * the algorithm will drive along a route, simulating
 * how a driver with that level of aggressiveness would
 * follow that trajectory.
 *
 * @}
 */

/*!
 * \copydoc Algorithm_RouteControl
 * \ingroup Algorithm_RouteControl
 */
class Algorithm_Routecontrol_Implementation : public UnrestrictedControllStrategyModelInterface
{
public:
  /// Name of the current component
  const std::string COMPONENTNAME = "Algorithm_RouteControl";

  //! Constructor
  //!
  //! @param[in]     componentName     Name of the component
  //! @param[in]     isInit            Corresponds to "init" of "Component"
  //! @param[in]     priority          Corresponds to "priority" of "Component"
  //! @param[in]     offsetTime        Corresponds to "offsetTime" of "Component"
  //! @param[in]     responseTime      Corresponds to "responseTime" of "Component"
  //! @param[in]     cycleTime         Corresponds to "cycleTime" of "Component"
  //! @param[in]     world             Pointer to the world interface
  //! @param[in]     stochastics       Pointer to the stochastics class loaded by the framework
  //! @param[in]     parameters        Pointer to the parameters of the module
  //! @param[in]     publisher         Pointer to the publisher instance
  //! @param[in]     callbacks         Pointer to the callbacks
  //! @param[in]     agent             Pointer to agent instance
  //! @param[in]     scenarioControl   scenarioControl of entity
  Algorithm_Routecontrol_Implementation(std::string componentName,
                                        bool isInit,
                                        int priority,
                                        int offsetTime,
                                        int responseTime,
                                        int cycleTime,
                                        StochasticsInterface *stochastics,
                                        WorldInterface *world,
                                        const ParameterInterface *parameters,
                                        PublisherInterface *const publisher,
                                        const CallbackInterface *callbacks,
                                        AgentInterface *agent,
                                        std::shared_ptr<ScenarioControlInterface> scenarioControl);
  Algorithm_Routecontrol_Implementation(const Algorithm_Routecontrol_Implementation &) = delete;
  Algorithm_Routecontrol_Implementation(Algorithm_Routecontrol_Implementation &&) = delete;
  Algorithm_Routecontrol_Implementation &operator=(const Algorithm_Routecontrol_Implementation &) = delete;
  Algorithm_Routecontrol_Implementation &operator=(Algorithm_Routecontrol_Implementation &&) = delete;
  ~Algorithm_Routecontrol_Implementation() override;

  void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time) override;
  void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time) override;
  void Trigger(int timeMs) override;

private:
  //! Function to read in trajectory data given seperately
  //!
  //! @return                       Vector of waypoints comprising all trajectory data
  void ReadWayPointData();

  // std::map<int, ComponentPort *> inputPorts; //!< map for all InputPort
  /** \addtogroup Algorithm_RouteControl
   *  @{
   *      \name InputPorts
   *      All input ports with PortId
   *      @{
   */
  // InputPort<TrajectorySignal, openScenario::Trajectory> trajectory{0, &inputPorts}; //!< given trajectory to follow

  /** \ingroup Algorithm_RouteControl
   *  @{
   *      \name OutputPorts
   *      All output ports
   *      @{
   */
  double outSteeringWheelAngle{}, outBrakePedalPosition{}, outAcceleratorPedalPosition{}, outGear{};
  ComponentState outComponentState{};

  /**
   *      @}
   *  @}
   */

  /** \ingroup Algorithm_RouteControl
   *  @{
   *      \name External Parameter
   *      Parameter which are set externally in agentConfiguration file.
   *      @{
   */
  units::mass::kilogram_t weight{0.0};
  double drivingAggressiveness;  //!< indicates the level of driving aggressiveness of the driver
  double pedalsKp;               //!< Kp parameter of the pedals PID control
  double pedalsKi;               //!< Ki parameter of the pedals PID control
  double pedalsKd;               //!< Kd parameter of the pedals PID control
  double steeringKp;             //!< Kp parameter of the steering PID control
  double steeringKi;             //!< Ki parameter of the steering PID control
  double steeringKd;             //!< Kd parameter of the steering PID control
  /**
   *      @}
   *  @}
   */

  /**
   *    \name Internal objects
   *    @{
   */
  //local computation objects
  std::unique_ptr<std::vector<WaypointData>> waypoints;  //!< vector of waypoints that should be followed
  std::unique_ptr<RouteControl> routeControl;  //!< class containing the actual algorithms for the route control
  units::time::second_t mTimeStep{0.0};        //!< time step in s
                                               /**
                                                *    @}
                                                */
};

#endif  // ALGORITHM_ROUTECONTROL_IMPLEMENTATION_H
