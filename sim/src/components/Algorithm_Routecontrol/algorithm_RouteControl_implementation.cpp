/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023-2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * @defgroup module_rc Route Control
 * image html @cond RC_00_Overview.png @endcond "RouteControl function overview"
 * This algorithm receives a trajectory in two dimensions (x- and y-values in cartesian coordinates with corresponding
 * velocities and time steps). Thus, the actual position and velocity of the agent are analyzed and the deviation to the
 * desired values is obtained. In order to correct vehicle's position and velocity, following outputs are generated:
 * - throttle pedal state [0; 1]
 * - brake pedal state [0; 1]
 * - steering angle [-pi/4; pi/4]
 */

/**
 * @addtogroup module_rc
 * Furthermore, different driver types are considered. The driver type is described by an aggressiveness parameter (a
 * scaling factor between "0" and "1"). Lower aggressiveness introduces scaled bounds to the three outputs decribed
 * above image @cond html RC_30_Driver.png @endcond "RouteControl driver types" Abbreviations:
 * - CS = coordinate system
 */

/**
 * @ingroup module_rc
 * @defgroup init_rc Initialization
 */

/**
 * @ingroup module_rc
 * @defgroup sim_step_00_rc_start Simulation step entry
 */

/**
 * @ingroup module_rc
 * @defgroup sim_step_10_rc_error Calculation of errors
 */

/**
 * @ingroup module_rc
 * @defgroup sim_step_20_rc_steer Calculation of steering control
 * image html @cond RC_10_Steer.png @endcond "Steering control"
 */

/**
 * @ingroup module_rc
 * @defgroup sim_step_30_rc_accel Calculation of acceleration control
 * image html @cond RC_20_Accel.png @endcond "Acceleration control"
 */

/**
 * @ingroup module_rc
 * @defgroup sim_step_40_rc_out Simulation step output
 */

#include "algorithm_RouteControl_implementation.h"

#include <MantleAPI/Common/poly_line.h>
#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Common/trajectory.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/control_strategy.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <array>
#include <memory>
#include <optional>
#include <stdexcept>
#include <utility>
#include <variant>

#include "components/Algorithm_Routecontrol/routeControl.h"
#include "components/common/vehicleProperties.h"
#include "include/agentInterface.h"
#include "include/callbackInterface.h"
#include "include/parameterInterface.h"
#include "include/scenarioControlInterface.h"

class PublisherInterface;
class SignalInterface;
class StochasticsInterface;
class WorldInterface;

Algorithm_Routecontrol_Implementation::Algorithm_Routecontrol_Implementation(
    std::string componentName,
    bool isInit,
    int priority,
    int offsetTime,
    int responseTime,
    int cycleTime,
    StochasticsInterface *stochastics,
    WorldInterface *world,
    const ParameterInterface *parameters,
    PublisherInterface *const publisher,
    const CallbackInterface *callbacks,
    AgentInterface *agent,
    std::shared_ptr<ScenarioControlInterface> scenarioControl)
    : UnrestrictedControllStrategyModelInterface(std::move(componentName),
                                                 isInit,
                                                 priority,
                                                 offsetTime,
                                                 responseTime,
                                                 cycleTime,
                                                 stochastics,
                                                 world,
                                                 parameters,
                                                 publisher,
                                                 callbacks,
                                                 agent,
                                                 std::move(scenarioControl))
{
  LOGINFO("Constructing " + COMPONENTNAME + " for agent " + std::to_string(agent->GetId()));

  mTimeStep = units::time::second_t(static_cast<double>(cycleTime) / 1000.0);

  try
  {
    drivingAggressiveness = parameters->GetParametersDouble().at("DrivingAggressiveness");
    pedalsKp = parameters->GetParametersDouble().at("PedalsKp");
    pedalsKi = parameters->GetParametersDouble().at("PedalsKi");
    pedalsKd = parameters->GetParametersDouble().at("PedalsKd");
    steeringKp = parameters->GetParametersDouble().at("SteeringKp");
    steeringKi = parameters->GetParametersDouble().at("SteeringKi");
    steeringKd = parameters->GetParametersDouble().at("SteeringKd");
  }
  catch (...)
  {
    const std::string msg = COMPONENTNAME + " could not init parameters";
    LOG(CbkLogLevel::Error, msg);
    throw std::runtime_error(msg);
  }

  LOGDEBUG("Construction of " + COMPONENTNAME + " successful");
}

Algorithm_Routecontrol_Implementation::~Algorithm_Routecontrol_Implementation()
{
  waypoints.reset();
  routeControl.reset();
}

void Algorithm_Routecontrol_Implementation::UpdateInput(
    [[maybe_unused]] int localLinkId,
    [[maybe_unused]] const std::shared_ptr<SignalInterface const> &data,
    [[maybe_unused]] int time)
{
  LOGDEBUG(COMPONENTNAME + " UpdateInput");
}

void Algorithm_Routecontrol_Implementation::UpdateOutput(int localLinkId,
                                                         std::shared_ptr<SignalInterface const> &data,
                                                         [[maybe_unused]] int time)
{
  if (localLinkId == 0)
  {
    try
    {
      data = std::make_shared<LongitudinalSignal const>(
          outComponentState, outAcceleratorPedalPosition, outBrakePedalPosition, outGear, GetComponentName());
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg = COMPONENTNAME + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }
  else if (localLinkId == 1)
  {
    try
    {
      data = std::make_shared<SteeringSignal const>(
          outComponentState, outSteeringWheelAngle * 1_rad, GetComponentName());
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg = COMPONENTNAME + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void Algorithm_Routecontrol_Implementation::Trigger(int timeMs)
{
  /** @addtogroup init_rc
   * Read trajectory to be followed:
   *  - time steps
   *  - x-position in cartesian coordinates
   *  - y-position in cartesian coordinates
   *  - vehicle's longitudinal velocity in vehicle's CS
   */
  if (!routeControl)  // only allocate and initialize once
  {
    routeControl = std::make_unique<RouteControl>(mTimeStep);
    /** @addtogroup init_rc
     * Define vehicle's and driver's characteristics:
     *  - total vehicle mass
     *  - engine power
     *  - maximum brake torque
     *  - driver aggressiveness
     */
    std::shared_ptr<const mantle_api::VehicleProperties> vehicleProperties
        = std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(GetAgent()->GetVehicleModelParameters());

    auto weight = GetAgent()->GetVehicleModelParameters()->mass;

    auto maxpower = helper::map::query(vehicleProperties->properties, "MaximumEnginePower");
    THROWIFFALSE(maxpower.has_value(), "MaximumEnginePower was not defined in VehicleCatalog");

    auto mintorque = helper::map::query(vehicleProperties->properties, "MinimumEngineTorque");
    THROWIFFALSE(mintorque.has_value(), "MinimumEngineTorque was not defined in VehicleCatalog");

    routeControl->SetVehicleProperties(weight,
                                       std::stod(maxpower.value()),
                                       units::torque::newton_meter_t(std::stod(mintorque.value())),
                                       drivingAggressiveness);

    routeControl->SetPIDParameters(pedalsKp, pedalsKi, pedalsKd, steeringKp, steeringKi, steeringKd);
  }

  if (!waypoints)  // only allocate once
  {
    ReadWayPointData();  // for periodic trajectory update: move this line outside the IF statement
  }

  routeControl->SetRequestedTrajectory(*waypoints);

  if (routeControl == nullptr)
  {
    LOGERROR(COMPONENTNAME + " Trigger not callable");
    return;
  }

  // perform calculations
  routeControl->Perform(units::time::millisecond_t(timeMs),
                        GetAgent()->GetPositionX(),
                        GetAgent()->GetPositionY(),
                        GetAgent()->GetYaw(),
                        GetAgent()->GetVelocity().Length());

  LOGDEBUG(COMPONENTNAME + " output (agent " + std::to_string(GetAgent()->GetId())
           + ") : FrontWheelAngle = " + std::to_string(180 / 3.14 * routeControl->GetFrontWheelAngle().value())
           + ", ThrottlePedal = " + std::to_string(routeControl->GetThrottlePedal())
           + ", BrakePedal = " + std::to_string(routeControl->GetBrakePedal()));

  /** @addtogroup sim_step_40_rc_out
   * Write new output signals:
   * - steering angle
   * - throttle pedal state
   * - brake pedal state
   * - brake superposition state
   */
  auto steeringRatio
      = helper::map::query(GetAgent()->GetVehicleModelParameters()->properties, Properties::Vehicle::STEERING_RATIO);
  THROWIFFALSE(steeringRatio.has_value(), "SteeringRatio was not defined in VehicleCatalog");
  outBrakePedalPosition = routeControl->GetBrakePedal();
  outSteeringWheelAngle = routeControl->GetFrontWheelAngle().value() * std::stod(steeringRatio.value());
  outAcceleratorPedalPosition = routeControl->GetThrottlePedal();
  outGear = 1;
  outComponentState = ComponentState::Acting;

  LOGDEBUG(COMPONENTNAME + " Trigger successful");
}

void Algorithm_Routecontrol_Implementation::ReadWayPointData()
{
  const auto controlStrategies
      = GetScenarioControl()->GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory);
  if (controlStrategies.empty())
  {
    return;
  }
  const auto &trajectory = std::get<mantle_api::PolyLine>(
      std::dynamic_pointer_cast<mantle_api::FollowTrajectoryControlStrategy const>(controlStrategies.front())
          ->trajectory.type);
  unsigned int n = trajectory.size();

  waypoints = std::make_unique<std::vector<WaypointData>>(n);

  units::velocity::meters_per_second_t vel{0.0};

  for (unsigned int i = 0; i < n; ++i)
  {
    auto polyLinePoint = trajectory.at(i);

    if (!polyLinePoint.time.has_value())
    {
      LOGERROR(COMPONENTNAME + " ReadWayPointData failed. Time in PolyLinePoint not set.");
    }
    waypoints->at(i).time = polyLinePoint.time.value();  // s
    waypoints->at(i).position.x = polyLinePoint.pose.position.x;
    waypoints->at(i).position.y = polyLinePoint.pose.position.y;
    if (i < n - 1)
    {
      auto polyLinePointNext = trajectory.at(i + 1);
      vel = units::math::sqrt(units::math::pow<2>(polyLinePointNext.pose.position.x - polyLinePoint.pose.position.x)
                              + units::math::pow<2>(polyLinePointNext.pose.position.y - polyLinePoint.pose.position.y))
          / mTimeStep;  // uniform motion approximation
    }
    waypoints->at(i).longVelocity = vel;
  }
}
