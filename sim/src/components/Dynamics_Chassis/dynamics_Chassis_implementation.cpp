/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023-2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * @defgroup module_3dc 3D Chassis
 * The dynamics component for simulating suspension and deformation of a vehicle due to inertia forces.
 * image html @cond \ref 3DT_Pitch.png "Force Analysis in Pitch with Deformation" @endcond
 * image html @cond \ref 3DT_Roll.png "Force Analysis in Roll with Deformation" @endcond
 */

/**
 * @ingroup module_3dc
 * @defgroup init_3dc Initialization
 */
/**
 * @ingroup module_3dc
 * @defgroup sim_step_3dc Entry
 */
/**
 * @ingroup module_3dc
 * @defgroup sim_step_3dc_2 Process
 */
/**
 * @ingroup module_3dc
 * @defgroup sim_step_3 Output
 */

#include "dynamics_Chassis_implementation.h"

#include <MantleAPI/Common/bounding_box.h>
#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <algorithm>
#include <boost/format.hpp>
#include <stdexcept>
#include <utility>

#include "common/commonTools.h"
#include "components/Dynamics_Chassis/ForceWheelZ.h"
#include "components/Dynamics_Chassis/VehicleBasics.h"
#include "components/Dynamics_Chassis/WheelOscillation.h"
#include "components/common/vehicleProperties.h"
#include "include/agentInterface.h"
#include "include/callbackInterface.h"
#include "include/parameterInterface.h"

class PublisherInterface;
class SignalInterface;
class StochasticsInterface;
class WorldInterface;

class PublisherInterface;
class SignalInterface;
class StochasticsInterface;
class WorldInterface;

DynamicsChassisImplementation::DynamicsChassisImplementation(std::string componentName,
                                                             bool isInit,
                                                             int priority,
                                                             int offsetTime,
                                                             int responseTime,
                                                             int cycleTime,
                                                             StochasticsInterface *stochastics,
                                                             WorldInterface *world,
                                                             const ParameterInterface *parameters,
                                                             PublisherInterface *const publisher,
                                                             const CallbackInterface *callbacks,
                                                             AgentInterface *agent)
    : DynamicsInterface(std::move(componentName),
                        isInit,
                        priority,
                        offsetTime,
                        responseTime,
                        cycleTime,
                        stochastics,
                        world,
                        parameters,
                        publisher,
                        callbacks,
                        agent)
{
  LOGINFO((boost::format("Constructing Dynamics_Chassis for agent %d...") % agent->GetId()).str());

  /** @addtogroup init_3dc
   * Get predefined parameters from the component's XML file.
   */
  springCoefficient = parameters->GetParametersDoubleVector().at("SpringCoefficient");
  damperCoefficient = parameters->GetParametersDoubleVector().at("DamperCoefficient");

  /** @addtogroup init_3dc
   * Get basic parameters of the ego vehicle.
   */
  if (agent->GetVehicleModelParameters()->type != mantle_api::EntityType::kVehicle)
  {
    throw std::runtime_error("Invalid EntityType for Component " + GetComponentName()
                             + ". Component can only be used by vehicles.");
  }

  vehicleProperties
      = std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(agent->GetVehicleModelParameters());

  const auto wheelBase = vehicleProperties->front_axle.bb_center_to_axle_center.x
                       - vehicleProperties->rear_axle.bb_center_to_axle_center.x;
  const auto trackWidthFront = vehicleProperties->front_axle.track_width;
  const auto trackWidthRear = vehicleProperties->rear_axle.track_width;
  units::length::meter_t hCOG;
  units::length::meter_t xCOG;
  units::length::meter_t yCOG;
  auto mass = GetAgent()->GetVehicleModelParameters()->mass;

  std::optional<std::string> const xPositionCog
      = helper::map::query(vehicleProperties->properties, Properties::Vehicle::POSITION_COG_X);
  std::optional<std::string> const yPositionCog
      = helper::map::query(vehicleProperties->properties, Properties::Vehicle::POSITION_COG_Y);
  std::optional<std::string> const zPositionCog
      = helper::map::query(vehicleProperties->properties, Properties::Vehicle::POSITION_COG_Z);

  if (zPositionCog.has_value())
  {
    hCOG = std::stod(zPositionCog.value()) * 1_m;
  }
  else
  {
    hCOG = vehicleProperties->bounding_box.dimension.height / 2.0;
  }

  if (xPositionCog.has_value())
  {
    xCOG = std::stod(xPositionCog.value()) * 1_m;
  }
  else
  {
    xCOG = wheelBase / 2.0;
  }

  if (yPositionCog.has_value())
  {
    yCOG = std::stod(yPositionCog.value()) * 1_m;
  }
  else
  {
    yCOG = 0_m;
  }

  units::length::meter_t const lenFront = wheelBase - xCOG;
  units::length::meter_t const lenRear = xCOG;

  std::vector<units::length::meter_t> const lLeft = {trackWidthFront * 0.5 - yCOG, trackWidthRear * 0.5 - yCOG};
  std::vector<units::length::meter_t> const lRight = {trackWidthFront * 0.5 + yCOG, trackWidthRear * 0.5 + yCOG};

  // define car parameters including lLeft, lRight, lFront, lRear, hMC, mass
  carParam = VehicleBasics(lLeft, lRight, lenFront, lenRear, hCOG, mass);

  /** @addtogroup init_3dc
   * For each wheel, initialize the correspondig oscillation object for later simulation.
   */
  for (int i = 0; i < NUMBER_WHEELS; i++)
  {
    oscillations.at(i).Init(i,
                            GetCycleTime() * 0.001,
                            springCoefficient[static_cast<int>(floor(i / 2))],
                            damperCoefficient[static_cast<int>(floor(i / 2))]);
  }

  LOGDEBUG((boost::format("Chassis: agent info: DistanceCOGtoFrontAxle %.2f, DistanceCOGtoRearAxle %.2f, trackWidth "
                          "%.2f, heightCOG %.2f, mass %.2f")
            % lenFront.value() % lenRear.value() % trackWidthFront.value() % hCOG.value() % mass.value())
               .str());

  pitchZ = 0.0_m;
  rollZ = 0.0_m;

  LOGINFO("Constructing Dynamics_Chassis successful");
}

DynamicsChassisImplementation::~DynamicsChassisImplementation() = default;

void DynamicsChassisImplementation::UpdateInput(int localLinkId,
                                                const std::shared_ptr<SignalInterface const> &data,
                                                [[maybe_unused]] int time)
{
  bool const success = inputPorts.at(localLinkId)->SetSignalValue(data);
  if (success)
  {
    LOGDEBUG((boost::format("Dynamics_Chassis: Update input #%d successful") % localLinkId).str());
  }
  else
  {
    LOGERROR((boost::format("Dynamics_Chassis: Update input #%d failed") % localLinkId).str());
  }
}

void DynamicsChassisImplementation::UpdateOutput(int localLinkId,
                                                 std::shared_ptr<SignalInterface const> &data,
                                                 [[maybe_unused]] int time)
{
  bool const success = outputPorts.at(localLinkId)->GetSignalValue(data);
  if (success)
  {
    LOGDEBUG((boost::format("Dynamics_Chassis: Update output #%d successful") % localLinkId).str());
  }
  else
  {
    LOGERROR((boost::format("Dynamics_Chassis: Update output #%d failed") % localLinkId).str());
  }
}

void DynamicsChassisImplementation::Trigger([[maybe_unused]] int time)
{
  /** @addtogroup sim_step_3dc
   * Get the inputs of inertia forces.
   */
  units::force::newton_t fInertiaX{0.0};
  units::force::newton_t fInertiaY{0.0};
  if (inertiaForce.GetValue().size() == 2)
  {
    fInertiaX = units::force::newton_t(inertiaForce.GetValue().at(0));
    fInertiaY = units::force::newton_t(inertiaForce.GetValue().at(1));
  }

  /** @addtogroup sim_step_3dc_2
   * Calculate the vehicle deformation and additional vertical forces against inertia.
   */
  // calculate the vertical force
  if (!wheelForces.CalForce(fInertiaX, fInertiaY, pitchZ, rollZ, carParam))
  {
    return;
  }

  units::force::newton_t const originalForce = -carParam.mass / NUMBER_WHEELS * GRAVITY_ACC;

  /** @addtogroup sim_step_3dc_2
   * For each wheel, calculate the total vertical force as well as the Z-position.
   */
  units::force::newton_t forceZ;
  units::mass::kilogram_t wheelMass;
  std::vector<double> resForces;
  for (int i = 0; i < NUMBER_WHEELS; i++)
  {
    // calculate deformation due to the vertical force
    wheelMass = carParam.GetWheelMass(i);  // the mass is distributed based on deformation
    forceZ = -wheelMass * GRAVITY_ACC - wheelForces.GetForce(i);
    if (forceZ > -1.0_N)
    {
      forceZ = -1.0_N;  // glue the tires to the ground and limit to at least 1N
    }
    resForces.push_back(std::fabs(forceZ.value()));

    oscillations.at(i).Perform(-wheelForces.GetForce(i), wheelMass);
  }

  LOGDEBUG((boost::format("Chassis %d at time %d: inertia force %f, %f, weight force %f") % GetAgent()->GetId() % time
            % fInertiaX.value() % fInertiaY.value() % originalForce.value())
               .str());
  LOGDEBUG((boost::format("Chassis %d at time %d: zForce %f, %f, %f, %f") % GetAgent()->GetId() % time % resForces.at(0)
            % resForces.at(1) % resForces.at(2) % resForces.at(3))
               .str());
  LOGDEBUG((boost::format("Chassis %d at time %d: zPos %f, %f, %f, %f") % GetAgent()->GetId() % time
            % oscillations[0].GetCurZPos() % oscillations[1].GetCurZPos() % oscillations[2].GetCurZPos()
            % oscillations[3].GetCurZPos())
               .str());

  /** @addtogroup sim_step_3dc_2
   * Calculate the Z-position of the vehicle in both longitudinal and lateral directions.
   */
  // the pitchZ is positive if pitching backward, otherwise negative
  // the rollZ is positive if rolling right, otherwise negative
  pitchZ = units::length::meter_t((oscillations[0].GetCurZPos() + oscillations[1].GetCurZPos()) / 2.0
                                  - (oscillations[2].GetCurZPos() + oscillations[3].GetCurZPos()) / 2.0);
  rollZ = units::length::meter_t((oscillations[0].GetCurZPos() + oscillations[2].GetCurZPos()) / 2.0
                                 - (oscillations[1].GetCurZPos() + oscillations[3].GetCurZPos()) / 2.0);

  /** @addtogroup sim_step_3
   * Output local vertical forces for the two-track dynamics.
   */
  verticalForce.SetValue(resForces);
}
