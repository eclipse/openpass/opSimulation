/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  dynamics_Chassis_global.h
//! @brief This file contains DLL export declarations
//-----------------------------------------------------------------------------

#ifndef DYNAMICS_CHASSIS_GLOBAL_H
#define DYNAMICS_CHASSIS_GLOBAL_H

#include "sim/src/common/opExport.h"

#if defined(DYNAMICS_CHASSIS_LIBRARY)
#define DYNAMICS_CHASSIS_SHARED_EXPORT OPEXPORT  //!< Export of the dll-functions
#else
#define DYNAMICS_CHASSIS_SHARED_EXPORT OPIMPORT  //!< Import of the dll-functions
#endif

#endif  // DYNAMICS_CHASSIS_GLOBAL_H
