/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "WheelOscillation.h"

WheelOscillation::WheelOscillation(int wId, double coeffSpringK, double coeffDampQ)
    : id(wId), coeffSpring(coeffSpringK), coeffDamp(coeffDampQ)
{
}

void WheelOscillation::Init(int wId, double timeStepInit, double coeffSpringK, double coeffDampQ)
{
  id = wId;
  timeStep = timeStepInit;
  coeffSpring = coeffSpringK;
  coeffDamp = coeffDampQ;
}

void WheelOscillation::Perform(units::force::newton_t forceZ, units::mass::kilogram_t mass)
{
  /*
   *  forceZ + counter_force = m * Az
   *  forceZ + fSpring + fDamp = m * Az
   *  forceZ - K * Z - Q * Vz= m * Az
   *     Z = prevZ + prevVz * dt + 0.5 * Az * dt^2
   *     Vz = prevVz + Az * dt
   *  forceZ - K * (prevZ + prevVz * dt + 0.5 * Az * dt^2) - Q * (prevVz + Az * dt) = m * Az
   *  forceZ - K * prevZ - K * prevVz * dt - Q * prevVz = (m + 0.5 * K * dt^2 + Q * dt ) * Az
   */

  curAz = (forceZ.value() - coeffSpring * curZ - coeffDamp * curVz) / (mass.value());
  curVz = prevVz + curAz * timeStep;
  curZ += prevVz * timeStep + 0.5 * curAz * timeStep * timeStep;

  curZ = (curZ > maxZ) ? maxZ : curZ;
  curZ = (curZ < minZ) ? minZ : curZ;

  // prepare for the next iteration
  prevVz = curVz;
}
