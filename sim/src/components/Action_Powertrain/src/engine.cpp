/********************************************************************************
 * Copyright (c) 2023-2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "engine.h"

#include <cmath>

Engine::Engine(units::power::watt_t powerEngine,
               units::frequency::hertz_t speedEngine,
               units::torque::newton_meter_t torqueEngine)
    : powerEngineLimit(powerEngine), speedEngineLimit(speedEngine), torqueEngineLimit(torqueEngine)
{
}

void Engine::CalculateDriveTorque(double throttlePosition, units::frequency::hertz_t engineSpeed)
{
  units::torque::newton_meter_t torqueEngineMax;
  if (std::numeric_limits<double>::epsilon() < std::abs(engineSpeed.value()))
  {
    torqueEngineMax = powerEngineLimit / (engineSpeed * 2 * M_PI);
  }
  else
  {
    torqueEngineMax = powerEngineLimit / (0.0001_Hz * 2 * M_PI);
  }

  units::torque::newton_meter_t currentTorqueEngineLimit = torqueEngineLimit;
  if (engineSpeed >= 0.98 * speedEngineLimit)
  {
    currentTorqueEngineLimit
        = torqueEngineLimit - torqueEngineLimit * (1 - (speedEngineLimit - engineSpeed) / speedEngineLimit);
  }
  torqueEngineMax = std::clamp<units::torque::newton_meter_t>(torqueEngineMax, 0.0_Nm, currentTorqueEngineLimit);

  driveTorque = throttlePosition * torqueEngineMax;
}

units::torque::newton_meter_t Engine::GetDriveTorque()
{
  return driveTorque;
}
