/********************************************************************************
 * Copyright (c) 2023-2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "powertrain.h"

#include <boost/format.hpp>
#include <cstdio>
#include <exception>
#include <utility>

#include "common/primitiveSignals.h"
#include "include/parameterInterface.h"

ActionPowertrain::ActionPowertrain(std::string componentName,
                                   bool isInit,
                                   int priority,
                                   int offsetTime,
                                   int responseTime,
                                   int cycleTime,
                                   StochasticsInterface *stochastics,
                                   const ParameterInterface *parameters,
                                   PublisherInterface *const publisher,
                                   const CallbackInterface *callbacks,
                                   AgentInterface *agent)
    : AlgorithmInterface(std::move(componentName),
                         isInit,
                         priority,
                         offsetTime,
                         responseTime,
                         cycleTime,
                         stochastics,
                         parameters,
                         publisher,
                         callbacks,
                         agent)
{
  LOGINFO((boost::format("Constructing PowerTrain for agent %d...") % agent->GetId()).str());

  const std::shared_ptr<const mantle_api::VehicleProperties> vehicleProperties
      = std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(GetAgent()->GetVehicleModelParameters());

  auto powerEngineMax = helper::map::query(vehicleProperties->properties, "MaximumEnginePower");
  THROWIFFALSE(powerEngineMax.has_value(), "MaximumEnginePower was not defined in VehicleCatalog");

  auto speedEngineMax = helper::map::query(vehicleProperties->properties, "MaximumEngineSpeed");
  THROWIFFALSE(speedEngineMax.has_value(), "MaximumEngineSpeed was not defined in VehicleCatalog");

  auto torqueEngineMax = helper::map::query(vehicleProperties->properties, "MaximumEngineTorque");
  THROWIFFALSE(torqueEngineMax.has_value(), "MaximumEngineTorque was not defined in VehicleCatalog");

  engine = std::make_unique<Engine>((std::stod(powerEngineMax.value()) * 1_W),
                                    ((std::stod(speedEngineMax.value()) / 60.0) * 1_Hz),
                                    (std::stod(torqueEngineMax.value()) * 1_Nm));

  auto axleRatio = helper::map::query(vehicleProperties->properties, "AxleRatio");
  THROWIFFALSE(axleRatio.has_value(), "AxleRatio was not defined in VehicleCatalog");
  std::vector<double> gearRatioVector;
  int gear = 1;
  auto gearRatio
      = helper::map::query(vehicleProperties->properties, Properties::Vehicle::GEAR_RATIO + std::to_string(gear));
  while (gearRatio.has_value())
  {
    gearRatioVector.push_back(std::stod(gearRatio.value()));
    gear++;
    gearRatio
        = helper::map::query(vehicleProperties->properties, Properties::Vehicle::GEAR_RATIO + std::to_string(gear));
  }
  Common::Vector2d velocityCar = GetAgent()->GetVelocity();
  velocityCar.Rotate(GetAgent()->GetYaw());

  std::vector<mantle_api::Axle> axles;
  axles.push_back(vehicleProperties->front_axle);
  axles.push_back(vehicleProperties->rear_axle);
  wheelRotationRate.resize(4);
  for (int i = 0; i < axles.size(); ++i)
  {
    const units::length::meter_t rTire = axles[i].wheel_diameter / 2;
    const units::angular_velocity::radians_per_second_t rotationVelocityTire
        = 1.0_rad_per_s * (velocityCar.x.value() / rTire.value());
    wheelRotationRate[static_cast<int>(2 * i)] = rotationVelocityTire;
    wheelRotationRate[static_cast<int>(2 * i + 1)] = rotationVelocityTire;
  }
  transmission = std::make_unique<Transmission>(gearRatioVector, std::stod(axleRatio.value()));
  TypeDrivetrain = parameters->GetParametersString().at("TypeDrivetrain");
  FrontRatioAWD = parameters->GetParametersDouble().at("FrontRatioAWD");
}

ActionPowertrain::~ActionPowertrain()
{
  engine.reset();
  transmission.reset();
}

void ActionPowertrain::UpdateInput(int localLinkId,
                                   const std::shared_ptr<SignalInterface const> &data,
                                   [[maybe_unused]] int time)
{
  std::stringstream log;
  log << componentname << " (component " << GetComponentName() << ", agent " << std::to_string(GetAgent()->GetId())
      << ", input data for local link " << localLinkId << ": ";
  LOG(CbkLogLevel::Debug, log.str());

  if (localLinkId == 0)
  {
    const std::shared_ptr<LongitudinalSignal const> signal = std::dynamic_pointer_cast<LongitudinalSignal const>(data);
    if (!signal)
    {
      const std::string msg = componentname + "_" + std::to_string(GetAgent()->GetId()) + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
    longitudinalSignal = *signal;
  }
  else if (localLinkId == 1)
  {
    const std::shared_ptr<SignalVectorDouble const> signal = std::dynamic_pointer_cast<SignalVectorDouble const>(data);
    if (!signal)
    {
      const std::string msg = componentname + "_" + std::to_string(GetAgent()->GetId()) + " invalid signaltype";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }

    wheelRotationRate
        = std::vector<units::angular_velocity::radians_per_second_t>(signal->value.begin(), signal->value.end());
  }
  else
  {
    const std::string msg = componentname + "_" + std::to_string(GetAgent()->GetId()) + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void ActionPowertrain::UpdateOutput(int localLinkId,
                                    std::shared_ptr<SignalInterface const> &signal,
                                    [[maybe_unused]] int time)
{
  std::stringstream log;
  log << componentname << " UpdateOutput";
  LOG(CbkLogLevel::Debug, log.str());
  log.str(std::string());

  const bool success = outputPorts.at(localLinkId)->GetSignalValue(signal);

  if (success)
  {
    log << componentname << " UpdateOutput successful";
    LOG(CbkLogLevel::Debug, log.str());
  }
  else
  {
    log << componentname << " UpdateOutput failed";
    LOG(CbkLogLevel::Error, log.str());
  }
}

void ActionPowertrain::Trigger([[maybe_unused]] int time)
{
  // calculate Engine Speed (average wheel rotation rate / gear ratio)
  units::frequency::hertz_t transmissionRotationRate = 0_Hz;
  if (TypeDrivetrain == "FWD")
  {
    transmissionRotationRate = (wheelRotationRate[0] + wheelRotationRate[1]).value() / (2_s * 2 * M_PI);
  }
  else if (TypeDrivetrain == "RWD")
  {
    transmissionRotationRate = (wheelRotationRate[2] + wheelRotationRate[3]).value() / (2_s * 2 * M_PI);
  }
  else if (TypeDrivetrain == "AWD")
  {
    transmissionRotationRate
        = (wheelRotationRate[0] + wheelRotationRate[1] + wheelRotationRate[2] + wheelRotationRate[3]).value()
        / ((4_s * 2 * M_PI));
  }

  units::frequency::hertz_t const engineSpeed
      = transmission->GetEngineSpeed(transmissionRotationRate, longitudinalSignal.gear);
  // Calculate current engine torque
  engine->CalculateDriveTorque(longitudinalSignal.accPedalPos, engineSpeed);

  std::vector<double> torque(4);

  // Set tire torque depending on drivetrain type
  if (TypeDrivetrain == "FWD")
  {
    torque[0] = engine->GetDriveTorque().value() * transmission->GetGearRatio(longitudinalSignal.gear) / 2;
    torque[1] = torque[0];
    torque[2] = 0;
    torque[3] = 0;
  }
  else if (TypeDrivetrain == "RWD")
  {
    torque[2] = engine->GetDriveTorque().value() * transmission->GetGearRatio(longitudinalSignal.gear) / 2;
    torque[3] = torque[2];
    torque[0] = 0;
    torque[1] = 0;
  }
  else if (TypeDrivetrain == "AWD")
  {
    torque[0]
        = engine->GetDriveTorque().value() * transmission->GetGearRatio(longitudinalSignal.gear) / 2 * FrontRatioAWD;
    torque[1] = torque[0];
    torque[2] = engine->GetDriveTorque().value() * transmission->GetGearRatio(longitudinalSignal.gear) / 2
              * (1 - FrontRatioAWD);
    torque[3] = torque[2];
  }

  DriveTorqueTires.SetValue(torque);
}
