/********************************************************************************
 * Copyright (c) 2023-2024 Volkswagen AG
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/** \addtogroup Vehicle_Dynamics
 *@{
 * \addtogroup Powertrain
 * \brief  This module represents a vehicle powertrain.
 *
 * \details The module represents the powertrain. It consists of two submodules.
 * The gearbox translates torque and torque based on the gear engaged.
 * The engine generates the required torque based on its characteristics and accelerator pedal position.
 * The drive type can be selected via an external parameter
 *
 * \section Powertrain_Inputs Inputs
 * name | meaning
 * -----|---------
 * WheelRotationRate | current rotation rate of each wheel
 * Longitudinal Signal | current acceleration pedal position of vehicle
 *
 * \section Powertrain_Outputs Outputs
 * name | meaning
 * -----|---------
 * DriveTorqueTires | current drive torque of each tire
 *
 * @} */

#ifndef POWERTRAIN_H
#define POWERTRAIN_H

#include <map>
#include <unordered_map>

#include "common/componentPorts.h"
#include "common/longitudinalSignal.h"
#include "common/primitiveSignals.h"
#include "common/vector2d.h"
#include "common/vectorSignals.h"
#include "components/common/vehicleProperties.h"
#include "engine.h"
#include "include/modelInterface.h"
#include "include/parameterInterface.h"
#include "transmission.h"

/*!
 * \copydoc Powertrain
 * \ingroup Powertrain
 */
class ActionPowertrain : public AlgorithmInterface
{
public:
  /// Component Name
  const std::string componentname = "Powertrain";

  /**
   * @brief Construct a new Action Powertrain Implementation object
   *
   * @param [in] componentName   Name of the component
   * @param [in] isInit          Query whether the component was just initialized
   * @param [in] priority        Priority of the component
   * @param [in] offsetTime      Offset time of the component
   * @param [in] responseTime    Response time of the component
   * @param [in] cycleTime       Cycle time of this components trigger task [ms]
   * @param [in] stochastics     Provides access to the stochastics functionality of the framework
   * @param [in] parameters      Interface provides access to the configuration parameters
   * @param [in] publisher       Instance  provided by the framework
   * @param [in] callbacks       Interface for callbacks to framework
   * @param [in] agent           This interface provides access to agent parameters, properties, attributes and dynamic
   * states
   */
  ActionPowertrain(std::string componentName,
                   bool isInit,
                   int priority,
                   int offsetTime,
                   int responseTime,
                   int cycleTime,
                   StochasticsInterface *stochastics,
                   const ParameterInterface *parameters,
                   PublisherInterface *publisher,
                   const CallbackInterface *callbacks,
                   AgentInterface *agent);

  ~ActionPowertrain() override;
  /*!
   * \brief Holds the signal if there are other with higher priority.
   *
   * @param[in]    localLinkId    Input local link id.
   * @param[in]    data           Signal.
   * @param[in]    time           Timestamp in milliseconds.
   */
  void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time) override;

  /*!
   * \brief Changes the signal to a signal on hold if there is still on on hold in the current time step.
   *
   * @param[in]    localLinkId    Input local link id.
   * @param[in]    signal         Signal.
   * @param[in]    time           Timestamp in milliseconds.
   */
  void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &signal, int time) override;

  void Trigger(int time) override;

private:
  /*!
   * \brief Translates the string/int parameter representation to the internally used representation of priorities
   *
   * \param[in] priorities   map linking moduleIds to priorities (larger = higher priority)
   */

  std::string TypeDrivetrain;  //!< type of drivetrain
  double FrontRatioAWD;        //!< torque ratio AWD

  LongitudinalSignal longitudinalSignal;  //!< input for acceleration pedal position
  std::vector<units::angular_velocity::radians_per_second_t> wheelRotationRate;  //!< input for wheel rotation rate

  std::map<int, ComponentPort *> outputPorts;  //!< map for all OutputPort

  OutputPort<SignalVectorDouble, std::vector<double>> DriveTorqueTires{
      0, &outputPorts};  //!< Output port for drive torque of each tire (id=0)

  std::unique_ptr<Engine> engine;              //!< Engine submodule
  std::unique_ptr<Transmission> transmission;  //!< Transmission submodule
};

#endif
