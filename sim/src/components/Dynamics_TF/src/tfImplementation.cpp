/********************************************************************************
 * Copyright (c) 2018-2020 in-tech GmbH
 *               2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  tfImplementation.cpp */
//-----------------------------------------------------------------------------

#include "tfImplementation.h"

#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/control_strategy.h>
#include <map>
#include <memory>
#include <new>
#include <optional>
#include <stdexcept>
#include <utility>
#include <variant>

#include "common/accelerationSignal.h"
#include "common/globalDefinitions.h"
#include "common/hypot.h"
#include "common/steeringSignal.h"
#include "include/agentInterface.h"
#include "include/callbackInterface.h"
#include "include/dataBufferInterface.h"
#include "include/parameterInterface.h"
#include "include/publisherInterface.h"
#include "include/scenarioControlInterface.h"

class StochasticsInterface;
class WorldInterface;

TrajectoryFollowerImplementation::TrajectoryFollowerImplementation(
    std::string componentName,
    bool isInit,
    int priority,
    int offsetTime,
    int responseTime,
    int cycleTime,
    StochasticsInterface *stochastics,
    WorldInterface *world,
    const ParameterInterface *parameters,
    PublisherInterface *const publisher,
    const CallbackInterface *callbacks,
    AgentInterface *agent,
    std::shared_ptr<ScenarioControlInterface> scenarioControl)
    : UnrestrictedControllStrategyModelInterface(std::move(componentName),
                                                 isInit,
                                                 priority,
                                                 offsetTime,
                                                 responseTime,
                                                 cycleTime,
                                                 stochastics,
                                                 world,
                                                 parameters,
                                                 publisher,
                                                 callbacks,
                                                 agent,
                                                 std::move(scenarioControl)),
      cycleTimeInSeconds(units::time::millisecond_t(cycleTime))
{
  ParseParameters(parameters);
  Init();
}

void TrajectoryFollowerImplementation::ParseParameters(const ParameterInterface *parameters)
{
  try
  {
    auto boolParameters = parameters->GetParametersBool();
    enforceTrajectory = boolParameters.at("EnforceTrajectory");
    automaticDeactivation = boolParameters.at("AutomaticDeactivation");
  }
  catch (const std::out_of_range &error)
  {
    LOG(CbkLogLevel::Error, error.what());
    throw std::runtime_error(error.what());
  }
}

void TrajectoryFollowerImplementation::Init()
{
  const mantle_api::Vec3<units::length::meter_t> position{
      GetAgent()->GetPositionX(), GetAgent()->GetPositionY(), 0.0_m};
  const mantle_api::Orientation3<units::angle::radian_t> orientation{GetAgent()->GetYaw(), 0.0_rad, 0.0_rad};
  const mantle_api::Pose pose{position, orientation};
  const mantle_api::Time time{0};
  lastWorldPosition = {pose, time};
  lastVelocity = GetAgent()->GetVelocity().Length();

  nextTrajectoryIterator = polyLine.begin();
  currentTime = time;
}

void TrajectoryFollowerImplementation::UpdateInput(int localLinkId,
                                                   const std::shared_ptr<SignalInterface const> &data,
                                                   [[maybe_unused]] int time)
{
  if (localLinkId == 0)
  {
    if (!enforceTrajectory)
    {
      const std::shared_ptr<ComponentStateSignalInterface const> stateSignal
          = std::dynamic_pointer_cast<ComponentStateSignalInterface const>(data);
      if (stateSignal != nullptr && stateSignal->componentState == ComponentState::Acting)
      {
        const std::shared_ptr<SteeringSignal const> signal = std::dynamic_pointer_cast<SteeringSignal const>(data);
        if (!signal)
        {
          ThrowInvalidSignalTypeError();
        }

        if (signal->componentState == ComponentState::Acting)
        {
          UpdateState(ComponentState::Disabled);
        }
      }
    }
  }
  else if (localLinkId == 1)
  {
    if (!enforceTrajectory)
    {
      const std::shared_ptr<ComponentStateSignalInterface const> stateSignal
          = std::dynamic_pointer_cast<ComponentStateSignalInterface const>(data);
      if (stateSignal != nullptr && stateSignal->componentState == ComponentState::Acting)
      {
        const std::shared_ptr<AccelerationSignal const> signal
            = std::dynamic_pointer_cast<AccelerationSignal const>(data);
        if (!signal)
        {
          ThrowInvalidSignalTypeError();
        }

        inputAccelerationActive = true;
        inputAcceleration = signal->acceleration;
        dynamicsOutputSignal.longitudinalController = signal->source;
      }
      else
      {
        inputAcceleration = 0_mps_sq;
      }
    }
  }

  else
  {
    const std::string msg = std::string(COMPONENTNAME) + " invalid signaltype";
    LOG(CbkLogLevel::Error, msg);
    throw std::runtime_error(msg);
  }
}

void TrajectoryFollowerImplementation::UpdateOutput(int localLinkId,
                                                    std::shared_ptr<SignalInterface const> &data,
                                                    [[maybe_unused]] int time)
{
  if (localLinkId == 0)
  {
    try
    {
      dynamicsOutputSignal.componentState = componentState;
      dynamicsOutputSignal.lateralController = GetComponentName();
      if (!inputAccelerationActive)
      {
        dynamicsOutputSignal.longitudinalController = GetComponentName();
      }
      data = std::make_shared<DynamicsSignal const>(dynamicsOutputSignal);
    }
    catch (const std::bad_alloc &)
    {
      ThrowCouldNotInstantiateSignalError();
    }
  }
  else
  {
    const std::string msg = std::string(COMPONENTNAME) + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void TrajectoryFollowerImplementation::Trigger([[maybe_unused]] int time)
{
  if (GetScenarioControl()->HasNewLongitudinalStrategy())
  {
    auto strategies = GetScenarioControl()->GetStrategies(mantle_api::ControlStrategyType::kFollowTrajectory);
    if (strategies.empty())
    {
      SetComponentState(ComponentState::Disabled);
      return;
    }
    auto strategy = strategies.front();
    auto followTrajectoryStrategy = std::dynamic_pointer_cast<mantle_api::FollowTrajectoryControlStrategy>(strategy);

    if (followTrajectoryStrategy != nullptr)
    {
      trajectory = followTrajectoryStrategy->trajectory;

      if (!std::holds_alternative<PolyLine>(trajectory.type))
      {
        LOGERRORANDTHROW(
            "Trajectory: " + trajectory.name
            + " has invalid type. TrajectoryFollower currently only supports trajectories of the type PolyLine.");
      }

      polyLine = std::get<PolyLine>(trajectory.type);

      for (auto &polyLinePoint : polyLine)
      {
        if (!polyLinePoint.time.has_value())
        {
          LOGERRORANDTHROW("Trajectory: " + trajectory.name
                           + " is incomplete. Time stamp is required for each polyline point.");
        }
      }

      SetComponentState(ComponentState::Acting);
      previousTrajectoryIterator = polyLine.begin();
      nextTrajectoryIterator = previousTrajectoryIterator + 1;
    }
  }

  CalculateNextTimestep();
}

ComponentState TrajectoryFollowerImplementation::GetState() const
{
  return componentState;
}

void TrajectoryFollowerImplementation::SetComponentState(const ComponentState newState)
{
  componentState = newState;
  GetPublisher()->Publish(COMPONENTNAME,
                          ComponentEvent({{"ComponentState", openpass::utils::to_string(componentState)}}));
}

void TrajectoryFollowerImplementation::UpdateState(const ComponentState newState)
{
  // only update state if the newstate differs from the current state
  if (newState != componentState)
  {
    if (newState == ComponentState::Acting)
    {
      if (canBeActivated)
      {
        SetComponentState(newState);
        Init();
      }
    }
    else if (newState == ComponentState::Disabled)
    {
      canBeActivated = false;
      SetComponentState(newState);
    }
    else
    {
      throw std::runtime_error("TrajectoryFollower cannot handle new component state");
    }
  }
}

bool TrajectoryFollowerImplementation::CheckEndOfTrajectory()
{
  if (nextTrajectoryIterator == polyLine.end())
  {
    return true;
  }
  if (!inputAccelerationActive)
  {
    if (nextTrajectoryIterator + 1 == polyLine.end() && nextTrajectoryIterator->time.value() == lastCoordinateTimestamp)
    {
      return true;
    }
  }
  return false;
}

[[noreturn]] void TrajectoryFollowerImplementation::ThrowCouldNotInstantiateSignalError()
{
  const std::string msg = std::string(COMPONENTNAME) + " could not instantiate signal";
  LOG(CbkLogLevel::Debug, msg);
  throw std::runtime_error(msg);
}

[[noreturn]] void TrajectoryFollowerImplementation::ThrowInvalidSignalTypeError()
{
  const std::string msg = std::string(COMPONENTNAME) + " invalid signaltype";
  LOG(CbkLogLevel::Debug, msg);
  throw std::runtime_error(msg);
}

void TrajectoryFollowerImplementation::HandleEndOfTrajectory()
{
  dynamicsOutputSignal.dynamicsInformation.velocityX = 0_mps;
  dynamicsOutputSignal.dynamicsInformation.velocityY = 0_mps;
  dynamicsOutputSignal.dynamicsInformation.acceleration = 0_mps_sq;
  dynamicsOutputSignal.dynamicsInformation.travelDistance = 0_m;
  dynamicsOutputSignal.dynamicsInformation.yawRate = 0_rad_per_s;
  dynamicsOutputSignal.dynamicsInformation.yawAcceleration = 0_rad_per_s_sq;

  if (automaticDeactivation)
  {
    UpdateState(ComponentState::Disabled);
    GetScenarioControl()->SetControlStrategyGoalReached(mantle_api::ControlStrategyType::kFollowTrajectory);
  }
}

void TrajectoryFollowerImplementation::TriggerWithActiveAccelerationInput()
{
  PolyLinePoint previousPosition = lastWorldPosition;
  PolyLinePoint nextPosition = (*nextTrajectoryIterator);

  const units::velocity::meters_per_second_t velocity = lastVelocity + inputAcceleration * cycleTimeInSeconds;

  if (velocity <= 0.0_mps)
  {
    HandleEndOfTrajectory();
    return;
  }

  units::length::meter_t remainingDistance = velocity * cycleTimeInSeconds;
  dynamicsOutputSignal.dynamicsInformation.travelDistance = remainingDistance;
  double percentageTraveledBetweenCoordinates = 0.0;

  while (remainingDistance > 0.0_m)
  {
    const auto distanceBetweenPoints = CalculateDistanceBetweenWorldCoordinates(previousPosition, nextPosition);
    if (distanceBetweenPoints < remainingDistance)
    {
      previousTrajectoryIterator++;
      nextTrajectoryIterator++;

      previousPosition = *previousTrajectoryIterator;

      if (nextTrajectoryIterator != polyLine.end())
      {
        nextPosition = *nextTrajectoryIterator;
      }
      else
      {
        break;
      }
    }
    else
    {
      percentageTraveledBetweenCoordinates = remainingDistance / distanceBetweenPoints;
    }
    remainingDistance -= distanceBetweenPoints;
  }

  Common::Vector2d<units::length::meter_t> direction
      = CalculateScaledVector(previousPosition, nextPosition, percentageTraveledBetweenCoordinates);
  const units::angle::radian_t deltaYawAngle
      = CalculateScaledDeltaYawAngle(previousPosition, nextPosition, percentageTraveledBetweenCoordinates);
  Common::Vector2d<units::length::meter_t> movementDirection{
      nextPosition.pose.position.x - previousPosition.pose.position.x,
      nextPosition.pose.position.y - previousPosition.pose.position.y};

  UpdateDynamics(previousPosition, direction, velocity, movementDirection, deltaYawAngle);
}

void TrajectoryFollowerImplementation::TriggerWithInactiveAccelerationInput()
{
  auto previousTimestamp = lastCoordinateTimestamp;
  auto previousCoordinate = lastWorldPosition;
  auto nextCoordinate = *nextTrajectoryIterator;

  auto remainingTime = units::make_unit<units::time::millisecond_t>(GetCycleTime());
  auto timeBetweenCoordinates = nextCoordinate.time.value() - previousTimestamp;
  units::length::meter_t deltaS{0};

  while (timeBetweenCoordinates < remainingTime && nextTrajectoryIterator != polyLine.end())
  {
    previousTrajectoryIterator = nextTrajectoryIterator;
    previousTimestamp = previousTrajectoryIterator->time.value();
    lastCoordinateTimestamp = previousTimestamp;

    nextTrajectoryIterator++;
    if (nextTrajectoryIterator != polyLine.end())
    {
      deltaS += CalculateDistanceBetweenWorldCoordinates(previousCoordinate, nextCoordinate);
      previousCoordinate = *previousTrajectoryIterator;
      nextCoordinate = *nextTrajectoryIterator;
      remainingTime -= timeBetweenCoordinates;
      timeBetweenCoordinates = nextCoordinate.time.value() - previousTimestamp;
    }
  }

  const auto &previousPosition = previousCoordinate;
  const auto &nextPosition = nextCoordinate;

  percentageTraveledBetweenCoordinates = remainingTime / timeBetweenCoordinates;
  Common::Vector2d<units::length::meter_t> direction
      = CalculateScaledVector(previousPosition, nextPosition, percentageTraveledBetweenCoordinates);
  const units::angle::radian_t deltaYawAngle
      = CalculateScaledDeltaYawAngle(previousPosition, nextPosition, percentageTraveledBetweenCoordinates);

  deltaS += direction.Length();
  dynamicsOutputSignal.dynamicsInformation.travelDistance = deltaS;
  const units::velocity::meters_per_second_t velocity = deltaS / cycleTimeInSeconds;
  Common::Vector2d<units::length::meter_t> movementDirection{
      nextPosition.pose.position.x - previousPosition.pose.position.x,
      nextPosition.pose.position.y - previousPosition.pose.position.y};

  UpdateDynamics(previousPosition, direction, velocity, movementDirection, deltaYawAngle);
}

void TrajectoryFollowerImplementation::CalculateNextTimestep()
{
  lastCoordinateTimestamp = currentTime;
  currentTime += units::make_unit<units::time::millisecond_t>(GetCycleTime());

  if (GetState() == ComponentState::Disabled)
  {
    return;
  }

  if (CheckEndOfTrajectory())
  {
    HandleEndOfTrajectory();
  }
  else
  {
    if (inputAccelerationActive)
    {
      TriggerWithActiveAccelerationInput();
    }
    else
    {
      TriggerWithInactiveAccelerationInput();
    }
  }
}

Common::Vector2d<units::length::meter_t> TrajectoryFollowerImplementation::CalculateScaledVector(
    const PolyLinePoint &previousPosition, const PolyLinePoint &nextPosition, const double &factor)
{
  auto direction = nextPosition.pose.position - previousPosition.pose.position;
  direction = direction * factor;

  return {direction.x, direction.y};
}

units::angle::radian_t TrajectoryFollowerImplementation::CalculateScaledDeltaYawAngle(
    const PolyLinePoint &previousPosition, const PolyLinePoint &nextPosition, const double &factor)
{
  return (nextPosition.pose.orientation.yaw - previousPosition.pose.orientation.yaw) * factor;
}

units::length::meter_t TrajectoryFollowerImplementation::CalculateDistanceBetweenWorldCoordinates(
    PolyLinePoint previousPosition, PolyLinePoint nextPosition)
{
  const auto direction = nextPosition.pose.position - previousPosition.pose.position;
  return openpass::hypot(direction.x, direction.y);
}

void TrajectoryFollowerImplementation::UpdateDynamics(const PolyLinePoint &previousPosition,
                                                      const Common::Vector2d<units::length::meter_t> &direction,
                                                      units::velocity::meters_per_second_t velocity,
                                                      Common::Vector2d<units::length::meter_t> movementDirection,
                                                      units::angle::radian_t deltaYawAngle)
{
  dynamicsOutputSignal.dynamicsInformation.positionX
      = previousPosition.pose.position.x + units::length::meter_t(direction.x);
  dynamicsOutputSignal.dynamicsInformation.positionY
      = previousPosition.pose.position.y + units::length::meter_t(direction.y);
  dynamicsOutputSignal.dynamicsInformation.yaw = previousPosition.pose.orientation.yaw + deltaYawAngle;

  dynamicsOutputSignal.dynamicsInformation.yawRate
      = CommonHelper::SetAngleToValidRange(dynamicsOutputSignal.dynamicsInformation.yaw
                                           - lastWorldPosition.pose.orientation.yaw)
      / cycleTimeInSeconds;
  dynamicsOutputSignal.dynamicsInformation.yawAcceleration
      = (dynamicsOutputSignal.dynamicsInformation.yawRate - lastYawVelocity) / cycleTimeInSeconds;

  auto normalizedVector = movementDirection.HasNoLength() ? Common::Vector2d<units::dimensionless::scalar_t>(0.0, 0.0)
                                                          : movementDirection.Norm();
  Common::Vector2d<units::velocity::meters_per_second_t> velocityVector
      = {normalizedVector.x * velocity, normalizedVector.y * velocity};

  dynamicsOutputSignal.dynamicsInformation.velocityX = velocityVector.x;
  dynamicsOutputSignal.dynamicsInformation.velocityY = velocityVector.y;
  dynamicsOutputSignal.dynamicsInformation.acceleration = (velocity - lastVelocity) / cycleTimeInSeconds;
  dynamicsOutputSignal.dynamicsInformation.centripetalAcceleration
      = units::inverse_radian(1) * dynamicsOutputSignal.dynamicsInformation.yawRate * velocity;

  const mantle_api::Vec3<units::length::meter_t> position{
      dynamicsOutputSignal.dynamicsInformation.positionX, dynamicsOutputSignal.dynamicsInformation.positionY, 0.0_m};
  const mantle_api::Orientation3<units::angle::radian_t> orientation{
      dynamicsOutputSignal.dynamicsInformation.yaw, 0.0_rad, 0.0_rad};
  const mantle_api::Pose pose{position, orientation};
  const mantle_api::Time time{currentTime};

  lastWorldPosition = {pose, time};
  lastYawVelocity = dynamicsOutputSignal.dynamicsInformation.yawRate;
  lastVelocity = velocity;
}
