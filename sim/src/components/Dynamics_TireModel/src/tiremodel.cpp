/********************************************************************************
 * Copyright (c) 2023-2024 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * @ingroup Tire_Model
 * @defgroup init_tire Initialization
 */

/**
 * @ingroup Tire_Model
 * @defgroup step_tire Process
 */
/**
 * @ingroup Tire_Model
 * @defgroup output_tire Output
 */

#include "tiremodel.h"

#include <boost/format.hpp>
#include <cmath>
#include <memory>
#include <span>
#include <utility>

#include "common/commonTools.h"
#include "components/common/vehicleProperties.h"

DynamicsTireModel::DynamicsTireModel(std::string componentName,
                                     bool isInit,
                                     int priority,
                                     int offsetTime,
                                     int responseTime,
                                     int cycleTime,
                                     StochasticsInterface *stochastics,
                                     WorldInterface *world,
                                     const ParameterInterface *parameters,
                                     PublisherInterface *const publisher,
                                     const CallbackInterface *callbacks,
                                     AgentInterface *agent)
    : UnrestrictedModelInterface(std::move(componentName),
                                 isInit,
                                 priority,
                                 offsetTime,
                                 responseTime,
                                 cycleTime,
                                 stochastics,
                                 world,
                                 parameters,
                                 publisher,
                                 callbacks,
                                 agent),
      mCycle(cycleTime)
{
  LOGINFO((boost::format("Constructing Dynamics_TireModel for agent %d...") % agent->GetId()).str());

  /** @addtogroup init_tire
   *
   * tire initialization:
   *  - Read Position center of gravity
   *  - Get friction coefficient
   *  - Set initial vertical tire forces and external parameters
   *  - Init TMEasy TireModel
   */

  const std::shared_ptr<const mantle_api::VehicleProperties> vehicleProperties
      = std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(GetAgent()->GetVehicleModelParameters());

  std::optional<std::string> xPositionCog
      = helper::map::query(vehicleProperties->properties, Properties::Vehicle::POSITION_COG_X);
  std::optional<std::string> yPositionCog
      = helper::map::query(vehicleProperties->properties, Properties::Vehicle::POSITION_COG_Y);

  if (xPositionCog.has_value())
  {
    PositionCOG.x = std::stod(xPositionCog.value()) * 1_m;
  }
  else
  {
    PositionCOG.x = GetAgent()->GetWheelbase() / 2;
  }
  if (yPositionCog.has_value())
  {
    PositionCOG.y = std::stod(yPositionCog.value()) * 1_m;
  }
  else
  {
    PositionCOG.y = 0_m;
  }

  auto frictionCoeff = helper::map::query(vehicleProperties->properties, Properties::Vehicle::FRICTION_COEFFICIENT);
  THROWIFFALSE(frictionCoeff.has_value(), "FrictionCoefficient was not defined in VehicleCatalog");

  auto weight = GetAgent()->GetVehicleModelParameters()->mass;

  std::vector<double> forceWheelVerticalInit;

  std::vector<mantle_api::Axle> axles;
  axles.resize(2);
  axles[0] = vehicleProperties->front_axle;
  axles[1] = vehicleProperties->rear_axle;

  const units::mass::kilogram_t massFront = weight
                                          * (PositionCOG.x
                                             / (vehicleProperties->front_axle.bb_center_to_axle_center.x
                                                - vehicleProperties->rear_axle.bb_center_to_axle_center.x));

  const units::mass::kilogram_t massRear = weight - massFront;

  const units::length::meter_t bbCenter = units::math::abs(vehicleProperties->bounding_box.geometric_center.x);

  // Calculate vertical force at timestep 0
  forceWheelVerticalInit.resize(axles.size() * 2);
  forceWheelVerticalInit[0] = 9.81 * massFront.value() * (axles[0].track_width.value() / 2 - PositionCOG.y.value())
                            / axles[0].track_width.value();
  forceWheelVerticalInit[1] = 9.81 * massFront.value()
                            * std::fabs(-axles[0].track_width.value() / 2 - PositionCOG.y.value())
                            / axles[0].track_width.value();
  forceWheelVerticalInit[2] = 9.81 * massRear.value() * (axles[1].track_width.value() / 2 - PositionCOG.y.value())
                            / axles[1].track_width.value();
  forceWheelVerticalInit[3] = 9.81 * massRear.value()
                            * std::fabs(-axles[1].track_width.value() / 2 - PositionCOG.y.value())
                            / axles[1].track_width.value();

  forceWheelVertical.SetDefaultValue(forceWheelVerticalInit);

  // read external parameters
  ParseParameters(parameters);

  Common::Vector2d velocity = agent->GetVelocity();
  velocity.Rotate(-agent->GetYaw());

  // Initialization tires
  InitTires(velocity,
            muTireMaxXFRef,
            muTireMaxYFRef,
            muTireSlideXFRef,
            muTireSlideYFRef,
            slipTireMaxXFRef,
            slipTireMaxYFRef,
            muTireMaxX2FRef,
            muTireMaxY2FRef,
            muTireSlideX2FRef,
            muTireSlideY2FRef,
            slipTireMaxX2FRef,
            slipTireMaxY2FRef,
            FRef,
            FRefNormalized,
            std::stod(frictionCoeff.value()),
            slipTireSlideXFRef,
            slipTireSlideYFRef,
            F0pXFRef,
            F0pYFRef,
            slipTireSlideX2FRef,
            slipTireSlideY2FRef,
            F0pX2FRef,
            F0pY2FRef,
            axles,
            inertia,
            pneumaticTrail,
            forceWheelVerticalInit,
            bbCenter);

  LOGINFO("Constructing Dynamics_TireModel successful");
}

void DynamicsTireModel::ParseParameters(const ParameterInterface *parameters)
{
  // Read external parameters
  muTireMaxXFRef = parameters->GetParametersDoubleVector().at("MuTireMaxXFRef");
  muTireMaxX2FRef = parameters->GetParametersDoubleVector().at("MuTireMaxX2FRef");

  muTireSlideXFRef = parameters->GetParametersDoubleVector().at("MuTireSlideXFRef");
  muTireSlideX2FRef = parameters->GetParametersDoubleVector().at("MuTireSlideX2FRef");

  slipTireMaxXFRef = parameters->GetParametersDoubleVector().at("SlipTireMaxXFRef");
  slipTireMaxX2FRef = parameters->GetParametersDoubleVector().at("SlipTireMaxX2FRef");

  slipTireSlideXFRef = parameters->GetParametersDoubleVector().at("SlipTireSlideXFRef");
  slipTireSlideX2FRef = parameters->GetParametersDoubleVector().at("SlipTireSlideX2FRef");

  F0pXFRef = parameters->GetParametersDoubleVector().at("F0pXFRef");
  F0pX2FRef = parameters->GetParametersDoubleVector().at("F0pX2FRef");

  muTireMaxYFRef = parameters->GetParametersDoubleVector().at("MuTireMaxYFRef");
  muTireMaxY2FRef = parameters->GetParametersDoubleVector().at("MuTireMaxY2FRef");

  muTireSlideYFRef = parameters->GetParametersDoubleVector().at("MuTireSlideYFRef");
  muTireSlideY2FRef = parameters->GetParametersDoubleVector().at("MuTireSlideY2FRef");

  slipTireMaxYFRef = parameters->GetParametersDoubleVector().at("SlipTireMaxYFRef");
  slipTireMaxY2FRef = parameters->GetParametersDoubleVector().at("SlipTireMaxY2FRef");

  slipTireSlideYFRef = parameters->GetParametersDoubleVector().at("SlipTireSlideYFRef");
  slipTireSlideY2FRef = parameters->GetParametersDoubleVector().at("SlipTireSlideY2FRef");

  F0pYFRef = parameters->GetParametersDoubleVector().at("F0pYFRef");
  F0pY2FRef = parameters->GetParametersDoubleVector().at("F0pY2FRef");

  FRef = parameters->GetParametersDoubleVector().at("FRef");
  FRefNormalized = parameters->GetParametersBoolVector().at("FRefNormalized");

  inertia = parameters->GetParametersDoubleVector().at("Inertia");

  pneumaticTrail = parameters->GetParametersDoubleVector().at("PneumaticTrail");
}

DynamicsTireModel::~DynamicsTireModel()
{
  for (auto &tire : tires)
  {
    tire.reset();
  }
}

void DynamicsTireModel::UpdateInput(int localLinkId,
                                    const std::shared_ptr<SignalInterface const> &data,
                                    [[maybe_unused]] int time)
{
  std::stringstream log;
  log << componentname << " UpdateInput";
  LOG(CbkLogLevel::Debug, log.str());
  log.str(std::string());

  const bool success = inputPorts.at(localLinkId)->SetSignalValue(data);

  if (success)
  {
    log << componentname << " UpdateInput successful";
    LOG(CbkLogLevel::Debug, log.str());
  }
  else
  {
    log << componentname << " UpdateInput failed";
    LOG(CbkLogLevel::Error, log.str());
  }
}

void DynamicsTireModel::UpdateOutput(int localLinkId,
                                     std::shared_ptr<SignalInterface const> &data,
                                     [[maybe_unused]] int time)
{
  std::stringstream log;
  log << componentname << " UpdateOutput";
  LOG(CbkLogLevel::Debug, log.str());
  log.str(std::string());

  const bool success = outputPorts.at(localLinkId)->GetSignalValue(data);

  if (success)
  {
    log << componentname << " UpdateOutput successful";
    LOG(CbkLogLevel::Debug, log.str());
  }
  else
  {
    log << componentname << " UpdateOutput failed";
    LOG(CbkLogLevel::Error, log.str());
  }
}

void DynamicsTireModel::Trigger([[maybe_unused]] int time)
{
  /** @addtogroup step_tire
   * Read previous vehicle's state:
   *  - global position (cartesian coordinates)
   *  - direction of vehicle's longitudinal axes (angle in polar coordinates)
   *  - vehicle's longitudinal and lateral velocity in vehicle's CS
   *  - vehicle's rotational velociy
   *  - vehicle's longitudinal and lateral acceleration in vehicle's CS
   *  - vehicle's rotational acceleration
   *
   */

  ReadPreviousState();

  /** @addtogroup step_tire
   * Apply tire forces at the tire/road interface:
   *  - calculate lateral tire slips and forces
   *  - calculate friction forces
   */
  TireForce();
}

void DynamicsTireModel::ReadPreviousState()
{
  yawVelocity = GetAgent()->GetYawRate();
  yawAngle = GetAgent()->GetYaw();
  velocityCar.x = GetAgent()->GetVelocity().x - yawVelocity.value() * (PositionCOG.y.value()) * 1_mps;  // vehicle COG
  velocityCar.y = GetAgent()->GetVelocity().y + yawVelocity.value() * (PositionCOG.x.value()) * 1_mps;  // vehicle COG

  velocityCar.Rotate(-yawAngle);  // vehicle CS

  LOGDEBUG((boost::format("Prev Velocity for Dynamics_TireModel for agent %d: %f, %f, %f") % GetAgent()->GetId()
            % velocityCar.x % velocityCar.y % yawVelocity)
               .str());
}

void DynamicsTireModel::InitTires(Common::Vector2d<units::velocity::meters_per_second_t> velocityCar,
                                  std::vector<double> muTireMaxX,
                                  std::vector<double> muTireMaxY,
                                  std::vector<double> muTireSlideX,
                                  std::vector<double> muTireSlideY,
                                  std::vector<double> sMaxX,
                                  std::vector<double> sMaxY,
                                  std::vector<double> muTireMaxX2,
                                  std::vector<double> muTireMaxY2,
                                  std::vector<double> muTireSlideX2,
                                  std::vector<double> muTireSlideY2,
                                  std::vector<double> sMaxX2,
                                  std::vector<double> sMaxY2,
                                  std::vector<double> fRef,
                                  std::vector<bool> fRefNorm,
                                  double muScale,
                                  std::vector<double> sSlideX,
                                  std::vector<double> sSlideY,
                                  std::vector<double> f0pX,
                                  std::vector<double> f0pY,
                                  std::vector<double> sSlideX2,
                                  std::vector<double> sSlideY2,
                                  std::vector<double> f0pX2,
                                  std::vector<double> f0pY2,
                                  std::vector<mantle_api::Axle> axles,
                                  std::vector<double> inertia,
                                  std::vector<double> pneumaticTrail,
                                  std::vector<double> forceWheelVertical,
                                  units::length::meter_t bbCenter)
{
  tires.resize(axles.size() * 2);

  for (int i = 0; i < axles.size(); ++i)
  {
    const units::length::meter_t rTire = axles[i].wheel_diameter / 2;

    Common::Vector2d<units::length::meter_t> positionTire;
    // Calculate tire position from axle data
    positionTire.x = axles[i].bb_center_to_axle_center.x + bbCenter - PositionCOG.x;
    positionTire.y = axles[i].track_width / 2 - PositionCOG.y;
    // Init rotational velocity
    const units::angular_velocity::radians_per_second_t rotationVelocityTire
        = 1.0_rad_per_s * velocityCar.x.value() / rTire.value();
    // init parameters
    Common::Vector2d<double> muTireMax;
    Common::Vector2d<double> muTireSlide;
    Common::Vector2d<double> sMax;
    Common::Vector2d<double> sSlide;
    Common::Vector2d<double> muTireMax2;
    Common::Vector2d<double> muTireSlide2;
    Common::Vector2d<double> sMax2;
    Common::Vector2d<double> sSlide2;

    Common::Vector2d<units::force::newton_t> f0p;
    Common::Vector2d<units::force::newton_t> f0p2;

    muTireMax = {muTireMaxX[i], muTireMaxY[i]};

    muTireSlide = {muTireSlideX[i], muTireSlideY[i]};
    sMax = {sMaxX[i], sMaxY[i]};
    sSlide = {sSlideX[i], sSlideY[i]};
    f0p = {f0pX[i] * 1_N, f0pY[i] * 1_N};

    muTireMax2 = {muTireMaxX2[i], muTireMaxY2[i]};

    muTireSlide2 = {muTireSlideX2[i], muTireSlideY2[i]};
    sMax2 = {sMaxX2[i], sMaxY2[i]};
    sSlide2 = {sSlideX2[i], sSlideY2[i]};
    f0p2 = {f0pX2[i] * 1_N, f0pY2[i] * 1_N};

    // if reference vertical force is normalized, then multiply with init vertical tire force
    if (fRefNorm[i])
    {
      f0p.x *= forceWheelVertical[static_cast<int>(2 * i)];
      f0p.y *= forceWheelVertical[static_cast<int>(2 * i)];

      f0p2.x *= 2 * forceWheelVertical[static_cast<int>(2 * i)];
      f0p2.y *= 2 * forceWheelVertical[static_cast<int>(2 * i)];

      fRef[i] = forceWheelVertical[static_cast<int>(2 * i)];
    }

    tires[static_cast<int>(2 * i)] = std::make_unique<Tire>(fRef[i] * 1_N,
                                                            Common::Vector2d<double>(muTireMaxX[i], muTireMaxY[i]),
                                                            muTireSlide,
                                                            sMax,
                                                            muTireMax2,
                                                            muTireSlide2,
                                                            sMax2,
                                                            rTire,
                                                            muScale,
                                                            sSlide,
                                                            sSlide2,
                                                            f0p,
                                                            f0p2,
                                                            positionTire,
                                                            rotationVelocityTire,
                                                            inertia[i] * 1_kg * 1_m * 1_m,
                                                            pneumaticTrail[i] * 1_m);

    tires[static_cast<int>(2 * i)]->CalcVelTire(0_rad_per_s, velocityCar);
    // update y position of second tire on axle
    positionTire.y = -axles[i].track_width / 2 - PositionCOG.y;

    f0p = {f0pX[i] * 1_N, f0pY[i] * 1_N};
    f0p2 = {f0pX2[i] * 1_N, f0pY2[i] * 1_N};
    if (fRefNorm[i])
    {
      fRef[i] /= forceWheelVertical[static_cast<int>(2 * i + 1)];

      f0p.x *= forceWheelVertical[static_cast<int>(2 * i + 1)];
      f0p.y *= forceWheelVertical[static_cast<int>(2 * i + 1)];

      f0p2.x *= 2 * forceWheelVertical[static_cast<int>(2 * i + 1)];
      f0p2.y *= 2 * forceWheelVertical[static_cast<int>(2 * i + 1)];

      fRef[i] = forceWheelVertical[static_cast<int>(2 * i + 1)];
    }

    tires[static_cast<int>(2 * i + 1)] = std::make_unique<Tire>(fRef[i] * 1_N,
                                                                Common::Vector2d<double>(muTireMaxX[i], muTireMaxY[i]),
                                                                muTireSlide,
                                                                sMax,
                                                                muTireMax2,
                                                                muTireSlide2,
                                                                sMax2,
                                                                rTire,
                                                                muScale,
                                                                sSlide,
                                                                sSlide2,
                                                                f0p,
                                                                f0p2,
                                                                positionTire,
                                                                rotationVelocityTire,
                                                                inertia[i] * 1_kg * 1_m * 1_m,
                                                                pneumaticTrail[i] * 1_m);

    tires[2 * i + 1]->CalcVelTire(0_rad_per_s, velocityCar);
  }
}

void DynamicsTireModel::TireForce()
{
  std::vector<double> steeringAngle = wheelAngle.GetValue();

  std::vector<double> lonTireForce;
  std::vector<double> latTireForce;
  std::vector<double> torque;
  std::vector<double> wheelRotationRate;

  lonTireForce.resize(tires.size());
  latTireForce.resize(tires.size());
  torque.resize(tires.size());
  wheelRotationRate.resize(tires.size());

  for (int idx = 0; idx < tires.size(); ++idx)
  {
    tires[idx]->SetTireAngle(steeringAngle[idx] * 1_rad);
    tires[idx]->Rescale(std::fabs(forceWheelVertical.GetValue()[idx]) * 1_N);
    // calculate tire velocity in tire CS
    tires[idx]->CalcVelTire(yawVelocity, velocityCar);
    // add tire torque from brake and powertrain
    units::torque::newton_meter_t torqueTireSum = 0.0_Nm;

    if (tires[idx]->GetVelocityTire().x < 0_mps)
    {
      torqueTireSum -= brakeTorque.GetValue()[idx] * 1_Nm;
    }
    else
    {
      torqueTireSum = brakeTorque.GetValue()[idx] * 1_Nm;
    }

    torqueTireSum += driveTorque.GetValue()[idx] * 1_Nm;
    tires[idx]->SetTorque(torqueTireSum);

    ///////////////////////////////////////////////////
    // Calculate Tire Force (classic Runge-Kutta method); Integration Time Step: 1ms or 0.1 ms if car is slower than 0.1
    // m/s

    unsigned int integrationStep = mCycle;

    if (velocityCar.x.value() < 3)
    {
      integrationStep *= 10;
    }

    units::time::second_t mDT = (1.0_s * mCycle) / (1000.0);
    mDT /= integrationStep;

    for (unsigned int i = 0; i < integrationStep; i++)
    {
      units::angular_velocity::radians_per_second_t const rotVelocity = tires[idx]->GetRotationVelocity();
      tires[idx]->CalcRotAcc();
      units::angular_acceleration::radians_per_second_squared_t const rk1 = tires[idx]->GetRotAcceleration();
      units::angular_acceleration::radians_per_second_squared_t rk2 = 0.0_rad_per_s_sq;
      units::angular_acceleration::radians_per_second_squared_t rk3 = 0.0_rad_per_s_sq;
      units::angular_acceleration::radians_per_second_squared_t rk4 = 0.0_rad_per_s_sq;
      tires[idx]->SetRotationVelocity(rotVelocity + 0.5 * mDT * rk1);
      tires[idx]->CalcTireForce();
      tires[idx]->CalcRotAcc();
      rk2 = tires[idx]->GetRotAcceleration();
      tires[idx]->SetRotationVelocity(rotVelocity + 0.5 * mDT * rk2);
      tires[idx]->CalcTireForce();
      tires[idx]->CalcRotAcc();
      rk3 = tires[idx]->GetRotAcceleration();
      tires[idx]->SetRotationVelocity(rotVelocity + mDT * rk3);
      tires[idx]->CalcTireForce();
      tires[idx]->CalcRotAcc();
      rk4 = tires[idx]->GetRotAcceleration();
      tires[idx]->SetRotationVelocity(rotVelocity + (1.0 / 6.0) * mDT * (rk1 + 2 * rk2 + 2 * rk3 + rk4));
      tires[idx]->CalcTireForce();
    }
    ///////////////////////////////////////////////////

    // Calculate tire self aligning torque
    tires[idx]->CalcSelfAligningTorque();

    // Update wheel rotation rate and tire force output
    lonTireForce[idx] = tires[idx]->GetLongitudinalForce().value() - tires[idx]->GetRollFriction().value();
    latTireForce[idx] = tires[idx]->GetLateralForce().value();
    torque[idx] = tires[idx]->GetSelfAligningTorque().value();
    wheelRotationRate[idx] = tires[idx]->GetRotationVelocity().value();
  }

  // Set Output
  out_longitudinalTireForce.SetValue(lonTireForce);
  out_lateralTireForce.SetValue(latTireForce);
  out_selfAligningTorque.SetValue(torque);
  out_wheelRotationRate.SetValue(wheelRotationRate);
}
