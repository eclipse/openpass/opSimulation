################################################################################
# Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#               2024 Volkswagen AG								  
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_NAME ControllerSwitch)

add_compile_definitions(CONTROLLER_SWITCH_LIBRARY)

add_openpass_target(
  NAME ${COMPONENT_NAME} TYPE library LINKAGE shared COMPONENT module

  HEADERS
    controllerSwitch.h
    src/controllerSwitchImpl.h

  SOURCES
    controllerSwitch.cpp
    src/controllerSwitchImpl.cpp

  LIBRARIES
    MantleAPI::MantleAPI
  
  GUIXML
    ControllerSwitch.xml						  
)
