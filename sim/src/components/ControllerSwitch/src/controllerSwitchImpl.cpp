/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  controllerSwitchImpl.cpp */
//-----------------------------------------------------------------------------

#include "controllerSwitchImpl.h"

#include <stdexcept>
#include <utility>

#include "common/primitiveSignals.h"
#include "include/callbackInterface.h"
#include "include/scenarioControlInterface.h"
#include "include/signalInterface.h"

class AgentInterface;
class ParameterInterface;
class PublisherInterface;
class StochasticsInterface;
class WorldInterface;

ControllerSwitchImplementation::ControllerSwitchImplementation(
    std::string componentName,
    bool isInit,
    int priority,
    int offsetTime,
    int responseTime,
    int cycleTime,
    StochasticsInterface *stochastics,
    WorldInterface *world,
    const ParameterInterface *parameters,
    PublisherInterface *const publisher,
    const CallbackInterface *callbacks,
    AgentInterface *agent,
    std::shared_ptr<ScenarioControlInterface> scenarioControl)
    : UnrestrictedControllStrategyModelInterface(std::move(componentName),
                                                 isInit,
                                                 priority,
                                                 offsetTime,
                                                 responseTime,
                                                 cycleTime,
                                                 stochastics,
                                                 world,
                                                 parameters,
                                                 publisher,
                                                 callbacks,
                                                 agent,
                                                 std::move(scenarioControl))
{
}

void ControllerSwitchImplementation::UpdateInput(int localLinkId,
                                                 const std::shared_ptr<SignalInterface const> &signal,
                                                 [[maybe_unused]] int time)
{
  if (localLinkId == 0)
  {
    defaultControllerSignal = signal;
  }
  else if (localLinkId == 1)
  {
    customControllerSignal = signal;
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void ControllerSwitchImplementation::UpdateOutput([[maybe_unused]] int localLinkId,
                                                  std::shared_ptr<SignalInterface const> &signal,
                                                  [[maybe_unused]] int time)
{
  std::shared_ptr<SignalInterface const> outputSignal
      = GetScenarioControl()->UseCustomController() ? customControllerSignal : defaultControllerSignal;

  if (outputSignal != nullptr)
  {
    signal = outputSignal;
  }
  else
  {
    signal = std::make_shared<ComponentStateSignal>(ComponentState::Undefined);
  }
}
