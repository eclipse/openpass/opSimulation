/********************************************************************************
 * Copyright (c) 2018      in-tech GmbH
 *               2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  AlgorithmCar2XSenderImplementation.cpp */
//-----------------------------------------------------------------------------

#include "AlgorithmCar2XSenderImplementation.h"

#include <algorithm>
#include <map>
#include <memory>
#include <osi3/osi_common.pb.h>
#include <stdexcept>
#include <utility>

#include "common/vector2d.h"
#include "include/agentInterface.h"
#include "include/callbackInterface.h"
#include "include/parameterInterface.h"
#include "include/radioInterface.h"
#include "include/worldInterface.h"

class PublisherInterface;
class SignalInterface;
class StochasticsInterface;

AlgorithmCar2XSenderImplementation::AlgorithmCar2XSenderImplementation(std::string componentName,
                                                                       bool isInit,
                                                                       int priority,
                                                                       int offsetTime,
                                                                       int responseTime,
                                                                       int cycleTime,
                                                                       StochasticsInterface *stochastics,
                                                                       WorldInterface *world,
                                                                       const ParameterInterface *parameters,
                                                                       PublisherInterface *const publisher,
                                                                       const CallbackInterface *callbacks,
                                                                       AgentInterface *agent)
    : UnrestrictedModelInterface(std::move(componentName),
                                 isInit,
                                 priority,
                                 offsetTime,
                                 responseTime,
                                 cycleTime,
                                 stochastics,
                                 world,
                                 parameters,
                                 publisher,
                                 callbacks,
                                 agent)
{
  // read parameters
  try
  {
    signalStrength = units::power::watt_t(parameters->GetParametersDouble().at("SignalStrength"));
    if (parameters->GetParametersBool().at("SendPositionXEnabled"))
    {
      informationToSend.push_back(SensorInformationType::PositionX);
    }
    if (parameters->GetParametersBool().at("SendPositionYEnabled"))
    {
      informationToSend.push_back(SensorInformationType::PositionY);
    }
    if (parameters->GetParametersBool().at("SendVelocityEnabled"))
    {
      informationToSend.push_back(SensorInformationType::Velocity);
    }
    if (parameters->GetParametersBool().at("SendAccelerationEnabled"))
    {
      informationToSend.push_back(SensorInformationType::Acceleration);
    }
    if (parameters->GetParametersBool().at("SendYawEnabled"))
    {
      informationToSend.push_back(SensorInformationType::Yaw);
    }
  }
  catch (...)
  {
    const std::string msg = COMPONENTNAME + " could not init parameters";
    LOG(CbkLogLevel::Error, msg);
    throw std::runtime_error(msg);
  }

  try
  {
    if (GetPublisher() == nullptr) throw std::runtime_error("");
  }
  catch (...)
  {
    const std::string msg = COMPONENTNAME + " invalid observation module setup";
    LOG(CbkLogLevel::Error, msg);
    throw std::runtime_error(msg);
  }
}

void AlgorithmCar2XSenderImplementation::UpdateInput(
    [[maybe_unused]] int localLinkId,
    [[maybe_unused]] const std::shared_ptr<SignalInterface const> &data,
    [[maybe_unused]] int time)
{
}

void AlgorithmCar2XSenderImplementation::UpdateOutput([[maybe_unused]] int localLinkId,
                                                      [[maybe_unused]] std::shared_ptr<SignalInterface const> &data,
                                                      [[maybe_unused]] int time)
{
}

void AlgorithmCar2XSenderImplementation::Trigger([[maybe_unused]] int time)
{
  const auto movingObject = FillObjectInformation();

  GetWorld()->GetRadio().Send(GetAgent()->GetPositionX(), GetAgent()->GetPositionY(), signalStrength, movingObject);
}

osi3::MovingObject AlgorithmCar2XSenderImplementation::FillObjectInformation()
{
  osi3::MovingObject object;
  Common::Vector2d<units::velocity::meters_per_second_t> ownVelocity{0.0_mps, 0.0_mps};
  Common::Vector2d<units::acceleration::meters_per_second_squared_t> ownAcceleration{0.0_mps_sq, 0.0_mps_sq};

  object.mutable_id()->set_value(GetAgent()->GetId());

  for (auto information : informationToSend)
  {
    switch (information)
    {
      case SensorInformationType::PositionX:
        object.mutable_base()->mutable_position()->set_x(GetAgent()->GetPositionX().value());
        break;

      case SensorInformationType::PositionY:
        object.mutable_base()->mutable_position()->set_y(GetAgent()->GetPositionY().value());
        break;

      case SensorInformationType::Yaw:
        object.mutable_base()->mutable_orientation()->set_yaw(GetAgent()->GetYaw().value());
        break;

      case SensorInformationType::Velocity:
        ownVelocity = GetAgent()->GetVelocity();
        object.mutable_base()->mutable_velocity()->set_x(ownVelocity.x.value());
        object.mutable_base()->mutable_velocity()->set_y(ownVelocity.y.value());
        break;

      case SensorInformationType::Acceleration:
        ownAcceleration = GetAgent()->GetAcceleration();
        object.mutable_base()->mutable_acceleration()->set_x(ownAcceleration.x.value());
        object.mutable_base()->mutable_acceleration()->set_y(ownAcceleration.y.value());
        break;

      default:
        const std::string msg = COMPONENTNAME + " invalid sensorInformationType";
        LOG(CbkLogLevel::Debug, msg);
        break;
    }
  }

  return object;
}
