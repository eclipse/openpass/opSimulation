/********************************************************************************
 * Copyright (c) 2019 AMFD GmbH
 *               2019-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  sensor_driverDefinitions.h
//! @brief This file contains several classes for sensor driver purposes
//-----------------------------------------------------------------------------

#pragma once
#include "common/globalDefinitions.h"
#include "common/worldDefinitions.h"

//! This struct is used to transport data of an object as seen be the driver
struct ObjectInformation
{
  //! Id of the object (default if not existing)
  int id{-1};
  //! false if there is no object in this position
  bool exist{false};
  //! true if stationary object, false if agent
  bool isStatic{false};
  //! Absolute velocity of the agent (default if object is not an agent)
  units::velocity::meters_per_second_t absoluteVelocity{-999.0};
  //! Acceleration of the agent (default if object is not an agent)
  units::acceleration::meters_per_second_squared_t acceleration{-999.0};
  //! Heading relative to the street (default if not existing)
  units::angle::radian_t heading{-999.0};
  //! Length of object (default if not existing)
  units::length::meter_t length{-999.0};
  //! Width of object (default if not existing)
  units::length::meter_t width{-999.0};
  //! Height of object (default if not existing)
  units::length::meter_t height{-999.0};
  //! Relative distance along the road (i.e in direction s) between own agent and object (default if not existing)
  units::length::meter_t relativeLongitudinalDistance{-999.0};
  //! Relative distance at right angle to the road (i.e. in direction t) between own agent and object (default if not
  //! existing)
  units::length::meter_t relativeLateralDistance{-999.0};
};

//! This struct is used to trasport data of a lane concerning traffic rules as seen by the driver
struct LaneInformationTrafficRules
{
  //! Vector of all traffic signs valid for this lane inside the visibility distance
  std::vector<CommonTrafficSign::Entity> trafficSigns;
  //! Vector of all traffic lights valid for this lane inside the visibility distance
  std::vector<CommonTrafficLight::Entity> trafficLights;
};

//! This struct is used to transport data of a lane concerning its geometric features as seen by the driver
struct LaneInformationGeometry
{
  //! Whether there is a lane on this position
  bool exists{false};
  //! Curvature at current s position (default if not existing)
  units::curvature::inverse_meter_t curvature{-999.0};
  //! Width at current s position (default if not existing)
  units::length::meter_t width{-999.0};
  //! Distance from current position to the end of the lane or infinity if the end is farther away than the visibility
  //! distance
  units::length::meter_t distanceToEndOfLane{-999.0};
};

//! Data about the lane to left (in driving direction) of the mainLane
struct OwnVehicleInformation
{
  //! Absolute velocity of agent
  units::velocity::meters_per_second_t absoluteVelocity{-999.0};
  //! Acceleration of agent
  units::acceleration::meters_per_second_squared_t acceleration{-999.0};
  //! t-coordinate
  units::length::meter_t lateralPosition{-999.0};
  //! Heading relative to lane
  units::angle::radian_t heading{-999.0};
  //! Angle of the steering wheel
  units::angle::radian_t steeringWheelAngle{-999.0};
  //! Distance between the left front point and the left boundary of the lane it is in.
  units::length::meter_t distanceToLaneBoundaryLeft{-999.0};
  //! Distance between the right front point and the right boundary of the lane it is in.
  units::length::meter_t distanceToLaneBoundaryRight{-999.0};
  //! Whether this agent has collided with another object
  bool collision{false};
};

//! This struct contains data of the objects surrounding the own agent as seen by the driver.
//! For all cases mainLaneId is used as own lane
struct SurroundingObjects
{
  //! Next object in the same lane
  ObjectInformation objectFront;
  //! Closest object behind the agent in the same lane
  ObjectInformation objectRear;
  //! Next object in the lane to the left
  ObjectInformation objectFrontLeft;
  //! Closest object behind the agent in the lane to the left
  ObjectInformation objectRearLeft;
  //! Next object in the lane to the right
  ObjectInformation objectFrontRight;
  //! Closest object behind the agent in the lane to the right
  ObjectInformation objectRearRight;
};

//! This struct contains infrastructure information of surrounding lanes that are used to establish traffic rules
struct TrafficRuleInformation
{
  //! Data about the lane to left (in driving direction) of the mainLane
  LaneInformationTrafficRules laneLeft;
  //! Data about the lane the where the middle of the front of the agent is (i.e. mainLane)
  LaneInformationTrafficRules laneEgo;
  //! Data about the lane to right (in driving direction) of the mainLane
  LaneInformationTrafficRules laneRight;
  //! List of lane markings on the left side of mainLane
  std::vector<LaneMarking::Entity> laneMarkingsLeft;
  //! List of lane markings on the right side of mainLane
  std::vector<LaneMarking::Entity> laneMarkingsRight;
  //! List of lane markings on the left side of lane to the left
  std::vector<LaneMarking::Entity> laneMarkingsLeftOfLeftLane;
  //! List of lane markings on the right side of lane to the right
  std::vector<LaneMarking::Entity> laneMarkingsRightOfRightLane;
};

//! This struct contains infrastructure information of surrounding lanes that describe its geometric features
struct GeometryInformation
{
  //! Current maximum visibility distance as specified by the world
  units::length::meter_t visibilityDistance{-999.0};
  //! Data about the lane to left (in driving direction) of the mainLane
  LaneInformationGeometry laneLeft;
  //! Data about the lane the where the middle of the front of the agent is (i.e. mainLane)
  LaneInformationGeometry laneEgo;
  //! Data about the lane to right (in driving direction) of the mainLane
  LaneInformationGeometry laneRight;
};
