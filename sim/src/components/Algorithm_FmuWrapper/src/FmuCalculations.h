/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2019-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "FmuHelper.h"
#include "common/globalDefinitions.h"

/// @brief Class representing Fmu calculations
class FmuCalculations
{
public:
  /// @brief Function to calculate speed limit
  /// @param range Range of the agent
  /// @param agent Pointer to the agent interface
  /// @return speed limit of the agent
  static double CalculateSpeedLimit(double range, AgentInterface* agent);

  /// @brief Function to calculate lane count
  /// @param side     Side of the lane
  /// @param agent    Pointer to the agent interface
  /// @return Number of lanes
  static int CalculateLaneCount(Side side, AgentInterface* agent);

  /// @brief Function to calculate sensor fusion information
  /// @param sensorDataIn Input OSI sensor data
  /// @param world        Pointer to the world interface
  /// @param agent        Pointer to the agent interface
  /// @return List of the sensor fusion object information
  static std::vector<SensorFusionObjectInfo> CalculateSensorFusionInfo(osi3::SensorData& sensorDataIn,
                                                                       WorldInterface* world,
                                                                       AgentInterface* agent);
};
