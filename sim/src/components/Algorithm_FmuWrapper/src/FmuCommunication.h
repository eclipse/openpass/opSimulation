/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <cstddef>
#include <vector>

#include "FmuHelper.h"
#include "include/fmuHandlerInterface.h"

template <size_t FMI>
class FmuCommunication  ///< Class representing a FMU communication
{
public:
  /// @brief Constructor for FMU Communication with FMI size type
  /// @param componentName        Name of the component
  /// @param cdata                FMU check data
  /// @param fmuVariableValues    Map of FMU value reference type and its value
  /// @param parameters           Reference to the parameter interface
  /// @param world                Pointer to the world interface
  /// @param agent                Pointer to the agent interface
  /// @param callbacks            Pointer to the callbacks interface
  FmuCommunication<FMI>(std::string componentName,
                        fmu_check_data_t& cdata,
                        std::map<ValueReferenceAndType, FmuValue>& fmuVariableValues,
                        const ParameterInterface* parameters,
                        WorldInterface* world,
                        AgentInterface* agent,
                        const CallbackInterface* callbacks);

  /// @brief Function to set FMI
  /// @param valueReferencesVec   List of value referemces
  /// @param fmuValuesIn          List of input fmu values
  /// @param dataType             Type of the variable
  void SetFMI(std::vector<int> valueReferencesVec, std::vector<FmuValue> fmuValuesIn, VariableType dataType);

  /// @brief Function to get GMI
  /// @param valueReferencesVec   List of value referemces
  /// @param fmuValuesOut         List of input fmu values
  /// @param dataType             Type of the variable
  void GetFMI(std::vector<int> valueReferencesVec, std::vector<FmuValue>& fmuValuesOut, VariableType dataType);

  /// @brief Set values in the fmu from integer reference
  /// @param valueReferencesVec   List of value references
  /// @param dataVecIn            List of input values
  template <typename ValueType>
  void SetValuesFromIntRef(std::vector<int> valueReferencesVec, std::vector<ValueType> dataVecIn);

  /// @brief Get values in the fmu from integer reference
  /// @param valueReferencesVec   List of value references
  /// @param dataVecOut           List of output values
  template <typename ValueType>
  void GetValuesFromIntRef(std::vector<int> valueReferencesVec, std::vector<ValueType>& dataVecOut);

  /*!
   * \brief Calls a FMIL function and handles the return status.
   *
   * \param[in]   fmiStatus   FMI Status
   * \param[in]   logPrefix   Prefix to prepend to log messages
   *
   * \note Calls \c fmi1_end_handling with \c cdata as argument on FMI error.
   */
  void HandleFmiStatus(const jm_status_enu_t& fmiStatus, const std::string& logPrefix);

  /*!
   * Prepares the FMU for simulation (loading, parsing,, ...)
   *
   * @return Status of the init operation
   */
  jm_status_enu_t PrepareFmuInit();

  /*!
   * Cleans up FMU structures
   *
   * @return Status of the cleanup operation
   */
  jm_status_enu_t FmiEndHandling();

  /*!
   * Steps the FMU
   *
   * @param time   Current simulation time
   *
   * @return Status of the stepping operation
   */
  jm_status_enu_t FmiSimulateStep(double time);

  /*!
   * Checks the FMU and sets a defined state
   *
   * @return Status of the prepare operation
   */
  jm_status_enu_t FmiPrepSimulate();

  /*!
   * @return Retrieve available TypeDefinitions from FMU
   */
  std::unordered_map<std::string, std::unordered_map<int, std::string>> GetFmuTypeDefinitions();

  /*!
   * @return Retrieve available variables from FMU
   */
  FmuVariables GetFmuVariables();

  /// @brief Log the information of FMI communication
  /// @param logLevel Level of log
  /// @param file     File to log
  /// @param line     Line
  /// @param message  Message to log
  void Log(CbkLogLevel logLevel, const char* file, int line, const std::string& message);

  const CallbackInterface* callbacks;  ///< Pointer to the callback interface
private:
  const std::string agentIdString;
  struct fmu_check_data_t& cdata;
  std::string componentName;

  template <typename ValueReference, typename ValueType>
  fmi_status_t SetValue(const std::vector<ValueReference>& valueReferences, const std::vector<ValueType>& dataVecIn);

  template <typename ValueReference, typename ValueType>
  fmi_status_t GetValue(const std::vector<ValueReference>& valueReferences, std::vector<ValueType>& dataVecOut);

  /*!
   * \brief Converts the FMU variable type to a unique C++ type id.
   *
   * \param[in]   fmiType     FMILibrary specific type specifier
   *
   * \return  Hash code of the associated C++ type id
   */
  VariableType FmiTypeToCType(const fmi1_base_type_enu_t fmiType);
  VariableType FmiTypeToCType(const fmi2_base_type_enu_t fmiType);
};

#include "FmuCommunication.tpp"