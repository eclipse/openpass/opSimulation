/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <memory>

#include "include/agentInterface.h"

//! Struct representing a translator for dynamics output signal
struct DynamicsOutputSignalBaseTranslator
{
  //! Constructor
  //! @param agent  Reference to the agent interface
  DynamicsOutputSignalBaseTranslator(AgentInterface &agent) : agent(agent) {}

  //! @brief Translator function for dynamics output signal
  //!
  //! @return pointer to the signal interface
  virtual std::shared_ptr<const SignalInterface> Translate(const google::protobuf::Message *const) = 0;

public:
  double acceleration{};             //!< Acceleration of th agent
  double velocityX{};                //!< Velocity in x direction
  double velocityY{};                //!< Velocity in y direction
  double positionX{};                //!< x-position of agent
  double positionY{};                //!< y-position of agent
  double yaw{};                      //!< Yaw angle of agent
  double yawRate{};                  //!< Yaw rate of the agent
  double yawAcceleration{};          //!< Yaw acceleration of the agent
  double roll{};                     //!< Roll angle of agent
  double steeringWheelAngle{};       //!< New angle of the steering wheel angle
  double centripetalAcceleration{};  //!< Centripetal acceleration of the agent
  double travelDistance{};           //!< Distance traveled by the agent during this timestep

  AgentInterface &agent;  //!< Reference to the agent interface
};