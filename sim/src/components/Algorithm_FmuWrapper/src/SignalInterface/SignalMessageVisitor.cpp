/*******************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "SignalMessageVisitor.h"

#include "common/secondaryDriverTasksSignal.h"

namespace SignalMessage
{

template <>
std::shared_ptr<const SignalInterface> Parse([[maybe_unused]] AgentInterface &agent,
                                             [[maybe_unused]] const std::string &componentName,
                                             [[maybe_unused]] SignalType outputType,
                                             const osi3::SensorData *sensorDataOut)
{
  return OSISensorDataParser::Translate(*sensorDataOut);
}

template <>
std::shared_ptr<const SignalInterface> Parse(AgentInterface &agent,
                                             const std::string &componentName,
                                             SignalType outputType,
                                             const osi3::TrafficUpdate *trafficUpdate)
{
  return TrafficUpdateSignalParser::Translate(agent, componentName, outputType, *trafficUpdate);
}

template <>
std::shared_ptr<const SignalInterface> Parse<SignalType::DynamicsSignal>(
    const std::set<SignalType> &outputSignals,
    const std::string &componentName,
    [[maybe_unused]] ComponentState componentState,
    const std::function<FmuValue &(SignalValue, VariableType)> &getFmuSignalValue)
{
  return DynamicsSignalParser::Translate(outputSignals, componentName, getFmuSignalValue);
}

template <>
std::shared_ptr<const SignalInterface> Parse<SignalType::AccelerationSignal>(
    const std::set<SignalType> &outputSignals,
    const std::string &componentName,
    ComponentState componentState,
    const std::function<FmuValue &(SignalValue, VariableType)> &getFmuSignalValue)
{
  return AccelerationSignalParser::Translate(outputSignals, componentName, componentState, getFmuSignalValue);
}

template <>
std::shared_ptr<const SignalInterface> Parse<SignalType::LongitudinalSignal>(
    const std::set<SignalType> &outputSignals,
    const std::string &componentName,
    ComponentState componentState,
    const std::function<FmuValue &(SignalValue, VariableType)> &getFmuSignalValue)
{
  return LongitudinalSignalParser::Translate(outputSignals, componentName, componentState, getFmuSignalValue);
}

template <>
std::shared_ptr<const SignalInterface> Parse<SignalType::SteeringSignal>(
    const std::set<SignalType> &outputSignals,
    const std::string &componentName,
    ComponentState componentState,
    const std::function<FmuValue &(SignalValue, VariableType)> &getFmuSignalValue)
{
  return SteeringSignalParser::Translate(outputSignals, componentName, componentState, getFmuSignalValue);
}

template <>
std::shared_ptr<const SignalInterface> Parse<SignalType::CompCtrlSignal>(
    const std::set<SignalType> &outputSignals,
    ComponentState componentState,
    const FmuEnumerations &fmuEnumerations,
    const std::function<FmuValue &(SignalValue, VariableType)> &getFmuSignalValue)
{
  return CompCtrlSignalParser::Translate(outputSignals, componentState, fmuEnumerations, getFmuSignalValue);
}

template <>
std::shared_ptr<const SignalInterface> Parse<SignalType::SecondaryDriverTasksSignal>(
    const std::set<SignalType> &signals,
    ComponentState componentState,
    const FmuEnumerations &,
    const std::function<FmuValue &(SignalValue, VariableType)> &getFmuSignalValue)
{
  if (std::find(signals.cbegin(), signals.cend(), SignalType::SecondaryDriverTasksSignal) == signals.cend())
  {
    return std::make_shared<const SecondaryDriverTasksSignal>(0, false, false, false, false, componentState);
  }
  return std::make_shared<const SecondaryDriverTasksSignal>(
      getFmuSignalValue(SignalValue::SecondaryDriverTasksSignal_IndicatorState, VariableType::Int)
          .intValue,  // NOLINT(cppcoreguidelines-pro-type-union-access)
      getFmuSignalValue(SignalValue::SecondaryDriverTasksSignal_HornSwitch, VariableType::Bool)
          .boolValue,  // NOLINT(cppcoreguidelines-pro-type-union-access)
      getFmuSignalValue(SignalValue::SecondaryDriverTasksSignal_HeadLightSwitch, VariableType::Bool)
          .boolValue,  // NOLINT(cppcoreguidelines-pro-type-union-access)
      getFmuSignalValue(SignalValue::SecondaryDriverTasksSignal_HighBeamLightSwitch, VariableType::Bool)
          .boolValue,  // NOLINT(cppcoreguidelines-pro-type-union-access)
      getFmuSignalValue(SignalValue::SecondaryDriverTasksSignal_FlasherSwitch, VariableType::Bool)
          .boolValue,  // NOLINT(cppcoreguidelines-pro-type-union-access)
      componentState);
}
}  // namespace SignalMessage
