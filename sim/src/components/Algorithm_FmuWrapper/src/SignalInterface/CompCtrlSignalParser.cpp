/*******************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "CompCtrlSignalParser.h"

std::shared_ptr<const SignalInterface> CompCtrlSignalParser::Translate(
    const std::set<SignalType> &outputSignals,
    ComponentState componentState,
    FmuEnumerations fmuEnumerations,
    const std::function<FmuValue &(SignalValue, VariableType)> &getFmuSignalValue)
{
  if (std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::CompCtrlSignal) != outputSignals.cend())
  {
    auto movementDomain = MovementDomain::Undefined;
    std::vector<ComponentWarningInformation> warnings;
    bool withWarningDirection
        = std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::CompCtrlSignalWarningDirection)
       != outputSignals.cend();

    auto movementDomains = fmuEnumerations.movementDomains.find(
        getFmuSignalValue(SignalValue::CompCtrlSignal_MovementDomain, VariableType::Enum)
            .intValue);  // NOLINT(cppcoreguidelines-pro-type-union-access)
    if (movementDomains == fmuEnumerations.movementDomains.cend())
    {
      throw std::runtime_error(
          std::string("CompCtrlSignal: Enumeration is not defined in FMU or enumeration value is invalid for output "
                      "type CompCtrlSignal_MovementDomain"));
    }
    movementDomain = movementDomains->second;
    bool warningActivity = getFmuSignalValue(SignalValue::CompCtrlSignal_WarningActivity, VariableType::Bool)
                               .boolValue;  // NOLINT(cppcoreguidelines-pro-type-union-access)
    auto warningLevels = fmuEnumerations.warningLevels.find(
        getFmuSignalValue(SignalValue::CompCtrlSignal_WarningLevel, VariableType::Enum)
            .intValue);  // NOLINT(cppcoreguidelines-pro-type-union-access)
    if (warningLevels == fmuEnumerations.warningLevels.cend())
    {
      throw std::runtime_error(
          std::string("CompCtrlSignal: Enumeration is not defined in FMU or enumeration value is invalid for output "
                      "type CompCtrlSignal_WarningLevel"));
    }
    auto warningLevel = warningLevels->second;
    auto warningTypes = fmuEnumerations.warningTypes.find(
        getFmuSignalValue(SignalValue::CompCtrlSignal_WarningType, VariableType::Enum)
            .intValue);  // NOLINT(cppcoreguidelines-pro-type-union-access)
    if (warningTypes == fmuEnumerations.warningTypes.cend())
    {
      throw std::runtime_error(
          std::string("CompCtrlSignal: Enumeration is not defined in FMU or enumeration value is invalid for output "
                      "type CompCtrlSignal_WarningType"));
    }
    auto warningType = warningTypes->second;
    auto warningIntensities = fmuEnumerations.warningIntensities.find(
        getFmuSignalValue(SignalValue::CompCtrlSignal_WarningIntensity, VariableType::Enum)
            .intValue);  // NOLINT(cppcoreguidelines-pro-type-union-access)
    if (warningIntensities == fmuEnumerations.warningIntensities.cend())
    {
      throw std::runtime_error(
          std::string("CompCtrlSignal: Enumeration is not defined in FMU or enumeration value is invalid for output "
                      "type CompCtrlSignal_WarningIntensity"));
    }
    auto warningIntensity = warningIntensities->second;
    auto warningDirections = fmuEnumerations.warningDirections.find(
        getFmuSignalValue(SignalValue::CompCtrlSignal_WarningDirection, VariableType::Enum)
            .intValue);  // NOLINT(cppcoreguidelines-pro-type-union-access)
    if (warningDirections == fmuEnumerations.warningDirections.cend())
    {
      throw std::runtime_error(
          std::string("CompCtrlSignal: Enumeration is not defined in FMU or enumeration value is invalid for output "
                      "type CompCtrlSignal_WarningDirection"));
    }
    auto direction = withWarningDirection ? std::make_optional(warningDirections->second) : std::nullopt;
    warnings.push_back(
        ComponentWarningInformation{warningActivity, warningLevel, warningType, warningIntensity, direction});

    return std::make_shared<VehicleCompToCompCtrlSignal const>(
        ComponentType::VehicleComponent, "FMU", componentState, movementDomain, warnings, AdasType::Safety);
  }
  return std::make_shared<VehicleCompToCompCtrlSignal const>(ComponentType::VehicleComponent,
                                                             "FMU",
                                                             ComponentState::Disabled,
                                                             MovementDomain::Undefined,
                                                             std::vector<ComponentWarningInformation>{},
                                                             AdasType::Safety);
}