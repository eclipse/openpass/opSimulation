/*******************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2024 Volkswagen AG
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "DynamicsSignalParser.h"

std::shared_ptr<const SignalInterface> DynamicsSignalParser::Translate(
    const std::set<SignalType> &outputSignals,
    const std::string &componentName,
    const std::function<FmuValue &(SignalValue, VariableType)> &getFmuSignalValue)
{
  if (std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::DynamicsSignal) != outputSignals.cend())
  {
    // NOLINTBEGIN(cppcoreguidelines-pro-type-union-access)
    const DynamicsInformation dynamicsInformation
        = {units::acceleration::meters_per_second_squared_t(
               getFmuSignalValue(SignalValue::DynamicsSignal_Acceleration, VariableType::Double).realValue),
           units::acceleration::meters_per_second_squared_t(
               getFmuSignalValue(SignalValue::DynamicsSignal_CentripetalAcceleration, VariableType::Double).realValue),
           0.0_mps_sq,
           0.0_mps_sq,
           units::velocity::meters_per_second_t(
               getFmuSignalValue(SignalValue::DynamicsSignal_Velocity, VariableType::Double).realValue)
               * units::math::cos(dynamicsInformation.yaw),
           units::velocity::meters_per_second_t(
               getFmuSignalValue(SignalValue::DynamicsSignal_Velocity, VariableType::Double).realValue)
               * units::math::sin(dynamicsInformation.yaw),
           units::length::meter_t(
               getFmuSignalValue(SignalValue::DynamicsSignal_PositionX, VariableType::Double).realValue),
           units::length::meter_t(
               getFmuSignalValue(SignalValue::DynamicsSignal_PositionY, VariableType::Double).realValue),
           units::angle::radian_t(getFmuSignalValue(SignalValue::DynamicsSignal_Yaw, VariableType::Double).realValue),
           units::angular_velocity::radians_per_second_t(
               getFmuSignalValue(SignalValue::DynamicsSignal_YawRate, VariableType::Double).realValue),
           units::angular_acceleration::radians_per_second_squared_t(
               getFmuSignalValue(SignalValue::DynamicsSignal_YawAcceleration, VariableType::Double).realValue),
           0.0_rad,
           0.0_rad,
           units::angle::radian_t(
               getFmuSignalValue(SignalValue::DynamicsSignal_SteeringWheelAngle, VariableType::Double).realValue),
           std::vector<units::angular_velocity::radians_per_second_t>(),
           std::vector<units::angle::radian_t>{},
           std::vector<units::angle::radian_t>{},
           std::vector<units::angle::radian_t>{},
           units::length::meter_t(
               getFmuSignalValue(SignalValue::DynamicsSignal_TravelDistance, VariableType::Double).realValue)

        };
    // NOLINTEND(cppcoreguidelines-pro-type-union-access)

    return std::make_shared<DynamicsSignal const>(
        ComponentState::Acting, dynamicsInformation, componentName, componentName);
  }
  return std::make_shared<DynamicsSignal const>(
      ComponentState::Disabled, DynamicsInformation{}, componentName, componentName);
}
