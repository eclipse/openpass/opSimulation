/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <memory>
#include <osi3/osi_sensordata.pb.h>

#include "common/sensorDataSignal.h"
#include "include/agentInterface.h"
#include "include/signalInterface.h"

//! @brief Parser for SensorData
struct OSISensorDataParser
{
  //! @brief Translator function for SensorData
  //!
  //! @param sensorDataOut  Sensor information that imitate the output of real sensors
  //! @return pointer to the signal interface
  static std::shared_ptr<const SignalInterface> Translate(const osi3::SensorData &sensorDataOut);
};