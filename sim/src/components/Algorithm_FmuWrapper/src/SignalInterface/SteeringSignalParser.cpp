/*******************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "SteeringSignalParser.h"

std::shared_ptr<const SignalInterface> SteeringSignalParser::Translate(
    const std::set<SignalType> &outputSignals,
    const std::string &componentName,
    ComponentState componentState,
    const std::function<FmuValue &(SignalValue, VariableType)> &getFmuSignalValue)
{
  if (std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::SteeringSignal) != outputSignals.cend())
  {
    units::angle::radian_t steeringWheelAngle{
        getFmuSignalValue(SignalValue::SteeringSignal_SteeringWheelAngle, VariableType::Double)
            .realValue};  // NOLINT(cppcoreguidelines-pro-type-union-access)
    return std::make_shared<SteeringSignal const>(componentState, steeringWheelAngle, componentName);
  }
  return std::make_shared<SteeringSignal const>(ComponentState::Disabled, 0.0_rad, componentName);
}