/*******************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "AccelerationSignalParser.h"

std::shared_ptr<const SignalInterface> AccelerationSignalParser::Translate(
    const std::set<SignalType> &outputSignals,
    const std::string &componentName,
    ComponentState componentState,
    const std::function<FmuValue &(SignalValue, VariableType)> &getFmuSignalValue)
{
  if (std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::AccelerationSignal) != outputSignals.cend())
  {
    units::acceleration::meters_per_second_squared_t acceleration{
        getFmuSignalValue(SignalValue::AccelerationSignal_Acceleration, VariableType::Double)
            .realValue};  // NOLINT(cppcoreguidelines-pro-type-union-access)
    return std::make_shared<AccelerationSignal const>(componentState, acceleration, componentName);
  }
  return std::make_shared<AccelerationSignal const>(ComponentState::Disabled, 0.0_mps_sq, componentName);
}