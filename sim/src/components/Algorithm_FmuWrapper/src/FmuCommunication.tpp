/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once
#include "fmuFileHelper.h"

#include <sstream>

template <size_t FMI>
FmuCommunication<FMI>::FmuCommunication(std::string componentName,
                                        fmu_check_data_t& cdata,
                                        std::map<ValueReferenceAndType, FmuValue> &fmuVariableValues,
                                        const ParameterInterface *parameters,
                                        WorldInterface *world,
                                        AgentInterface *agent,
                                        const CallbackInterface *callbacks) :
        cdata(cdata),
        componentName(componentName),
        callbacks(callbacks),
        agentIdString(FmuFileHelper::CreateAgentIdString(agent->GetId()))
{
}

template <size_t FMI>
void FmuCommunication<FMI>::Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message)
{
    if (callbacks)
    {
        callbacks->Log(logLevel, file, line, message);
    }
}

template <size_t FMI>
void FmuCommunication<FMI>::SetFMI(std::vector<int> valueReferencesVec, std::vector<FmuValue> fmuValuesIn, VariableType dataType)
{
    fmi_t_data dataIn;
    int i = 0;
    switch(dataType)
    {
        case VariableType::String:
            dataIn.emplace<std::vector<fmi_string_t>>();
            std::get<std::vector<fmi_string_t>>(dataIn).resize(fmuValuesIn.size());
            for(FmuValue fmuValue : fmuValuesIn)
            {
                std::get<std::vector<fmi_string_t>>(dataIn)[i++].emplace<FMI>(fmuValue.stringValue);
            }
            SetValuesFromIntRef(valueReferencesVec, std::get<std::vector<fmi_string_t>>(dataIn));
            break;
        case VariableType::Double:
            dataIn.emplace<std::vector<fmi_real_t>>();
            std::get<std::vector<fmi_real_t>>(dataIn).resize(fmuValuesIn.size());
            for(FmuValue fmuValue : fmuValuesIn)
            {
                std::get<std::vector<fmi_real_t>>(dataIn)[i++].emplace<FMI>(fmuValue.realValue);
            }
            SetValuesFromIntRef(valueReferencesVec, std::get<std::vector<fmi_real_t>>(dataIn));
            break;
        case VariableType::Int:
        case VariableType::Enum:
            dataIn.emplace<std::vector<fmi_integer_t>>();
            std::get<std::vector<fmi_integer_t>>(dataIn).resize(fmuValuesIn.size());
            for(FmuValue fmuValue : fmuValuesIn)
            {
                std::get<std::vector<fmi_integer_t>>(dataIn)[i++].emplace<FMI>(fmuValue.intValue);
            }
            SetValuesFromIntRef(valueReferencesVec, std::get<std::vector<fmi_integer_t>>(dataIn));
            break;
        case VariableType::Bool:
            dataIn.emplace<std::vector<fmi_boolean_t>>();
            std::get<std::vector<fmi_boolean_t>>(dataIn).resize(fmuValuesIn.size());
            for(FmuValue fmuValue : fmuValuesIn)
            {
                std::get<std::vector<fmi_boolean_t>>(dataIn)[i++].emplace<FMI>(fmuValue.boolValue);
            }
            SetValuesFromIntRef(valueReferencesVec, std::get<std::vector<fmi_boolean_t>>(dataIn));
            break;
        default:
            LOGWARN("Could not set value on FMU, because of unknown datatype");
            break;
    }
}

template <size_t FMI>
void FmuCommunication<FMI>::GetFMI(std::vector<int> valueReferencesVec, std::vector<FmuValue>& fmuValuesOut, VariableType dataType)
{
    fmi_t_data dataOut;
    int i = 0;
    switch(dataType)
    {
        case VariableType::String:
        {
            dataOut.emplace<std::vector<fmi_string_t>>();
            GetValuesFromIntRef(valueReferencesVec, std::get<std::vector<fmi_string_t>>(dataOut));
            auto dataOutConverted = std::get<std::vector<fmi_string_t>>(dataOut);
            fmuValuesOut.resize(dataOutConverted.size());
            for(fmi_string_t dataOutEntry : dataOutConverted)
            {
                FmuValue fmuValue;
                fmuValue.stringValue = std::get<FMI>(dataOutEntry);
                fmuValuesOut[i++] = fmuValue;
            }
            break;
        }
        case VariableType::Double:
        {
            dataOut.emplace<std::vector<fmi_real_t>>();
            GetValuesFromIntRef(valueReferencesVec, std::get<std::vector<fmi_real_t>>(dataOut));
            auto dataOutConverted = std::get<std::vector<fmi_real_t>>(dataOut);
            fmuValuesOut.resize(dataOutConverted.size());
            for(fmi_real_t dataOutEntry : dataOutConverted)
            {
                FmuValue fmuValue;
                fmuValue.realValue = std::get<FMI>(dataOutEntry);
                fmuValuesOut[i++] = fmuValue;
            }
            break;
        }
        case VariableType::Int:
        case VariableType::Enum:
        {
            dataOut.emplace<std::vector<fmi_integer_t>>();
            GetValuesFromIntRef(valueReferencesVec, std::get<std::vector<fmi_integer_t>>(dataOut));
            fmuValuesOut.resize(std::get<std::vector<fmi_integer_t>>(dataOut).size());
            for(fmi_integer_t dataOutEntry : std::get<std::vector<fmi_integer_t>>(dataOut))
            {
                FmuValue fmuValue;
                fmuValue.intValue = std::get<FMI>(dataOutEntry);
                fmuValuesOut[i++] = fmuValue;
            }
            break;
        }
        case VariableType::Bool:
        {
            dataOut.emplace<std::vector<fmi_boolean_t>>();
            GetValuesFromIntRef(valueReferencesVec, std::get<std::vector<fmi_boolean_t>>(dataOut));
            auto dataOutConverted = std::get<std::vector<fmi_boolean_t>>(dataOut);
            fmuValuesOut.resize(dataOutConverted.size());
            for(fmi_boolean_t dataOutEntry : dataOutConverted)
            {
                FmuValue fmuValue;
                fmuValue.boolValue = std::get<FMI>(dataOutEntry);
                fmuValuesOut[i++] = fmuValue;
            }
            break;
        }
        default:
            LOGWARN("FmuCommunication: Could not get value on FMU, because of unknown datatype");
            break;
    }

}

template <size_t FMI>
template <typename ValueType>
void FmuCommunication<FMI>::SetValuesFromIntRef(std::vector<int> valueReferencesVec, std::vector<ValueType> dataVecIn)
{
    if(valueReferencesVec.empty() || dataVecIn.empty())
        return;

    fmi_value_references_t valueReferences = FmuHelper::GetReferencesFromIntVector<FMI>(valueReferencesVec);

    fmi_status_t fmiStatus = SetValue(std::get<FMI>(valueReferences), dataVecIn);

    if (fmiStatus == fmi_status_t{fmi1_status_warning} || fmiStatus == fmi_status_t{fmi2_status_warning})
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi variable returned a warning");
    }
    else if (fmiStatus == fmi_status_t{fmi1_status_error} || fmiStatus == fmi_status_t{fmi2_status_error})
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi variable returned an error");
    }
}

template <size_t FMI>
template <typename ValueType>
void FmuCommunication<FMI>::GetValuesFromIntRef(std::vector<int> valueReferencesVec, std::vector<ValueType>& dataVecOut)
{
    if(valueReferencesVec.empty())
        return;

    fmi_value_references_t valueReferences = FmuHelper::GetReferencesFromIntVector<FMI>(valueReferencesVec);

    fmi_status_t fmiStatus = GetValue(std::get<FMI>(valueReferences), dataVecOut);

    if (fmiStatus == fmi_status_t{fmi1_status_warning} || fmiStatus == fmi_status_t{fmi2_status_warning})
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi variable returned a warning");
    }
    else if (fmiStatus == fmi_status_t{fmi1_status_error} || fmiStatus == fmi_status_t{fmi2_status_error})
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi variable returned an error");
    }
}

template <size_t FMI>
template <typename ValueReference, typename ValueType>
fmi_status_t FmuCommunication<FMI>::SetValue([[maybe_unused]] const std::vector<ValueReference>& valueReferences, [[maybe_unused]] const std::vector<ValueType>& dataVecIn)
{
    return fmi1_status_error;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::SetValue(const std::vector<fmi1_value_reference_t>& valueReferences, const std::vector<fmi_string_t>& dataVecIn);

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::SetValue(const std::vector<fmi2_value_reference_t>& valueReferences, const std::vector<fmi_string_t>& dataVecIn);

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::SetValue(const std::vector<fmi1_value_reference_t>& valueReferences, const std::vector<fmi_real_t>& dataVecIn);

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::SetValue(const std::vector<fmi2_value_reference_t>& valueReferences, const std::vector<fmi_real_t>& dataVecIn);

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::SetValue(const std::vector<fmi1_value_reference_t>& valueReferences, const std::vector<fmi_integer_t>& dataVecIn);

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::SetValue(const std::vector<fmi2_value_reference_t>& valueReferences, const std::vector<fmi_integer_t>& dataVecIn);

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::SetValue(const std::vector<fmi1_value_reference_t>& valueReferences, const std::vector<fmi_boolean_t>& dataVecIn);

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::SetValue(const std::vector<fmi2_value_reference_t>& valueReferences, const std::vector<fmi_boolean_t>& dataVecIn);

template <size_t FMI>
template <typename ValueReference, typename ValueType>
fmi_status_t FmuCommunication<FMI>::GetValue([[maybe_unused]] const std::vector<ValueReference>& valueReferences, [[maybe_unused]] std::vector<ValueType>& dataOut)
{
    return fmi1_status_error;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::GetValue(const std::vector<fmi1_value_reference_t>& valueReferences, std::vector<fmi_string_t>& dataOut);

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::GetValue(const std::vector<fmi2_value_reference_t>& valueReferences, std::vector<fmi_string_t>& dataOut);

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::GetValue(const std::vector<fmi1_value_reference_t>& valueReferences, std::vector<fmi_real_t>& dataOut);

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::GetValue(const std::vector<fmi2_value_reference_t>& valueReferences, std::vector<fmi_real_t>& dataOut);

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::GetValue(const std::vector<fmi1_value_reference_t>& valueReferences, std::vector<fmi_integer_t>& dataOut);

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::GetValue(const std::vector<fmi2_value_reference_t>& valueReferences, std::vector<fmi_integer_t>& dataOut);

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::GetValue(const std::vector<fmi1_value_reference_t>& valueReferences, std::vector<fmi_boolean_t>& dataOut);

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::GetValue(const std::vector<fmi2_value_reference_t>& valueReferences, std::vector<fmi_boolean_t>& dataOut);

template <size_t FMI>
jm_status_enu_t FmuCommunication<FMI>::PrepareFmuInit()
{
    jm_status_enu_t status;

    if(FMI == FMI1)
        status = fmi1_cs_prep_init(&cdata);
    else if(FMI == FMI2)
        status = fmi2_cs_prep_init(&cdata);
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    if (status == jm_status_error)
    {
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Error in prepare fmu init");
    }

    HandleFmiStatus(status, "prep init");

    return status;
}

template <size_t FMI>
jm_status_enu_t FmuCommunication<FMI>::FmiEndHandling()
{
    jm_status_enu_t status;

    if(FMI == FMI1)
        status = fmi1_end_handling(&cdata);
    else if(FMI == FMI2)
        status = fmi2_end_handling(&cdata);
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    if (status == jm_status_error)
    {
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Error in FMU end handling");
    }

    return status;
}

template <size_t FMI>
jm_status_enu_t FmuCommunication<FMI>::FmiSimulateStep(double time)
{
    jm_status_enu_t status;

    if(FMI == FMI1)
        status = fmi1_cs_simulate_step(&cdata, time);
    else if(FMI == FMI2)
        status = fmi2_cs_simulate_step(&cdata, time);
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    HandleFmiStatus(status, "simulation step");

    return status;
}

template <size_t FMI>
jm_status_enu_t FmuCommunication<FMI>::FmiPrepSimulate()
{
    jm_status_enu_t status;

    if(FMI == FMI1)
        status = fmi1_cs_prep_simulate(&cdata);
    else if(FMI == FMI2)
        status = fmi2_cs_prep_simulate(&cdata);
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    HandleFmiStatus(status, "prep simulate");

    return status;
}

template <size_t FMI>
void FmuCommunication<FMI>::HandleFmiStatus(const jm_status_enu_t &fmiStatus, const std::string &logPrefix)
{
    switch (fmiStatus)
    {
        case jm_status_success:
            LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + logPrefix + " successful");
            break;

        case jm_status_warning:
            LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + logPrefix + " returned with warning");
            break;

        case jm_status_error:
            FmiEndHandling();
            LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString, componentName) + logPrefix + " returned with error")
    }
}

template <size_t FMI>
std::unordered_map<std::string, std::unordered_map<int, std::string>> FmuCommunication<FMI>::GetFmuTypeDefinitions()
{
    std::unordered_map<std::string, std::unordered_map<int, std::string>> fmuTypeDefinitions;

    if (FMI == FMI1)
    {
        fmi1_import_type_definitions_t *fmuTypeDefinitionList = fmi1_import_get_type_definitions(cdata.fmu1);
        size_t fmuTypeDefinitionCount = fmi1_import_get_type_definition_number(fmuTypeDefinitionList);

        for (size_t i = 0; i < fmuTypeDefinitionCount; ++i)
        {
            std::unordered_map<int, std::string> enumItems;
            fmi1_import_variable_typedef_t *fmuTypeDef = fmi1_import_get_typedef(fmuTypeDefinitionList, static_cast<unsigned int>(i));
            const std::string fmuTypeDefName(fmi1_import_get_type_name(fmuTypeDef));
            fmi1_import_enumeration_typedef_t *enumType = fmi1_import_get_type_as_enum(fmuTypeDef);
            unsigned int fmuEnumTypeMin = fmi1_import_get_enum_type_min(enumType);
            unsigned int fmuEnumTypeMax = fmi1_import_get_enum_type_max(enumType);
            for (unsigned int i = fmuEnumTypeMin; i <= fmuEnumTypeMax; ++i)
            {
                const char *itemName = fmi1_import_get_enum_type_item_name(enumType, i);
                enumItems.insert(std::make_pair(static_cast<int>(i), itemName));
            }
            fmuTypeDefinitions.insert(std::make_pair(fmuTypeDefName, enumItems));
        }
    }
    else if (FMI == FMI2)
    {

        fmi2_import_type_definitions_t *fmuTypeDefinitionList = fmi2_import_get_type_definitions(cdata.fmu2);
        size_t fmuTypeDefinitionCount = fmi2_import_get_type_definition_number(fmuTypeDefinitionList);

        for (size_t i = 0; i < fmuTypeDefinitionCount; ++i)
        {
            std::unordered_map<int, std::string> enumItems;
            fmi2_import_variable_typedef_t *fmuTypeDef = fmi2_import_get_typedef(fmuTypeDefinitionList, static_cast<unsigned int>(i));
            const std::string fmuTypeDefName(fmi2_import_get_type_name(fmuTypeDef));
            fmi2_import_enumeration_typedef_t *enumType = fmi2_import_get_type_as_enum(fmuTypeDef);
            unsigned int fmuEnumTypeMin = fmi2_import_get_enum_type_min(enumType);
            unsigned int fmuEnumTypeCount = fmi2_import_get_enum_type_size(enumType);
            for (unsigned int i = fmuEnumTypeMin; i <= fmuEnumTypeCount; ++i)
            {
                int itemValue = fmi2_import_get_enum_type_item_value(enumType, i);
                const char *itemName = fmi2_import_get_enum_type_value_name(enumType, itemValue);
                enumItems.insert(std::make_pair(itemValue, itemName));
            }
            fmuTypeDefinitions.insert(std::make_pair(fmuTypeDefName, enumItems));
        }
    }
    else
    {
        LOGERRORANDTHROW("Invalid FMI version");
    }
    return fmuTypeDefinitions;
}

template <size_t FMI>
FmuVariables FmuCommunication<FMI>::GetFmuVariables()
{
    FmuVariables fmuVariables;

    if (FMI == FMI1)
    {
        std::vector<std::pair<std::string, FmuVariable1>> fmuVariables1;
        fmi1_import_variable_list_t *fmuVariableList = fmi1_import_get_variable_list(cdata.fmu1);
        size_t fmuVariableCount = fmi1_import_get_variable_list_size(fmuVariableList);

        for (size_t i = 0; i < fmuVariableCount; ++i)
        {
            fmi1_import_variable_t *fmuVar = fmi1_import_get_variable(fmuVariableList, static_cast<unsigned int>(i));
            const std::string fmuVarName(fmi1_import_get_variable_name(fmuVar));
            const fmi1_value_reference_t fmuValueReference = fmi1_import_get_variable_vr(fmuVar);
            const fmi1_base_type_enu_t fmuVarType = fmi1_import_get_variable_base_type(fmuVar);
            const VariableType variableType = FmiTypeToCType(fmuVarType);
            fmi1_import_variable_typedef_t *fmuDeclaredType = fmi1_import_get_variable_declared_type(fmuVar);
            std::string declaredType = "";
            if(fmuDeclaredType) declaredType = fmi1_import_get_type_name(fmuDeclaredType);
            const fmi1_causality_enu_t causality = fmi1_import_get_causality(fmuVar);
            const fmi1_variability_enu_t variability = fmi1_import_get_variability(fmuVar);
            fmuVariables1.push_back(std::make_pair(fmuVarName, *std::make_shared<FmuVariable1>(fmuValueReference, variableType, declaredType, causality, variability)));
        }
        fmuVariables = std::unordered_map<std::string, FmuVariable1>(fmuVariables1.begin(), fmuVariables1.end());
        fmi1_import_free_variable_list(fmuVariableList);
    }
    else if (FMI == FMI2)
    {
        std::vector<std::pair<std::string, FmuVariable2>> fmuVariables2;
        fmi2_import_variable_list_t *fmuVariableList = fmi2_import_get_variable_list(cdata.fmu2, 0);
        size_t fmuVariableCount = fmi2_import_get_variable_list_size(fmuVariableList);

        for (size_t i = 0; i < fmuVariableCount; ++i)
        {

            fmi2_import_variable_t *fmuVar = fmi2_import_get_variable(fmuVariableList, static_cast<unsigned int>(i));
            const std::string fmuVarName(fmi2_import_get_variable_name(fmuVar));
            const fmi2_value_reference_t fmuValueReference = fmi2_import_get_variable_vr(fmuVar);
            const fmi2_base_type_enu_t fmuVarType = fmi2_import_get_variable_base_type(fmuVar);
            const VariableType variableType = FmiTypeToCType(fmuVarType);
            fmi2_import_variable_typedef_t *fmuDeclaredType = fmi2_import_get_variable_declared_type(fmuVar);
            std::string declaredType = "";
            if(fmuDeclaredType) declaredType = fmi2_import_get_type_name(fmuDeclaredType);
            const fmi2_causality_enu_t causality = fmi2_import_get_causality(fmuVar);
            const fmi2_variability_enu_t variability = fmi2_import_get_variability(fmuVar);

            fmuVariables2.push_back(std::make_pair(fmuVarName, *std::make_shared<FmuVariable2>(fmuValueReference, variableType, declaredType, causality, variability)));
        }
        fmuVariables = std::unordered_map<std::string, FmuVariable2>(fmuVariables2.begin(), fmuVariables2.end());
        fmi2_import_free_variable_list(fmuVariableList);
    }
    else
    {
        LOGERRORANDTHROW("Invalid FMI version");
    }
    return fmuVariables;
}

template <size_t FMI>
VariableType FmuCommunication<FMI>::FmiTypeToCType(const fmi1_base_type_enu_t fmiType)
{
    switch (fmiType)
    {
        case fmi1_base_type_bool:
            return VariableType::Bool;

        case fmi1_base_type_int:
            return VariableType::Int;

        case fmi1_base_type_real:
            return VariableType::Double;

        case fmi1_base_type_str:
            return VariableType::String;

        case fmi1_base_type_enum:
            return VariableType::Enum;

        default:
            throw std::runtime_error("Invalid type is not supported.");
    }
}

template <size_t FMI>
VariableType FmuCommunication<FMI>::FmiTypeToCType(const fmi2_base_type_enu_t fmiType) {
    switch (fmiType) {
        case fmi2_base_type_bool:
            return VariableType::Bool;

        case fmi2_base_type_int:
            return VariableType::Int;

        case fmi2_base_type_real:
            return VariableType::Double;

        case fmi2_base_type_str:
            return VariableType::String;

        case fmi2_base_type_enum:
            return VariableType::Enum;

        default:
            throw std::runtime_error("Invalid type is not supported.");
    }
}