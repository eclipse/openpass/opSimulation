/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2016-2017 ITK Engineering GmbH
 *               2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  longCalcs.h
 * @brief This file contains the calculations
 *
 * This class contains the data calculations.
 * */
//-----------------------------------------------------------------------------

#pragma once

#include <MantleAPI/Traffic/entity_properties.h>
#include <functional>
#include <string>
#include <units.h>

#include "include/callbackInterface.h"

//! \brief This class does all the calculations in the Algorithm_Longitudinal module.
//!
//! Based on the current velocity of the agent and a desired acceleration, this class calculates the
//! gear and the pedal positions required to achieve this acceleration. The AlgorithmLongitudinal constructs
//! an instance of this class with the parameters, then calls the calculate functions and then acquires
//! the results be calling the respective getter.
//!
//! \ingroup Algorithm_Longitudinal

class AlgorithmLongitudinalCalculations
{
public:
  //!
  //! \param velocity                 current velocity of the agent
  //! \param accelerationWish         desired acceleration (can be negative)
  //! \param vehicleModelParameters   parameters of the vehicle model
  //! \param Log                      log
  //!
  AlgorithmLongitudinalCalculations(units::velocity::meters_per_second_t velocity,
                                    units::acceleration::meters_per_second_squared_t accelerationWish,
                                    mantle_api::VehicleProperties vehicleModelParameters,
                                    std::function<void(CbkLogLevel, const char *, int, const std::string &)> Log);

  //!
  //! \brief Calculates the necessary gear and engine to achieve the desired acceleration at the current velocity
  //!
  void CalculateGearAndEngineSpeed();

  //!
  //! \brief Calculates the necessary accelerator pedal and brake pedal position to achieve desired acceleration
  //! depending on the gear. Therefor CalculateGearAndEngineSpeed needs to be called first.
  //!
  void CalculatePedalPositions();

  //!
  //! \return Calculated position of brake pedal in percent
  //!
  double GetBrakePedalPosition() const;

  //!
  //! \return Calculated position of accelerator pedal in percent
  //!
  double GetAcceleratorPedalPosition() const;

  //!
  //! \return Calculated engine speed
  //!
  units::angular_velocity::revolutions_per_minute_t GetEngineSpeed() const;

  //!
  //! \return Calculated gear
  //!
  int GetGear() const;

  //!
  //! \brief Calculates the acceleration that will result from the engineTorque at the given gear
  //!
  //! \param engineTorque engine torque
  //! \param chosenGear   gear number
  //! \return             return acceleration from the engine torque at the given gear
  units::acceleration::meters_per_second_squared_t GetAccFromEngineTorque(
      const units::torque::newton_meter_t &engineTorque, int chosenGear);

  //!
  //! \brief Calculates the engine speed required to drive with the given velocity at the specified gear
  //!
  //! \param velocity     velocity
  //! \param gear         gear number
  //! \return             engine speed required to drive with the given velocity at the specified gear
  units::angular_velocity::revolutions_per_minute_t GetEngineSpeedByVelocity(
      const units::velocity::meters_per_second_t &velocity, int gear);

  //!
  //! \brief Checks whether the engineSpeed and acceleration/torque can be achieved by the engine
  //!
  //! \param engineSpeed  velocity
  //! \param gear         gear number
  //! \param acceleration acceleration
  //! \return             engine speed required to drive with the given velocity at the specified gear
  bool isWithinEngineLimits(int gear,
                            const units::angular_velocity::revolutions_per_minute_t &engineSpeed,
                            const units::acceleration::meters_per_second_squared_t &acceleration);

  /// @brief Checks if the engine speed is with in engine limits
  /// @param engineSpeed speed of the engine
  /// @return True, if engine speed is within engine limits
  inline bool isEngineSpeedWithinEngineLimits(const units::angular_velocity::revolutions_per_minute_t &engineSpeed);

  //!
  //! \brief Checks whether the engineSpeed and acceleration/torque can be achieved by the engine
  //!
  //! \param torque       Engine torque
  //! \param engineSpeed  Engine speed
  //! \return             engine speed required to drive with the given velocity at the specified gear
  bool isTorqueWithinEngineLimits(const units::torque::newton_meter_t &torque,
                                  const units::angular_velocity::revolutions_per_minute_t &engineSpeed);

  /**
   * @brief Get the maximum possible engineTorque for the given engineSpeed
   *
   * @param engineSpeed Engine speed
   * @return Returns the maximum possible engineTorque for the given engineSpeed
   */
  units::torque::newton_meter_t GetEngineTorqueMax(
      const units::angular_velocity::revolutions_per_minute_t &engineSpeed);

  /**
   * @brief Get the minimum engineTorque (i.e. maximum drag) for the given engineSpeed
   *
   * @param engineSpeed Engine speed
   * @return Returns the minimum engineTorque (i.e. maximum drag) for the given engineSpeed
   */
  units::torque::newton_meter_t GetEngineTorqueMin(
      const units::angular_velocity::revolutions_per_minute_t &engineSpeed);

  /**
   * @brief Calculates the engine torque required to achieve the given acceleration at the specified gear
   *
   * @param gear          Gear Number
   * @param acceleration  Acceleration
   * @return returns the engine torque required to achieve the given acceleration at the specified gear
   */
  units::torque::newton_meter_t GetEngineTorqueAtGear(
      int gear, const units::acceleration::meters_per_second_squared_t &acceleration);

protected:
  /// @brief Function to return the property value
  /// @param propertyName Name of the property
  /// @return Value of the property
  double GetVehicleProperty(const std::string &propertyName);

  /// @brief Function to log the message
  std::function<void(CbkLogLevel, const char *, int, const std::string &)> Log;

  //Input
  /// velocity
  units::velocity::meters_per_second_t velocity{0.0};
  /// desired accelerations
  units::acceleration::meters_per_second_squared_t accelerationWish{0.0};
  /// vehicle properties
  const mantle_api::VehicleProperties vehicleModelParameters;

  //Output
  /// gear number
  int gear{1};
  /// engine speed
  units::angular_velocity::revolutions_per_minute_t engineSpeed{0.0};
  /// position of the break pedal
  double brakePedalPosition{0.0};
  /// position of accelerator pedal
  double acceleratorPedalPosition{0.0};
};
