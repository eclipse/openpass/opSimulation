/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  algorithm_Switch_global.h
//! @brief This file contains DLL export declarations
//-----------------------------------------------------------------------------

#ifndef ALGORITHM_SWITCH_GLOBAL_H
#define ALGORITHM_SWITCH_GLOBAL_H

#include "sim/src/common/opExport.h"

#if defined(ALGORITHM_SWITCH_LIBRARY)
#define ALGORITHM_SWITCH_SHARED_EXPORT OPEXPORT  //!< Export of the dll-functions
#else
#define ALGORITHM_SWITCH_SHARED_EXPORT OPIMPORT  //!< Import of the dll-functions
#endif

#endif  // ALGORITHM_SWITCH_GLOBAL_H
