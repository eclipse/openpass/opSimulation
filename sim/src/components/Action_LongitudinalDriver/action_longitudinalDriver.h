/********************************************************************************
 * Copyright (c) 2016-2017 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *               2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  action_longitudinalDriver.h
 *	@brief This file provides the exported methods.
 *
 *   This file provides the exported methods which are available outside of the library. */
//-----------------------------------------------------------------------------

#ifndef ACTION_LONGITUDINAL_DRIVER_H
#define ACTION_LONGITUDINAL_DRIVER_H

#include "sim/src/common/opExport.h"

#if defined(ACTION_LONGITUDINAL_DRIVER_LIBRARY)
#define ACTION_LONGITUDINAL_DRIVER_SHARED_EXPORT OPEXPORT  //!< Export of the dll-functions
#else
#define ACTION_LONGITUDINAL_DRIVER_SHARED_EXPORT OPIMPORT  //!< Import of the dll-functions
#endif

#endif  // ACTION_LONGITUDINAL_DRIVER_H
