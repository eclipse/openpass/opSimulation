/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <filesystem>
#include <google/protobuf/util/json_util.h>
#include <string>
#include <utility>

#include "Importer/SsdFileImporter.h"
#include "OSMPConnectorFactory.h"
#include "components/Algorithm_SspWrapper/Visitors/Network/ParameterInitializationVisitor.h"
#include "include/modelInterface.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/System.h"
#include "src/components/Algorithm_FmuWrapper/src/error_handler.h"
#include "src/components/Algorithm_FmuWrapper/src/fmuWrapper.h"

/// class representing implementation of algorithm for SSP wrapper
class AlgorithmSspWrapperImplementation : public UnrestrictedControllStrategyModelInterface
{
public:
  /**
   * @brief Construct a new Algorithm Ssp Wrapper Implementation object
   *
   * @param componentName       Name of this component
   * @param isInit              Indicates if component is an init module
   * @param priority            Priority of this component
   * @param offsetTime          Offset time of this component
   * @param responseTime        Response time of this component
   * @param cycleTime           Cycle time of this component
   * @param stochastics         Reference to the stochastics interface
   * @param world               World representation
   * @param parameters          Reference to the parameter interface
   * @param publisher           Publisher instance
   * @param agent               Agent that the component type is a part of
   * @param callbacks           Reference to the callback interface
   * @param scenarioControl     scenarioControl of entity
   */
  AlgorithmSspWrapperImplementation(std::string componentName,
                                    bool isInit,
                                    int priority,
                                    int offsetTime,
                                    int responseTime,
                                    int cycleTime,
                                    StochasticsInterface *stochastics,
                                    WorldInterface *world,
                                    const ParameterInterface *parameters,
                                    PublisherInterface *const publisher,
                                    AgentInterface *agent,
                                    const CallbackInterface *callbacks,
                                    std::shared_ptr<ScenarioControlInterface> scenarioControl);
  AlgorithmSspWrapperImplementation(const AlgorithmSspWrapperImplementation &) = delete;
  AlgorithmSspWrapperImplementation(AlgorithmSspWrapperImplementation &&) = delete;
  AlgorithmSspWrapperImplementation &operator=(const AlgorithmSspWrapperImplementation &) = delete;
  AlgorithmSspWrapperImplementation &operator=(AlgorithmSspWrapperImplementation &&) = delete;

  ~AlgorithmSspWrapperImplementation() override { DeleteTemporaryPaths(); }

  /// @brief Initialize algorithm for ssp wrapper implementation
  void Init();
  void UpdateInput(int modelInterface, const std::shared_ptr<const SignalInterface> &data, int time) override;

  /*!
   * \brief Update outputs.
   *
   * Function is called by framework when this Component.has to deliver a signal over
   * a channel to another component (scheduler calls update task of this component).
   *
   * Refer to module description for output channels and output ids.
   *
   * \param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
   * \param[out]    data           Referenced signal (copied by this component)
   * \param[in]     time           Current scheduling time
   */
  void UpdateOutput(int localLinkId, std::shared_ptr<const SignalInterface> &data, int time) override;

  /*!
   * \brief Process data within component.
   *
   * Function is called by framework when the scheduler calls the trigger task
   * of this component.
   *
   * Refer to module description for information about the module's task.
   *
   * \param[in]     time           Current scheduling time
   */
  void Trigger(int time) override;

  /// Name of this component
  const std::string COMPONENTNAME = "AlgorithmSspWrapper";

  /**
   * @brief Get the Root System object
   *
   * @return std::shared_ptr<ssp::System> ssp system
   */
  std::shared_ptr<ssp::System> GetRootSystem();

private:
  void SetParameterInitializations();
  void PropagateParameterInitializations();

  std::filesystem::path GetAbsolutePath(const std::string &configPath, const std::basic_string<char> &configBasePath_);
  void DeleteTemporaryPaths();
  std::filesystem::path GenerateAndRegisterTemporaryPath();
  void RecursiveUnpackSsp(const std::filesystem::path &sspPath, std::set<std::filesystem::path> &unpackedPaths);

  std::shared_ptr<ssp::System> rootSystem;
  std::vector<ParameterConnectorInitialization> parameterInitializations = {};
  std::filesystem::path outputDir;
  const std::string agentIdString;
  const CallbackInterface *callbacks;
  std::vector<std::filesystem::path> unzipRootPaths{};
  bool isInitialized = false;

  template <typename T>
  void SetParameterInitializations(ssp::ParameterInput<T> input, ParameterConnectorInitialization paramInit)
  {
    ssp::ParameterVisitor parameterVisitor{input};
    ssp::ParameterInitializationVisitor allComponentVisitor{parameterVisitor, paramInit.componentName};
    for (const auto &system : rootSystem->elements)
    {
      system->Accept(allComponentVisitor);
    }
  }
};
