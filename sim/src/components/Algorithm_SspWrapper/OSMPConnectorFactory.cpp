/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "OSMPConnectorFactory.h"

#include <any>

#include "SSPElements/Connector/OSMPConnector.h"
#include "Visitors/Connector/UpdateInputSignalVisitor.h"
#include "sim/src/components/Algorithm_FmuWrapper/src/FmuHelper.h"
#include "sim/src/components/Algorithm_FmuWrapper/src/fmuFileHelper.h"
#include "type_helper.h"

RegisteredConnectorDirection OSMPConnectorFactory::RegisterConnector(
    const SspParserTypes::Connector &connector,
    const std::shared_ptr<FmuWrapperInterface> &fmuWrapper,
    int priority,
    ConnectorKind kind)
{
  if (kind == ConnectorKind::Input || kind == ConnectorKind::Inout)
  {
    RegisterSingleConnector(inputOsmpLinks,
                            osmpInput,
                            std::visit(osmpConnectorNameVisitor, connector),
                            std::visit(fmuOSMPLinkNameVisitor, connector),
                            fmuWrapper,
                            priority,
                            OSMPConnector::RoleFromString(std::visit(connectorOSMPRoleVisitor, connector)),
                            std::visit(osiTypeVisitor, connector),
                            false);
    return RegisteredConnectorDirection::Input;
  }
  if (kind == ConnectorKind::Output || kind == ConnectorKind::Inout)
  {
    RegisterSingleConnector(outputOsmpLinks,
                            osmpOutput,
                            std::visit(osmpConnectorNameVisitor, connector),
                            std::visit(fmuOSMPLinkNameVisitor, connector),
                            fmuWrapper,
                            priority,
                            OSMPConnector::RoleFromString(std::visit(connectorOSMPRoleVisitor, connector)),
                            std::visit(osiTypeVisitor, connector),
                            false);
    return RegisteredConnectorDirection::Output;
  }
  if (kind == ConnectorKind::Parameter)
  {
    RegisterSingleConnector(inputOsmpLinks,
                            osmpInput,
                            std::visit(osmpConnectorNameVisitor, connector),
                            std::visit(fmuOSMPLinkNameVisitor, connector),
                            fmuWrapper,
                            priority,
                            OSMPConnector::RoleFromString(std::visit(connectorOSMPRoleVisitor, connector)),
                            std::visit(osiTypeVisitor, connector),
                            true);
    return RegisteredConnectorDirection::Input;
  }
  if (kind == ConnectorKind::Calculated_Parameter)
  {
    RegisterSingleConnector(outputOsmpLinks,
                            osmpOutput,
                            std::visit(osmpConnectorNameVisitor, connector),
                            std::visit(fmuOSMPLinkNameVisitor, connector),
                            fmuWrapper,
                            priority,
                            OSMPConnector::RoleFromString(std::visit(connectorOSMPRoleVisitor, connector)),
                            std::visit(osiTypeVisitor, connector),
                            true);
    return RegisteredConnectorDirection::Output;
  }
  return RegisteredConnectorDirection::None;
}

void OSMPConnectorFactory::RegisterSingleConnector(std::vector<std::string> &osmpLinks,
                                                   OSMPData &osmpData,
                                                   const std::string &connectorName,
                                                   const std::string &osmpLinkName,
                                                   const std::shared_ptr<FmuWrapperInterface> &fmuWrapper,
                                                   int priority,
                                                   OSMPConnector::OsmpLinkRole role,
                                                   const std::string &signalString,
                                                   bool isParameter)
{
  if (std::find(osmpLinks.begin(), osmpLinks.end(), osmpLinkName) == osmpLinks.end())
  {
    osmpData.osmpLinkToConnectorName.try_emplace(osmpLinkName, connectorName);
    osmpData.osmpLinkToFMU.try_emplace(osmpLinkName, fmuWrapper);
    osmpData.osmpLinkToPriority.try_emplace(osmpLinkName, priority);
    osmpData.osmpLinkToOsmpRoles.try_emplace(osmpLinkName, OSMPRoles{});
    osmpData.osmpLinkToSignalIdentifier.try_emplace(osmpLinkName, signalString);
    osmpData.osmpLinkToIsParameter.try_emplace(osmpLinkName, isParameter);
    osmpLinks.emplace_back(osmpLinkName);
  }
  switch (role)
  {
    case OSMPConnector::base_lo:
      osmpData.osmpLinkToOsmpRoles.at(osmpLinkName).lo = true;
      break;
    case OSMPConnector::base_hi:
      osmpData.osmpLinkToOsmpRoles.at(osmpLinkName).high = true;
      break;
    case OSMPConnector::size:
      osmpData.osmpLinkToOsmpRoles.at(osmpLinkName).size = true;
      break;
    default:
      break;
  }
}