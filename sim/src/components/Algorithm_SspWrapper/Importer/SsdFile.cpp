/********************************************************************************
 * Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SsdFile.h"

#include <utility>

SsdFile::SsdFile(std::filesystem::path fileName, std::shared_ptr<SsdSystem> ssdSystem)
    : fileName(std::move(fileName)), ssdSystem(std::move(ssdSystem))
{
}
