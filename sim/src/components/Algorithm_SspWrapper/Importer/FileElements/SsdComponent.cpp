/********************************************************************************
 * Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SsdComponent.h"

#include <utility>

SsdComponent::SsdComponent(std::string name, std::string source, SspComponentType componentType)
    : name(std::move(name)), source(std::move(source)), componentType(componentType)
{
}

SsdComponent::SsdComponent(std::string name, SsdURI source, SspComponentType componentType)
    : name(std::move(name)), source(std::move(source)), componentType(componentType)
{
}

void SsdComponent::SetParameters(openpass::parameter::internal::ParameterSetLevel3 setLevel3Parameters)
{
  this->parameters = std::move(setLevel3Parameters);
}

void SsdComponent::EmplaceConnector(std::string &&kind, SspParserTypes::Connector &&connector)
{
  connectors.emplace_back(std::forward<std::string>(kind), std::forward<SspParserTypes::Connector>(connector));
}

const std::string &SsdComponent::GetName() const
{
  return name;
}

const SsdURI &SsdComponent::GetSource() const
{
  return source;
}

const openpass::parameter::internal::ParameterSetLevel3 &SsdComponent::GetParameters() const
{
  return parameters;
}

const std::vector<std::pair<std::string, SspParserTypes::Connector>> SsdComponent::GetConnectors() const
{
  return connectors;
}

const std::optional<int> SsdComponent::GetPriority() const
{
  // return priority.value_or(-1);
  return priority;
}
void SsdComponent::SetPriority(int newPriority)
{
  this->priority = newPriority;
}
const std::vector<std::pair<std::string, std::string>> &SsdComponent::GetWriteMessageParameters() const
{
  return writeMessageParameters;
}
void SsdComponent::SetWriteMessageParameters(std::vector<std::pair<std::string, std::string>> messageParameters)
{
  this->writeMessageParameters = std::move(messageParameters);
}
const SspComponentType &SsdComponent::GetComponentType() const
{
  return componentType;
}
const std::vector<ParameterConnectorInitialization> &SsdComponent::GetConnectorInitializations() const
{
  return parameterConnectorInitializations;
}
void SsdComponent::SetConnectorInitializations(std::vector<ParameterConnectorInitialization> &&connectorInitializations)
{
  parameterConnectorInitializations = std::move(connectorInitializations);
}
