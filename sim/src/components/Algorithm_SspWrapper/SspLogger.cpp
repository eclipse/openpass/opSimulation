/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SspLogger.h"

void SspLogger::SetLogger(const CallbackInterface* callback, int agentId, const std::string& componentName)
{
  SspLogger::callback = callback;
  SspLogger::logPrefix = "Agent " + std::to_string(agentId) + ": " + " Component " + componentName + ": ";
}

void SspLogger::Log(CbkLogLevel logLevel, const char* file, int line, const std::string& message)
{
  if (callback)
  {
    callback->Log(logLevel, file, line, logPrefix + message);
  }
}
