/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "include/callbackInterface.h"

//! This class represents logger for SSP Wrapper
class SspLogger
{
public:
  //! @brief Sets logger
  //!
  //! @param callback         Pointer to the CallbackInterface
  //! @param agentId          Id of the agent
  //! @param componentName    Name of the component
  static void SetLogger(const CallbackInterface* callback, int agentId, const std::string& componentName);

  //! @brief Provides callback to LOG() macro
  //!
  //! @param logLevel  Importance of log
  //! @param file      Name of file where log is called
  //! @param line      Line within file where log is called
  //! @param message   Message to log
  static void Log(CbkLogLevel logLevel, const char* file, int line, const std::string& message);

private:
  static inline std::string logPrefix;
  static inline const CallbackInterface* callback;
};