/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TriggerSignalVisitor.h"

ssp::TriggerSignalVisitor::TriggerSignalVisitor(const int time,
                                                WorldInterface *world,
                                                AgentInterface *agent,
                                                const CallbackInterface *callbacks)
    : time(time), world(world), agent(agent), callbacks(callbacks)
{
}

void ssp::TriggerSignalVisitor::Visit(ssp::ScalarConnectorBase *connector)
{
  SSPVisitorHelper::TriggerScalarConnectorOnce(alreadyTriggered, connector, time);

  LOGDEBUG("SSP Trigger Signal Visitor: Visit FMU connector " + connector->GetConnectorName());

  std::shared_ptr<const SignalInterface> signal;
  UpdateOutputSignalVisitor outVisitor{0, signal, time, world, agent, callbacks};
  connector->Accept(outVisitor);
  UpdateInputSignalVisitor inputVisit{0, signal, time, world, agent, callbacks};

  for (const auto &nonParameterConnector : connector->GetNonParameterConnectors())
  {
    nonParameterConnector->Accept(inputVisit);
  }
}

void ssp::TriggerSignalVisitor::Visit(ssp::GroupConnector &groupConnector)
{
  LOGDEBUG("SSP Trigger Signal Visitor: Visit system connector ");          // + connector.GetConnectorName());
  LOGDEBUG("SSP priority queue of system connector will now be handled.");  //+ connector.GetConnectorName() + "will no
                                                                            //be handled.");
  SSPVisitorHelper::PriorityAccept(groupConnector, *this);
}

void ssp::TriggerSignalVisitor::Visit(ssp::OSMPConnectorBase *osmpConnector)
{
  SSPVisitorHelper::TriggerOMSPConnectorOnce(alreadyTriggered, osmpConnector, time);

  LOGDEBUG("SSP Trigger Signal Visitor: Visit OSMP connector " + osmpConnector->GetConnectorName());
  const auto message = osmpConnector->GetMessage();
  osmpConnector->HandleFileWriting(this->time);
  for (const auto &connector : osmpConnector->connectors)
  {
    if (auto osmpConnectorBase = std::dynamic_pointer_cast<OSMPConnectorBase>(connector))
    {
      osmpConnectorBase->SetMessage(message.get());
      osmpConnectorBase->HandleFileWriting(this->time);
    }
  }
}

void ssp::TriggerSignalVisitor::Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const
{
  SspLogger::Log(logLevel, file, line, message);
}
