/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "../../SSPElements/Connector/ConnectorHelper.h"
#include "../../SSPElements/Connector/OSMPConnectorBase.h"
#include "../../SSPElements/Connector/ScalarConnectorBase.h"
#include "../SspVisitorHelper.h"
#include "ConnectorVisitorInterface.h"

namespace ssp
{

template <typename T>
class UpdateOutputVisitor : public ConnectorVisitorInterface  ///< Class representing a visitor to update output
{
public:
  /// @brief Visitor function to update output
  /// @param connector Pointer to the scalar connector base
  void Visit(ScalarConnectorBase *connector) override;

  /// @brief Visitor function to update output
  /// @param groupConnector Reference to the group connector
  void Visit(GroupConnector &groupConnector) override;

  /// @brief Visitor function to update output
  /// @param connector Reference to OSMP Connector base
  void Visit(OSMPConnectorBase *connector) override;

  /// @return get data
  auto GetData();

  /// @return get data type
  cvt_t<T> GetDataType();

private:
  cvt_t<T> data;

protected:
  //-------------------------------------------------------------------------
  //! Provides callback to LOG() macro.
  //!
  //! @param[in]     logLevel    Importance of log
  //! @param[in]     file        Name of file where log is called
  //! @param[in]     line        Line within file where log is called
  //! @param[in]     message     Message to log
  //-------------------------------------------------------------------------
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const override;
};

template <typename T>
void ssp::UpdateOutputVisitor<T>::Visit(ssp::ScalarConnectorBase *connector)
{
  if (connector->IsParameterConnector())
  {
    LOGDEBUG(connector->GetConnectorName() + " is a parameter Connector, UpdateInput Skipped");
    return;
  }

  LOGDEBUG("SSP Output Visitor: Visit FMU connector " + connector->GetConnectorName());
  data = ConnectorHelper::GetFmuWrapperValue<T>(connector->fmuWrapperInterface, connector->fmuScalarVariableName);
}

template <typename T>
void UpdateOutputVisitor<T>::Visit(GroupConnector &groupConnector)
{
  LOGDEBUG("SSP Output Visitor: Visit system connector ");                  //+ connector.GetConnectorName());
  LOGDEBUG("SSP priority queue of system connector will now be handled.");  //+ connector.GetConnectorName() + "will no
                                                                            //be handled.");
  SSPVisitorHelper::PriorityAccept(groupConnector, *this);
}

template <typename T>
void UpdateOutputVisitor<T>::Visit(OSMPConnectorBase *connector)
{
  LOGDEBUG("SSP Output Visitor: Visit OSMP connector " + connector->GetConnectorName());
  LOGERRORANDTHROW("SSP Output Visitor: Visit OSMP connector not implemented yet");
}

template <typename T>
auto UpdateOutputVisitor<T>::GetData()
{
  return data;
}

template <typename T>
cvt_t<T> UpdateOutputVisitor<T>::GetDataType()
{
  return typeid(T);
}

template <typename T>
void ssp::UpdateOutputVisitor<T>::Log(CbkLogLevel logLevel,
                                      const char *file,
                                      int line,
                                      const std::string &message) const
{
  SspLogger::Log(logLevel, file, line, message);
}

}  //namespace ssp
