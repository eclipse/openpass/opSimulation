/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "../../SSPElements/Connector/ConnectorHelper.h"
#include "../../SSPElements/Connector/OSMPConnectorBase.h"
#include "../../SSPElements/Connector/ScalarConnectorBase.h"
#include "../SspVisitorHelper.h"
#include "ConnectorVisitorInterface.h"

namespace ssp
{

template <typename T>
class UpdateInputVisitor : public ConnectorVisitorInterface  ///< Class representing a visitor to update input
{
public:
  /// @param data type
  explicit UpdateInputVisitor(cvt_t<T> data);

  /// @brief Visitor function to update output
  /// @param connector Pointer to the scalar connector base
  void Visit(ScalarConnectorBase *connector) override;

  /// @brief Visitor function to update output
  /// @param groupConnector Reference to the group connector
  void Visit(GroupConnector &groupConnector) override;

  /// @brief Visitor function to update output
  /// @param connector Reference to OSMP Connector base
  void Visit(OSMPConnectorBase *connector) override;

private:
  const cvt_t<T> data;

protected:
  //-------------------------------------------------------------------------
  //! Provides callback to LOG() macro.
  //!
  //! @param[in]     logLevel    Importance of log
  //! @param[in]     file        Name of file where log is called
  //! @param[in]     line        Line within file where log is called
  //! @param[in]     message     Message to log
  //-------------------------------------------------------------------------
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const override;
};

template <typename T>
ssp::UpdateInputVisitor<T>::UpdateInputVisitor(cvt_t<T> data) : data(data)
{
}

template <typename T>
void UpdateInputVisitor<T>::Visit(ScalarConnectorBase *connector)
{
  if (connector->IsParameterConnector())
  {
    LOGDEBUG(connector->GetConnectorName() + " is a parameter Connector, UpdateInput Skipped");
    return;
  }

  LOGDEBUG("SSP Input Visitor: Visit FMU connector " + connector->GetConnectorName());
  ConnectorHelper::SetFmuWrapperValue<T>(connector->fmuWrapperInterface, connector->fmuScalarVariableName, data);
}

template <typename T>
void UpdateInputVisitor<T>::Visit(GroupConnector &groupConnector)
{
  LOGDEBUG("SSP Input Visitor: Visit system connector ");                   //+ connector.GetConnectorName());
  LOGDEBUG("SSP priority queue of system connector will now be handled.");  //+ connector.GetConnectorName() + "will no
                                                                            //be handled.");
  SSPVisitorHelper::PriorityAccept(groupConnector, *this);
}

template <typename T>
void ssp::UpdateInputVisitor<T>::Visit(OSMPConnectorBase *connector)
{
  LOGDEBUG("SSP Input Visitor: Visit OSMP connector " + connector->GetConnectorName());
  LOGERRORANDTHROW("SSP Input Visitor: Visit OSMP connector not implemented yet");
}

template <typename T>
void ssp::UpdateInputVisitor<T>::Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const
{
  SspLogger::Log(logLevel, file, line, message);
}
}  //namespace ssp