/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "ParserTypes.h"

std::string SSPParserHelper::RemoveOSMPRoleFromOSMPAnnotated(std::string annotatedString)
{
  std::string edit = annotatedString;
  size_t count = 0;
  auto it = annotatedString.find(".size");
  count = 5;
  if (it == std::string::npos)
  {
    it = annotatedString.find(".base.lo");
    count = 8;
  }
  if (it == std::string::npos)
  {
    it = annotatedString.find(".base.hi");
    count = 8;
  }
  if ((it != std::string::npos))
  {
    edit = annotatedString.erase(it, count);
  }
  return edit;
}

std::optional<std::string> SSPParserHelper::GetValueFromEnumeration(const std::string& enumerationName,
                                                                    const std::string& itemName,
                                                                    const SspParserTypes::Enumerations& enumerations)
{
  for (const auto& enumeration : enumerations)
  {
    if (enumeration.first == enumerationName)
    {
      for (const auto& item : enumeration.second)
      {
        if (item.first == itemName) return std::to_string(item.second);
      }
    }
  }
  return std::nullopt;
}