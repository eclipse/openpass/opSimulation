/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <map>
#include <optional>
#include <stdexcept>
#include <string>
#include <unordered_map>

namespace ssp
{

/// @brief class representing connection
class Connection
{
public:
  std::string componentStartName;      ///< Name of the startElement
  std::string variableReferenceStart;  ///< Name of the startConnector
  std::string componentEndName;        ///< Name of the endElement
  std::string variableReferenceEnd;    ///< Name of the endConnector

  /// @brief Constructor for connection object
  /// @param componentStartName       Name of the startElement
  /// @param variableReferenceStart   Name of the startConnector
  /// @param componentEndName         Name of the endElement
  /// @param variableReferenceEnd     Name of the endConnector
  Connection(std::string componentStartName,
             std::string variableReferenceStart,
             std::string componentEndName,
             std::string variableReferenceEnd);

  /// @brief Function to compare two different OSMP connection
  /// @param other one osmp connection
  /// @return true, if both connections are same
  bool operator==(const Connection &other) const;

  /// @brief Function to compare two different OSMP connection
  /// @param other one osmp connection
  /// @return true, if this connections is less than object b
  bool operator<(const Connection &other) const { return componentStartName < other.componentStartName; }

  Connection(const Connection &) = default;  ///< copy constructor
  Connection(Connection &&) = default;       ///< move constructor
};

}  //namespace ssp

namespace std
{

/// @brief Hash function specialization for ssp::Connection struct.
template <>
struct hash<ssp::Connection>
{
  /// @brief Calculates the hash value for a given ssp::Connection object.
  /// @param connection The ssp::Connection object to hash.
  /// @return The hash value of the given ssp::Connection object.

  size_t operator()(const ssp::Connection &connection) const
  {
    // Concatenate the members with a delimiter
    std::string concatenated = connection.componentStartName + "|" + connection.variableReferenceStart + "|"
                             + connection.componentEndName + "|" + connection.variableReferenceEnd;

    // Generate hash from the concatenated string
    return std::hash<std::string>{}(concatenated);
  }
};
}  //namespace std

namespace ssp
{
/// @brief class representing OSMP connection
class OSMPConnection : public Connection
{
public:
  std::string osmpRole;      ///< OSMP role
  std::string osmpLinkName;  ///< Name of the OSMP link

  /// @brief Constructor for OSMP connection
  /// @param componentStartName       Name of the startElement
  /// @param variableReferenceStart   Name of the startConnector
  /// @param componentEndName         Name of the endElement
  /// @param variableReferenceEnd     Name of the endConnector
  /// @param osmpRole                 OSMP role
  /// @param osmpLinkName             Name of the OSMP link
  OSMPConnection(std::string componentStartName,
                 std::string variableReferenceStart,
                 std::string componentEndName,
                 std::string variableReferenceEnd,
                 std::string osmpRole,
                 std::string osmpLinkName);

  OSMPConnection(const OSMPConnection &) = default;  ///< Copy constructor
  OSMPConnection(OSMPConnection &&) = default;       ///< Move constructor

  /// @brief Operator overload
  /// @param other Reference to the another OSMP connection
  /// @return Returns true, if both OSMP Connection are same
  bool operator==(const OSMPConnection &other) const;
};

/// @brief Class representing a completness check for OSMP connection
class OSMPConnectionCompletenessCheck
{
public:
  OSMPConnectionCompletenessCheck() = default;

  /// @brief Function to return OSMP connection if the osmp connection is incomplete
  /// @param connection OSMP connection to check if it is complete
  /// @return Returns OSMPConnection if complete, optional
  std::optional<OSMPConnection> ReturnOSMPConnectionIfComplete(const OSMPConnection &connection);

private:
  struct OSMPConnectionCompleteness
  {
    bool hasBaseLo = false;
    bool hasBaseHi = false;
    bool hasSize = false;

    [[nodiscard]] bool IsComplete() const { return hasBaseLo && hasBaseHi && hasSize; }

    void SetComplete(const std::string &role);
  };
  std::unordered_map<Connection, OSMPConnectionCompleteness> connectionCompletenessMap;
};

}  //namespace ssp
