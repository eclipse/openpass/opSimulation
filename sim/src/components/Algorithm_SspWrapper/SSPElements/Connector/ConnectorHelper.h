/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#pragma once

#include <cassert>
#include <iostream>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>
#include <utility>

#include <queue>

extern "C"
{
#include <fmilib.h>

#include "fmuChecker.h"
}

#include "sim/include/fmuHandlerInterface.h"
#include "sim/include/fmuWrapperInterface.h"
#include "sim/src/components/Algorithm_FmuWrapper/src/fmuFileHelper.h"
#include "sim/src/components/Algorithm_SspWrapper/type_helper.h"

namespace ssp
{
class ConnectorHelper  ///< Class representing a helper for the connector
{
public:
  /**
   * @brief Get the Scalar Variable Reference object
   *
   * @param fmuWrapperInterface       FMU wrapper interface
   * @param fmuScalarVariableName     FMU scalar variable name
   * @return                          fmi value reference
   */
  static fmi2_value_reference_t GetScalarVariableReference(
      const std::shared_ptr<FmuWrapperInterface> &fmuWrapperInterface, const std::string &fmuScalarVariableName);

  /**
   * @brief Get the Fmu Wrapper Value object
   *
   * @tparam T                    Type of the FMU value
   * @param fmuWrapperInterface   Pointer to the FMU wrapper interface
   * @param fmuScalarVariableName FMU scalar variable name
   * @return
   */
  template <typename T, typename = std::is_invocable<T>>
  static auto GetFmuWrapperValue(const std::shared_ptr<FmuWrapperInterface> &fmuWrapperInterface,
                                 const std::string &fmuScalarVariableName) -> cvt_t<T>
  {
    return GetUnionValue<T>(
        fmuWrapperInterface->GetValue(GetScalarVariableReference(fmuWrapperInterface, fmuScalarVariableName), T()));
  }

  /**
   * @brief Set the Fmu Wrapper Value object
   *
   * @tparam T                    Type of the FMU value
   * @param fmuWrapperInterface   Pointer to the FMU wrapper interface
   * @param fmuScalarVariableName FMU scalar variable name
   * @param value                 FMU value
   */
  template <typename T, typename = std::is_invocable<T>>
  static void SetFmuWrapperValue(const std::shared_ptr<FmuWrapperInterface> &fmuWrapperInterface,
                                 const std::string &fmuScalarVariableName,
                                 const cvt_t<T> &value)
  {
    fmuWrapperInterface->SetValue(
        ToFmuValue<T>(value), GetScalarVariableReference(fmuWrapperInterface, fmuScalarVariableName), T());
  }
};

}  // namespace ssp
