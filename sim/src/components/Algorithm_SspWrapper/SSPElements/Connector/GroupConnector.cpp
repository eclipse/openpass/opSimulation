/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "GroupConnector.h"

void ssp::GroupConnector::Accept(ssp::ConnectorVisitorInterface &visitor)
{
  visitor.Visit(*this);
}
ssp::GroupConnector::GroupConnector(const std::vector<std::shared_ptr<ConnectorInterface>> &connectorList)
{
  this->connectors = connectorList;
}

const std::string &ssp::GroupConnector::GetConnectorName() const
{
  LOGERRORANDTHROW("SSP system connector GetConnectorName not implemented");
}
