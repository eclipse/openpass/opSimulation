/********************************************************************************
 * Copyright (c) 2017-2018 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *               2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  collisionImpl.cpp
//! @brief This file contains the implementation header file
//-----------------------------------------------------------------------------

#include "collisionImpl.h"

#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <algorithm>
#include <new>
#include <stdexcept>
#include <utility>
#include <vector>

#include "common/globalDefinitions.h"
#include "common/hypot.h"
#include "common/vector2d.h"
#include "include/agentInterface.h"
#include "include/callbackInterface.h"
#include "include/signalInterface.h"
#include "include/worldInterface.h"

class ParameterInterface;
class PublisherInterface;
class StochasticsInterface;

DynamicsCollisionImplementation::DynamicsCollisionImplementation(std::string componentName,
                                                                 bool isInit,
                                                                 int priority,
                                                                 int offsetTime,
                                                                 int responseTime,
                                                                 int cycleTime,
                                                                 StochasticsInterface *stochastics,
                                                                 WorldInterface *world,
                                                                 const ParameterInterface *parameters,
                                                                 PublisherInterface *const publisher,
                                                                 const CallbackInterface *callbacks,
                                                                 AgentInterface *agent)
    : UnrestrictedModelInterface{std::move(componentName),
                                 isInit,
                                 priority,
                                 offsetTime,
                                 responseTime,
                                 cycleTime,
                                 stochastics,
                                 world,
                                 parameters,
                                 publisher,
                                 callbacks,
                                 agent}
{
  dynamicsSignal.componentState = ComponentState::Disabled;
  dynamicsSignal.longitudinalController = GetComponentName();
  dynamicsSignal.lateralController = GetComponentName();
}

void DynamicsCollisionImplementation::UpdateInput([[maybe_unused]] int localLinkId,
                                                  [[maybe_unused]] const std::shared_ptr<SignalInterface const> &data,
                                                  [[maybe_unused]] int time)
{
}

void DynamicsCollisionImplementation::UpdateOutput(int localLinkId,
                                                   std::shared_ptr<SignalInterface const> &data,
                                                   [[maybe_unused]] int time)
{
  if (localLinkId == 0)
  {
    try
    {
      data = std::make_shared<DynamicsSignal const>(dynamicsSignal);
    }
    catch (const std::bad_alloc &)
    {
      const std::string msg = COMPONENTNAME + " could not instantiate signal";
      LOG(CbkLogLevel::Debug, msg);
      throw std::runtime_error(msg);
    }
  }
  else
  {
    const std::string msg = COMPONENTNAME + " invalid link";
    LOG(CbkLogLevel::Debug, msg);
    throw std::runtime_error(msg);
  }
}

void DynamicsCollisionImplementation::Trigger([[maybe_unused]] int time)
{
  if (GetAgent()->GetCollisionPartners().size() > numberOfCollisionPartners)
  {
    numberOfCollisionPartners = GetAgent()->GetCollisionPartners().size();
    isActive = true;
    dynamicsSignal.componentState = ComponentState::Acting;
    bool collisionWithFixedObject = false;

    //Calculate new velocity by inelastic collision
    units::mass::kilogram_t weight = GetAgent()->GetVehicleModelParameters()->mass;

    units::mass::kilogram_t sumOfWeights{weight};
    auto sumOfImpulsesX = GetAgent()->GetVelocity().x * weight;
    auto sumOfImpulsesY = GetAgent()->GetVelocity().y * weight;

    auto collisionPartners = GetAgent()->GetCollisionPartners();
    for (const auto &partner : collisionPartners)
    {
      if (partner.first == ObjectTypeOSI::Object)
      {
        collisionWithFixedObject = true;
        break;
      }
      const AgentInterface *partnerAgent = GetWorld()->GetAgent(partner.second);
      if (partnerAgent != nullptr)
      {
        weight = partnerAgent->GetVehicleModelParameters()->mass;

        sumOfWeights += weight;
        sumOfImpulsesX += partnerAgent->GetVelocity().x * weight;
        sumOfImpulsesY += partnerAgent->GetVelocity().y * weight;
      }
    }

    if (collisionWithFixedObject)
    {
      velocity = 0.0_mps;
    }
    else
    {
      units::velocity::meters_per_second_t velocityX = sumOfImpulsesX / sumOfWeights;
      units::velocity::meters_per_second_t velocityY = sumOfImpulsesY / sumOfWeights;
      velocity = openpass::hypot(velocityX, velocityY);
      if (velocityY > 0.0_mps)
      {
        movingDirection = units::math::acos(velocityX / velocity);
      }
      else if (velocity != 0.0_mps)
      {
        movingDirection = -units::math::acos(velocityX / velocity);
      }
      else
      {
        movingDirection = 0.0_rad;
      }
    }
    dynamicsSignal.dynamicsInformation.yaw = GetAgent()->GetYaw();
    dynamicsSignal.dynamicsInformation.yawRate = 0.0_rad_per_s;
    dynamicsSignal.dynamicsInformation.yawAcceleration = 0.0_rad_per_s_sq;
  }

  if (isActive)
  {
    const units::acceleration::meters_per_second_squared_t deceleration{10.0};
    velocity -= deceleration * units::time::millisecond_t(GetCycleTime());

    if (velocity < 0.0_mps)
    {
      isActive = false;
    }

    velocity = std::max(0.0_mps, velocity);
    // NOLINTBEGIN(readability-identifier-length)
    // change of path coordinate since last time step [m]
    units::length::meter_t ds = velocity * units::time::millisecond_t(GetCycleTime());
    // change of inertial x-position due to ds and yaw [m]
    units::length::meter_t dx = ds * units::math::cos(movingDirection);
    // change of inertial y-position due to ds and yaw [m]
    units::length::meter_t dy = ds * units::math::sin(movingDirection);
    // new inertial x-position [m]
    units::length::meter_t x = GetAgent()->GetPositionX() + dx;
    // new inertial y-position [m]
    units::length::meter_t y = GetAgent()->GetPositionY() + dy;
    // NOLINTEND(readability-identifier-length)

    dynamicsSignal.dynamicsInformation.velocityX = velocity * units::math::cos(movingDirection);
    dynamicsSignal.dynamicsInformation.velocityY = velocity * units::math::sin(movingDirection);
    dynamicsSignal.dynamicsInformation.acceleration = isActive ? -deceleration : 0.0_mps_sq;
    dynamicsSignal.dynamicsInformation.positionX = x;
    dynamicsSignal.dynamicsInformation.positionY = y;
    dynamicsSignal.dynamicsInformation.travelDistance = ds;
  }
}
