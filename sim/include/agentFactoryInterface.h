/********************************************************************************
 * Copyright (c) 2016 ITK Engineering GmbH
 *               2018-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  agentFactoryInterface.h
//! @brief This file contains the interface for communicating between framework
//!        and world.
//-----------------------------------------------------------------------------

#pragma once

#include <list>
#include <map>

#include "include/agentBlueprintInterface.h"

/**
 * \brief Agent Interface within the openPASS framework.
 * \details This interface provides access to agent parameters, properties, attributes and dynamic states.
 */

namespace core
{

class Agent;

/// @brief interface for communicating between framework and world
class AgentFactoryInterface
{
public:
  AgentFactoryInterface() = default;
  virtual ~AgentFactoryInterface() = default;

  //-----------------------------------------------------------------------------
  //! Clear factory for next simulation cycle
  //-----------------------------------------------------------------------------
  virtual void Clear() = 0;

  //-----------------------------------------------------------------------------
  //! Creates a new agent based on the provided parameters, then adds it to the
  //! agent network in the world representation. Also adds agents during runtime.
  //!
  //! @param[in]  id                      Id of the new agent
  //! @param[in]  agentBuildInstructions  Contains all necessary informations
  //!                                     to create an agent
  //!
  //! @return                             The added agent
  //-----------------------------------------------------------------------------
  virtual Agent* AddAgent(mantle_api::UniqueId id, const AgentBuildInstructions& agentBuildInstructions) = 0;
};

}  //namespace core
