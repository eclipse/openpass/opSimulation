/********************************************************************************
 * Copyright (c) 2017 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  roadElevation.h
//! @brief This file contains a road elevation profile as a cubic polynomial.
//-----------------------------------------------------------------------------

#ifndef ROADELEVATION
#define ROADELEVATION

//-----------------------------------------------------------------------------
//! Class representing a road elevation profile as a cubic polynomial.
//-----------------------------------------------------------------------------
class RoadElevation
{
public:
  /**
   * @brief Construct a new Road Elevation object
   *
   * @param s start position of the elevation
   * @param a constant factor of the polynomial
   * @param b linear factor of the polynomial
   * @param c quadratic factor of the polynomial
   * @param d cubic factor of the polynomial
   */
  RoadElevation(units::length::meter_t s,
                units::length::meter_t a,
                double b,
                units::unit_t<units::inverse<units::length::meter>> c,
                units::unit_t<units::inverse<units::squared<units::length::meter>>> d)
      : s(s), a(a), b(b), c(c), d(d)
  {
  }
  RoadElevation(const RoadElevation&) = delete;
  RoadElevation(RoadElevation&&) = delete;
  RoadElevation& operator=(const RoadElevation&) = delete;
  RoadElevation& operator=(RoadElevation&&) = delete;
  virtual ~RoadElevation() = default;

  //-----------------------------------------------------------------------------
  //! Returns the start position (s-coordinate) of the elevation.
  //!
  //! @return                         start position of the elevation
  //-----------------------------------------------------------------------------
  units::length::meter_t GetS() const { return s; }

  //-----------------------------------------------------------------------------
  //! Returns the constant factor of the polynomial.
  //!
  //! @return                         constant factor of the polynomial
  //-----------------------------------------------------------------------------
  units::length::meter_t GetA() const { return a; }

  //-----------------------------------------------------------------------------
  //! Returns the linear factor of the polynomial.
  //!
  //! @return                         linear factor of the polynomial
  //-----------------------------------------------------------------------------
  double GetB() const { return b; }

  //-----------------------------------------------------------------------------
  //! Returns the quadratic factor of the polynomial.
  //!
  //! @return                         quadratic factor of the polynomial
  //-----------------------------------------------------------------------------
  units::unit_t<units::inverse<units::length::meter>> GetC() const { return c; }

  //-----------------------------------------------------------------------------
  //! Returns the cubic factor of the polynomial.
  //!
  //! @return                         cubic factor of the polynomial
  //-----------------------------------------------------------------------------
  units::unit_t<units::inverse<units::squared<units::length::meter>>> GetD() const { return d; }

private:
  units::length::meter_t s;
  units::length::meter_t a;
  double b;
  units::unit_t<units::inverse<units::length::meter>> c;
  units::unit_t<units::inverse<units::squared<units::length::meter>>> d;
};

#endif  // ROADELEVATION
