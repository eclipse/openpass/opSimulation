/********************************************************************************
 * Copyright (c) 2017-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "include/worldObjectInterface.h"

using OpenDriveId = std::string;

//-----------------------------------------------------------------------------
//! Class representing a traffic object interface
//-----------------------------------------------------------------------------
class TrafficObjectInterface : public virtual WorldObjectInterface
{
public:
  TrafficObjectInterface() = default;
  TrafficObjectInterface(const TrafficObjectInterface&) = delete;
  TrafficObjectInterface(TrafficObjectInterface&&) = delete;
  TrafficObjectInterface& operator=(const TrafficObjectInterface&) = delete;
  TrafficObjectInterface& operator=(TrafficObjectInterface&&) = delete;
  virtual ~TrafficObjectInterface() = default;

  /// @return Returns OpenDriveId of the traffic object
  virtual OpenDriveId GetOpenDriveId() const = 0;

  /// @return Returns true, if the traffic object is collidable
  virtual bool GetIsCollidable() const = 0;
};
