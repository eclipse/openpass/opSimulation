/********************************************************************************
 * Copyright (c) 2018-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <filesystem>
#include <unordered_map>

#include "include/agentTypeInterface.h"
#include "include/profilesInterface.h"
#include "include/sceneryInterface.h"
#include "include/simulationConfigInterface.h"
#include "include/systemConfigInterface.h"

/// Class representing configuration container interface
class ConfigurationContainerInterface
{
public:
  virtual ~ConfigurationContainerInterface() = default;

  /// @param executionPath Target of the /proc/self/exe symlink on Linux, canonical path of executable on Windows
  /// @return Returns true when all configurations are imported
  virtual bool ImportAllConfigurations(const std::filesystem::path& executionPath) = 0;

  /// @return Returns reference to the system configuration interface
  virtual std::shared_ptr<SystemConfigInterface> GetSystemConfigBlueprint() const = 0;

  /// @return Returns reference to the simulation configuration interface
  virtual const SimulationConfigInterface* GetSimulationConfig() const = 0;

  /// @return Returns reference to the profiles
  virtual const ProfilesInterface* GetProfiles() const = 0;

  /// @return Returns system configs
  virtual const std::map<std::string, std::shared_ptr<SystemConfigInterface>>& GetSystemConfigs() const = 0;

  /// @return Returns runtime information
  virtual const openpass::common::RuntimeInformation& GetRuntimeInformation() const = 0;
};
